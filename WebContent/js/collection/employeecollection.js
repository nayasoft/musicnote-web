window.EmployeeCollection = Backbone.Collection.extend({

    model: Employee,

    url:"../api/employees",

    findByName:function (key) {
        var url = (key == '') ? '../api/employees' : "../api/employees/search/" + key;
        console.log('findByName: ' + key);
        var self = this;
        $.ajax({
        	headers: { 
        	"Mn-Callers" : musicnote,
        	"Mn-time" :musicnoteIn				
        	},
            url:url,
            dataType:"json",
            success:function (data) {
                console.log("search success: " + data.length);
                self.reset(data);
            }
        });
    }

});