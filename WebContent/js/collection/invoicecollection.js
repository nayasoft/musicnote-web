//Create the collection
var Collection = Backbone.Collection.extend();

// Init the collection
var dogs = new Collection( [ {
	Description : "Lesson from 2/1/2013",
	Qty : "01",
	Unit : "20",
	Amount : "200.00"
}, {
	Description : "Lesson from 8/1/2013",
	Qty : "02",
	Unit : "25",
	Amount : "300.00"
}, {
	Description : "Lesson from 13/1/2013",
	Qty : "01",
	Unit : "25",
	Amount : "400.00"
}, {
	Description : "Lesson from 24/1/2013",
	Qty : "04",
	Unit : "20",
	Amount : "200.00"
} ]);

// For each dog, add a row in the table
var gridData = [];
dogs.each(function(dog) {
	var item = dog.toJSON();
	item.id = $.jgrid.randId();
	gridData.push(item);
});

// Create the table
var dogsTable = jQuery("#dogsList");
dogsTable.jqGrid( {
	datatype : 'local',
	data : gridData,
	width : '100%',
	height : 'auto',
	gridview : true,
	colNames : [ 'Description of work', 'Qty/Hrs', 'Unit Price', 'Amount' ],
	colModel : [ {
		name : 'Description',
		index : 'Description',
		width : 350
	}, {
		name : 'Qty',
		index : 'Qty',
		width : 60,
		align : "right",
		formatter : {
			classes : [ 'classtest' ]
		}
	}, {
		name : 'Unit',
		index : 'Unit',
		width : 100,
		align : "right"
	}, {
		name : 'Amount',
		index : 'Amount',
		width : 80,
		align : "right"
	} ],
	loadComplete : function(data) {
		//alert('grid loading completed ' + data);
},

loadError : function(xhr, status, error) {
	alert('grid loading error' + error);
}

});
var grandToalAmount;
function calculateAmount()
{
var amount = document.getElementById('percentageAmount').value;
	var totalAmount = 0;
	var discountAmount = 0;
	var percentageAmount=0;
	if (amount == 0) {
		alert("Please enter value");
		return false;
	}
   var totalValue=$("#totalPriceId").text();
  
	if (document.getElementById('optionsRadios1').checked) {
	 	discountAmount = totalValue- (totalValue * amount / 100);
		totalAmount = discountAmount;
		percentageAmount=(totalValue*amount/100);
   	   document.getElementById('grandTotalAmount').innerHTML=percentageAmount;

	} else {
		totalAmount = (totalValue- amount);
	    document.getElementById('grandTotalAmount').innerHTML=amount;
	}
	grandToalAmount=totalAmount;
	document.getElementById('totalAmount').innerHTML = "<html><head><title></title></head><body><b>" +totalAmount + "</b></body></html>";}





function printDiv(divID) {

        /*var amount=($("#qytId").val())*($("#unitId").val());
        var url = urlForServer+"invoice/createInvoice";
		var datastr = '{"classId":"'+$("#classId").val()+'","totalQty":"'+$("#qytId").val()+'","subTotal":"'+amount+'","totalAmount":"'+grandToalAmount+'","unitPrice":"'+$("#unitId").val()+'"}';	
		console.log("<-------Sdatastr -------> "+ datastr);
		var params = encodeURIComponent(datastr);
		$.ajax({
		headers: { 
		"Ajax-Call" : userTokens,
		"Ajax-Time" :userLoginTime				
		},
			type : 'POST', url : url, data : params,
			success : function(responseText){
				console.log("<-------Scuccessfully created user with response as -------> "+ responseText);
				$('#main').hide();
				$('#sidecontent').show();
				$('#footer').show();
				$('#header').show();
				app.navigate("home", {trigger: true});
				var data=jQuery.parseJSON(responseText);
				},
			error : function(){
				console.log("<-------Error returned while creating user-------> ");
			}
		});
*/
	
	var table = document.getElementById('table');    
	var rowCount = table.rows.length; 
	var unitIds=0;
	var qtyIds=0;
	var classIds=0;
	for(var value=1; value<rowCount-4; value++)
	{
		qtyIds=$("#qytId"+value);
		classIds=$("#classId"+value);
		unitIds=$("#unitId"+value);
		$("#qytId"+value).attr('style','display : none');
		$("#classId"+value).attr('style','display : none');
		$("#unitId"+value).attr('style','display : none');
	}
	$("#unitIds").text(unitIds.val());
	$("#classIds").text(classIds.val());
	$("#qytIds").text(qtyIds.val());


    document.getElementById('feedbacksId').innerHTML = document.getElementById('feedbackId').value;
    document.getElementById('discountAmount').innerHTML = document.getElementById('percentageAmount').value;
	//alert(document.getElementById('classId').value);
    /*document.getElementById('classId1').innerHTML =document.getElementById('classId').value
	document.getElementById('qytId1').innerHTML =document.getElementById('qytId').value;
    document.getElementById('unitId1').innerHTML =document.getElementById('unitId').value;
	
	document.getElementById('classId3').innerHTML =document.getElementById('classId2').value;
	document.getElementById('qytId3').innerHTML =document.getElementById('qytId2').value;
    document.getElementById('unitId3').innerHTML =document.getElementById('unitId2').value;*/
	
	
	document.getElementById('paybleTerm1').innerHTML =document.getElementById('paybleTerm').value;
    document.getElementById('paymentTerm1').innerHTML =document.getElementById('paymentTerm').value;
	document.getElementById('dateTerm1').innerHTML =document.getElementById('dateTerm').value;
    document.getElementById('forTerm1').innerHTML =document.getElementById('forTerm').value;
	document.getElementById('nameTerm1').innerHTML =document.getElementById('nameTerm').value;
	document.getElementById('approvedTerm1').innerHTML =document.getElementById('approvedTerm').value;
	
	
	
    var divElements = document.getElementById(divID).innerHTML;
    var oldPage = document.body.innerHTML;
    document.body.innerHTML = "<html><head><title></title></head><body>" +divElements + "</body>";
    //alert("test"+divElements);
	document.getElementById('simplePrint').style.visibility = 'hidden';
	document.getElementById('optionsRadios1').style.visibility = 'hidden';
	document.getElementById('optionsRadios2').style.visibility = 'hidden';
	document.getElementById('percetage').style.visibility = 'hidden';
	document.getElementById('amount').style.visibility = 'hidden';
	document.getElementById('percentageAmount').style.visibility = 'hidden';
	document.getElementById('feedbackId').style.visibility = 'hidden';
	document.getElementById('studentLabel').style.visibility = 'hidden';
	document.getElementById('studentId').style.visibility = 'hidden';
	document.getElementById('paybleTerm').style.visibility = 'hidden';
	document.getElementById('paymentTerm').style.visibility = 'hidden';
	document.getElementById('dateTerm').style.visibility = 'hidden';
	document.getElementById('forTerm').style.visibility = 'hidden';
	document.getElementById('nameTerm').style.visibility = 'hidden';
	document.getElementById('approvedTerm').style.visibility = 'hidden';
	
	/*document.getElementById('classId').style.visibility = 'hidden';
	document.getElementById('qytId').style.visibility = 'hidden';
	document.getElementById('unitId').style.visibility = 'hidden';
	document.getElementById('classId2').style.visibility = 'hidden';
	document.getElementById('qytId2').style.visibility = 'hidden';
	document.getElementById('unitId2').style.visibility = 'hidden';
	document.getElementById('classId').style.visibility = 'visible';
	document.getElementById('qytId').style.visibility = 'visible';
	document.getElementById('unitId').style.visibility = 'visible';
	document.getElementById('classId2').style.visibility = 'visible';
	document.getElementById('qytId2').style.visibility = 'visible';
	document.getElementById('unitId2').style.visibility = 'visible';
	*/

	
	

    window.print();
 // print enable purpose
	document.getElementById('simplePrint').style.visibility = 'visible';
	document.getElementById('optionsRadios1').style.visibility = 'visible';
	document.getElementById('optionsRadios2').style.visibility = 'visible';
	document.getElementById('percetage').style.visibility = 'visible';
	document.getElementById('amount').style.visibility = 'visible';
	document.getElementById('percentageAmount').style.visibility = 'visible';
	document.getElementById('feedbackId').style.visibility = 'visible';
	document.getElementById('studentLabel').style.visibility = 'visible';
	document.getElementById('studentId').style.visibility = 'visible';
	document.getElementById('paybleTerm').style.visibility = 'visible';
	document.getElementById('paymentTerm').style.visibility = 'visible';
	document.getElementById('dateTerm').style.visibility = 'visible';
	document.getElementById('forTerm').style.visibility = 'visible';
	document.getElementById('nameTerm').style.visibility = 'visible';
	document.getElementById('approvedTerm').style.visibility = 'visible';
	//Restore orignal HTML
	document.body.innerHTML = oldPage;
	for(var value=1; value<rowCount-4; value++)
	{
		
		$("#qytId"+value).attr('style','display : block');
		$("#classId"+value).attr('style','display : block');
		$("#unitId"+value).attr('style','display : block');
	}
	/*alert("true");
	app.navigate("home", {
		trigger : true
	});*/
	clearLabelValues();
	
	return false;
	

}

function clearLabelValues() {

    document.getElementById('grandTotalAmount').innerHTML = "";
	document.getElementById('totalPriceId').innerHTML = "";
	document.getElementById('qtyPriceId').innerHTML = "";
	$("#unitIds").text("");
	$("#classIds").text("");
	$("#qytIds").text("");
	$("#amountId").text("");
	
	//document.getElementById('amountId').innerHTML ="";
	/*document.getElementById('amountId1').innerHTML ="";*/

	
	document.getElementById('feedbacksId').innerHTML = "";
	document.getElementById('discountAmount').innerHTML = "";
	document.getElementById('totalAmount').innerHTML = "";
	
	/*document.getElementById('classId1').innerHTML ="";
	document.getElementById('qytId1').innerHTML ="";
	document.getElementById('unitId1').innerHTML ="";
	
	document.getElementById('classId3').innerHTML ="";
	document.getElementById('qytId3').innerHTML ="";
	document.getElementById('unitId3').innerHTML ="";*/
	
	document.getElementById('dateTerm1').innerHTML="";
	document.getElementById('paybleTerm1').innerHTML ="";
    document.getElementById('paymentTerm1').innerHTML ="";
	document.getElementById('dateTerm1').innerHTML ="";
    document.getElementById('forTerm1').innerHTML ="";
	document.getElementById('nameTerm1').innerHTML ="";
	document.getElementById('approvedTerm1').innerHTML ="";
	$("#studentName").text("");
    $("#studentPhone").text("");
    $("#studentAddress").text("");
	    var currentDate = new Date();
	    var day = currentDate.getDate();
	    var month = currentDate.getMonth() + 1;
	    var year = currentDate.getFullYear();
	    var dateValue=day + "-" + month + "-" + year ;
		$("#dateTerm").val(dateValue);
	var invoiceNo=$("#invoiceNo").text();
	$("#invoiceNo").text(++invoiceNo);
}



var studentList;
function fetchStudent()
{
	var url = urlForServer+"user/fetchUserDetails";
	var datastr = '{"userRole":"Student"}';

	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
		"Mn-time" :musicnoteIn				
		},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		console.log("<-------data returned from url for get student with reponse as -------> "+ responseText);
		var data = jQuery.parseJSON(responseText);
		if(data!=null && data!='')
		{
			studentList=data;
			$("#studentId").empty();
			$("#studentId").append('<option value="--Select Student">--Select Student</option>');
			for ( var i = 0; i < data.length; i++) 
			{
				if(data[i].userFirstName!=null && data[i].userFirstName!='')
				$("#studentId").append('<option value="'+data[i].userFirstName+'">'+data[i].userFirstName+'</option>');
				
			}
		}
	},
	error : function() {
		console.log("<-------error returned for get student -------> ");
		}
	});
}


function getStudentName()
{
for(var i=0;i<studentList.length;i++)
{
if(studentList[i].userFirstName==$("#studentId").val())
{
$("#studentName").text(studentList[i].userFirstName);
$("#studentPhone").text(studentList[i].contactNumber);
$("#studentAddress").text(studentList[i].address);
break;
}
else
{
$("#studentName").text("");
$("#studentPhone").text("");
$("#studentAddress").text("");
}
}
}


var amount=0;
function getUnit(i,j)
{
	var n=i;
	var m=j;
	var table = document.getElementById('table');    
	var rowCount = table.rows.length; 
	/*if($("#classId"+n).val()!='--Select Class/Lession' && $("#qytId"+n).val()!='--Select Qty' && $("#unitId"+n).val()!='--Select Unit')
	{*/

	var tot=0;
	var totAmount=0;
	var totalPrice=0;
	for(var value=1; value<rowCount-4; value++)
	{
		var id=$("#qytId"+value);
		if(id!=null)
		tot=tot+Number(id.val());
		var amount=$("#unitId"+value);
		totalPrice=totalPrice+Number(id.val()*amount.val().replace("$",""));
		totAmount=id.val()*amount.val().replace("$","");
		$("#amountId"+value).text(totAmount);

	}	


	//$("#amountId").text(totAmount);
	$("#qtyPriceId").text(tot);
	$("#totalPriceId").text(totalPrice);
	/*}
	else
	{
		$("#amountId").text("");
		$("#qtyPriceId").text("");
		$("#totalPriceId").text("");
	}
*/
}


var amount=0;
var qty=0;
function getQuty(i,j)
{
var n=i;
var m=j;
var table = document.getElementById('table');    
var rowCount = table.rows.length; 


/*if($("#classId"+n).val()!='--Select Class/Lession' && $("#qytId"+n).val()!='--Select Qty' && $("#unitId"+n).val()!='--Select Unit')
{
*/
var tot=0;
var totAmount=0;
var totalPrice=0;
for(var value=1; value<rowCount-4; value++)
{
	var id=$("#qytId"+value);
	if(id!=null)
	tot=tot+Number(id.val());
	var amount=$("#unitId"+value);
	totalPrice=totalPrice+Number(id.val()*amount.val().replace("$",""));
	totAmount=id.val()*amount.val().replace("$","");
	$("#amountId"+value).text(totAmount);
}	


$("#qtyPriceId").text(tot);
$("#totalPriceId").text(totalPrice);
/*}
else
{
	$("#amountId").text("");
	$("#qtyPriceId").text("");
	$("#totalPriceId").text("");
}*/

}

