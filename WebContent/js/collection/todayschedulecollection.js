//Create the collection
var todayScheduleList = Backbone.Collection.extend();
var clickedMenu="";
var windowSize=$(window).width();
// Init the collection
$(document).ready(function(){
	$(window).bind('resize', function() {
		if(windowSize > $(window).width())
			$('#jqgh_todaySchedules_Description').css('text-align','left');
		else 
			$('#jqgh_todaySchedules_Description').css('text-align','center');
		
	    $("#todaySchedules").setGridWidth($('#sideBar').width()-30);
	}).trigger('resize');
//	 getTodaySchedule();
	$("#tempTablke").empty();
	$("#tempTablke").append('<table id="todaySchedules"></table>');
	var today = new Date();
	var monthStr=today.getMonth()+1;
	if(monthStr<10 && monthStr>0)
	{
		monthStr="0"+monthStr;
	}
	var dateStr=today.getDate();
	if(dateStr<10)
	{
		dateStr="0"+dateStr;
	}
	
	if(userId!=null && userId!=''){
	today=monthStr+'/'+dateStr+'/'+today.getFullYear();

	var url = urlForServer+"note/fetchTodayScheduleEvents";
	var params = '{"userId":"'+userId+'","date":"'+today+'"}';
    
    params = encodeURIComponent(params);
	
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
//    		loadList(response);
    		
    		var schedules = new todayScheduleList(response);
    		
    			                       // For each schedule, add a row in the table
    			                       var gridData = [];
    			                       schedules.each(function(schedule) {
    			                           var item = schedule.toJSON();
    			                           item.id = $.jgrid.randId();
    			                           gridData.push(item);
    			                       });

    			                       if(gridData.length!=0)
    			                       {
    			                       // Create the table
									   var pageWidth = $("#sideBar").width() - 30;
									   if(pageWidth <100){
											pageWidth = 230;
									   }
    			                       var scheduleTable = jQuery("#todaySchedules");
    			                       scheduleTable.jqGrid({ 
    			                           datatype: 'local',
    			                           data: gridData,
    			                           width:'100%',
    			                           height: 'auto',
    			                       	gridview: true,
    			                       	colNames:['Today\'s Schedule'], 
    			                       	colModel:[ 
    			                       		  		{name:'Description',index:'Description', width:pageWidth} 
    			                       		  	],
    			                       	loadComplete : function(data) {
    			                               //alert('grid loading completed ' + data);
    			                           },
    			                           loadError : function(xhr, status, error) {
    			                              // alert('grid loading error' + error);
    			                           }
    			                       	
    			                       	
    			                       });
    			                     
    			                       }
    			                      
    		
    		
    		
    		
        },
        error: function(e) {
           // alert("Please try again later");
        }
    
    });
	}
//	
//	/* Note Menu Mouse Over ,Out and click Event Fire Methods */
//	$('#sideBarMenu').on('mouseover','.sideBarBox',function(){
//		$(this).parent().find('div').each(function(index){
//			if($(this).hasClass(clickedMenu)){
//			}
//			else
//			{
//			$(this).css({"background-color":""});
//			$(this).children( "a" ).css( "color", "black" );
//			}
//		});
//		$(this).css({"background-color":"linear-gradient(to bottom, #A4A4A4 0%, #A4A4A4 100%) repeat scroll 0 0 transparent"});
//		$(this).children( "a" ).css( "color", "white" );
//	});
//	$('#sideBarMenu').on('mouseout','.sideBarBox',function(){
//		$(this).parent().find('.sideBarBox').each(function(index){
//			if($(this).hasClass(clickedMenu)){
//				$(this).css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
//				$(this).children( "a" ).css( "color", "white" );
//			}else{
//				$(this).css({"background-color":""});
//				$(this).children( "a" ).css( "color", "black" );
//			}
//		});
//	});
//	$('#sideBarMenu').on('click','.sideBarBox',function(){
//		$(this).parent().find('div').each(function(index){
//			$(this).css({"background-color":""});
//			$(this).children( "a" ).css( "color", "black" );
//			
//		});
//		var classStr = $(this).attr('class');
//		clickedMenu = classStr.substr( classStr.lastIndexOf(' ') + 1);
//		
//		$(this).css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
//		$(this).children( "a" ).css( "color", "white" );
//	});
//	
	$('#recentActivity').on('click','.js-note-recnt',function(e){
		var ids = $(this).attr('id');
		var idArray = ids.split("~");
		var cls = $(this).attr('class');
		var pageType = cls.split(" ");
		e.preventDefault();
		listType=pageType[1];
		
		if(listType=="bill"){
			app.navigate('memos', {
				trigger : true
			});
		}else{
			app.navigate(''+pageType[1]+'', {
				trigger : true
			});
		}
		$('#sideBarMenu').children('.sideBarBox').each(function( index ){
			//$(this).css({"background-color":"none repeat scroll 0 0 #F3F3F3"});
			//$(this).children( "a" ).css( "color", "#3C769D" );
		});
		var classStr ='';
		if(listType=="music"){
			classStr = $('#sideBarMenu').children('.sideBarNotes').attr('class');
			$('#sideBarMenu').children('.sideBarNotes').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$('#sideBarMenu').children('.sideBarNotes').children().css( "color", "white" );
		}else if(listType=="schedule"){
			classStr = $('#sideBarMenu').children('.sideBarMenuSchedule').attr('class');
			$('#sideBarMenu').children('.sideBarMenuSchedule').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$('#sideBarMenu').children('.sideBarMenuSchedule').children().css( "color", "white" );
		}else if(listType=="bill"){
			classStr = $('#sideBarMenu').children('.sideBarMemos').attr('class');
			$('#sideBarMenu').children('.sideBarMemos').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$('#sideBarMenu').children('.sideBarMemos').children().css( "color", "white" );
		}
		clickedMenu = classStr.substr( classStr.lastIndexOf(' ') + 1);
		
	});
	
	//Added by Veeraprathaban for notification link model
	
	$('#notificationlists').on('click','.notificationLink',function(e){
		var cls = $(this).attr('class');
		//alert(cls);
		var scheduleFind=$(this).children('.phenom-desc').children('#addEve').attr('id');
		//alert(scheduleFind);
		var pageType = cls.split(" ");
		e.preventDefault();
		listType=pageType[3];
		if(listType=="" || listType!="undefined"){
		if(pageType[0]=="phenom"){
		if(listType=="bill"){
			app.navigate('memos', {
				trigger : true
			});
		}else if(listType=="contact"){
			app.navigate('students', {
				trigger : true
			});
		}else if(scheduleFind=="addEve"){
			app.navigate('schedule', {
				trigger : true
			});
		}else{
			//alert("pageType[3]");
			if(scheduleFind!=addEve){
			app.navigate(''+pageType[3]+'', {
				trigger : true
			});
		}else{
			
		}
		}
		$('#sideBarMenu').children('.sideBarBox').each(function( index ){
			//$(this).css({"background-color":"none repeat scroll 0 0 #F3F3F3"});
			//$(this).children( "a" ).css( "color", "#3C769D" );
		});
		var classStr ='';
		if(listType=="music"){
			classStr = $('#sideBarMenu').children('.sideBarNotes').attr('class');
			$('#sideBarMenu').children('.sideBarNotes').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$('#sideBarMenu').children('.sideBarNotes').children().css( "color", "white" );
		}else if(listType=="schedule"){
			classStr = $('#sideBarMenu').children('.sideBarMenuSchedule').attr('class');
			$('#sideBarMenu').children('.sideBarMenuSchedule').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			getTodaySchedule();
			//$('#sideBarMenu').children('.sideBarMenuSchedule').children().css( "color", "white" );
		}else if(listType=="bill"){
			classStr = $('#sideBarMenu').children('.sideBarMemos').attr('class');
			$('#sideBarMenu').children('.sideBarMemos').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$('#sideBarMenu').children('.sideBarMemos').children().css( "color", "white" );
		}else if(listType=="contact"){
			classStr = $('#sideBarMenu').children('.sideBarContact').attr('class');
			$('#sideBarMenu').children('.sideBarContact').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$('#sideBarMenu').children('.sideBarContact').children().css( "color", "white" );
		}else if(listType=="crowd"){
			classStr = $('#sideBarMenu').children('.sideBarCrowd').attr('class');
			$('#sideBarMenu').children('.sideBarCrowd').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$('#sideBarMenu').children('.sideBarCrowd').children().css( "color", "white" );
		}
		clickedMenu = classStr.substr( classStr.lastIndexOf(' ') + 1);
		}
		}
	});
	
	$('#userProfile').click(function(){
		$('.webside').css({"background-color":""});
	});
	$('#resetpassword').click(function(){
		$('.webside').css({"background-color":""});
	});
	$('#help').click(function(){
		$('.webside').css({"background-color":""});
	});
	
	
	$('#recentActivity').on('mouseover','.recentP',function(){
		$(this).css({"background-color":"#A4A4A4"});
	});
	$('#recentActivity').on('mouseout','.recentP',function(){
		$(this).css({"background-color":""});
	});
	
	//event share information modal navigation
	$('#mailSharingModel').on('click','.eventSharingAcceptDecline',function(e){
		var id=$(this).attr('id');
		var status = id.split("~");
		e.preventDefault();
		listType="schedule";
		
		if(status=="accept"){
			$('#mailSharingModel').modal('hide');
			app.navigate('schedule', {
				trigger : true
			});
		
			$('#sideBarMenu').children('.sideBarBox').each(function( index ){
				//$(this).css({"background-color":"none repeat scroll 0 0 #F3F3F3"});
				//$(this).children( "a" ).css( "color", "#3C769D" );
			});
			
		var classStr ='';
		classStr = $('#sideBarMenu').children('.sideBarMenuSchedule').attr('class');
		$('#sideBarMenu').children('.sideBarMenuSchedule').css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
		//$('#sideBarMenu').children('.sideBarMenuSchedule').children().css( "color", "white" );
		
		clickedMenu = classStr.substr( classStr.lastIndexOf(' ') + 1);	
		}
		else{
			$('#mailSharingModel').modal('hide');
		}
	});
	
	
	
});

function addFriend()
{
	$("#sideBarMenu").find('div').each(function(index){
		if($(this).attr('class') == 'sideBarBox sideBarContact'){
			//$(this).css({"background-color":"linear-gradient(to bottom, #2887BD 0%, #1F6993 100%) repeat scroll 0 0 transparent"});
			//$(this).children( "a" ).css( "color", "white" );
			var classStr = $(this).attr('class');
			clickedMenu = classStr.substr( classStr.lastIndexOf(' ') + 1);	
		}else{
			//$(this).css({"background-color":"none repeat scroll 0 0 #F3F3F3"});
			//$(this).children( "a" ).css( "color", "#3C769D" );
		}
	});
	
	app.navigate("students", {trigger: true});
	
	$("#main").hide();
	$("#sidecontent").show();
	$('#footer').show();
	$('#header').show();
}
function getTodaySchedule(){

	$("#tempTablke").empty();
	$("#tempTablke").append('<table id="todaySchedules"></table>');
	
	var today = new Date();
	var monthStr=today.getMonth()+1;
	if(monthStr<10 && monthStr>0)
	{
		monthStr="0"+monthStr;
	}
	var dateStr=today.getDate();
	if(dateStr<10)
	{
		dateStr="0"+dateStr;
	}
	today=monthStr+'/'+dateStr+'/'+today.getFullYear();

	var url = urlForServer+"note/fetchTodayScheduleEvents";
	var params = '{"userId":"'+userId+'","date":"'+today+'"}';
    
    params = encodeURIComponent(params);
	
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
//    		loadList(response);
    
    		
    		var schedules = new todayScheduleList(response);
    		
    			                       // For each schedule, add a row in the table
    			                       var gridData = [];
    			                       schedules.each(function(schedule) {
    			                           var item = schedule.toJSON();
    			                           item.id = $.jgrid.randId();
    			                           gridData.push(item);
    			                       });
    			                   	

    			                       if(gridData.length!=0)
    			                       {
    			                       // Create the table
									   var pageWidth = $("#sideBar").width() - 30;
									   if(pageWidth <100){
											pageWidth = 230;
									   }
    			                       var scheduleTable = jQuery("#todaySchedules");
    			                       scheduleTable.jqGrid({ 
    			                           datatype: 'local',
    			                           data: gridData,
    			                           width:'100%',
    			                           height: 'auto',
    			                       	gridview: true,
    			                       	colNames:['Today\'s Schedule'], 
    			                       	colModel:[ 
    			                       		  		{name:'Description',index:'Description', width:pageWidth} 
    			                       		  	],
    			                       	loadComplete : function(data) {
    			                               //alert('grid loading completed ' + data);
    			                           },
    			                           loadError : function(xhr, status, error) {
    			                              // alert('grid loading error' + error);
    			                           }
    			                       	
    			                       	
    			                       });
    			                      
    			                       }
    			                      
    		
    		
    		
    		
        },
        error: function(e) {
           // alert("Please try again later");
        }
    
    });

	
	
}