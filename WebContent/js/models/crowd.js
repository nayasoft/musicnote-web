//var searchNotes=new Array();
var mostviewsearchNotes=new Array();
var newsearchNotes=new Array();

var  noteNameFromList;
var listIdFromList;
var noteIdFromList;
var listOwnerFlag = false;
var listOwnerUserId ;
var vote =new Array();
var moreActionBasedId;
var votedFlag;
var crowdTagMap={};

function CrowdAutoFetchClear(){
	window.clearInterval(autoCrowdDetailsFetchTimer);
}

window.clearInterval(autoNoteDetailsFetchTimer);
window.clearInterval(autoContactDetailsFetchTimer);

    
$(document).ready( function() {
	
	// coding for loading symbol
	$("#spinner").bind("ajaxSend", function() {
        $(this).show();
    }).bind("ajaxStop", function() {
        $(this).hide();
    }).bind("ajaxError", function() {
        $(this).hide();
    }).bind("ajaxComplete", function() {
        $(this).hide();
    });

	
	if(autoCrowdDetailsFetchTimer==null || autoCrowdDetailsFetchTimer==0){
		console.log("crowd auto method called");
		autoCrowdDetailsFetchTimer=window.setInterval(function(){
			if(userSession=='Active')
			{
			newNotes();
			}
			},60000);
	}
	// Fixing height of the page.
	var size=$(window).height() - 90;
	$('#crowdView').attr('style','overflow: auto;margin-right:-30px;height:46%;');
	
    if(listType!='schedule'){
	loadTagsForCrowd();
    }
    
	$( "#searching" ).keypress(function(e) {
		
		var unicode=e.charCode? e.charCode:e.keyCode; 
		if(unicode==13){
			if($("#searching").val()!=null && $("#searching").val()!='')
			{
				recentSearch();
			}
			else
			{
				alert("Please enter a search term to perform a search");

			}
		}
	});

	// searching
	
	function recentSearch()
	{
	$('#newNote').empty();
	 var url = urlForServer + "note/getSearching/"+userId;
	  var datastr = '{"Searching":"'+$("#searching").val()+'"}';
	  
	  $.ajax({
		  headers: { 
		  "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
         url: url,
         data: datastr,
         dataType: "json",
         type: "POST",
         success: function(data){

       	if(data!=null && data!='')
			{
				noteData="";
				 
			for ( var i = 0; i < data.length; i++) {
				notesDetailsJsonObj=data[i];
				var tagArray = new Array();
				var tagNames="";
				var noteTags="";
				
				var notes=data[i];
				if(notesDetailsJsonObj['tagIds']!=null && notesDetailsJsonObj['tagIds']!=''){
					var tagWithParams= notesDetailsJsonObj['tagIds'];
					if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
						tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
					}
					if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
						tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
					}
					if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
						tagArray = tagWithParams.split(',');
					}else{
						if(tagWithParams!= undefined && tagWithParams.trim() != ''){
							tagArray = tagWithParams.split(',');
						}
					}
					for(var siz=0;siz<tagArray.length;siz++){
						var tagId=tagArray[siz].trim();
						if (tagId in crowdTagMap){
							tagNames+=" "+crowdTagMap[tagId]+" ,";
						}
					}
					if(tagNames!=null && tagNames!=''){
						var lastIndex  = tagNames.lastIndexOf(",");
						var tagNames = tagNames.substring(0, lastIndex);	
					noteTags='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB;word-spacing:-6px;" title="'+tagNames+'"><span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">'+tagNames+'</span></div>';
					}
				}
				
				datedesc='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;word-spacing:-6px;" title=" Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'</span></div>';
				noteData=noteData+'<div class=row><div></div><div class=col-md-1></div><div id='+notesDetailsJsonObj['listId']+' class="listDivs col-md-10" style="background-color: rgb(243, 243, 243)"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15"  ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';
			
			}
			
			var data='<div class="crowd">'+noteData+'</div>';
		
			$('#newNote').append(data);
			}
       	$("#searching").val("");
          
          
          }
       });
		
       CrowdAutoFetchClear();
		//autoCrowdDetailsFetchTimer=window.setInterval(function(){
			
			//recentSearch();},60000);
	}
	
	
$( "#searchingMostView" ).keypress(function(e) {
	
	
	
		var unicode=e.charCode? e.charCode:e.keyCode; 
		if(unicode==13){
			if($("#searchingMostView").val()!=null && $("#searchingMostView").val()!='')
			{
				searchMostViewed();
			}
			else
			{
				alert("Please enter a search term to perform a search");

			}
		}
		});


// search mostviewed

function searchMostViewed()
{
	loadTagsForCrowd();
	  $('#mostView').empty();
	  var url = urlForServer + "note/getsearchMostViewd/"+userId;
	  var datastr = '{"Searching":"'+$("#searchingMostView").val()+'","noteAccess":"public"}';
	  $.ajax({
		  headers: { 
		  "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
          url: url,
          data: datastr,
          dataType: "json",
          type: "POST",
          success: function(data){
        	if(data!=null && data!='')
			{
				noteData="";		 
			for ( var i = 0; i < data.length; i++) {
				var tagArray = new Array();
				var tagNames="";
				var noteTags="";
				getMostViewed=data[i];
				var notes=data[i];
				if(getMostViewed['viewed']>0)
				{
						if(getMostViewed['tagIds']!=null && getMostViewed['tagIds']!=''){
							var tagWithParams= getMostViewed['tagIds'];
							if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
								tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
							}
							if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
								tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
							}
							if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
								tagArray = tagWithParams.split(',');
							}else{
								if(tagWithParams!= undefined && tagWithParams.trim() != ''){
									tagArray = tagWithParams.split(',');
								}
							}
							for(var siz=0;siz<tagArray.length;siz++){
								var tagId=tagArray[siz].trim();
								if (tagId in crowdTagMap){
									tagNames+=" "+crowdTagMap[tagId]+" ,";
								}
							}
							
							if(tagNames!=null && tagNames!=''){
								var lastIndex  = tagNames.lastIndexOf(",");
								var tagNames = tagNames.substring(0, lastIndex);	
							noteTags='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB;word-spacing:-6px;" title="'+tagNames+'"><span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">'+tagNames+'</span></div>';
							}
						}
					if(isAndroid || isIphone )
					{
					    datedesc='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;word-spacing:-6px;" title=" Posted by'+getMostViewed['publicUser']+' on '+getMostViewed['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by '+getMostViewed['publicUser']+' on '+getMostViewed['publicDate']+'</span></div>';

						noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=left><label></label><br/><span class="label label-success">'+getMostViewed['viewed']+'</span></div><div id='+getMostViewed['listId']+'  class="listDivs col-md-10"><div class="listBodyAll"><div id='+getMostViewed['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostViewed['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div><br>';

					}
					else
					{
					    datedesc='<div class="badge glyphicon glyphicon-sm decs2" title=" Posted by'+getMostViewed['publicUser']+' on '+getMostViewed['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by '+getMostViewed['publicUser']+' on '+getMostViewed['publicDate']+'</span></div>';

						noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right><label></label><br/><span class="label label-success">'+getMostViewed['viewed']+'</span></div><div></div><div id='+getMostViewed['listId']+'  class="listDivs col-md-10" style="background-color: rgb(243, 243, 243)"><div class="listBodyAll"><div id='+getMostViewed['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostViewed['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';

					}
				}
				else
				{
				}
			}
			var data='<div class="crowd">'+noteData+'</div>';
				$('#mostView').append(data);
			}
			else
			{
				//mostViewed();
				$('#mostView').empty();
			}
        	$("#searchingMostView").val("");
           }
        });
        
        CrowdAutoFetchClear();
		//autoCrowdDetailsFetchTimer=window.setInterval(function(){searchMostViewed();},60000);
}


$( "#searchingMostPopular" ).keypress(function(e) {
	loadTagsForCrowd();
	var unicode=e.charCode? e.charCode:e.keyCode; 
	if(unicode==13){
		if($("#searchingMostPopular").val()!=null && $("#searchingMostPopular").val()!='')
		{
  	  $('#mostPopular').empty();
  	  var url = urlForServer + "note/getSearchMostVoted/"+userId;
		  var datastr = '{"Searching":"'+$("#searchingMostPopular").val()+'","noteAccess":"public"}';
		  $.ajax({
			  headers: { 
			  "Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
            url: url,
            data: datastr,
            dataType: "json",
            type: "POST",
            success: function(data){
          	if(data!=null && data!='')
  			{
  				
  				noteData="";		 
  			for ( var i = 0; i < data.length; i++) {
  				var tagArray = new Array();
				var tagNames="";
				var noteTags="";
  				getMostVoted=data[i];
  				var notes=data[i];
  				if(getMostVoted['vote']>0)
  				{
  					if(getMostVoted['tagIds']!=null && getMostVoted['tagIds']!=''){
						var tagWithParams= getMostVoted['tagIds'];
						if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
							tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
						}
						if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
							tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
						}
						if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
							tagArray = tagWithParams.split(',');
						}else{
							if(tagWithParams!= undefined && tagWithParams.trim() != ''){
								tagArray = tagWithParams.split(',');
							}
						}
						for(var siz=0;siz<tagArray.length;siz++){
							var tagId=tagArray[siz].trim();
							if (tagId in crowdTagMap){
								tagNames+=" "+crowdTagMap[tagId]+" ,";
							}
						}
						if(tagNames!=null && tagNames!=''){
							var lastIndex  = tagNames.lastIndexOf(",");
							var tagNames = tagNames.substring(0, lastIndex);	
						noteTags='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB;word-spacing:-6px;" title="'+tagNames+'"><span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">'+tagNames+'</span></div>';
						}
					}
  				    datedesc='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;word-spacing:-6px;" title=" Posted by '+getMostVoted['publicUser']+' on  '+getMostVoted['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by '+getMostVoted['publicUser']+' on  '+getMostVoted['publicDate']+'</span></div>';
  					//noteData=noteData+'<div class=row><div class=col-md-2 style="width:16px;" align=right></div><div class=col-md-1></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div id='+getMostVoted['listId']+' style="left:-14px;" class="listDivs col-md-7"><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions"><p>'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+'</div></div></div></div></div><br>';
  				  if(is_chrome ||  is_Ie)
                  {
					//noteData=noteData+'<div class=row><div></div><div class=col-md-2 style="width:16px;" align=right></div><div class=col-md-1></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div class="col-md-1"></div><div id='+getMostVoted['listId']+' style="left:-34px;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions"><p>'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div><br>';
  					  noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right></div><div class=col-md-1 style="float:left" align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div id='+getMostVoted['listId']+' style="left:-14px; background-color: rgb(243, 243, 243)" class="offset2 listDivs" ><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions fontStyle15"><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';
                  }
                  else
                  {
  					noteData=noteData+'<div class=row><div></div><div class=col-md-2 style="width:16px;" align=right></div><div class=col-md></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div id='+getMostVoted['listId']+' style="left:-14px; background-color: rgb(243, 243, 243)" class="listDivs col-md-10" ><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions fontStyle15"><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';

                  }
  	
  				}
  				else
  				{

  				}
  				
  			}
  			
  			var data='<div class="crowd">'+noteData+'</div>';
  			$('#mostPopular').append(data);
  			}
  			else
  			{
  				$('#mostPopular').empty();
  			}
          	$("#searchingMostPopular").val("");
             }
          });
	}
	else
	{
		alert("Please enter a search term to perform a search");

	}
	}
	CrowdAutoFetchClear();
});
	
	
	
	
	
	
	
	
	
	
	

	/*window.addEvent('domready', function(){
		
		$('#mostVieweddatepicker').datetimepicker({
			weekStart: 0,
			todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
			showMeridian: 1,
			minView:2,
			maxView:4,
			format : 'M/d/yyyy'
		}).on('changeDate', function(ev) {
		//alert(dateText);
			//var currentDate = new Date(ev.date);
			var day = ev.date.getDate();
			var month = ev.date.getMonth() + 1;
			var year = ev.date.getFullYear();
			var dateValue= month+ "/" + day + "/" + year ;

				$('#mostView').empty();
				var url = urlForServer + "note/MostVieweddateSearchNotes";
				var datastr = '{"userId":"'+userId+'","noteAccess":"public","startDate":"'+dateValue+'"}';	
				console.log("<-------Sdatastr -------> " + datastr);
				var notesDetailsJsonObj="";
				var params = encodeURIComponent(datastr);
				$.ajax({
				headers: { 
	    	"Ajax-Call" : userTokens,
	    	"Ajax-Time" :userLoginTime				
	    	},
							type : 'POST',
							url : url,
							data : params,
							success : function(responseText) {
								console.log("<-------Scuccessfully public notes with response as -------> "+ responseText);
							
								var data = jQuery.parseJSON(responseText);
								var noteData;
								if(data!=null && data!='')
								{
									noteData="";
									 
								for ( var i = 0; i < data.length; i++) {
									notesDetailsJsonObj=data[i];
									if(isAndroid || isIphone )
									{
										var datedesc='<div class="badge glyphicon glyphicon-sm decs2" title=" Posted by '+notesDetailsJsonObj['publicUser']+' on  '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text"> Posted by '+notesDetailsJsonObj['publicUser']+' on  '+notesDetailsJsonObj['publicDate']+'</span></div>';

									 	noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=left><label></label><br/><span class="label label-success">'+notesDetailsJsonObj['viewed']+'</span></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+'</div></div></div></div></div><br>';

									}
									else
									{
										var datedesc='<div class="badge glyphicon glyphicon-sm decs2" title=" Posted by '+notesDetailsJsonObj['publicUser']+' on  '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text"> Posted by '+notesDetailsJsonObj['publicUser']+' on  '+notesDetailsJsonObj['publicDate']+'</span></div>';

									 	noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right><label></label><br/><span class="label label-success">'+notesDetailsJsonObj['viewed']+'</span></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+'</div></div></div></div></div><br>';

									}

									
								}
								
								var data='<div class="crowd">'+noteData+'</div>';
							
								$('#mostView').append(data);
								}
								else
								{
									 mostViewedDateSearch(dateValue);
								}
						
							},
							error : function() {
								console
										.log("<-------Error returned while public notes-------> ");
							}
				});
			
		});
	});*/
	
	/*window.addEvent('domready', function(){
		$('#newNotedatepicker').datetimepicker({
				weekStart: 0,
				todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0,
				showMeridian: 1,
				minView:2,
				maxView:4,
				format : 'M/d/yyyy'
		}).on('changeDate', function(ev) {
				var currentDate = new Date(ev.date);
				var day = ev.date.getDate();
			    var month = ev.date.getMonth() + 1;
			    var year = ev.date.getFullYear();
			    var dateValue= month+ "/" + day + "/" + year ;
				  
				noteDateSearch(dateValue);
		});
				new Picker.Date('newNotedatepicker', {
					dateFormat: 'dd/mm/yy',
					pickerClass: 'datepicker_bootstrap',
					onSelect: function(dateText) {
					//alert(dateText);
					var currentDate = new Date(dateText);
					 var day = currentDate.getDate();
				    var month = currentDate.getMonth() + 1;
				    var year = currentDate.getFullYear();
				    var dateValue= month+ "/" + day + "/" + year ;
				  
		           noteDateSearch(dateValue);
				}
				});
	});*/

		/*window.addEvent('domready', function(){
			$('#mostPopulardatepicker').datetimepicker({
					weekStart: 0,
					todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					forceParse: 0,
					showMeridian: 1,
					minView:2,
					maxView:4,
					format : 'M/d/yyyy'
				}).on('changeDate', function(ev){
					var currentDate = new Date(ev.date);
					
					var day = ev.date.getDate();
				    var month = ev.date.getMonth() + 1;
				    var year = ev.date.getFullYear();
				    var dateValue= month+ "/" + day + "/" + year ;
					$('#mostPopular').empty();
					
					var url = urlForServer + "note/MostVoteddateSearchNotes";
					var datastr = '{"userId":"'+userId+'","noteAccess":"public","startDate":"'+dateValue+'"}';	
					console.log("<-------Sdatastr -------> " + datastr);
					var notesDetailsJsonObj="";
					var params = encodeURIComponent(datastr);
						
						$.ajax({
						headers: { 
	    	"Ajax-Call" : userTokens,
	    	"Ajax-Time" :userLoginTime				
	    	},
								type : 'POST',
								url : url,
								data : params,
								success : function(responseText) {
									console.log("<-------Scuccessfully public notes with response as -------> "+ responseText);
									
									var data = jQuery.parseJSON(responseText);
									var noteData;
									if(data!=null && data!='')
									{
										noteData="";
										for ( var i = 0; i < data.length; i++) {
											notesDetailsJsonObj=data[i];
											var datedesc='<div class="badge glyphicon glyphicon-sm decs2" title=" Posted by '+notesDetailsJsonObj['publicUser']+' on  '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text"> Posted by '+notesDetailsJsonObj['publicUser']+' on  '+notesDetailsJsonObj['publicDate']+'</span></div>';
											noteData=noteData=noteData+'<div class=row><div class=col-md-1 style="width:16px;" align=right></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+notesDetailsJsonObj['listId']+','+notesDetailsJsonObj['noteId']+');></i><span class="label label-success vote-text">'+notesDetailsJsonObj['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+notesDetailsJsonObj['listId']+','+notesDetailsJsonObj['noteId']+');></i></div><div id='+notesDetailsJsonObj['listId']+' style="left:-14px;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv"><div class="todo_descriptions fontStyle15"><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+'</div></div></div></div></div><br>';											
										}
										var data='<div class="crowd">'+noteData+'</div>';
										$('#mostPopular').append(data);
									}else{
										$('#mostPopular').empty();
									}
								},
								error : function() {
									console.log("<-------Error returned while public notes-------> ");
								}
							});
				}); 
				/*new Picker.Date('mostPopulardatepicker', {
					dateFormat: 'dd/mm/yy',
					pickerClass: 'datepicker_bootstrap',
					onSelect: function(dateText) {	
					var currentDate = new Date(dateText);
					 var day = currentDate.getDate();
				    var month = currentDate.getMonth() + 1;
				    var year = currentDate.getFullYear();
				    var dateValue= month+ "/" + day + "/" + year ;
					$('#mostPopular').empty();
					var url = urlForServer + "note/MostVoteddateSearchNotes";
					var datastr = '{"userId":"'+userId+'","noteAccess":"public","startDate":"'+dateValue+'"}';	
					console.log("<-------Sdatastr -------> " + datastr);
					var notesDetailsJsonObj="";
					var params = encodeURIComponent(datastr);
						$
								.ajax({
									type : 'POST',
									url : url,
									data : params,
									success : function(responseText) {
										console
												.log("<-------Scuccessfully public notes with response as -------> "
														+ responseText);
									
										var data = jQuery.parseJSON(responseText);
										var noteData;
										if(data!=null && data!='')
										{
											noteData="";
											 
										for ( var i = 0; i < data.length; i++) {
											notesDetailsJsonObj=data[i];
										  	noteData=noteData=noteData+'<div class=row><div class=col-md-1 style="width:16px;" align=right></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+notesDetailsJsonObj['listId']+','+notesDetailsJsonObj['noteId']+');></i><span class="label label-success vote-text">'+notesDetailsJsonObj['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+notesDetailsJsonObj['listId']+','+notesDetailsJsonObj['noteId']+');></i></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;left:-14px;" class="listDivs col-md-7"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv"><div class="todo_descriptions"><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges"></div></div></div></div></div><br>';
											
											//noteData+'<div class=row><div class=col-md-1></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-7"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions" ><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges"></div></div></div></div></div>';
											//noteData=noteData+'<div class=row><div class=col-md-1></div><div class=col-md-1 align=right><label>'+notesDetailsJsonObj['vote']+'</label></div><div class=col-md-1 align=right><i class=glyphicon glyphicon-arrow-up  onclick=javascript:arrowvotedNotes(true,'+notesDetailsJsonObj['listId']+','+notesDetailsJsonObj['noteId']+');></i><i class=glyphicon glyphicon-arrow-down onclick=javascript:arrowvotedNotes(false,'+notesDetailsJsonObj['listId']+','+notesDetailsJsonObj['noteId']+');></i></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-6"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv"><div class="todo_descriptions"><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges"></div></div></div></div></div>';

											
										}
										
										var data='<div class="crowd">'+noteData+'</div>';
									
										$('#mostPopular').append(data);
										}
										else
										{
											$('#mostPopular').empty();
										}
								
									},
									error : function() {
										console
												.log("<-------Error returned while public notes-------> ");
									}
								});
					
				  }
				});*/
		//});
		 /*window.addEvent('domready', function(){
				
				new Picker.Date('mostVieweddatepicker', {
					dateFormat: 'dd/mm/yy',
					pickerClass: 'datepicker_bootstrap',
					onSelect: function(dateText) {
					
					var currentDate = new Date(dateText);
					 var day = currentDate.getDate();
				    var month = currentDate.getMonth() + 1;
				    var year = currentDate.getFullYear();
				    var dateValue= month+ "/" + day + "/" + year ;
					
			$('#mostView').empty();
			var url = urlForServer + "note/MostVieweddateSearchNotes";
			var datastr = '{"userId":"'+userId+'","noteAccess":"public","startDate":"'+dateValue+'"}';	
			console.log("<-------Sdatastr -------> " + datastr);
			var notesDetailsJsonObj="";
			var params = encodeURIComponent(datastr);
				$
						.ajax({
							type : 'POST',
							url : url,
							data : params,
							success : function(responseText) {
								console
										.log("<-------Scuccessfully public notes with response as -------> "
												+ responseText);
							
								var data = jQuery.parseJSON(responseText);
								var noteData;
								if(data!=null && data!='')
								{
									noteData="";
									 
								for ( var i = 0; i < data.length; i++) {
									notesDetailsJsonObj=data[i];
								 	noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right><label></label><br/><span class="label label-success">'+notesDetailsJsonObj['viewed']+'</span></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-7"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions" ><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges"></div></div></div></div></div><br>';

									
								}
								
								var data='<div class="crowd">'+noteData+'</div>';
							
								$('#mostView').append(data);
								}
								else
								{
									 mostViewedDateSearch(dateValue);
								}
						
							},
							error : function() {
								console
										.log("<-------Error returned while public notes-------> ");
							}
						});
		
	 }
		});
			 
		 });*/
	
		window.addEvent('domready', function(){
				new Picker.Date('featuresdatepicker', {
					dateFormat: 'dd/mm/yy',
					pickerClass: 'datepicker_bootstrap',
					onSelect: function(dateText) {
				}
				});
		});
		
	
});


var viewed=true;			
var voteFlag=true;
var viewFlag=true;
var voteCount;

//LogWrite for most view ,new note,most popular
function LogWrite(noteId,listId,userId,id)
{
         	
	            
	            var currentDate = new Date();
				var viteWithUser='vote';
				var voteDivClass = 'js-vote';
				var day = currentDate.getDate();
				var month = currentDate.getMonth() + 1;
				var year = currentDate.getFullYear();
				//kishore for log date formate modification
				var selectedDate=new Date();
				var date=selectedDate.getMonth()+1;
				date=date+"/"+selectedDate.getDate()+"/"+selectedDate.getFullYear();
				
				// end kishore
				var currentTime = new Date();
				var hour        = currentTime.getHours();
				var minute      = currentTime.getMinutes();
				var second      = currentTime.getSeconds();
				var time= hour + ":" + minute + ":" +second;
				var url = urlForServer+"note/newNoteLog";
				var datastr = '{"userId":"'+userId+'","listId":"'+listId+'","noteId":"'+noteId+'","pageType":"crowd","logType":"A","status":"A","time":"'+time+'","date":"'+date+'","logDescription":"'+userFulName+' checking for Recent Notes"}';
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type : 'POST',
					data : datastr,
					url : url,
					success : function(responseText) {
					var data = jQuery.parseJSON(responseText);
					
				},
				error : function() {
					console.log("<-------error returned for get new note -------> ");
					}
				});
			
				//var viteWithUser='vote';
				//var voteDivClass = 'js-vote';
				
				//if(viewed==true)
				//{
					MostVieweds(noteId,listId);
				//}
}


//New Note fetching
var notesDetailsJsonObj;
var getVoteViewed;	
var Vote;
var filterUserId;
function newNotes(filterUserId)
{
	loadTagsForCrowd();
	//searchNotes();
	$("#searching").val("");
	viewed=true;
	var newsearchNotes=new Array();
	var notesDetailMap={};
	var activeNotedetailsMap={};
	var listDetailMap={};
//	$('#tab1').find('#mostSubDiv3').each(function( index ){
//		$(this).remove();
//	});
	
	
	var url = urlForServer + "note/RecentnewNotes";
    var datastr = '{"userId":"'+userId+'","noteAccess":"public","filteredUserId":"UserLevel"}';
	notesDetailsJsonObj="";
	var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
			},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) {
						
						var data = jQuery.parseJSON(responseText);
						var noteData;
						var datedesc='';
						if(data!=null && data!='')
						{
							noteData="";
							 
						for ( var i = 0; i < data.length; i++) {
							notesDetailsJsonObj=data[i];
							var tagArray = new Array();
							var tagNames="";
							var noteTags="";
							var notes=data[i];
							newsearchNotes.push(notes.noteName);
							activeNotedetailsMap[notes.noteName]=notesDetailsJsonObj;
							notesDetailMap[notes.noteName]=notes.noteId;
							listDetailMap[notes.noteName]=notes.listId;
							if(notesDetailsJsonObj['tagIds']!=null && notesDetailsJsonObj['tagIds']!=''){
								var tagWithParams= notesDetailsJsonObj['tagIds'];
								if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
									tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
								}
								if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
									tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
								}
								if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
									tagArray = tagWithParams.split(',');
								}else{
									if(tagWithParams!= undefined && tagWithParams.trim() != ''){
										tagArray = tagWithParams.split(',');
									}
								}
								for(var siz=0;siz<tagArray.length;siz++){
									var tagId=tagArray[siz].trim();
									if (tagId in crowdTagMap){
										
										tagNames+=" "+crowdTagMap[tagId]+" ,";
									}
								}
								
								if(tagNames!=null && tagNames!=''){
									var lastIndex  = tagNames.lastIndexOf(",");
									var tagNames = tagNames.substring(0, lastIndex);
								noteTags='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB;word-spacing:-6px;" title="'+tagNames+'"><span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">'+tagNames+'</span></div>';
								}
							}
							
							datedesc='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;word-spacing:-6px;" title=" Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by <a href="javascript:void(0);" onClick="descriptionView(\'' + notesDetailsJsonObj['publicUser'] + '\');">'+notesDetailsJsonObj['publicUser']+'</a> on '+notesDetailsJsonObj['publicDate']+'</span></div>';
							noteData=noteData+'<div class=row><div></div> <div class=col-md-1></div><div id='+notesDetailsJsonObj['listId']+' class="listDivs col-md-10" style="background-color: rgb(243, 243, 243)"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';
						
						}
						
						$('#newNote').empty();
						var recentData='<div class="crowd">'+noteData+'</div>';
							$('#tab1').find('#mostSubDiv3').each(function( index ){
									$(this).remove();
								});
						$('#newNote').append(recentData);
						}
				
					},
					error : function() {
						console
								.log("<-------Error returned while public notes-------> ");
					}
				});
	}


//First Most viewed values
function fetchMostViewed()
{
	$( "#searchingMostView" ).val("");
	var datedesc='';
	$('#mostVieweddatepicker').val('');
	viewed=true;
	var mostviewsearchNotes=new Array();
	var notesDetailMap={};
	var listDetailMap={};
	var notesObjMap={};
//	$('#tab2').find('#mostSubDiv2').each(function( index ){
//		$(this).remove();
//	});
	
	loadTagsForCrowd();
	
	
	
	var noteData;
	   var url = urlForServer+"note/getmostViewed";
		
	   var datastr = '{"userId":"'+userId+'","noteAccess":"public"}';
	   $.ajax({
		   headers: { 
		   "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			data : datastr,
			url : url,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null && data!='')
			{
				noteData="";		 
			for ( var i = 0; i < data.length; i++) {
				var tagArray = new Array();
				var tagNames="";
				var noteTags="";
				getMostViewed=data[i];
				var notes=data[i];
				mostviewsearchNotes.push(notes.noteName);
				notesDetailMap[notes.noteName]=notes.noteId;
				listDetailMap[notes.noteName]=notes.listId;
				notesObjMap[notes.noteName] = notes;
				
				if(getMostViewed['viewed']>0)
				{
					if(getMostViewed['tagIds']!=null && getMostViewed['tagIds']!=''){
						var tagWithParams= getMostViewed['tagIds'];
						if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
							tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
						}
						if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
							tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
						}
						if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
							tagArray = tagWithParams.split(',');
						}else{
							if(tagWithParams!= undefined && tagWithParams.trim() != ''){
								tagArray = tagWithParams.split(',');
							}
						}
						for(var siz=0;siz<tagArray.length;siz++){
							var tagId=tagArray[siz].trim();
							if (tagId in crowdTagMap){
								tagNames+=" "+crowdTagMap[tagId]+" ,";
							}
						}
						
						if(tagNames!=null && tagNames!=''){
						var lastIndex  = tagNames.lastIndexOf(",");
						var tagNames = tagNames.substring(0, lastIndex);	
						noteTags='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB;word-spacing:-6px;" title="'+tagNames+'"><span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">'+tagNames+'</span></div>';
						}
					}
					if(isAndroid || isIphone )
					{
					    datedesc='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;word-spacing:-6px;" title=" Posted by'+getMostViewed['publicUser']+' on '+getMostViewed['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by <a href="javascript:void(0);" onClick="descriptionView(\'' + getMostViewed['publicUser'] + '\');">'+getMostViewed['publicUser']+'</a> on '+getMostViewed['publicDate']+'</span></div>';

						noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=left><label></label><br/><span class="label label-success">'+getMostViewed['viewed']+'</span></div><div id='+getMostViewed['listId']+'  class="listDivs col-md-10"><div class="listBodyAll"><div id='+getMostViewed['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostViewed['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div><br>';

					}
					else
					{
					    datedesc='<div class="badge glyphicon glyphicon-sm decs2" title=" Posted by'+getMostViewed['publicUser']+' on '+getMostViewed['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by <a href="javascript:descriptionView(\'' + getMostViewed['publicUser'] + '\');">'+getMostViewed['publicUser']+'</a> on '+getMostViewed['publicDate']+'</span></div>';

						noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right><label></label><br/><span class="label label-success">'+getMostViewed['viewed']+'</span></div><div id='+getMostViewed['listId']+'  class="listDivs col-md-10" style="background-color: rgb(243, 243, 243)"><div class="listBodyAll"><div id='+getMostViewed['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostViewed['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';

					}
				}
				else
				{
				}
			}
			$('#mostView').empty();
			var mostData='<div class="crowd">'+noteData+'</div>';
			$('#tab2').find('#mostSubDiv2').each(function( index ){
				$(this).remove();
			});
				$('#mostView').append(mostData);
			}
			else
			{
				//mostViewed();
				$('#mostView').empty();
			}
					},
		error : function() {
			console.log("<-------error returned for get most viewed -------> ");
			}
		});
		
		//Auto Fetch Most Viewed Notes
		CrowdAutoFetchClear();
		autoCrowdDetailsFetchTimer=window.setInterval(function(){
			if(userSession=='Active')
			{
			fetchMostViewed();
			}
			},60000);
						
}


//Fetching most viewed functionlaity
function mostViewed()
{

	viewed=true;
	voteFlag=true;
	
	var url = urlForServer + "note/getList";
	var datastr = '{"userId":"'+userId+'","noteAccess":"public"}';	
	notesDetailsJsonObj="";
	var params = encodeURIComponent(datastr);
		$
				.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
				    	},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) {
						
					
						var data = jQuery.parseJSON(responseText);
						var noteData;
						if(data!=null && data!='')
						{
							noteData="";
							var datedesc='';
							 
						for ( var i = 0; i < data.length; i++) {
							notesDetailsJsonObj=data[i];
							var notes=data[i];
							mostviewsearchNotes.push(notes.noteName);
							//<i class='glyphicon glyphicon-arrow-up'></i><i class='glyphicon glyphicon-arrow-down'></i>
						    datedesc='<div class="badge glyphicon glyphicon-sm decs2" title="This note public added on '+notesDetailsJsonObj['publicUser']+'  '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;">'+notesDetailsJsonObj['publicUser']+'  '+notesDetailsJsonObj['publicDate']+'</span></div>';

						noteData=noteData+'<div class=row><div class=col-md-1></div><div class=col-md-1></div><div class=col-md-1><i class=glyphicon glyphicon-arrow-up ></i><i class=glyphicon glyphicon-arrow-down></i></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+'</div></div></div></div></div>';
   						}
							
						$('#mostView').empty();
						var mostDataView='<div class="crowd">'+noteData+'</div>';
					
						$('#mostView').append(mostDataView);
						}
				
					},
					error : function() {
						console
								.log("<-------Error returned while public notes-------> ");
					}
				});
		
	}

//Most viewed into mongo db

//Fetch Notes for features

function featuresnote(){
	var featureSearchNotes=new Array();
	var featureNotesDetailMap={};
	var featureListDetailMap={};
	
	$('#tab4').find('#featuressub').each(function( index ){
		$(this).remove();
	});
	var searchContent='<div class="col-md-3" id="featuressub"><input type="text" class="search-query col-md-9" placeholder="Search" id="featuressearch"></div>';
	$("#features").append(searchContent);
	$( "#featuressearch" ).typeahead({
		source: featureSearchNotes,
		highlighter: function(item){
	        var user = featureNotesDetailMap[item];
	        return  item;
	    },updater: function(item){
	    	$('#featuresnote').empty();
	    	var	noteData;
	    	var selectedNoteId=featureNotesDetailMap[item];
	    	var selectedListId=featureListDetailMap[item];
	    	noteData="";
	    	noteData=noteData+'<div class=row><div class=col-md-2></div><div id='+selectedListId+' style="float:left;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+selectedNoteId+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ item+'</p></div><div class="badges"></div></div></div></div></div>';
			var data='<div class="crowd">'+noteData+'</div>';
			$('#featuresnote').append(data);
	    	
	    }
		});
	
	
	$('#featuresnote').empty();
	var url = urlForServer+"note/getFeaturedNotes";
	var datastr = '{"userId":"'+userId+'"}';
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
    	},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
		if(data!=null && data!='')
		{
			//alert("Datas");
			noteData="";		 
		for ( var i = 0; i < data.length; i++) {
			//alert("Datas"+data[i].noteName);
			getMostViewed=data[i];
			var notes=data[i];
			featureSearchNotes.push(notes.noteName);
			featureNotesDetailMap[notes.noteName]=notes.noteId;
			featureListDetailMap[notes.noteName]=notes.listId;
			noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right><label></label><br/></div><div id='+getMostViewed['listId']+' style="float:left;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+getMostViewed['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostViewed['noteName']+'</p></div><div class="badges"></div></div></div></div></div>';
		}
		var data='<div class="crowd">'+noteData+'</div>';
			$('#featuresnote').append(data);
		}
		else
		{
			//mostViewed();
			$('#featuresnote').empty();
		}
		
	},
	error : function() {
		console.log("<-------error returned for get new note -------> ");
		}
	});
}


//Fetching viewed most view functionlity
var getMostViewed;
function MostVieweds(noteId,listId)
{
	
	   var url = urlForServer+"note/maxNostView";
		
		var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'"}';
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			data : datastr,
			url : url,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null && data!='')
			{
						 
			for ( var i = 0; i < data.length; i++) {
				getMostViewed=data[i];
						
				if(getMostViewed['noteId']!=null && getMostViewed['listId']!=null )
				{
					updateMostViewed(getMostViewed['noteId'],getMostViewed['listId'],++getMostViewed['viewed'],getMostViewed['countId'],getMostViewed['vote'],voteFlag);
					fetchMostViewed();
				}
				else
				{
					var vote=0;
					var viewed=1;
					insertMostViewed(getMostViewed['noteId'],getMostViewed['listId'],vote,viewed);
					fetchMostViewed();
				}
			}
			
			}
			else
			{
				var vote=0;
				var viewed=1;
				insertMostViewed(noteId,listId,vote,viewed);
				fetchMostViewed();
				
			}
		},
		error : function() {
			console.log("<-------error returned for get most viewed -------> ");
			}
		});

}


//Insert most viewed functionality

function insertMostViewed(noteId,listId,vote,viewed)
{
	
   var url = urlForServer+"note/insertMostViewed/"+userId;
	
	var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'","vote":"'+vote+'","viewed":"'+viewed+'"}';
	
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
	
	},
	error : function() {
		console.log("<-------error returned for get most viewed -------> ");
		}
	});
}


//update most view functionality
function updateMostViewed(noteId,listId,viewed,countId,vote)
{
   var url = urlForServer+"note/updatemostViewed/"+userId;
	
	var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'","viewed":"'+viewed+'","countId":"'+countId+'","vote":"'+vote+'"}';
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
		
	},
	error : function() {
		console.log("<-------error returned for get most viewed -------> ");
		}
	});
}




function updateMostVoted(noteId,listId,viewed,countId,vote)
{
   var url = urlForServer+"note/updatemostViewed/"+userId;
   var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'","viewed":"'+viewed+'","countId":"'+countId+'","vote":"'+vote+'"}';
   $.ajax({
	   headers: { 
	   "Mn-Callers" : musicnote,
   	"Mn-time" :musicnoteIn				
   	},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
		
	},
	error : function() {
		console.log("<-------error returned for get most viewed -------> ");
		}
	});
}
function noteDateSearch(dateText)
{
	$('#newNote').empty();
	var url = urlForServer + "note/dateSearchNotes";
	var datastr = '{"userId":"'+userId+'","noteAccess":"public","startDate":"'+dateText+'"}';	
	var notesDetailsJsonObj="";
	var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
			},
			type : 'POST',
			url : url,
			data : params,
			success : function(responseText) {
					
				var data = jQuery.parseJSON(responseText);
				var noteData;
				if(data!=null && data!='')
				{
					noteData="";
							 
					for ( var i = 0; i < data.length; i++) {
						notesDetailsJsonObj=data[i];
						var datedesc='<div class="badge glyphicon glyphicon-sm decs2" title=" Posted by  '+notesDetailsJsonObj['publicUser']+' on  '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'</span></div>';

						noteData=noteData+'<div class=row><div></div><div class=col-md-1></div><div id='+notesDetailsJsonObj['listId']+' class="listDivs col-md-10"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+'</div></div></div></div></div><br>';
					}
						
					var data='<div class="crowd">'+noteData+'</div>';
					
					$('#newNote').append(data);
				}else{
					$('#newNote').empty();
				}
				
			},
			error : function() {
				console.log("<-------Error returned while public notes-------> ");
			}
		});
}

function mostViewedDateSearch(dateText)
{
	
	$('#mostView').empty();
	var url = urlForServer + "note/dateSearchNotes";
	var datastr = '{"userId":"'+userId+'","noteAccess":"public","startDate":"'+dateText+'"}';	
	var notesDetailsJsonObj="";
	var params = encodeURIComponent(datastr);
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
			type : 'POST',
			url : url,
			data : params,
			success : function(responseText) {
					
				var data = jQuery.parseJSON(responseText);
				var noteData;
				if(data!=null && data!='')
				{
					noteData="";
							 
					for ( var i = 0; i < data.length; i++) {
							notesDetailsJsonObj=data[i];
							noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right><label></label><br/><span class="label label-success">'+notesDetailsJsonObj['viewed']+'</span></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges"></div></div></div></div></div>';

						   //	noteData=noteData+'<div class=row><div class=col-md-1></div><div class=col-md-1></div><div class=col-md-1></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-6"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions" ><p>'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges"></div></div></div></div></div>';
	
					}
						
					var data='<div class="crowd">'+noteData+'</div>';
					
					$('#mostView').append(data);
				}else{
					$('#mostView').empty();
				}
				
			},
			error : function() {
				console.log("<-------Error returned while public notes-------> ");
			}
		});
	
  
}

function maxMostView(noteId,listId)
{
	   var url = urlForServer+"note/maxNostView";
		
		var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'"}';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
	    	},
			type : 'POST',
			data : datastr,
			url : url,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			
		},
		error : function() {
			console.log("<-------error returned for get most viewed -------> ");
			}
		});


}



function mostPopular()
{
	
	viewed=false;
	$('#mostPopular').empty();
	var url = urlForServer + "note/getList";
	var datastr = '{"userId":"'+userId+'","noteAccess":"public"}';	
	notesDetailsJsonObj="";
	var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
			},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) {
					
						var data = jQuery.parseJSON(responseText);
						var noteData;
						if(data!=null && data!='')
						{
							noteData="";
							for ( var i = 0; i < data.length; i++) {
							notesDetailsJsonObj=data[i];
							var notes=data[i];
							searchNotes.push(notes.noteName);
							noteData=noteData+'<div class=row><div class=col-md-1></div><div class=col-md-1></div><div class=col-md-1><i class=glyphicon glyphicon-arrow-up></i><i class=glyphicon glyphicon-arrow-down></i></div><div id='+notesDetailsJsonObj['listId']+' style="float:left;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges"></div></div></div></div></div>';
							
						}
						
						
						var data='<div class="crowd">'+noteData+'</div>';
					
						$('#mostPopular').append(data);
						}
						
				
					},
					error : function() {
						console.log("<-------Error returned while public notes-------> ");
					}
				});
		
	}

function voteLists()
{
	var datedesc='';
	$('#mostPopulardatepicker').val('');
	var searchNotes=new Array();
	var activeNotedetailsMap={};
	var notesDetailMap={};
	var listDetailMap={};
//	$('#tab3').find('#mostSubDiv').each(function( index ){
//		$(this).remove();
//	});
	loadTagsForCrowd();
	
	var getMostVoted;
	$('#mostPopular').empty();
	var noteData;
	   var url = urlForServer+"note/MostVoted";
		
	   var datastr = '{"userId":"'+userId+'","noteAccess":"public"}';

	   $.ajax({
		   headers: { 
		   "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
	    	},
			type : 'POST',
			data : datastr,
			url : url,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null && data!='')
			{
				
				noteData="";		 
			for ( var i = 0; i < data.length; i++) {
				var tagArray = new Array();
				var tagNames="";
				var noteTags="";
				getMostVoted=data[i];
				var notes=data[i];
				searchNotes.push(notes.noteName);
				activeNotedetailsMap[notes.noteName] = notes;
				notesDetailMap[notes.noteName]=notes.noteId;
				listDetailMap[notes.noteName]=notes.listId;
				
				
				if(getMostVoted['vote']>0)
				{
					if(getMostVoted['tagIds']!=null && getMostVoted['tagIds']!=''){
						var tagWithParams= getMostVoted['tagIds'];
						if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
							tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
						}
						if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
							tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
						}
						if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
							tagArray = tagWithParams.split(',');
						}else{
							if(tagWithParams!= undefined && tagWithParams.trim() != ''){
								tagArray = tagWithParams.split(',');
							}
						}
						for(var siz=0;siz<tagArray.length;siz++){
							var tagId=tagArray[siz].trim();
							if (tagId in crowdTagMap){
								tagNames+=" "+crowdTagMap[tagId]+" ,";
							}
						}
						
						if(tagNames!=null && tagNames!=''){
						var lastIndex  = tagNames.lastIndexOf(",");
						var tagNames = tagNames.substring(0, lastIndex);	
						noteTags='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB;word-spacing:-6px;" title="'+tagNames+'"><span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">'+tagNames+'</span></div>';
						}
					}
				    datedesc='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;word-spacing:-6px;" title=" Posted by '+getMostVoted['publicUser']+' on  '+getMostVoted['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by <a href="javascript:descriptionView(\'' + getMostVoted['publicUser'] + '\');">'+getMostVoted['publicUser']+'</a> on  '+getMostVoted['publicDate']+'</span></div>';
                    if(is_chrome || is_Ie || is_safari)
                    {
					//noteData=noteData+'<div class=row><div></div><div class=col-md-2 style="width:16px;" align=right></div><div class=col-md-1></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div class="col-md-1"></div><div id='+getMostVoted['listId']+' style="left:-34px;" class="listDivs col-md-10"><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions"><p>'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div><br>';
    					//noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right></div><div class=col-md-1 style="float:left" align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div id='+getMostVoted['listId']+' style="left:-14px; background-color: rgb(243, 243, 243)" class="offset2 listDivs"><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions fontStyle15"><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';
                    	noteData=noteData+'<div class=row><div></div><div class=col-md-2 style="width:16px;" align=right></div><div class=col-md></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div id='+getMostVoted['listId']+' style="left:-14px; background-color: rgb(243, 243, 243)" class="listDivs col-md-10" ><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions fontStyle15"><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';
                    }
                    else
                    {
    					noteData=noteData+'<div class=row><div></div><div class=col-md-2 style="width:16px;" align=right></div><div class=col-md></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div id='+getMostVoted['listId']+' style="left:-14px; background-color: rgb(243, 243, 243)" class="listDivs col-md-10" ><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions fontStyle15"><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ getMostVoted['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';

                    }
					
	//noteData=noteData+'<div class=row><div></div><div class=col-md-1 align=right><label></label><br/><span class="label label-success">'+getMostVoted['vote']+'</span></div><div class=col-md-1 align=right><i class="glyphicon glyphicon-arrow-up arrow" onclick=javascript:castVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i><span class="label label-success vote-text">'+getMostVoted['vote']+'</span><i class="glyphicon glyphicon-arrow-down arrow" onclick=javascript:uncastVote('+getMostVoted['listId']+','+getMostVoted['noteId']+');></i></div><div id='+getMostVoted['listId']+' style="float:left;" class="listDivs col-md-7"><div class="listBodyAll"><div id='+getMostVoted['noteId']+' class="noteDiv"><div class="todo_descriptions"><p>'+ getMostVoted['noteName']+'</p></div><div class="badges"></div></div></div></div></div>';
	
				}
				else
				{

				}
				
			}
			
			
			var data='<div class="crowd">'+noteData+'</div>';
			$('#tab3').find('#mostSubDiv').each(function( index ){
				$(this).remove();
			});
			$('#mostPopular').append(data);
			}
			else
			{
				$('#mostPopular').empty();
			}
					},
		error : function() {
			console.log("<-------error returned for get most viewed -------> ");
			}
		});
		
		//Auto Fetch Most Popular Notes
		CrowdAutoFetchClear();
		autoCrowdDetailsFetchTimer=window.setInterval(function(){
			if(userSession=='Active')
			{
			voteLists();
			}
			},60000);

}

function votedNotes(votedFlag,listId,noteId)
{
	var votedNotes;
	
	   var url = urlForServer+"note/maxNostView";
		
		var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'"}';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
	    	},
			type : 'POST',
			data : datastr,
			url : url,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null && data!='')
			{
						 
			for ( var i = 0; i < data.length; i++) {
				votedNotes=data[i];
						
				if(votedNotes['noteId']!=null && votedNotes['listId']!=null )
				{
					if(votedFlag==true)
					{	
						updateMostVoted(votedNotes['noteId'],votedNotes['listId'],votedNotes['viewed'],votedNotes['countId'],votedNotes['vote']);
						voteLists();
					}
					else
					{
						updateMostVoted(votedNotes['noteId'],votedNotes['listId'],votedNotes['viewed'],votedNotes['countId'],votedNotes['vote']);
						voteLists();
					}
					
					
				}
				else
				{
					if(votedFlag==true)
					{
					var vote=1;
					var viewed=0;
					insertMostViewed(votedNotes['noteId'],votedNotes['listId'],vote,viewed);
					voteLists();
					}
					else
					{
						var vote=0;
						var viewed=0;
						insertMostViewed(votedNotes['noteId'],votedNotes['listId'],vote,viewed);
						voteLists();
					}
					
				}
			}
			
			}
			else
			{
				if(votedFlag==true)
				{
					
				var vote=1;
				var viewed=0;
				insertMostViewed(noteId,listId,vote,viewed);
				voteLists();
				}
				else
				{
					var vote=0;
					var viewed=0;
					insertMostViewed(noteId,listId,vote,viewed);
					voteLists();
				}
				
			}
		},
		error : function() {
			console.log("<-------error returned for get most voted -------> ");
			}
		});


}

function createListForVote(){
	var userNameForDiv ;
	var userIDForDiv;
	
	var castButtonClass= 'js-vote btn btn-success';
	var castedVote ='Vote';
	var contentDiv='';//'<li><center><a class="btn btn-success" > Voters</a></center></li><li class="divider"></li>';
		if(vote.length > 0 && vote[0] != null){
			for (i=0;i<vote.length;i++){
				userNameForDiv = '';
				if(userId == vote[i]){
					userNameForDiv = userName;
					userIDForDiv = userId;
					castedVote ='Voted';
					castButtonClass= 'js-un-vote btn btn-danger';
				}else{
					var userObje = activeUserObjectMap[vote[i]];
					userNameForDiv = userObje['userName'];
					userIDForDiv = userObje['userId'];
				}
				contentDiv = contentDiv +'<li class="voter '+userIDForDiv+'"> '
							+'<div class="member">'
								+'<span class="member-initials" title="'+userNameForDiv+'">'+userNameForDiv.substring(0,2).toUpperCase()+'</span>'
							+'</div>'
							+'<p class="popover-user-name title">'+userNameForDiv+'</p>'
						+'</li>';
			}
		}
						
		contentDiv = contentDiv	+'<li class="divider"></li>'
					+'<li class="vote-btns"><center><a class="'+castButtonClass+'">'+castedVote+'</a></center></li>';
					
	return contentDiv;				
}
function castVote(listIdFromList,noteIdFromList)
{
	var url = urlForServer+"note/getVotes/" + listIdFromList + "/"+ noteIdFromList;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
			type : 'POST',
			url : url,
			success : function(response) {
				if(response!= null && response!= ''){
					vote = response.split(",");
					if(vote.indexOf(userId) == -1){
						vote.push(userId);
						var params= encodeURIComponent(vote);
						var url = urlForServer+"note/castUncastVote/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
						$.ajax({
							headers: { 
							"Mn-Callers" : musicnote,
					    	"Mn-time" :musicnoteIn			
					    	},
							type : 'POST',
							data : params,
							url : url,
							success : function(responseText) {
								voteLists();
							},
							error : function() {
								console.log("<-------error returned for vote by click arrow -------> ");
							}
						});
					}
				}
			},error: function(){
				console.log("<-------error returned for cast vote by click arrow -------> ");
			}
		});
}

function uncastVote(listIdFromList,noteIdFromList)
 {
	
		var url = urlForServer+"note/getVotes/" + listIdFromList + "/"+ noteIdFromList;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			url : url,
			success : function(response) {
				
				if(response!= null && response!= ''){
					vote = response.split(",");
					if(vote.indexOf(userId) != -1){
						vote.splice(vote.indexOf(userId), 1);
						var clickId = $(this);
						var params = '';
						if (vote.length > 0 && vote[0] != null) {
							params = encodeURIComponent(vote);
						} else {
							params = '  ';
							params = encodeURIComponent(params);
						}
						var url = urlForServer + "note/castUncastVote/" + listIdFromList + "/"
								+ noteIdFromList + "/"+userId;
						$.ajax({
							headers: { 
							"Mn-Callers" : musicnote,
					    	"Mn-time" :musicnoteIn				
					    	},
							type : 'POST',
							data : params,
							url : url,
							success : function(responseText) {
								voteLists();
							},
							error : function() {
								console.log("<-------error returned for get most viewed -------> ");
							}
						});		
					}
				}
			},error: function(){
			}
		});	
}
function keypressnotallowed(event)
{
	return false;
}

function loadTagsForCrowd(){
	
	var url = urlForServer + "note/getAllTags";
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		url : url,
		cache : false,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(response) {
			if (response != null && response != '') {
				for ( var i = 0; i < response.length; i++) {
					crowdTagMap[response[i].tagId]=response[i].tagName;
				}
			}
		}
	});
}
function descriptionView(userName)
{
	var url = urlForServer+"user/getCrowdUserDetails";
	var datastr = '{"userName":"'+userName+'"}';
	var params = encodeURIComponent(datastr);
	
	$.ajax({
		 headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
			for ( var i = 0; i < data.length; i++) {
				userDetailsJsonObj = data[i];
				if(userDetailsJsonObj['filePath']!='null' && userDetailsJsonObj['filePath']!='')
				 {
					 $("#crowdprofileImg").attr("src", uploadUrl+userDetailsJsonObj['filePath']);
				 }
				 else
				 {
					 $("#crowdprofileImg").attr("src", "pics/dummy.jpg");

				 }
				
				if(userDetailsJsonObj['userFirstName']!=null && userDetailsJsonObj['userFirstName']!='' && userDetailsJsonObj['userLastName']!=null && userDetailsJsonObj['userLastName']!='')
				{
					//$("#crowdUserName").attr(userDetailsJsonObj['userName']);
					$("#flName").text(userDetailsJsonObj['userFirstName']+" "+userDetailsJsonObj['userLastName']);
				}
				else
				{
					$("#flName").text("");
				}
				if(userDetailsJsonObj['userName']!=null && userDetailsJsonObj['userName']!='')
				{
					//$("#crowdUserName").attr(userDetailsJsonObj['userName']);
					$("#crowdUserName").text(userDetailsJsonObj['userName']);
				}
				else
				{
					$("#crowdUserName").text("");
				}
				if(userDetailsJsonObj['description']!='null' && userDetailsJsonObj['description']!='')
				{
					$("#crowdDescription").text(userDetailsJsonObj['description']);
					/*if(userDetailsJsonObj['description'].length<60)
					{
						$("#crowdDescription").text(userDetailsJsonObj['description']);
					}
					else
					{
						$("#crowdDescription").text(userDetailsJsonObj['description'].substring(0,60)+"...");
					}*/
				}
				else
				{
					$("#crowdDescription").text("");
				}
				if(userDetailsJsonObj['url']!='null' && userDetailsJsonObj['url']!='')
				{
					$("#crowdUrl").text(userDetailsJsonObj['url']);
					if(userDetailsJsonObj['url'].substring(0, 4)=="http")
					{
						$("#crowdUrl").attr('href',userDetailsJsonObj['url']);
					}
					else
					{
						$("#crowdUrl").attr('href',"http://"+userDetailsJsonObj['url']);
					}
				}
				else
				{
					$("#crowdUrl").text("");
				}
				newnote(userDetailsJsonObj['userId']);
			}
		}
	    	}
	});
	app.navigate("crowdProfiles", {
		trigger : true
	});
}
function newnote(noteUserId)
{

	loadTagsForCrowd();
	//searchNotes();
	$("#searching").val("");
	viewed=true;
	var newsearchNotes=new Array();
	var notesDetailMap={};
	var activeNotedetailsMap={};
	var listDetailMap={};
//	$('#tab1').find('#mostSubDiv3').each(function( index ){
//		$(this).remove();
//	});
	
	
	var url = urlForServer + "note/RecentnewNotes";
    var datastr = '{"userId":"'+userId+'","noteAccess":"public","filteredUserId":"'+noteUserId+'"}';
	notesDetailsJsonObj="";
	var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
			},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) {
					
						var data = jQuery.parseJSON(responseText);
						var noteData;
						var datedesc='';
						if(data!=null && data!='')
						{
							noteData="";
							 
						for ( var i = 0; i < data.length; i++) {
							notesDetailsJsonObj=data[i];
							var tagArray = new Array();
							var tagNames="";
							var noteTags="";
							var notes=data[i];
							newsearchNotes.push(notes.noteName);
							activeNotedetailsMap[notes.noteName]=notesDetailsJsonObj;
							notesDetailMap[notes.noteName]=notes.noteId;
							listDetailMap[notes.noteName]=notes.listId;
							if(notesDetailsJsonObj['tagIds']!=null && notesDetailsJsonObj['tagIds']!=''){
								var tagWithParams= notesDetailsJsonObj['tagIds'];
								if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
									tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
								}
								if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
									tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
								}
								if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
									tagArray = tagWithParams.split(',');
								}else{
									if(tagWithParams!= undefined && tagWithParams.trim() != ''){
										tagArray = tagWithParams.split(',');
									}
								}
								for(var siz=0;siz<tagArray.length;siz++){
									var tagId=tagArray[siz].trim();
									if (tagId in crowdTagMap){
										
										tagNames+=" "+crowdTagMap[tagId]+" ,";
									}
								}
								
								if(tagNames!=null && tagNames!=''){
									var lastIndex  = tagNames.lastIndexOf(",");
									var tagNames = tagNames.substring(0, lastIndex);
								noteTags='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;background-color:#FFF5BB;word-spacing:-6px;" title="'+tagNames+'"><span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">'+tagNames+'</span></div>';
								}
							}
							
							datedesc='<div class="badge glyphicon glyphicon-sm decs2" style="white-space: normal;display:inline;word-spacing:-6px;" title=" Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Posted by '+notesDetailsJsonObj['publicUser']+' on '+notesDetailsJsonObj['publicDate']+'</span></div>';
							noteData=noteData+'<div class=row><div></div> </div><div id='+notesDetailsJsonObj['listId']+' class="listDivs col-md-10" style="background-color: rgb(243, 243, 243)"><div class="listBodyAll"><div id='+notesDetailsJsonObj['noteId']+' class="noteDiv" ><div class="todo_descriptions fontStyle15" ><p id="'+data[i].userId+'~'+data[i].listId+'~'+data[i].noteId+' ">'+ notesDetailsJsonObj['noteName']+'</p></div><div class="badges">'+datedesc+noteTags+'</div></div></div></div></div>';
						
						}
						
						$('#crowdUser').empty();
						var recentData='<div class="crowdUser">'+noteData+'</div>';
							$('#tab1').find('#mostSubDiv3').each(function( index ){
									$(this).remove();
								});
						$('#crowdUser').append(recentData);
						}
				
					},
					error : function() {
						console
								.log("<-------Error returned while public notes-------> ");
					}
				});
		
	
}

