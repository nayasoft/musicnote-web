//userId=1002;
var videoDeleted=false;
var fileDeleted=false;
$(document).ready( function() {
	 recordedLessons();
	    otherFiles();
	$(".sideBarRecord").click(function(){
		
		var color = $( this ).css( "background-color" );
		var def='rgb(40, 135, 189)';
		
		if(color!=def){
			//alert('match---->');
		}else{
			//alert('not match--->');
			window.location.reload(true);
		}
	});
	
	$('#recordwithmic').click(function(){
		
		document.getElementById("audio_recording").style.display="block";
	});
	$('#fileuploaddiv').find('.ajax-upload-dragdrop').each(function(index){
		$(this).removeAttr('.ajax-upload-dragdrop');
	});
	
	/*$('#myForm').submit(function ()
	{

       var url = urlForServer+'Upload/file/'+userId;
       $(this).attr('action', url);
      // recordedLessons();
      
	});


var options = { 
	beforeSend: function() 
	{
		$("#progress").show();
		//clear everything
		$("#bar").width('0%');
		$("#message").html("");
		$("#percent").html("0%");
		
	},
	uploadProgress: function(event, position, total, percentComplete) 
	{
		$("#bar").width(percentComplete+'%');
		$("#percent").html(percentComplete+'%');

	},
	success: function() 
	{
		$("#bar").width('100%');
		$("#percent").html('100%');
		 otherFiles();

	},
	complete: function(response) 
	{
		$("#myfile").val("");
		setTimeout(function(){
			$('#progress').hide();	
		},500);
	},
	error: function()
	{

	}

	}; 

	 $("#myForm").ajaxForm(options);*/




var settings = {
/*headers: { 
"Ajax-Call" : userTokens,
"Ajax-Time" :userLoginTime				
},*/
url:urlForServer+'Upload/file/'+userId+'/'+musicnote,
method: "POST",
//allowedTypes:"jpg,png,gif,doc,pdf,zip",
multiple: true,
onSuccess:function(files,data,xhr)
{
	var x = JSON.parse(data);
	if(x.status=='success')
	{
	}
	else
	{
    alert(x.status);
	}
$("#status").html("<font color='green'>Upload is success</font>");


},
afterUploadAll:function(files,data,xhr)
{
	
	recordedLessons();
	 otherFiles();
},
onError: function(files,status,errMsg)
{    
	
$("#status").html("<font color='red'>Upload is Failed</font>");
}
}

$("#mulitplefileuploader").uploadFile(settings);


	// Fixing height of the page.
	var size=$(window).height() - 90;
    $('#recordLessonView').attr('style','height:'+size+'px;overflow: auto;');
	
   
				  $("ul#viewRecordedLesson").on('click','.glyphicon-download-alt',function() {
					if(!videoDeleted)
						
						getFileFor($(this).attr('id'),$(this).attr('id'));
					else
						videoDeleted=false;
					});
	             /*$("ul#viewOtherFiles").on('click','.glyphicon-download-alt',function() {
					if(!fileDeleted)
						getFileFor($(this).attr('id'),$(this).attr('id'));
					else
						fileDeleted=false;
					});*/
	             
				  $(".downloadicon").on('click','#downloadFile',function() {
		            	 var selected = new Array();
		            	 var fileNameExt="";
		            	 var fileName;
	          	       var tempfileName;
		            	if(!fileDeleted){
		            		$("input:checkbox[name=downloadfile]:checked").each(function() {
		            		  var fileName=$(this).attr('id');
		            		 // getFileFor($(this).attr('id'),$(this).attr('id'));
		            	       selected.push(fileName);
		            	       return fileName;
		            	   	});
		            		if(selected.length!=0){
		            			$("#recDownloaderrormsg").attr('style','display:none');
		            			if((selected.length)>1){
		            				getFilesFor(selected);
		            			}else{
		            				getFileFor(selected,selected);
		            			}
		            		}else{
		            			$("#recDownloaderrormsg").attr('style','display:block');
		            		}
		            	}else{
		            		fileDeleted=false;
						}
		             });
				  
	           
				  
				  
				  
				  
	             $('ul#viewRecordedLesson').on('click','.playRecorded',function(){
	         			var lessonName = $(this).attr('id');
	         			
	         			if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
	         			{
	         					$('ul#viewRecordedLesson').children('li').children('embed').each(function( index ){
	         						$('ul#viewRecordedLesson').children('li').children('embed').remove();
	         					});
		         			
		         			
		         			$(this).after('<embed width="260" height="282" src="'+uploadUrl+lessonName+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/>');
		         			
	         			}
	         			else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
	         			{
         					$('ul#viewRecordedLesson').children('li').children('embed').each(function( index ){
         						$('ul#viewRecordedLesson').children('li').children('embed').remove();
         					});
	         				
		         			$(this).after('<embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> ');
		         			
	         			}
	         		
	         	});
	             
	             
	             $('ul#viewOtherFiles').on('click','.playRecorded',function(){
	         			var lessonName = $(this).attr('id');
	         			
	         			if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
	         			{
	         				$('ul#viewOtherFiles').children('li').children('embed').each(function( index ){
         						$('ul#viewOtherFiles').children('li').children('embed').remove();
         					});
	         				
		         			$(this).after('<embed width="260" height="282" src="'+uploadUrl+lessonName+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/>');
	         			}
	         			else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
	         			{
	         				$('ul#viewOtherFiles').children('li').children('embed').each(function( index ){
         						$('ul#viewOtherFiles').children('li').children('embed').remove();
         					});
	         				
		         			$(this).after('<embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> ');
	         			}
	         		
	         	});
	             
			  
				  });

function recordedLessons()
{
	
	var url = urlForServer+"Lesson/getRecordedLessons/"+userId;
	
	$.support.cors = true;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
		"Mn-time" :musicnoteIn					
		},
           type : 'POST',
           url : url,
           success : function(responseText) {
           
           var data=jQuery.parseJSON(responseText);
           
           $("#viewRecordedLesson").empty();
           if(data!=null && data!='')
           {
        	  
           for ( var i = 0; i < data.length; i++) {
			   var obj = data[i];
			   var lessonName = obj['fileName'];
			   var fileDummyName=obj['fileDummyName'];
			   var uploadDate=obj['uploadDate'];
			   var playOption=obj['fullPath'];
			   
			      if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
			      {
			    	  $("#viewRecordedLesson").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);"><i  id="'+lessonName+'" class="glyphicon glyphicon-download-alt ~' +lessonName+'" title="Click to download" style="margin-right:10px;"></i>' + fileDummyName + '<i  class="col-md-offset-2">'+uploadDate+'</i> <span id="'+lessonName+'" class="close"  >&times;</span> </a></li>');
			      }
			      else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
				  {
			    	  $("#viewRecordedLesson").append('<li><a id="'+playOption+'" class="playRecorded"><i  id="'+lessonName+'" class="glyphicon glyphicon-download-alt ~' +lessonName+'" title="Click to download" style="margin-right:10px;"></i>' + fileDummyName + '<i  class="col-md-offset-2">'+uploadDate+'</i> <span id="'+lessonName+'" class="close"  >&times;</span> </a></li>');
				  }
			      else
			      {
				 // $("#viewRecordedLesson").append('<li><a id="'+lessonName+'" href="javascript:void(0);">' + lessonName + ' <span id="'+lessonName+'" class="close"  >&times;</span> </a><video  id="client-video"  loop controls src="http://musicnoteapp.com/UploadMusicFiles/1002/video/Nov-26-201322-43-01_test.mov "  type="video/mov" width=320 height=240></video> </li>');
			    	  $("#viewRecordedLesson").append('<li><a id="'+playOption+'" href="javascript:void(0);"><i id="'+lessonName+'" class="glyphicon glyphicon-download-alt ~' +lessonName+'" title="Click to download" style="margin-right:10px;"></i>' + fileDummyName + '<i  class="col-md-offset-2">'+uploadDate+'</i> <span id="'+lessonName+'" class="close"  >&times;</span> </a></li>');
			      }
           }
           }
           },
           error : function() {
           console.log("<-------error returned retrieving recorded lessons-------> ");
           }
           });
}

/*function getFileFor(fileName,tempfileName){
	  
		var fileExt=fileName.split('.');
		var	ext = fileExt[fileExt.length-1];
		var href=urlForServer+"Lesson/download/"+userId+'/'+fileName+'/'+ext+"/"+tempfileName;
	    $.fileDownload(href, {
		   httpMethod: "POST"
	   });  	
}*/
function getFileFor(fileName,tempfileName){
	var filename;
	filename=fileName.toString().split(".");
	var	ext = filename[1];
	var href=urlForServer+"Lesson/download/"+userId+'/'+fileName+'/'+ext+'/'+tempfileName+'/'+musicnote;
    $.fileDownload(href, {
	   httpMethod: "POST"
   });  	
}
function getFilesFor(fileNames){
var href=urlForServer+"Lesson/downloadFile/"+userId+'/'+fileNames+'/'+musicnote;
$.fileDownload(href, {
   httpMethod: "POST"
});  	
}
function otherFiles()
{
	 $("#recDownloaderrormsg").attr('style','display:none;');
	var url = urlForServer+"Lesson/getOtherFiles/"+userId;
	$.support.cors = true;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
		"Mn-time" :musicnoteIn				
		},
           type : 'POST',
           url : url,
           success : function(responseText) {
           
           var data=jQuery.parseJSON(responseText);
           $("#viewOtherFiles").empty();
           if(data!=null && data!='')
           {
        	   $("#downloadButton").attr('style','display:block;margin-left:25px');
           for ( var i = 0; i < data.length; i++) {
			   var obj = data[i];
			   var fileName = obj['fileName'];
			  
			   var originalFileName=obj['fileDummyName'];

			   var uploadDate=obj['uploadDate'];
			   var playOption=obj['fullPath'];
			   if(fileName.contains(".mov") ||  fileName.contains(".mp4") || fileName.contains(".avi") || fileName.contains(".wmv") || fileName.contains(".wma") )
			   {
				   $("#viewOtherFiles").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);" ><input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top: -1px;"  name="downloadfile"></input> ' + originalFileName + '<i  class="col-md-offset-2">'+uploadDate+'</i><span id="'+fileName+'" class="close">&times;</span> </a>  </li>');
				   //$("#viewOtherFiles").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);" ><i id="'+fileName+'" class="glyphicon glyphicon-download-alt ~' +fileName+'" title="Click to download"></i>' + originalFileName + '<i  class="offset2">'+uploadDate+'</i><span id="'+fileName+'" class="close">&times;</span> </a>  </li>');
			   }
			   
			   else if(fileName.contains(".mp3") || fileName.contains(".wav") || fileName.contains(".amr"))
			   {
				   $("#viewOtherFiles").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);"><input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top: -1px;" name="downloadfile"></input> ' + originalFileName + '<i  class="col-md-offset-2">'+uploadDate+'</i> <span id="'+fileName+'" class="close">&times;</span> </a></li>');
				  // $("#viewOtherFiles").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);"><i id="'+fileName+'" class="glyphicon glyphicon-download-alt ~' +fileName+'" title="Click to download"></i>' + originalFileName + '<i  class="offset2">'+uploadDate+'</i> <span id="'+fileName+'" class="close">&times;</span> </a></li>');

			   }
			   		   
			   else
			   {
				   $("#viewOtherFiles").append('<li><a id="'+playOption+'" href="javascript:void(0);"><input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top: -1px;" name="downloadfile"></input> ' + originalFileName + '<i  class="col-md-offset-2">'+uploadDate+'</i> <span id="'+fileName+'" class="close">&times;</span> </a> </li>');
				  // $("#viewOtherFiles").append('<li><a id="'+playOption+'" href="javascript:void(0);"><i id="'+fileName+'" class="glyphicon glyphicon-download-alt ~' +fileName+'" title="Click to download"></i>' + originalFileName + '<i  class="offset2">'+uploadDate+'</i> <span id="'+fileName+'" class="close">&times;</span> </a> </li>');

			   }
				  //$("#viewRecordedLesson").append('<li><a id="'+lessonName+'" href="javascript:void(0);">' + lessonName + ' <span id="'+lessonName+'" class="close"  >&times;</span> </a><video  id="client-video"  loop controls src="http://musicnoteapp.com/UploadMusicFiles/1002/video/Nov-26-201322-43-01_test.mov "  type="video/mov" width=320 height=240></video> </li>');

           }
           }
           
           },
           error : function() {
           console.log("<-------error returned retrieving files-------> ");
           }
         });
}

var fileType='';
$("ul#viewRecordedLesson").on('click','li span',function() {
	
	fileType="Recordings";
	deleteFiles(this.id,fileType);
	videoDeleted=true;
	});
	
$("ul#viewOtherFiles").on('click','li span',function() {
	fileType="UploadFiles";
	deleteFiles(this.id,fileType);
	fileDeleted=true;
	});
	
	function deleteFiles(fileName,fileType)
	{
		var url='';
		if(fileType=='UploadFiles')
		url = urlForServer+"Lesson/deleteVideo/"+fileName+"/"+userId;
		else
		url = urlForServer+"Lesson/deleteVideoFiles/"+fileName+"/"+userId;
		

		
		$.support.cors = true;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
			"Mn-time" :musicnoteIn					
			},
				type : 'POST',
				url : url,
				success : function(responseText) {
			    var data=jQuery.parseJSON(responseText);
			         if(data[0].status=='success')
			         {
			        	 
			        	 $('#uploadmsgModal').modal('show');
			     		//$("#loadingImg").attr('src',loadingImgUrl);
			     		$("#uploadmsg-header-span").attr('class',"label label-info").text("info");
			     		$("#uploadmodal-message").attr('class',"label label-info").text("  Creating user");
			     		$("#schedulestatusicon").remove();
			     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon"></i>');
			     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
			     		$("#uploadmsg-header-span").attr('class',"label label-info").text("info");
			     		$("#uploadmodal-message").attr('class',"label label-info").text("  File Deleted successfully");
			     		$("#schedulestatusicon").remove();
			     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon"></i>');
			     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
			     		setTimeout(function(){
			     			$('#uploadmsgModal').modal('hide');	
			     		},5000);
					recordedLessons();
					otherFiles();
			         }
			         
			         else
			         {
			        	 $('#uploadmsgModal').modal('show');
				     		//$("#loadingImg").attr('src',loadingImgUrl);
				     		$("#uploadmsg-header-span").attr('class',"label label-info").text("info");
				     		$("#uploadmodal-message").attr('class',"label label-info").text("  Creating user");
				     		$("#schedulestatusicon").remove();
				     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon"></i>');
				     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
				     		$("#uploadmsg-header-span").attr('class',"label label-warning").text("info");
				     		$("#uploadmodal-message").attr('class',"label label-warning").text("  File Attached some notes");
				     		$("#schedulestatusicon").remove();
				     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon"></i>');
				     		$("#uploadmsgfooterbtn").attr('class','btn btn-warning');
				     		setTimeout(function(){
				     			$('#uploadmsgModal').modal('hide');	
				     		},5000); 
			         }
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
	}
	function hideAudio()
	{
		document.getElementById("audio_recording").style.display="none";
	}
	function viewRecord()
	{
		 recordedLessons();
	}
	
