window.clearInterval(autoNoteDetailsFetchTimer);
window.clearInterval(autoContactDetailsFetchTimer);
window.clearInterval(autoCrowdDetailsFetchTimer);

var userName="";
var userId;
var userFirstName='';
var userEmail='';
$(document).ready( function() {
	$("#userfeedback").click(function(){
		$("#limitChar1").attr("style", "dispaly:none");
		$("#limitChar1").hide();
		$("#limitChar").show();
	  });
	$('#helpCancel').click(function(){
		app.navigate("home", {trigger: true});
		$("#main").hide();
		$("#sidecontent").show();
		$('#footer').show();
		$('#header').show();
	});
	
	$('#helpSubmit').click(function(){
		if($("#userfeedback").val().trim()== '')
		{
			$("#limitChar").hide();
			$("#limitChar1").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
			$("#limitChar1").text("No feedback entered");
		}	
		else{	
			feedback();		
		}
	});

	// commented by ramaraj for the purpose of beta to don't show payment info
	/*$(function(){
			var url = urlForServer + "group/userInformation";
			var datastr = '{"userId":"' + userId + '"}';
			console.log("<-------Sdatastr -------> " + datastr);
			
			$('#helpGrid').empty();
			$('#helpGrid').append('<table id="helpTable" style="table-layout: fixed;" ></table><div id="helpPage"></div>');
			var params = encodeURIComponent(datastr);
		   
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
	 	    	"Mn-time" :musicnoteIn				
		    	},
						type : 'POST',
						url : url,
						data : params,
						success : function(responseText) 
						{
					var data1 = "";
					var dataLength="";
					var lastsel;
					data1 = jQuery.parseJSON(responseText);
					if(data1!=null && data1!="")
					{
					dataLength= data1.length ;
					var height;
					$('#helpTable').jqGrid({
						data:data1,
	   	        		datatype: "local",
	   	        		sortable: true,       		
	   	        		height: 23,
		                width:462,
		               
	   	        		 colNames:['Subscribed-Date','Subscription-Type','Expired-Date'],
	   	        	   	 colModel:[
	   	        	   	    {name:'startDate', width:154},
	   	        	   		{name:'offer',width:154},
	   	        	   		{name:'expiryDate',width:154},
	   	        	   ],
	   	        	   
	   	        	  // pager: '#helpPage',
	   	        	   //sortname: 'taskId', 
	   	        	   viewrecords: true, 
	   	        	  // multiselect: true,
	   	        	   //multiboxonly: true,
	   	        	   shrinkToFit : true,
	   	        	   sortorder: "desc",
	          	       caption: "Purchasing Details"
	          	           
	   	        	 });
					$("#helpTable").jqGrid('navGrid',"#helpPage",{edit:false,add:false,del:false});
					}
			},
			error:function(){
				console.log("<-------Error returned while welcome user mail-------> ");
			}
			});
		});
	*/
	  
	
	
});
function viewHelpVideoUploaded()
{
	var extensionType="";
	var files="";
	$(".myAccordion").empty();
	$('#videoGrid').empty();
	var url=urlForServer+"admin/getUploadedFilesForHelp";
	$.ajax(
		 	{
		 		headers:
		 		{ 
		 			"Mn-Callers" : musicnote,
		 	    	"Mn-time" :musicnoteIn				
		 		},
		 		url:url,
		 		type:'post',
		 		success:function(responseText)
		 		{
		 			if(responseText=='failed')
		 			{
		    			$("#noVideoFound").attr('style','display:block;color:red;margin-left:170px;margin-top:60px;');	
		 			}
		 			else
		 			{
		 			var data1 = jQuery.parseJSON(responseText);
		 			if(data1!=null && data1!="")
					{
						for(var i=0;i<data1.length;i++){
							var fileExtension=data1[i].fileName.split('.');
							var extension=fileExtension[1];
							
							if(extension=="mov" || extension=="mp4" || extension=="avi" || extension=="wmv"  || extension=="wma"){
								/*files='<video id="adminUploadedVideo" width="530" height="240" controls style="margin-left:40px;">'+
										'<source src='+uploadUrl+data1[i].filePath+' type="video/mp4">'+
										'</video>';*/
								files='<embed width="530" height="240" style="margin-left:40px;"src="'+uploadUrl+data1[i].filePath+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/>';
							}
							else if(extension=="mp3" || extension=="wav" || extension=="amr"){
								files='<embed  height="50" width="180" name="plugin" src="'+uploadUrl+data1[i].filePath+'" type="audio/x-wav" controls AUTOSTART="false"/>';
							}else if(extension=="jpg" || extension=="png" || extension=="jpeg"){
								files='<img width="530" height="240" alt="'+uploadUrl+data1[i].filePath+'" style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid" src="'+uploadUrl+data1[i].filePath+'">';
							}
							else if(extension=="pdf"){
								if(is_chrome){
									files='<iframe src="'+uploadUrl+data1[i].filePath+'" style="width: 100%; height: 400px;" frameborder="0" scrolling="no">'
								    +'<p>It appears your web browser doesn\'t support iframes.</p>'
								    +' <p>It appears because you don\'t have pdf viewer or PDF support in this web browser.Please add (pdf.js) pdf Viewer to your browser </p> </iframe>';	
								}else{
									files='<object data="'+uploadUrl+data1[i].filePath+'" type="application/pdf" width="100%" height="600px">'
								   /*+'<embed src="'+uploadUrl+data1[i].filePath+'" type="application/pdf" width="100%" height="600pc" /><p>To view PDFs please ensure your browser has a PDF viewer (PDF.js) installed.</p>'*/
									+'</object>';
								}
							}
							$(".myAccordion").append('<div class="panel panel-default">'+
							'<div class="panel-heading" data-target="#collapseRecentVideos'+i+'" data-toggle="collapse" data-parent="#accordion">'+
								'<a class="panel-title" id="fileTitle'+i+'" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseRecentVideos'+i+'"><h4>'+
								data1[i].fileTitle+
								'</h4></a>'+
								'</div>'+
							'<div id="collapseRecentVideos'+i+'" class="panel-collapse collapse fontStyle">'+
								'<div class="panel-body">'+
									'<div class="row">'+
										'<p id="fileDescription">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+data1[i].description+'</p> <br>'+
									'</div>'+files+
									 
									
									/*'<video id="adminUploadedVideo" width="530" height="240" controls style="margin-left:40px;">'+
										'<source src='+uploadUrl+data1[i].filePath+' type="video/mp4">'+
										'</video>'+*/
										/*'<embed width="530" height="240" style="margin-left:40px;"src="'+uploadUrl+data1[i].filePath+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/>'+*/
										
										
										
										'</div></div></div>');
							}
						}
		 			}
		 		}
		 	});
}
function countChar(val) {
    var len = $("#userfeedback").val().length;
    if (len > 600) {
      val.value = val.value.substring(0, 600);
    } else {
      $('#limitChar').text((600 - len)+" characters remaining").attr("style", "font-family: Helvetica Neue;font-size:14px;");
    }
  };
function feedback(){
	$('#helpModal1').modal('show');
	$("#modal-message1").text("Sending feedback. We will look into your request as soon as possible.");
	if($('#userfeedback').val()!= null && $('#userfeedback').val().trim()!= ""){
 	var userfeedback=$('#userfeedback').val().trim();
 	userfeedback = userfeedback.replace( /[\s\n\r]+/g, ' ' );
	var userName=$("#username").val().toLowerCase();
	var userFirstName=userDetailsJsonObj['userFirstName'];
	var userLastName=userDetailsJsonObj['userLastName'];
	var userId=userDetailsJsonObj['userId'];
	var feedbackType="feedback";
	var date="";
	var status="A";
	userMailId=userName;
	while(userfeedback.indexOf("\"") != -1){
		userfeedback = userfeedback.replace("\"", "`*`");
	}
	var paramet='{"userId":"'+userId+'","userFirstName":"'+userFirstName+'","userLastName":"'+userLastName+'","userMailId":"'+userMailId+'","userfeedback":"'+escape(userfeedback)+'","date":"'+date+'","feedbackType":"'+feedbackType+'","status":"'+status+'"}';
	var url = urlForServer+"user/userFeedback";
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
    	},
		type:"POST",
		url:url,
		data:paramet,
		success:function(response){
			if(response.match("success")) {
				$('#helpModal1').modal('show');
				$("#modal-message1").text("Your feedback was submitted successfully");
				setTimeout(function(){ $('#helpModal1').modal('hide');},3000);
				$('#userfeedback').val('').empty();
				$("#limitChar").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
				$("#limitChar").text("600 characters remaining");
			}else{ 
				$('#helpModal1').modal('hide');
				$('#userfeedback').val('').empty();
				alert("invalid entry");
			 }
		},
		error:function(e){
			$('#helpModal1').modal('hide');
			alert("error occured"+e);
		}
});
	}else{
		alert("empty value");
	}


};
