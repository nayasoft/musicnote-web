var userDetailsJsonObj;
var userName="";
var userId;
var userFulName="";
var userFullNameMail='';
var userFirstName='';
var userLastName='';
var userInitial='';
var userEmail='';
var timeZoneList='';
var timeZoneSelect='';
window.clearInterval(autoNoteDetailsFetchTimer);
window.clearInterval(autoContactDetailsFetchTimer);
window.clearInterval(autoCrowdDetailsFetchTimer);

function fetchUsers()
{
	    var url = urlForServer+"user/getUserDetails";
		var datastr = '{"userId":"'+userId+'"}';
	    console.log("<-------Sdatastr -------> "+ datastr);
		var params = encodeURIComponent(datastr);
		var role="";
		var userRoles=new Array();
		userRoles[0]="Teacher";
		userRoles[1]="Student";
		userRoles[2]="Administrator";
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
		type : 'POST',
		url : url,
		data : params,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
		  if(data!=null && data!='')
			{
				for ( var i = 0; i < data.length; i++) {
					
					 userDetailsJsonObj = data[i];
					 if(data[i].securityCheck=='true'){
						 $('#securityQuestion').attr('style','display:block');
						 }else{
						 $('#securityQuestion').attr('style','display:none');
						 }

					 userName=userDetailsJsonObj['userName'];
					 displayUserName=userDetailsJsonObj['displayUserName'];
					 
					 if(userDetailsJsonObj['userFirstName']!='null'){
						 $("#userView").text(userDetailsJsonObj['userFirstName']+"  "+userDetailsJsonObj['userLastName']);
						// $("#userView").text(userName);
					 }else if(userDetailsJsonObj['displayUserName']!='null'){
						 $("#userView").text(displayUserName);
					 }else{
						 $("#userView").text(userName);
						//$("#userView").text(userDetailsJsonObj['userFirstName']+"  "+userDetailsJsonObj['userLastName']);
					 }
					 userFulName = userDetailsJsonObj['userFirstName']+" "+userDetailsJsonObj['userLastName'];
					 role=userDetailsJsonObj['userRole'];
					 userId=userDetailsJsonObj['userId'];
					 var noteAccess=userDetailsJsonObj['noteCreateBasedOn'];
					 allNoteCreateBasedOn=noteAccess;
					 userFirstName=userDetailsJsonObj['userFirstName'];
					 userLastName=userDetailsJsonObj['userLastName'];
					 userFullNameMail=userDetailsJsonObj['userFirstName']+"  "+userDetailsJsonObj['userLastName'];
					 userInitial=userFirstName.substring(0, 1).toUpperCase()+userLastName.substring(0, 1).toUpperCase();
					 userEmail=userDetailsJsonObj['emailId'];
					
					 
				}
	            	 
	            	 
	            	
					
						
			}
		  //$('#msgLoadingModal').modal('hide');
		
	},
	error : function() {
		console.log("<-------error returned for User details -------> ");
		}
	});   

}


function userProfile()
{
	if(stuNoti.match('true') && stuFirst.match('true')) // this is used to check notification on any demo video updated by admin
	{
	 $("#alertMessagesPro").modal('show');
	 stuFirst="false";
	}
	$("#ValidatepemailidDiv").attr("style", "display:none;");
    var url = urlForServer+"user/getUserDetails";
	var datastr = '{"userId":"'+userId+'"}';
    console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	var role="";
	var userRoles=new Array();
	userRoles[0]="Teacher";
	userRoles[1]="Student";
	userRoles[2]="Administrator";
	
	
	var instruments="";
	var instrument=new Array();
	
	instrument[0]="Bass";
	instrument[1]="Brass";
	instrument[2]="Cello";
	instrument[3]="Drums";
	instrument[4]="Guitar";
	instrument[5]="Harmonica";
	instrument[6]="Harp";
	instrument[7]="Oboe";
	instrument[8]="Percussion";
	instrument[9]="Piano/Keys";
	instrument[10]="Saxophone";
	instrument[11]="Trombone";
	instrument[12]="Trumpet";
	instrument[13]="Violin";
	instrument[14]="Vocals";
	instrument[15]="Woodwinds";
	instrument[16]="Other";
	
	
	
	
	
	var skillLevel="";
	var skillLevels=new Array();
	
	skillLevels[0]="Beginner";
	skillLevels[1]="Intermediate";
	skillLevels[2]="Advanced";
	
	var favoriteMusic="";
	var timeZone="";
	var favoriteGenre=new Array();
	 favoriteGenre[0]="Alternative";
	 favoriteGenre[1]="Asian Pop (J-Pop, K-pop)";
	 favoriteGenre[2]="Bollywood";
	 favoriteGenre[3]="Blues";
	 favoriteGenre[4]="Carnatic";
	 favoriteGenre[5]="Classical";
	 favoriteGenre[6]="Country";
	 favoriteGenre[7]="Dance";
	 favoriteGenre[8]="Easy Listening";
	 favoriteGenre[9]="Electronic";
	 favoriteGenre[10]="European Music (Folk / Pop)";
	 favoriteGenre[11]="Folk";
	 favoriteGenre[12]="Hip Hop / Rap";
	 favoriteGenre[13]="Indian Classical";
	 favoriteGenre[14]="Indie Pop";
	 favoriteGenre[15]="Inspirational (incl. Gospel)";
	 favoriteGenre[16]="Jazz";
	 favoriteGenre[17]="Latin Music";
	 favoriteGenre[18]="New Age";
	 favoriteGenre[19]="Opera";
	 favoriteGenre[20]="Pop";
	 favoriteGenre[21]="R&B / Soul";
	 favoriteGenre[22]="Reggae";
	 favoriteGenre[23]="Rock";
	 favoriteGenre[24]="Singer / Songwriter";
	 favoriteGenre[25]="World Music";
	 favoriteGenre[26]="Other";
	 
	 
	 timeZoneList=new Array();
	 timeZoneList[0]="(GMT-12:00) International Date Line West";
	 timeZoneList[1]="(GMT-11:00) Midway Island, Samoa ";
	 timeZoneList[2]="(GMT-10:00) Hawaii ";
	 timeZoneList[3]="(GMT-09:00) Alaska";
	 timeZoneList[4]="(GMT-08:00) Pacific Time (US and Canada); Tijuana ";
	 timeZoneList[5]="(GMT-07:00) Mountain Time (US and Canada) ";
	// timeZoneList[6]="(GMT-07:00) Chihuahua, La Paz, Mazatlan ";
	// timeZoneList[7]="(GMT-07:00)U.S. Mountain Standard Time  Arizona ";
	 timeZoneList[6]="(GMT-06:00) Central Time (US and Canada),Saskatchewan ";
	// timeZoneList[9]="(GMT-06:00) Guadalajara, Mexico City, Monterrey,Central America ";
	 timeZoneList[7]="(GMT-05:00) Eastern Time (US and Canada),Indiana (East) ";
	// timeZoneList[11]="(GMT-05:00) Bogota, Lima, Quito ";
	 timeZoneList[8]="(GMT-04:00) Atlantic Time (Canada),Caracas, La Paz, Santiago  ";
	 timeZoneList[9]="(GMT-03:30) Newfoundland and Labrador";
	 timeZoneList[10]="(GMT-03:00) Brasilia,Buenos Aires, Georgetown,Greenland";
	 timeZoneList[11]="(GMT-02:00) Mid-Atlantic ";
	 timeZoneList[12]="(GMT-01:00) Azores,Cape Verde Islands ";
	 timeZoneList[13]="(GMT-00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London ";
	// timeZoneList[18]="(GMT-00:00) Casablanca, Monrovia ";
	 timeZoneList[14]="(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague ";
	 //timeZoneList[20]="(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb ";
	// timeZoneList[21]="(GMT+01:00) Brussels, Copenhagen, Madrid, Paris ";
	// timeZoneList[22]="(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna ";
	// timeZoneList[23]="(GMT+01:00) West Central Africa ";
	// timeZoneList[15]="(GMT+02:00) Bucharest ";
	// timeZoneList[25]="(GMT+02:00) Cairo ";
	 timeZoneList[15]="(GMT+02:00) Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius ";
	// timeZoneList[27]="(GMT+02:00) Athens, Istanbul, Minsk ";
	// timeZoneList[28]="(GMT+02:00) Jerusalem";
	// timeZoneList[29]="(GMT+02:00) Harare, Pretoria ";
	 timeZoneList[16]="(GMT+03:00) Moscow, St. Petersburg, Volgograd ";
	// timeZoneList[31]="(GMT+03:00) Kuwait, Riyadh ";
	// timeZoneList[32]="(GMT+03:00) Nairobi ";
	// timeZoneList[33]="(GMT+03:00) Baghdad ";
	// timeZoneList[34]="(GMT+03:30) Tehran ";
	 timeZoneList[17]="(GMT+04:00) Abu Dhabi, Muscat ";
	// timeZoneList[36]="(GMT+04:00) Baku, Tbilisi, Yerevan ";
	 timeZoneList[18]="(GMT+04:30) Kabul ";
	 timeZoneList[19]="(GMT+05:00) Ekaterinburg,Islamabad, Karachi, Tashkent  ";
	 timeZoneList[20]="(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi";
	 timeZoneList[21]="(GMT+05:45) Kathmandu ";
	 timeZoneList[22]="(GMT+06:00) Astana, Dhaka ,Sri Jayawardenepura ,Almaty, Novosibirsk";
	 timeZoneList[23]="(GMT+06:30) Yangon (Rangoon)";
	 timeZoneList[24]="(GMT+07:00) Bangkok, Hanoi, Jakarta, Krasnoyarsk ";
	 timeZoneList[25]="(GMT+08:00) Beijing, Chongqing, Hong Kong SAR, Urumqi ";
	 timeZoneList[26]="(GMT+09:00) Seoul ,Osaka, Sapporo, Tokyo ";
	 timeZoneList[27]="(GMT+09:30) Darwin, Adelaide ";
	 timeZoneList[28]="(GMT+10:00) Canberra, Melbourne, Sydney";
	 //timeZoneList[48]="(GMT+10:00) Brisbane ,Hobart,Vladivostok,Guam, Port Moresby ";
	 timeZoneList[29]="(GMT+11:00) Magadan, Solomon Islands, New Caledonia ";
	 timeZoneList[30]="(GMT+12:00) Fiji Islands, Kamchatka, Marshall Islands ";
	 timeZoneList[31]="(GMT+13:00) Nuku'alofa";
	 
	 
	 
	 timeZoneList1=new Array();
	 timeZoneList1[0]="-1200";
	 timeZoneList1[1]="-1100";
	 timeZoneList1[2]="-1000";
	 timeZoneList1[3]="-9000";
	 timeZoneList1[4]="-0800";
	 timeZoneList1[5]="-0700";
	// timeZoneList1[6]="(GMT-07:00) Chihuahua, La Paz, Mazatlan ";
	// timeZoneList1[7]="(GMT-07:00)U.S. Mountain Standard Time  Arizona ";
	 timeZoneList1[6]="-0600";
	// timeZoneList1[9]="(GMT-06:00) Guadalajara, Mexico City, Monterrey,Central America ";
	 timeZoneList1[7]="-0500";
	// timeZoneList1[11]="(GMT-05:00) Bogota, Lima, Quito ";
	 timeZoneList1[8]="-0400";
	 timeZoneList1[9]="-0330";
	 timeZoneList1[10]="-0300";
	 timeZoneList1[11]="-0200";
	 timeZoneList1[12]="-0100";
	 timeZoneList1[13]="-0000";
	// timeZoneList1[18]="(GMT-00:00) Casablanca, Monrovia ";
	 timeZoneList1[14]="+0100";
	 //timeZoneList1[20]="(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb ";
	// timeZoneList1[21]="(GMT+01:00) Brussels, Copenhagen, Madrid, Paris ";
	// timeZoneList1[22]="(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna ";
	// timeZoneList1[23]="(GMT+01:00) West Central Africa ";
	// timeZoneList1[15]="(GMT+02:00) Bucharest ";
	// timeZoneList1[25]="(GMT+02:00) Cairo ";
	 timeZoneList1[15]="+0200";
	// timeZoneList1[27]="(GMT+02:00) Athens, Istanbul, Minsk ";
	// timeZoneList1[28]="(GMT+02:00) Jerusalem";
	// timeZoneList1[29]="(GMT+02:00) Harare, Pretoria ";
	 timeZoneList1[16]="+0300";
	// timeZoneList1[31]="(GMT+03:00) Kuwait, Riyadh ";
	// timeZoneList1[32]="(GMT+03:00) Nairobi ";
	// timeZoneList1[33]="(GMT+03:00) Baghdad ";
	// timeZoneList1[34]="(GMT+03:30) Tehran ";
	 timeZoneList1[17]="+0400";
	// timeZoneList1[36]="(GMT+04:00) Baku, Tbilisi, Yerevan ";
	 timeZoneList1[18]="+0430";
	 timeZoneList1[19]="+0500";
	 timeZoneList1[20]="+0530";
	 timeZoneList1[21]="+0545";
	 timeZoneList1[22]="+0600";
	 timeZoneList1[23]="+0630";
	 timeZoneList1[24]="+0700";
	 timeZoneList1[25]="+0800";
	 timeZoneList1[26]="+0900";
	 timeZoneList1[27]="+0930";
	 timeZoneList1[28]="+1000";
	 //timeZoneList1[48]="(GMT+10:00) Brisbane ,Hobart,Vladivostok,Guam, Port Moresby ";
	 timeZoneList1[29]="+1100";
	 timeZoneList1[30]="+1200";
	 timeZoneList1[31]="+1300";
	
	 
	 $.ajax({
		 headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
			for ( var i = 0; i < data.length; i++) {
				 userDetailsJsonObj = data[i];
				 if(data[i].secutityCheck=='true'){
					 $('#securityQuestion').attr('style','display:block');
				 }else{
					 $('#securityQuestion').attr('style','display:none');
				 }
				 console.log("datas :"+userDetailsJsonObj);
				 var userName=userDetailsJsonObj['userName'];
				 role=userDetailsJsonObj['userRole'];
				 if(userDetailsJsonObj['userFirstName']!='null'){
				 $("#userView").text(userDetailsJsonObj['userFirstName']+"  "+userDetailsJsonObj['userLastName']);
				 }else{
					 $("#userView").text(userName);	 
				 }	 
				if(userDetailsJsonObj['userFirstName']!='null'){
				 $("#pfirstname").val(userDetailsJsonObj['userFirstName']);
				}else{
					$("#pfirstname").val('');	
				}
				if(userDetailsJsonObj['userLastName']!='null'){
				 $("#plastname").val(userDetailsJsonObj['userLastName']);
				}else{
					$("#plastname").val('');	
				}
				
				 if(userDetailsJsonObj['emailId']!='null'){
					
					 if(userDetailsJsonObj['userRole']=='Music Student'){
						 
						// $("#pemailid").val('');
						 $("#pemailid").val(userDetailsJsonObj['emailId']);
						 var cls=$("#pemailid").attr('class');
						 $("#pemailid").attr('class',"");
						 $("#pemailid").attr('class',cls+' ~studentMail');
						 $("#pemailid").removeAttr('required');
						 $('#pemailidfont').attr('style','display:none');
						  $('#pemailidDiv').attr('style','display:none');
						 $('#emailfont').css('margin-left','10px');
					 }else{
						 $("#pemailid").val(userDetailsJsonObj['emailId']);
					 }
				 }else{
					 $("#pemailid").val('');
					 var cls=$("#pemailid").attr('class');
					 $("#pemailid").attr('class',"");
					 $("#pemailid").attr('class',cls+' ~studentMail');
					 $("#pemailid").removeAttr('required');
					 $('#pemailidfont').attr('style','display:none');
					 $('#pemailidDiv').attr('style','display:none');
					 $('#emailfont').css('margin-left','10px');
				 }
				 $("#fbusernames").val(userDetailsJsonObj['userName']);
				 $("#tempemailid").val(userDetailsJsonObj['emailId']);
				 if(userDetailsJsonObj['url']!='null')
				 {
					 $("#urlId").val(userDetailsJsonObj['url']);
				 }
				 else
				 {
					 $("#urlId").val("");
				 }
				 if(userDetailsJsonObj['description']!='null')
				 {
					 $("#descriptionId").val(userDetailsJsonObj['description']);
				 }
				 else
				 {
					 $("#descriptionId").val("");
				 }
				 //$("#dateofIncorparation").val(userDetailsJsonObj['dateofIncorparation']);
				 
				 timeZone=userDetailsJsonObj['timeZone'];
				 
				 if(timeZone=="null" || timeZone=='')
				 {
					var date = new Date();
					var d = date.toTimeString(); 
					d = d.split("GMT");
					 d = d[1].split(" ");
					 timeZone=d[0];
				 }

//				 for(var k=0;k<timeZoneList.length;k++){
//					if(timeZoneList[k].indexOf(timeZone)!=-1){
//						//$('#ptimeZone').val(timeZoneList[k]);
//						alert("timeZoneList[k]"+timeZoneList[k]);
//					}
//				 }
				 
				  var crowdNotification=userDetailsJsonObj['publicShareWarnMsgFlag'];
				  if(crowdNotification=='true')
					 crowdNotification='True';
				   else
					 crowdNotification='False';
				   $("[name=crowdnotificationFlag]").filter("[value="+crowdNotification+"]").attr("checked","checked");
				 
				   var noteAccess=userDetailsJsonObj['noteCreateBasedOn'];
				   $("[name=notesAccessFlag]").filter("[value="+noteAccess+"]").attr("checked","checked");
				   allNoteCreateBasedOn=noteAccess;
				 
            	    userId=userDetailsJsonObj['userId'];
            	   
    				role=userDetailsJsonObj['userRole'];
    				instruments=userDetailsJsonObj['instrument'];
    				skillLevel=userDetailsJsonObj['skillLevel'];
    				favoriteMusic=userDetailsJsonObj['favoriteMusic'];
    				
    				 if(userDetailsJsonObj['filePath']!='null' && userDetailsJsonObj['filePath']!='')
					 {
    					 $(".deleteIcon").attr("style", "");
						 $("#profileImg").attr("src", uploadUrl+userDetailsJsonObj['filePath']);
					 }
					 else
					 {
						 $("#profileImg").attr("src", "pics/dummy.jpg");
						 $(".deleteIcon").attr("style", "visibility:hidden");
					 }
    				
			}
			
			
			
			  $("#pSkillLevel").empty();
			  $("#pInstrument").empty();
			  $("#pfavoriteGenre").empty();
			  $("#ptimeZone").empty();
			  
			 
			  
			for(var k=0;k<skillLevels.length;k++){
				  if(skillLevels[k]!=skillLevel)
				     $("#pSkillLevel").append('<option class="fontStyle"  value="'+skillLevels[k]+'">'+skillLevels[k]+'</option>');
				 else
				     $("#pSkillLevel").append('<option  class="fontStyle" selected  value="'+skillLevels[k]+'">'+skillLevels[k]+'</option>');
				}
		
			for(var k=0;k<instrument.length;k++){
				
				  if(instrument[k]!=instruments)
				     $("#pInstrument").append('<option class="fontStyle"  value="'+instrument[k]+'">'+instrument[k]+'</option>');
				 else
				     $("#pInstrument").append('<option selected class="fontStyle"  value="'+instrument[k]+'">'+instrument[k]+'</option>');
				}
			
			for(var k=0;k<favoriteGenre.length;k++){
				
				  if(favoriteGenre[k]!=favoriteMusic)
				     $("#pfavoriteGenre").append('<option  value="'+favoriteGenre[k]+'" class="fontStyle">'+favoriteGenre[k]+'</option>');
				 else
				     $("#pfavoriteGenre").append('<option selected  value="'+favoriteGenre[k]+'" class="fontStyle">'+favoriteGenre[k]+'</option>');
				}
			
			
			
			for(var k=0;k<userRoles.length;k++){
				  if(userRoles[k]!=role)
				     $("#proles").append('<option class="fontStyle"  value="'+userRoles[k]+'">'+userRoles[k]+'</option>');
				 else
				     $("#proles").append('<option class="fontStyle" selected  value="'+userRoles[k]+'">'+userRoles[k]+'</option>');
				}
			
			 var url = urlForServer+"user/getSecurityQuestion";
			
			 $.ajax({
				 headers: { 
				 "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type : 'POST',
					url : url,
					//data : params,
					success : function(responseText) {	
					var split=responseText.split('~');
					
					if(responseText!='' && responseText!=null){
						var split=responseText.split('~');
						$('#securityQuestion').children('#securityBox').empty();
						$('#securityQuestion').children('#securitySelect').empty();
						$('#securityQuestion').children('#securitySelect').append('<option value="0">Please select a security Question</option>');
						for(var i=0;i<split.length;i++){
						var quesId=split[i].split('-');	
						$('#securityQuestion').children('#securitySelect').append('<option value="'+quesId[1]+'">'+quesId[0]+'</option>');		
						}
					}
						},
					error:function(responsetext){
					//if(userSession=="Active")
					alert('Please try again later');
				}
				});
			 //selectText(timeZone);
			
		/*
			var res = timeZone.substring(0,3);
			var res1=timeZone.substring(3);
			var final=res+":"+res1;
			alert("res1"+final);
			//alert("res"+res);
		  for (var j=0; j<timeZoneList.length; j++) {
		        if (timeZoneList[j].match(final)){
		        	timeZoneSelect=timeZoneList[j];
		        	//$("#ptimeZone").empty();
		        	//$("#ptimeZone").append('<option selected  value="test" class="fontStyle">test</option>');
		        	alert("searchStringInArray"+timeZoneSelect);
		        }else{
		        	//alert("not match");
		        }
		   } 
			 alert("timeZoneSelectssss"+timeZoneSelect);*/
			
			var timezone1=timeZoneList1.indexOf(timeZone);
			 var timezone2=timeZoneList[timezone1];
			
			 for(var k=0;k<timeZoneList.length;k++){
				 
				  if(timeZoneList[k]!=timezone2){
				     $("#ptimeZone").append('<option  value="'+timeZoneList[k]+'" class="fontStyle">'+timeZoneList[k]+'</option>');
				  }else{
				     $("#ptimeZone").append('<option selected  value="'+timeZoneList[k]+'" class="fontStyle">'+timeZoneList[k]+'</option>');
				  }
				  
				}
			  
			$('#personalinfo1').attr('style','display:block;font-family: Helvetica Neue;font-size:14px;');
			$('#personalinfo2').attr('style','display:block;font-family: Helvetica Neue;font-size:14px;');
		
		}
	
},
error : function() {
	console.log("<-------error returned for User details -------> ");
	}
});   


}

function usersMember()
{
    var userId=1015;
    var url = urlForServer+"user/userMemberDetails/"+userId;

    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn					
    	},
	type : 'POST',
	url : url,
	success : function(responseText) {
	var data = jQuery.parseJSON(responseText);
	  
},
error : function() {
	console.log("<-------error returned for User details -------> ");
	}
});   


}
var j=0;

function fetchTeacher()
{
	++j;
	var url = urlForServer+"user/getUserDetails";
	var datastr = '{"userId":"'+userId+'"}';
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		console.log("<-------data returned from url for get Teacher with reponse as -------> "+ responseText);
		var data = jQuery.parseJSON(responseText);
		if(data!=null && data!='')
		{
		for ( var i = 0; i < data.length; i++) {
							userDetailsJsonObj = data[i];
							$("#teacherName").text(data[i].userFirstName);
							$("#teacherAddress").text(data[i].address);
							$("#teacherPhone").text(data[i].contactNumber);
						//$("#table").prepend('<tr id=row'+j+'><td ><select id=classId'+j+' class=span8><option value=--Select Class>--Select Class</option><option value="'+data[i].classId+'">'+data[i].classId+'</option></select><label id=classIds></label></td> <td><select id=qytId'+j+' class=span6 onchange=javascript:getQuty("'+j+'","'+i+'")><option value=--Select Quty>--Select Quty</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select><label id=qytIds></label></td> <td><select id=unitId'+j+' class=span6 onchange=javascript:getUnit("'+j+'","'+i+'")><option value=--Select Unit>--Select Unit</option><option value="'+data[i].payperHours+'">'+data[i].payperHours+'</option></select><label id=unitIds></label></td> <td><label id="amountId"></label></td></tr>');
						$("#rows").before('<tr id=row'+j+'><td ><select id=classId'+j+' class=span8><option value=--Select Class>--Select Class</option><option value="'+data[i].classId+'">'+data[i].classId+'</option></select><label id=classIds></label></td> <td><select id=qytId'+j+' class=span6 onchange=javascript:getQuty("'+j+'","'+i+'")><option value=--Select Quty>--Select Quty</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select><label id=qytIds></label></td> <td><select id=unitId'+j+' class=span6 onchange=javascript:getUnit("'+j+'","'+i+'")><option value=--Select Unit>--Select Unit</option><option value="'+data[i].payperHours+'">'+data[i].payperHours+'</option></select><label id=unitIds></label></td> <td><label id="amountId'+j+'"></label></td></tr>');

						}
		}
		
	},
	error : function() {
		console.log("<-------error returned for get Teacher -------> ");
		}
	});
}
var j=0;
function addRow()
{
	++j;
	
	$("#rows").after('<tr><td><input id="classId'+j+'" name="classId" type="text"	class="input-block-level" required/></td><td><input id="batchNo'+j+'" name="batchNo" type="text"	class="input-block-level" required/></td><td><input id="startDate'+j+'" name="startDate" type="text"	class="input-block-level" required onclick=javascript:datePicker()/></td><td> <input id="endDate'+j+'" name="endDate" type="text"	class="input-block-level" required/></td> <td><input id="duration'+j+'" name="duration" type="text"	class="input-block-level" required/></td> <td><input id="price'+j+'" name="price" type="text"	class="input-block-level" required/></td><td><input id="maxStudent'+j+'" name="maxStudent" type="text"	class="input-block-level" required/> </td><td><input type="checkbox" id="mon'+j+'"> </td> <td> <input type="checkbox" id="tue'+j+'"></td><td><input type="checkbox" id="wed'+j+'"></td><td><input type="checkbox" id="thu'+j+'"> </td><td> <input type="checkbox" id="fri'+j+'"></td><td><input type="checkbox" id="sat'+j+'"></td><td><input type="checkbox" id="sun'+j+'"></td>/tr>');
	var table = document.getElementById('table');    
	var rowCount = table.rows.length; 
	  for ( var i = 1 ; i <rowCount-1 ; i++ ) {
		  
	  }
	
	

}
$(function(){
	$("#showMsgsPro").click(function(){
		$("#alertMessagesPro").modal('hide');
		app.navigate("help", {trigger: true});
		showRecentFile();
	});
	});



function  showRecentFile(){
	var url=urlForServer+"admin/getUploadedFilesForHelp";
	$.ajax(
		 	{
		 		headers:
		 		{ 
		 			"Mn-Callers" : musicnote,
		 	    	"Mn-time" :musicnoteIn				
		 		},
		 		url:url,
		 		type:'post',
		 		success:function(responseText)
		 		{
		 			if(responseText=='failed')
		 			{
		    			//$("#noVideoFound").attr('style','display:block;color:red;margin-left:170px;margin-top:60px;');	
		 			}
		 			else
		 			{
		 				var data1 = jQuery.parseJSON(responseText);
		 				if(data1!=null && data1!="")
		 				{
		 					var exactFile=data1.length-1;
		 					$('#collapseRecentVideos'+exactFile).attr("class","in collapse");
		 					//$('#collapseRecentVideos1').attr("class","in collapse");
		 				}
		 			}
		 		}
		 	});
}

$(document).ready( function() {

	$('#securityQuestion').children('#securitySelect').change(function(){
	if($(this).val()!='0'){	
		$('#securityQuestion').children('#securityBox').removeAttr('disabled');
	}else{
		$('#securityQuestion').children('#securityBox').attr('disabled','disabled' );
		$('#securityQuestion').children('#securityBox').val('');
	}
			
	});
});
function removePicture()
{
	var url=urlForServer+"user/deleteProfilePicture/"+userDetailsJsonObj['userId'];
	$.ajax(
		 	{
		 		headers:
		 		{ 
		 			"Mn-Callers" : musicnote,
		 	    	"Mn-time" :musicnoteIn				
		 		},
		 		url:url,
		 		type:'post',
		 		success:function(responseText)
		 		{
		 			//alert(responseText);
		 			uploadedphoto();
		 		}
		 	});
}
function countChar(val) {
	
    var len = $("#descriptionId").val().length;
    if (len > 500) {
      val.value = val.value.substring(0, 140);
    } else {
      $('#bioVarning').text((500 - len)+" characters remaining").attr("style", "font-family: Helvetica Neue;font-size:14px;");
    }
}


	





