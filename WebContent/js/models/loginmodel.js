var emailStr;
var homeBodyId;
var remindersMap={};
var userMailId='';
var mailIdForResetcode='';
var userTokens;
var newPassWarningFlag;
var userIdSharedIdSetArray= new Array();
window.clearInterval(autoNoteDetailsFetchTimer);
window.clearInterval(autoContactDetailsFetchTimer);
window.clearInterval(autoCrowdDetailsFetchTimer);


function statusChangeCallback(response) {
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
        FBuserLogin=true;
        testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
     // document.getElementById('status').innerHTML = 'Please log ' +'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
     // document.getElementById('status').innerHTML = 'Please log ' +'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '{541136035945063}',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1', // use version 2.1
    status     : true
  });
  /* FB.login(function(response) {
	  alert('FB.login>>>>>>>>');
	   if (response.authResponse) {
		   alert('FB.login>>>>>>>>response.authResponse:::'+response.authResponse);
		   testAPI();
	   } else {
	     console.log('User cancelled login or did not fully authorize.');
	   }
	 }); */

	 /*FB.logout(function(response) {
		  FB.Auth.setAuthResponse(null, 'unknown');
		   alert('logout called');
      // Person is now logged out
  }); */
  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.
  /* FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  }); */

  };

  // Load the SDK asynchronously
 /*   (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    //js.src = "sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));  */
 /* (function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1466485926948895&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk')); */





  

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    FB.api('/me', function(response) {
    	
    	 /* console.log('Successful login for response: ' + JSON.stringify(response));
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!'; */

    	 if(response.name!=undefined){
    		  var url = urlForServer+"login/checkuser";
    		  var userName=response.name;
    		  var emailId=response.email;
    		  var facebookID=response.id;
    			var datastr = '{"userName":"'+userName+'","facebookEmailId":"'+emailId+'","facebookID":"'+facebookID+'"}';	
				emailStr=datastr;
    			console.log("<-------Sdatastr -------> "+ datastr);
    			var params = encodeURIComponent(datastr);
    			var musicnoteAn=$.base64.encode(url);
    			$.ajax({
    				headers: { 
    				"Mn-Callers" : musicnoteAn				
    				},
    				type : 'POST', url : url, data : params,
    				success : function(responseText){
    					console.log("Login response"+responseText);
    					
    					var data = jQuery.parseJSON(responseText);
    					musicnote=data.token;
    					//logoutFB();
    					if(data.userId=="0"){
    						app.navigate("registration", {trigger: true});	
        					$("#userfirstname").val(response.first_name);
        					$("#userlastname").val(response.last_name);
        					//$("#gender").val(response.gender);
        					$("#emailid").val(response.email);
        					$("#facebookId").val(response.id);
        					//$("#dob").val(response.birthday);
        					var fbUserName=response.name;
        					fbUserName = fbUserName.replace(/\s+/g, ''); 
        					$("#fbusername").val(fbUserName);
        					/*$("#role").append('<option value="Music Teacher">Music Teacher</option>');
        					$("#role").append('<option value="Music Student">Music Student</option>');
        					$("#role").append('<option value="Musician">Musician</option>');
        					$("#role").append('<option value="Other">Other</option>');
        					
        					$("#skillLevel").append('<option value="Beginner">Beginner</option><option value="Intermediate">Intermediate</option><option value="Advanced">Advanced</option>');
        					$("#instrument").append('<option value="Bass">Bass</option><option value="Brass">Brass</option><option value="Cello">Cello</option><option value="Drums">Drums</option><option value="Guitar">Guitar</option><option value="Harmonica">Harmonica</option><option value="Harp">Harp</option><option value="Oboe">Oboe</option><option value="Percussion">Percussion</option><option value="Piano/Keys">Piano/Keys</option><option value="Saxophone">Saxophone</option><option value="Trombone">Trombone</option><option value="Trumpet">Trumpet</option><option value="Violin">Violin</option><option value="Vocals">Vocals</option><option value="Woodwinds">Woodwinds</option><option value="Other">Other</option>');
        					$("#favoriteMusic").append('<option value="Alternative">Alternative</option><option value="Asian Pop (J-Pop, K-pop)">Asian Pop (J-Pop, K-pop)</option><option value="Bollywood">Bollywood</option><option value="Blues">Blues</option><option value="Carnatic">Carnatic</option><option value="Classical">Classical</option><option value="Country">Country</option><option value="Dance">Dance</option><option value="Easy Listening">Easy Listening</option><option value="Electronic">Electronic</option><option value="European Music (Folk / Pop)">European Music (Folk / Pop)</option><option value="Folk">Folk</option><option value=" Hip Hop / Rap"> Hip Hop / Rap</option><option value="Indian Classical">Indian Classical</option><option value="Indie Pop">Indie Pop</option><option value="Inspirational (incl. Gospel)">Inspirational (incl. Gospel)</option><option value="Jazz">Jazz</option><option value="Latin Music">Latin Music</option><option value="New Age">New Age</option><option value="Opera">Opera</option><option value="Pop">Pop</option><option value="R&B / Soul">R&B / Soul</option><option value="Reggae">Reggae</option><option value="Rock">Rock</option><option value="Singer / Songwriter">Singer / Songwriter</option><option value="World Music">World Music</option><option value="Other">Other</option>');*/
        					$("#main").show();
        					$("#sidecontent").hide();

    					}
    					else if(data.userId=="blocked")
    					{
    						$("#signinspan").text("Your account is temporarily suspended due to a violation of our Terms of Service.").css({"color":"red","font-weight": "bold"});
    					}
    					else if(data.userId.indexOf("mailcheck")!=-1)
    					{
    						FBuserId=response.id;
    						FBuserName=response.name;
    						FBemailId=response.email;
    						musicnote=data.token;
    						musicnoteIn=data.datess;
    						FBuserName = FBuserName.replace(/\s+/g, ''); 
    						mailIdForResetcode=FBuserName;
    						$("#emailVerify").hide();
    						$("#emailButton").show();
    						$("#emailCode").empty();
    						$("#emailCode").hide();
    						
    						$("#emailVerify-message").empty();
    						$("#emailOkButton").empty();
    						$('#emailVerifyModal').modal('show');
    						$("#emailVerify-header-span").text("Email Verification");
    						$("#msg-modal-body").after('<input id="emailCode" name="emailCode" style="margin-left:10px;font-family: Helvetica Neue;font-size:14px; width:80%;" type="text" class="form-control input-block-level" placeholder="Enter verification code" />');
    						$('#verifyResetButtons').attr('disabled',false);
    						$("#verifyResetButtons").show();
    						
    					}
    					else if(data.userId!="0"){
    						
    						musicnote=data.token;
    						musicnoteIn=data.datess;	
    						userId=data.userId;
    						fetchUsersList();
    						if(is_chrome || is_safari)
    						    $.jStorage.set("loginId",userId);
    						    else
    						    document.cookie=userId;    
    						
    						console.log("Login response-->>"+data.userId);
    						
    						fetchUsers();
    						doUserAppDetils();
    						fetchRemainders();
    						getrecentActivity();
    						fetchNotifications();
    						//setInterval(function() {getrecentActivity()},8000);
    						setTimeout(function(){getRequests();},1000);
    						//setInterval(function() {fetchNotifications();},8000);
    						console.log("Jstorage values-->>"+$.jStorage.get("loginId"));
    						console.log("Jstorage values-->>"+document.cookie);
    						app.navigate("home", {trigger: true});

    						setTimeout(function(){
    						 if(data.studentCheck.match('true')){
    								app.navigate("profile", {trigger: true});
    								$('#securityQuestion').attr('style','display:block');
    								}
    						},1000);
    						$("#main").hide();
    						$("#sidecontent").show();
    						$('#footer').show();
    						$('#header').show();
    						$(homeBodyId).css( "background-color","#eaedef");
    						timerNew();
    						setTimeout(mnQurtz(),60000);
    						//$('#welcomemsg').attr('style','display:block');
    						
    						var userSharedIdString =data.sharedIdSet;
    						if(userSharedIdString.indexOf("[") != -1){
    							userSharedIdString = userSharedIdString.substring(userSharedIdString.indexOf("[")+1,userSharedIdString.length);
    						}
    						if(userSharedIdString.indexOf("]") != -1){
    							userSharedIdString = userSharedIdString.substring(0,userSharedIdString.indexOf("]"));
    						}
    						if(userSharedIdString.indexOf(",") != -1){
    							userIdSharedIdSetArray = userSharedIdString.split(',');
    						
    						}else{
    							if(userSharedIdString.trim() != ''){
    								userIdSharedIdSetArray = userSharedIdString.split(',');
    							}
    						}

    						if(data.requestPending){
    							var userIdSetArray= new Array();
    							var divFrdAc = "";
    									var userIdString =data.userIdSet;
    									if(userIdString.indexOf("[") != -1){
    										userIdString = userIdString.substring(userIdString.indexOf("[")+1,userIdString.length);
    									}
    									if(userIdString.indexOf("]") != -1){
    										userIdString = userIdString.substring(0,userIdString.indexOf("]"));
    									}
    									if(userIdString.indexOf(",") != -1){
    										userIdSetArray = userIdString.split(',');
    									
    									}else{
    										if(userIdString.trim() != ''){
    											userIdSetArray = userIdString.split(',');
    										}
    									}
    									
    									
    									if(userIdSetArray.length > 0 && userIdSetArray[0]!=null ){
    										setTimeout(function(){
    											divFrdAc = createFriendAceeptDivByMail(userIdSetArray,userIdSharedIdSetArray);
    											$('#mailSharingModel').children('.modal-dialog').children('.modal-content').children('.modalBody').children().append("<br>"+divFrdAc+"<br>");
    											$('#mailSharingModel').prependTo('body').modal('show');
    										},1000);
    									}
    									
    							
    						        }
    								else
    								{
    							/*if(userIdSharedIdSetArray.length > 0 && userIdSharedIdSetArray[0]!=null ){
    							setTimeout(function(){
    								var divFrdES = createEventAceeptDiv(userIdSharedIdSetArray);
    								//$('#mailSharingModel').children('.modalBody').children().append(divFrdES);
    								//$('#mailSharingModel').modal('show');
    							},1000);
    						}*/
    					  }

    					}
    					
    				},error: function(e){
				alert('error in response');
			}
            });
            }
    });
  }




function sendPasswordViaMail(){
	if($('#emailId').val()!= null && $('#emailId').val().trim()!= ""){
		 var url = urlForServer+"user/getExistsEmail";
			var datastr = '{"emailId":"'+$('#emailId').val()+'"}';
			var params = encodeURIComponent(datastr);
			$.ajax({
				headers:{
					"Mn-Callers1":regMusicnote ,
					"Mn-time":regMusicnoteIn
					},
			type : 'POST',
			url : url,
			data : params,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			  if(data!=null && data!='')
				{
				  $( "#forgotdialog" ).attr('style','display : block');
				  if($('#emailId').val().trim()!= ''){
						var emailId = $('#emailId').val();
						
						var params = "{\"emailId\":\""+emailId+"\"}";
						params = encodeURIComponent(params);
						var url=urlForServer+"user/forgetPassword";
						$("#emailId").remove();
						$("#modalhead").children().text('');
						//$("#modalhead").append('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>');
						$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").text("  Password reset request processed");
						$("#fotgersubmit").addClass("hidden");
						
						$.ajax({
							headers:{
								"Mn-Callers1":regMusicnote ,
								"Mn-time":regMusicnoteIn
								},
							type: 'POST',
							url : url,
							data:params,
							success : function(response){
								var data = jQuery.parseJSON(response); 
								if(data!= null && data.status!='' ){
									if(data.status == "success"){
										
										$("#fotgercancel").prepend('<i class="icon-white icon-time" id="schedulestatusicon"></i>');
										$("#fotgercancel").attr('class','btn btn-info');
										$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").text("  Password sent successfully");
										$("#schedulestatusicon").remove();
										$("#fotgercancel").prepend('<i class="icon-white icon-ok" id="schedulestatusicon"></i>');
										$("#fotgercancel").attr('class','btn btn-success');
										setTimeout(function(){ $('#msgModal').modal('hide');},5000);
									
									}else if (data.status == "invalid"){
										
										$("#fotgercancel").prepend('<i class="icon-white icon-time" id="schedulestatusicon"></i>');
										$("#fotgercancel").attr('class','btn btn-info');
										$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").text("Invalid Username");
										$("#schedulestatusicon").remove();
										$("#fotgercancel").prepend('<i class="icon-white icon-ok" id="schedulestatusicon"></i>');
										$("#fotgercancel").attr('class','btn btn-important');
										setTimeout(function(){ $('#msgModal').modal('hide');},5000);
									}
									
								}
							},error:function(e){
								alert("Please try again later!",e);
							}
						});
					}
		    	
				}else
				{
					 $( "#forgotdialog" ).append('<p><font color="red" class="fontStyle">Email does not exist</font>');
					 $( "#forgotdialog" ).attr('style','display : block');
				}
		
		},
		error : function() {
			console.log("<-------error returned for User details -------> ");
			}
		}); 
	}else
	{
		$( "#forgotdialog" ).append('<p><font color="red" class="fontStyle">Field cannot be blank</font>');
		$( "#forgotdialog" ).attr('style','display : block;font-family: Helvetica Neue;font-size:14px;');
	}
	
	return false
	}


$('#msgModal').on('click','#resetPass',function(){
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#forgotdialog').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
$('#resetPass').removeAttr('disabled','disabled');
//$('#msgModal').children("#msg-modal-body").append('<div id="resetMessage"></div>');
var password=$('#confirmPass').val();
var userName=$('#emailId').val();
var password1=$('#pass').val();
var length=password1.length;
if(userName ==""){
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetPassDiv').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').remove();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#confirmPassDiv').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#passDiv').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetPassDiv').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetPasswordDiv').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#forgotdialog').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="errormsg1" style="color:red;margin-top:10px;">Field cannot be blank</div>');

}else{
if(password =='' || password1=='' || password=='null' || password=='null' ){
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').append('<div style="color:red;margin-top:10px;">Fields cannot be blank</div>');
return false;
}
if(length<6)  
{ 
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').append('<div style="color:red;margin-top:10px;">Short passwords are easy to guess. Try one with at least 6 characters.</div>');
	return false;
} 
if(password1==password){
$('#resetPass').attr('disabled','disabled');
var datastr = '{"userName":"'+userName+'","password":"'+password+'"}';
var params = encodeURIComponent(datastr);
var url =urlForServer+"user/setResetPassword";
$.ajax({
headers: { 
"Mn-Callers" : regMusicnote,
"Mn-time" :regMusicnoteIn				
},
type : 'POST',
url : url,
data : params,
success : function(response) {
if(response=="UserName not found"){
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="errormsg1" style="color:red;margin-top:10px;">User Name does not exist</div>');
	$('#resetPass').removeAttr('disabled','disabled');
}else{
	$('#confirmPass').val("");
	$('#pass').val("");
	$('#resetPass').removeAttr('disabled','disabled');
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').append('<div style="color:green;margin-top:10px;">Password set successfully</div>');
	setTimeout(function(){$('#msgModal').modal('hide');},3000);
}
},
error:function(response){
alert('please try again later');
}
});
}else{
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').empty();
$('#resetPass').removeAttr('disabled','disabled');
$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').append('<div style="color:red;margin-top:10px;">Password mismatch</div>');
}

}
});

function getSecurityQuestion(){
	$( "#forgotdialog" ).empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetPassDiv').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetMessage').remove();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#confirmPassDiv').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#passDiv').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetPassDiv').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#resetPasswordDiv').empty();
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
	var userName=($('#emailId').val());
	if(userName ==""){
		$( "#forgotdialog" ).empty();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="errormsg1" style="color:red;margin-top:10px;">Field cannot be blank</div>');
	
	}else{
		
	var params = '{"userName":"'+$('#emailId').val()+'","emailId":"'+$('#emailId').val()+'"}';
	var url =urlForServer+"user/ExistsUser";
	$.ajax({
		headers:{
			"Mn-Callers1":regMusicnote ,
			"Mn-time":regMusicnoteIn
			},
		type : 'POST',
		url : url,
		data : params,
		success : function(response) {
		var responseText=jQuery.parseJSON(response);
		/*if(response=='null' || response==''){
			
			$('#msgModal').children("#msg-modal-body").find('#questionDiv').empty();
			$('#msgModal').children("#msg-modal-body").find('#answerDiv').empty();
			$("#fotgersubmit").addClass('hidden');
			$("#fotgercancel").addClass('hidden');
			var userName=($('#emailId').val());
			var params = '{"emailId":"'+$('#emailId').val()+'"}';
			var url =urlForServer+"user/getExistsEmail";
			
			$.ajax({
				headers:{
					"Mn-Callers1":regMusicnote ,
					"Mn-time":regMusicnoteIn
					},
				type : 'POST',
				url : url,
				data : params,
				success : function(response) {
					alert("ssss"+response);	*/
				if(response=='null' || response==''){
					
					$( "#forgotdialog" ).empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#questionDiv').empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#answerDiv').empty();	
					$("#fotgersubmit").addClass('hidden');
					$("#fotgercancel").addClass('hidden');
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="errormsg1" style="color:red;margin-top:10px;">User Name does not exist</div>');
				
				}
				else if(responseText[0].userId=="blocked")
				{
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#questionDiv').empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#answerDiv').empty();	
					$("#fotgersubmit").addClass('hidden');
					$("#fotgercancel").addClass('hidden');
					$( "#forgotdialog" ).empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#errormsg1').empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="errormsg1" style="color:red;margin-top:10px;">Your account is temporarily suspended</div>');
				}
				else{
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#questionDiv').empty();
					$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#answerDiv').empty();
					
					var url =urlForServer+"user/getSecurityQuestions";
					var params = '{"userName":"'+responseText[0].userName+'"}';
					$.ajax({
						headers:{
							"Mn-Callers1":regMusicnote ,
							"Mn-time":regMusicnoteIn
							},
						type : 'POST',
						url : url,
						data : params,
						success : function(response) {
						var responseTexts=jQuery.parseJSON(response);
						if(responseTexts!='' && responseTexts!=null){
							attachSecurityQuestionField();
						}else if(responseText[0].emailId=="empty"){
							$("#fotgersubmit").attr('style','display : none');
							$("#fotgercancel").attr('style','display : none');
							$( "#forgotdialog" ).empty();
							$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').empty();
							$( "#forgotdialog" ).append('<p><font color="red" class="fontStyle">Please contact musicnote admin</font>');
							 $( "#forgotdialog" ).attr('style','display : block');
						}
						else{
							$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#questionDiv').empty();
							$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#answerDiv').empty();
							sendPasswordViaMail();
							}
						
					},
					error:function(response){
						alert('please try again later');
					}
					});
					
				}
				
			},
			error:function(response){
				alert('please try again later');
			}
			});
			
			
		/*}else{
			$('#msgModal').children("#msg-modal-body").children('#questionDiv').empty();
			$('#msgModal').children("#msg-modal-body").children('#answerDiv').empty();
			attachSecurityQuestionField();
		}
		
		
	},
	error:function(response){
		alert('please try again later');
	}
	});*/
	}	
}
function attachSecurityQuestionField(){
	var url = urlForServer+"user/getSecurityQuestion";
	$.ajax({
		headers:{
			"Mn-Callers1":regMusicnote ,
			"Mn-time":regMusicnoteIn
			},
		type : 'POST',
		url : url,
		success : function(responseText) {	
		var split=responseText.split('~');
		$( "#forgotdialog" ).empty();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#questionDiv').empty();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#answerDiv').empty();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="questionDiv" style="margin-top:15px;"><select class="form-control" id="questionList"><option value="0">Please select a security Question</option></select></div>');
		for(var i=0;i<split.length;i++){
			var quesId=split[i].split('-');	
			$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#questionDiv').children('#questionList').append('<option value="'+quesId[1]+'">'+quesId[0]+'</option>');		
		}
		
	
	$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="answerDiv" class=""><input type="text" id="answer" required="" class="form-control" autofocus placeholder="Enter Answer" style="margin-top:5px;"></input></div>');
	
	$("#fotgersubmit").removeClass('hidden');
	$("#fotgercancel").removeClass('hidden');
	},
		error:function(responsetext){
		
		alert('Please try again later');
	}
	});
}


$(document).ready( function() {
	$("#newpassword").keyup(function (e) {
		  $(this).val(function(i, v) { return v.replace(/ /g,""); });
		});
	$("#cfrmpassword").keyup(function (e) {
		  $(this).val(function(i, v) { return v.replace(/ /g,""); });
		});
	$('#msgModal').on('keyup','#confirmPass',function(e){
		  $(this).val(function(i, v) { return v.replace(/ /g,""); });
		});
	$('#msgModal').on('keyup','#pass',function(e){
		  $(this).val(function(i, v) { return v.replace(/ /g,""); });
		});
	var urlLocation=window.location.href;
	urlLocation=decodeURIComponent(urlLocation);
	
	var linkUrlCheck=urlLocation.indexOf('ExternalPayment');
	if(urlLocation.indexOf('?ExternalPayment')!=-1)
	{
		$("#emailButton").hide();
		$("#emailVerify").show();
		$("#emailCode").empty();
		$("#emailCode").hide();
		$('#emailVerifyModal').modal('show');
		$("#emailVerify-header-span").text("Email Verification Sent");
		$("#emailVerify-message").text("Please check your Email. Click the link and log in to complete your registration!");
		//$(".modal-footer").append('<button class="modalBtn fontStyle" id="emailVerify" >OK</button>');
		$("#emailVerify").click(function(){$('#emailVerifyModal').modal('hide');});
	}
	
	else if(urlLocation.indexOf('?auth')!=-1)
		{
			urlLocation=urlLocation.split('?');
			urlLocation=urlLocation[1].split('=');
			var urlLocation1=window.location.href;
			var urlCheck=urlLocation1.substring(urlLocation1.lastIndexOf('#')+1);
				if(!urlCheck .match('home')){
					var urlLocation=window.location.href;
					var checkString=urlLocation.substring(urlLocation.indexOf('?')+1,urlLocation.indexOf('='));
					if(checkString=="auth"){
						$('#emailVerifyModal').modal('show');
						$("#emailButton").hide();
						$("#emailVerify").show();
						$("#emailCode").empty();
						$("#emailCode").hide();
						$("#emailVerify-header-span").text("Email Verification Sent");
						$("#emailVerify-message").text("Please check your Email. Click the link and log in to complete your registration!");
						//$(".modal-footer").append('<button class="modalBtn fontStyle" id="emailVerify" >OK</button>');
						$("#emailVerify").click(function(){$('#emailVerifyModal').modal('hide');});
							}
						}
			}
	$('#userLogin').click( function() {
		homeBodyId=$(this).parent().parent().parent().parent().parent();
		userLogin();
	});
//	$('#loginContainer').on('keypress','.loginPasswd',function(e){
//		var unicode=e.charCode? e.charCode:e.keyCode; 
//		if(unicode==13){
//			homeBodyId=$(this).parent().parent().parent().parent().parent();
//			userLogin();
//			alert('456');
//			
//		}
//	});
	/*$('#register').click(function(){
		registerUser();
	});*/
	$('#resetCancel').click(function(){
		app.navigate("home", {trigger: true});
		$("#main").hide();
		$("#sidecontent").show();
		$('#footer').show();
		$('#header').show();
	});
	
	$('#resetSubmit').click(function(){
			resetPassword();
	});
	
	$('#forgotpasswordId').click(function(){
		forgetPassTokenGenerator();
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").empty();
		$('#msgModal').modal('show');
		$("#modalhead").find('.close').remove();
		$("#modalhead").append('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>');
		$("#msg-header-span").attr('style',"font-weight: bold;").text("Retrieve Password");
		
		$('#msg-header-span').attr('class',"");
		$('#msg-header-span').attr('class',"fontStyle14");
		$("#fotgersubmit").removeClass('hidden');
		$("#fotgercancel").attr('class',"btn btn-small  btn-danger");
		$("#fotgersubmit").addClass('hidden');
		$("#fotgercancel").addClass('hidden');
		$("#modal-message").attr('class',"").text("");
	
		$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").prepend('<input type="email" id="emailId" name="emailId"  class="form-control" onkeydown="if (event.keyCode == 13){javascript:getSecurityQuestion();}"  placeholder="Enter User Name/Email" required><div id="forgotdialog" class="fontStyle" title="Basic dialog" style="display:none;"></div>');
		
		$("#schedulestatusicon").remove();
	});

	

	
	/*$('#resetCancel').click(function(){
		$('#main').hide();
		$('#sidecontent').show();
		$('#footer').show();
		$('#header').show();
		app.navigate("home", {trigger : true});
	});*/
	
	$('#fotgersubmit').click(function(){
		//$("#fotgersubmit").addClass('hidden');
		//$("#fotgercancel").addClass('hidden');
		$( "#forgotdialog" ).empty();
		var userName=($('#emailId').val().toLowerCase());
		var params = '{"userName":"'+$('#emailId').val().toLowerCase()+'"}';
		var url =urlForServer+"user/getSecurityQuestions";
		$.ajax({
			headers: { 
			"Mn-Callers1" : regMusicnote,
	    	"Mn-time" :regMusicnoteIn				
	    	},
			type : 'POST',
			url : url,
			data : params,
			success : function(response) {
			var responseText=jQuery.parseJSON(response);
			if(responseText!='' && responseText!=null){
			//for(var i=0;i<responseText.length;i++){
			var answer=responseText.answer;
			var questionId=responseText.questionId;
			var question=responseText.question;
			if($('#answer').val()== answer && $('#questionList').val()==questionId ){
				$("#fotgersubmit").addClass('hidden');
				$("#fotgercancel").addClass('hidden');
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="resetPasswordDiv"><h4>Reset Password</h4></div>');
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="passDiv"><input type="password" class="form-control" id="pass" required="" autofocus placeholder="Enter Password" style="margin-top:5px;"></input></div>');
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="confirmPassDiv"><input type="password" class="form-control" id="confirmPass" required="" autofocus placeholder="Re-Enter Password" style="margin-top:5px;"></input><br></div>');
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="resetPassDiv"><a class="btn btn-primary" id="resetPass">Reset Password</a></div>');
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div id="resetMessage"></div>');
				return false;
				
				
			}else if($('#answer').val()!= answer){
				//$('#msgModal').children("#msg-modal-body").children('#resetPasswordDiv').empty();
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div class="errorMsg3" style="color:red;">Incorrect data</div>');
			}else if($('#questionList').val()!= questionId){
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div class="errorMsg3" style="color:red;">Incorrect data</div>');
			}
			else{
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div class="errorMsg3" style="color:red;">Incorrect data</div>');
				return false;
				sendPasswordViaMail();
				
			}
			if($('#answer').val()==''){
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('.errorMsg3').remove();
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").append('<div class="errorMsg3" style="color:red;">Field cannot be blank</div>');
			}
			
			}else{
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#questionDiv').empty();
				$('#msgModal').children('.modal-dialog').children('.modal-content').children("#msg-modal-body").children('#answerDiv').empty();
			}
			
		},
		error:function(response){
			alert('please try again later');
		}
		});
		
		});
	
	//$('#emailButton').click(function()
		
	//});
	$('.verifyClose').click(function(){
		$("#signinspan").empty();
	});
	
	// used to submit email verify code 
	//$('#emailOkButton').on('click','.emailButton',function(){
	//$('#emailverifysubmitButton').click(function(){
		
		//alert("this");
	//});
	
});


function codeVerify(){
	if(FBuserLogin){
		codeVerifyViaFBLogin();
	}
	else{
		$("#resetemptyerror").empty();
		$("#emailresetVerify-message").hide();
		$("#emailVerify-message").empty();
		$("#emailCode").show();
		var userName=$("#username").val().toLowerCase();
		mailIdForResetcode=userName;
		var password=$("#password").val();
		var emailCode=$("#emailCode").val();
		if(emailCode==""){
			$("#emailVerify-message").text("Please Enter Verification Code").css({"color":"red","font-weight": "bold"});
		}else{
			var param='{"loginId":"'+userName+'","password":"'+password+'","id":"'+emailCode+'"}';
			var url= urlForServer+"login/updateLoginCheck";
			$.ajax({
				headers: { 
					"Mn-Callers" : musicnote,
					"Mn-time" :musicnoteIn				
				},
				type : 'POST', 
				url : url,
				data : param,
				success : function(responseText){
					
					if(responseText!='success')
						$("#emailVerify-message").text("Please Enter Valid Code").css({"color":"red","font-weight": "bold"});
					else
					{
						$('#emailVerifyModal').modal('hide');
						loginAuthendication(userName,password);
					}
				},error:function(){}
			});
	 
		}
	}
}
function codeVerifyViaFBLogin()
{
	$("#resetemptyerror").empty();
	$("#emailresetVerify-message").hide();
	$("#emailVerify-message").empty();
	$("#emailCode").show();
	 //var userName=$("#username").val().toLowerCase();
	  //userMailId=userName;
	  //var password=$("#password").val();
	var facebookID=FBuserId;
	var userName=FBuserName;
	var emailId=FBemailId;
	  var emailCode=$("#emailCode").val();
	  if(emailCode==""){
		  $("#emailVerify-message").text("Please Enter Verification Code").css({"color":"red","font-weight": "bold"});
	  }else{
		 var param='{"facebookID":"'+facebookID+'","id":"'+emailCode+'"}';
			var url= urlForServer+"login/updateLoginCheckViaFBLogin";
			$.ajax({
				headers: { 
					"Mn-Callers" : musicnote,
					"Mn-time" :musicnoteIn				
				},
				type : 'POST', 
				url : url,
				data : param,
				success : function(responseText){
				
				if(responseText!='success')
					
					$("#emailVerify-message").text("Please Enter Valid Code").css({"color":"red","font-weight": "bold"});
				else
				{
					$('#emailVerifyModal').modal('hide');
			    		  var url = urlForServer+"login/checkuser";
			    		  var userName=FBuserName;
			    		  var emailId=FBemailId;
			    		  var facebookID=FBuserId;
			    			var datastr = '{"userName":"'+userName+'","facebookEmailId":"'+emailId+'","facebookID":"'+facebookID+'"}';	
							emailStr=datastr;
			    			var params = encodeURIComponent(datastr);
			    			var musicnoteAn=$.base64.encode(url);
			    			$.ajax({
			    				headers: { 
			    				"Mn-Callers" : musicnoteAn				
			    				},
			    				type : 'POST', url : url, data : params,
			    				success : function(responseText){
			    					var data = jQuery.parseJSON(responseText);
			    					musicnote=data.token;
			    					if(data.userId=="blocked")
			    					{
			    						$("#signinspan").text("Your account is temporarily suspended due to a violation of our Terms of Service.").css({"color":"red","font-weight": "bold"});
			    					}
			    					else if(data.userId!="0"){
			    						
			    						musicnote=data.token;
			    						musicnoteIn=data.datess;	
			    						userId=data.userId;
			    						fetchUsersList();
			    						if(is_chrome || is_safari)
			    						    $.jStorage.set("loginId",userId);
			    						    else
			    						    document.cookie=userId;    
			    						
			    						
			    						fetchUsers();
			    						doUserAppDetils();
			    						fetchRemainders();
			    						getrecentActivity();
			    						fetchNotifications();
			    						//setInterval(function() {getrecentActivity()},8000);
			    						setTimeout(function(){getRequests();},1000);
			    						//setInterval(function() {fetchNotifications();},8000);
			    						console.log("Jstorage values-->>"+$.jStorage.get("loginId"));
			    						console.log("Jstorage values-->>"+document.cookie);
			    						app.navigate("home", {trigger: true});

			    						setTimeout(function(){
			    						 if(data.studentCheck.match('true')){
			    								app.navigate("profile", {trigger: true});
			    								$('#securityQuestion').attr('style','display:block');
			    								}
			    						},1000);
			    						$("#main").hide();
			    						$("#sidecontent").show();
			    						$('#footer').show();
			    						$('#header').show();
			    						$(homeBodyId).css( "background-color","#eaedef");
			    						timerNew();
			    						setTimeout(mnQurtz(),60000);
			    						//$('#welcomemsg').attr('style','display:block');
			    						
			    						var userSharedIdString =data.sharedIdSet;
			    						if(userSharedIdString.indexOf("[") != -1){
			    							userSharedIdString = userSharedIdString.substring(userSharedIdString.indexOf("[")+1,userSharedIdString.length);
			    						}
			    						if(userSharedIdString.indexOf("]") != -1){
			    							userSharedIdString = userSharedIdString.substring(0,userSharedIdString.indexOf("]"));
			    						}
			    						if(userSharedIdString.indexOf(",") != -1){
			    							userIdSharedIdSetArray = userSharedIdString.split(',');
			    						
			    						}else{
			    							if(userSharedIdString.trim() != ''){
			    								userIdSharedIdSetArray = userSharedIdString.split(',');
			    							}
			    						}

			    						if(data.requestPending){
			    							var userIdSetArray= new Array();
			    							var divFrdAc = "";
			    									var userIdString =data.userIdSet;
			    									if(userIdString.indexOf("[") != -1){
			    										userIdString = userIdString.substring(userIdString.indexOf("[")+1,userIdString.length);
			    									}
			    									if(userIdString.indexOf("]") != -1){
			    										userIdString = userIdString.substring(0,userIdString.indexOf("]"));
			    									}
			    									if(userIdString.indexOf(",") != -1){
			    										userIdSetArray = userIdString.split(',');
			    									
			    									}else{
			    										if(userIdString.trim() != ''){
			    											userIdSetArray = userIdString.split(',');
			    										}
			    									}
			    									
			    									
			    									if(userIdSetArray.length > 0 && userIdSetArray[0]!=null ){
			    										setTimeout(function(){
			    											divFrdAc = createFriendAceeptDivByMail(userIdSetArray,userIdSharedIdSetArray);
			    											$('#mailSharingModel').children('.modal-dialog').children('.modal-content').children('.modalBody').children().append("<br>"+divFrdAc+"<br>");
			    											$('#mailSharingModel').prependTo('body').modal('show');
			    										},1000);
			    									}
			    									
			    							
			    						        }
			    								else
			    								{
			    							/*if(userIdSharedIdSetArray.length > 0 && userIdSharedIdSetArray[0]!=null ){
			    							setTimeout(function(){
			    								var divFrdES = createEventAceeptDiv(userIdSharedIdSetArray);
			    								//$('#mailSharingModel').children('.modalBody').children().append(divFrdES);
			    								//$('#mailSharingModel').modal('show');
			    							},1000);
			    						}*/
			    					  }

			    					}else{
			    						$("#signinspan").text("wrong user").css({"color":"red","font-weight": "bold"});
			    					}
			    					
			    				},error: function(e){
			    					alert('Please try again later');
			    				}
			            });
					
					
					
					
				}
				},error:function(){}
		});
	  } 
	 }
/*function fbAsyncInit() {
    FB.init({appId: '541136035945063', status: true, cookie: true, xfbml: true
    	 //channelUrl : 'http://www.jobulu.com/musicnote/index.html'
    		 });

    FB.Event.subscribe('auth.login', function(response) {
        // do something with response
        login();
    });
    FB.Event.subscribe('auth.logout', function(response) {
        // do something with response
        logout();
    });

    FB.getLoginStatus(function(response) {
        if (response.session) {
            // logged in and connected user, someone you know
            login();
        }
    });

};

(function() {
    var e = document.createElement('script');
    e.type = 'text/javascript';
    e.src = document.location.protocol +
        '//connect.facebook.net/en_US/all.js';
    e.async = true;
    document.getElementById('fb-root').appendChild(e);
}());*/

function login(){
    FB.api('/me', function(response) {
    	if(response.name!=undefined){
    		  var url = urlForServer+"login/checkuser";
    		  var userName=response.username;
    			var datastr = '{"userName":"'+userName+'"}';	
				emailStr=datastr;
    			//console.log("<-------Sdatastr -------> "+ datastr);
    			var params = encodeURIComponent(datastr);
    			$.ajax({
    				headers: { 
    				"Mn-Callers" : musicnote,
    		    	"Mn-time" :musicnoteIn				
    		    	},
    				type : 'POST', url : url, data : params,
    				success : function(responseText){
    				if(responseText=="0"){
    					app.navigate("registration", {trigger: true});		
    					$("#userfirstname").val(response.first_name);
    					$("#userlastname").val(response.last_name);
    					$("#gender").val(response.gender);
    					$("#emailid").val(response.email);
    					$("#dob").val(response.birthday);
    					$("#fbusername").val(response.username);
    					$("#role").append('<option value="Student">Student</option>');
    					$("#role").append('<option value="Teacher">Teacher</option>');
						$("#role").append('<option value="Administrator">Administrator</option>');
    					$("#main").show();
    					$("#sidecontent").hide();
//    					$('#footer').show();
//    					$('#header').show();
    				}else if(responseText!="0"){
    					userId=responseText;
    					fetchUsersList();
    					if(is_chrome || is_safari)
    					    $.jStorage.set("lkeyX", responseText);
    					    else
    					    document.cookie=responseText;    
    		               
    					
    					fetchUsers();
    					doUserAppDetils();
						fetchRemainders();
						getrecentActivity();
						fetchNotifications();
						getRequests();
						//setInterval(function() {getrecentActivity()},8000);
						//autoFriendRequestGetForHeader=window.setInterval(function(){getRequests()},60000);
						//setInterval(function() {fetchNotifications();},8000);
    					app.navigate("home", {trigger: true});
    					$("#main").hide();
    					$("#sidecontent").show();
    					$('#footer').show();
    					$('#header').show();
    					$('#welcomemsg').attr('style','display:block;font-family: Helvetica Neue;font-size:14px;');
    					//fetchMailMessages('inbox');
    				}
    			}
    			});
    	}
    });
}

function logout(){
	$("#loginContainer").show();
}


function userLogin(){
$('#verifyResetButtons').attr('disabled',false);
var urlLocation=window.location.href;
urlLocation=decodeURIComponent(urlLocation);	
	if(urlLocation.indexOf('?')!=-1)
	{
		
	var checkString=urlLocation.substring(urlLocation.indexOf('?')+1,urlLocation.indexOf('='));
	
	if(checkString=="InternalPayment"){
		//addInternalPayment();
	}
	else if(checkString=="ExternalPayment"){
		//addPayment();
	}
	else if(checkString=="mailIdLink"){
		var urlLocation=window.location.href;
		urlLocation=decodeURIComponent(urlLocation);
		var userName=$("#username").val().toLowerCase();
		var password=$("#password").val();
		urlLocation=urlLocation.split('=');
		var urlId=urlLocation[1];
		//if(userName == name){
			var param='{"loginId":"'+userName+'","password":"'+password+'","id":"'+urlId+'"}';
			var url= urlForServer+"login/updateLoginCheck";
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
				type : 'POST', 
				url : url,
				data : param,
				success : function(responseText){
				
				if(responseText!='success')
					$("#signinspan").text("Incorrect User Name/Password").css({"color":"red","font-weight": "bold"});
				},error:function(){
			}
		});
		/*}else{
			$("#signinspan").text("Incorrect Email/Password").css({"color":"red","font-weight": "bold"});
			return false;
		}*/
	}
	else{
		//do nothing
	}
	
	}
			  $("#signinspan").text("Please wait!").css({"color":"black","font-weight": "bold"});
    		  var userName=$("#username").val().toLowerCase();
			  mailIdForResetcode=userName;
    		  var password=$("#password").val();
    		  if(userName==""||password==""){
    			  $("#signinspan").text("Please Enter User Name and Password").css({"color":"red","font-weight": "bold"});
    		 }else{
    			 loginAuthendication(userName,password);
    		 }
}
function addPayment(){
	var urlLocation=window.location.href;
	urlLocation=decodeURIComponent(urlLocation);
	var urlCheck=urlLocation.substring(urlLocation.lastIndexOf('~')+1);
	if(urlCheck=='InternalPayment'){
	}else{
	
	urlLocation=urlLocation.split('?');
	urlLocation=urlLocation[1].split('=');
	var values=urlLocation[1].split('~');
		
		var id=values[0];
		var offer=values[1];
		var amt=values[2];
		var payNo=values[3];
	
	var params='{"userId":"'+id+'","amount":"'+amt+'","offer":"'+offer+'","type":"'+offer+'","paidNo":"'+payNo+'"}';
	var url=urlForServer+"payment/makePayment";
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
    	},
		type : 'POST', 
		url : url,
		data : params,
		success : function(responseText){
		//////////////////
		/*var payNo1=payNo.replace('#',"");
		var params='{"paidNo":"'+payNo1+'","userId":"'+id+'"}';
		var url=urlForServer+ "payment/updatePayNo";
		$.ajax({
		headers: { 
	    	"Ajax-Call" : userTokens,
	    	"Ajax-Time" :userLoginTime				
	    	},
			type : 'POST', 
			url : url,
			data : params,
			success : function(responseText){
			},error:function(){
				
			}
		});*/
		////////////////////////	
		},error:function(){
			
		}
	});
	}
	}

function addInternalPayment(){
	var url=window.location.href;
	url=decodeURIComponent(url);
	if(url.indexOf('?')!=-1){
		
	var paidUser=url.substring(url.lastIndexOf('=')+1,url.indexOf('~'));
	var queryString=url.substring(url.indexOf('~')+1);
	var split=queryString.split('~');
	var d1=split[0];
	var userId=split[0];
	if(userId.match("HomepageLink")){
		//do nothing
	}else{
		
		
	var offerValue=split[1];
	var amount=split[2];
	var payNo=split[3];
	var internalPayment=split[4];
	
	//if(internalPayment.match('InternalPayment')){
		
	var params='{"paidUser":"'+paidUser+'","userId":"'+userId+'","amount":"'+amount+'","offer":"'+offerValue+'","type":"'+offerValue+'","paidNo":"'+payNo+'"}';
	var url=urlForServer+"payment/renewalPayment";
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn						
    	},
		type : 'POST', 
		url : url,
		data : params,
		success : function(responseText){
		},error:function(){
			
		}
	});
	
	}
	
	
	}
	
	}

/*var previousHomeLog="Active";
window.clearInterval(autoContactDetailsFetchTimer);
setInterval(function()
		{
			if(userSession!=previousHomeLog)
				{
					previousHomeLog=userSession;
					window.clearInterval(autoFriendRequestGetForHeader);
					autoFriendRequestGetForHeader=setInterval(function(){myTimer();},updateTime);
				}
		},
30000);*/

function timerNew(){
	setInterval(function(){myTimerHome();},60000);
}
function myTimerHome(){
	if(userSession=='Active')
	{
	console.log("called timer");
	fetchUsers();
	fetchRemainders();
	getrecentActivity();
	fetchNotifications();
	getRequests();
	}
}

function mnQurtz(){
	
	autoGenerateTokenId=setInterval(function(){mnQuartzCall();},3600000);
}


function mnQuartzCall()
{
	if(userSession=='Active')
	{
	console.log("after 5 min token gen called");
	var url = urlForServer+"login/toGen";
	var datastr = '{"userId":"'+userId+'","musicnoteIn":"'+musicnoteIn+'"}';	
	emailStr=datastr;
	//console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST', url : url, data : params,
		success : function(responseText){
			if(responseText!=null && responseText!=""){
				userTokens=responseText;
			}
		}
	});
	}
}

function loginAuthendication(userName,password){
	
	 var url = urlForServer+"login/userlogin";
		var datastr = '{"loginId":"'+escape(userName)+'","password":"'+escape(password)+'"}';	
		emailStr=datastr;
		console.log("<-------Sdatastr -------> "+ datastr);
		var params = encodeURIComponent(datastr);
		var musicnoteAn=$.base64.encode(url);
		$.ajax({
	headers: { 
			"Mn-Callers" : musicnoteAn				
			},
			type : 'POST', url : url, data : params,
			success : function(responseText){
			var data = jQuery.parseJSON(responseText);
			userId=data.userId;
			
			if(data.userId=="0"){
				$("#signinspan").text("Incorrect User Name/Password").css({"color":"red","font-weight": "bold"});
			}
			else if(data.userId=="blocked")
			{
				$("#signinspan").text("Your account is temporarily suspended due to a violation of our Terms of Service.").css({"color":"red","font-weight": "bold"});
			}
			else if(data.userId.indexOf("expire") != -1){
				$("#signinspan").text("Your Trial period Expired").css({"color":"red","font-weight": "bold"});
				// comment by Ramaraj for beta purpose
				/*var renwalId=data.userId.split('~');
				renwalId=renwalId[1];
				userId=renwalId;
				$("#signinspan").text("");
				//$("#signinspan").append('<a id="renewal"  href="http://musicnoteapp.com/musicnote/Payment.html?id=Renewal~'+renwalId+'" style="color:red; font-weight:bold;">Your Trail period Expired Click here to Pay Now!</a>');
				$("#signinspan").append('<a id="renewal"  href="http://localhost:8080/musicnote/Payment.html?id=Renewal~'+renwalId+'" style="color:red; font-weight:bold;">Your Trail period Expired Click here to Pay Now!</a>');*/
			}
			else if(data.userId.indexOf("mailcheck")!=-1)
			{
				musicnote=data.token;
				musicnoteIn=data.datess;	
				if(is_chrome || is_safari)
				{
					$.jStorage.set("lkeyX", userId);
				    $.jStorage.set("tkeyY", musicnote);
				    $.jStorage.set("ltkeyZ", musicnoteIn);
				}
				else
					 document.cookie=userId+"~"+musicnote+"~"+musicnoteIn;
				
				$("#emailVerify").hide();
				$("#emailButton").show();
				$("#emailCode").empty();
				$("#emailCode").hide();
				$("#emailVerify-message").empty();
				$('#emailVerifyModal').modal('show');
				$("#emailVerify-header-span").text("Email Verification");
				$("#msg-modal-body").after('<input id="emailCode" name="emailCode" style="margin-left:10px;font-family: Helvetica Neue;font-size:14px; width:250px;" type="text" class="input-block-level" placeholder="Enter Verification code" />');
				$('#verifyResetButtons').attr('disabled',false);
				$("#verifyResetButtons").show();
				//$(".emailVerifyResetButton").append('');
				//$(".modal-footer").append('<button class="modalBtn fontStyle" id="emailButton" >OK</button>');
				//$("#emailVerify").click(function(){$('#emailVerifyModal').modal('hide');});
				//$("#signinspan").text("Your account is not yet activated. Please follow the link sent to validate your account").css({"color":"red","font-weight": "bold"});
				
					/*$('#emailverifyId').val("");
					$("#emailCode").hide();
					$("#verifyResetButton").hide();
					$("#emailButton").hide();
					$("#emailVerify").hide();
					$("#emailVerify-message").empty();
					$("#resetemptyerror").empty();
					$('#emailverifysubmitButton').attr('disabled',false);
					//$("#emailVerify-message").text("field cannot be blank").css({"color":"red","font-weight": "bold"});
					//$("#emailverifyId").remove();
					//$("#emailverifysubmitButton").remove();
					$("#emailresetVerify-message").show();
					
				});*/
				
			}
			else if(data.userId!="0"){
				stuNoti=data.adminVideoFlag;
				musicnote=data.token;
				musicnoteIn=data.datess;
				userId=data.userId;
				userMailId=data.userMailId;
				fetchUsersList();
				//fetchTrailCount();
				
				if(is_chrome || is_safari)
					{
				    $.jStorage.set("lkeyX", userId);
				    $.jStorage.set("tkeyY", musicnote);
				    $.jStorage.set("ltkeyZ", musicnoteIn);
					}
				else
				    {
				    document.cookie=userId+"~"+musicnote+"~"+musicnoteIn;
				    }
	               
				console.log("Login response-->>"+data.userId);
				loginauthenticationcheck();
				fetchUsers();
				doUserAppDetils();
				fetchRemainders();
				getrecentActivity();
				fetchNotifications();
				getRequests();
				app.navigate("home", {trigger: true});
				setTimeout(function(){
					 if(data.studentCheck.match('true')){
						 stuFirst='true';
							app.navigate("profile", {trigger: true});
							$('#securityQuestion').attr('style','display:block;margin-top:10px;');
							}
					 else
						 stuFirst='false';
					 
					},500);
				if(!data.studentCheck.match('true'))
				{
			 if(data.adminVideoFlag.match('true')) // this is used to check notification on any demo video updated by admin
				 $("#alertMessages").modal('show');
				}
				$("#main").hide();
				$("#sidecontent").show();
				$('#footer').show();
				$('#header').show();
				$(homeBodyId).css( "background-color","#eaedef");
				timerNew();
				//setTimeout(mnQurtz(),60000);
				mnQurtz();
				$('#welcomemsg').attr('style','display:block;font-family: Helvetica Neue;font-size:14px;');
				
				var userSharedIdString =data.sharedIdSet;
				
				if(userSharedIdString.indexOf("[") != -1){
					userSharedIdString = userSharedIdString.substring(userSharedIdString.indexOf("[")+1,userSharedIdString.length);
				}
				if(userSharedIdString.indexOf("]") != -1){
					userSharedIdString = userSharedIdString.substring(0,userSharedIdString.indexOf("]"));
				}
				if(userSharedIdString.indexOf(",") != -1){
					userIdSharedIdSetArray = userSharedIdString.split(',');
				
				}else{
					if(userSharedIdString.trim() != ''){
						userIdSharedIdSetArray = userSharedIdString.split(',');
					}
				}
				
				if(data.requestPending){
					var userIdSetArray= new Array();
					var divFrdAc = "";
							var userIdString =data.userIdSet;
							if(userIdString.indexOf("[") != -1){
								userIdString = userIdString.substring(userIdString.indexOf("[")+1,userIdString.length);
							}
							if(userIdString.indexOf("]") != -1){
								userIdString = userIdString.substring(0,userIdString.indexOf("]"));
							}
							if(userIdString.indexOf(",") != -1){
								userIdSetArray = userIdString.split(',');
							
							}else{
								if(userIdString.trim() != ''){
									userIdSetArray = userIdString.split(',');
								}
							}
							
							
							if(userIdSetArray.length > 0 && userIdSetArray[0]!=null ){
								setTimeout(function(){
									divFrdAc = createFriendAceeptDivByMail(userIdSetArray,userIdSharedIdSetArray);
									$('#mailSharingModel').children('.modal-dialog').children('.modal-content').children('.modalBody').children().append("<br>"+divFrdAc+"<br><br>");
									$('#mailSharingModel').modal('show');
								},1000);
							}
						}
				/*else
						{
				if(userIdSharedIdSetArray.length > 0 && userIdSharedIdSetArray[0]!=null ){
					setTimeout(function(){
						var divFrdES = createEventAceeptDiv(userIdSharedIdSetArray);
						//$('#mailSharingModel').children('.modalBody').children().append("<br>"+divFrdES+"<br><br>");
						//$('#mailSharingModel').modal('show');
					},1000);
				}
			  }*/
				
				
			}
			
		}
});
		
}



function userLogout(){
	/*if(FBuserLogin){
		alert('login via FB=='+FBuserLogin);
		  FB.getLoginStatus(function(response) {
			  alert('FB getLoginStatus=='+response.status);
			  alert('@@FB getLoginStatus=='+response);
		        if (response && response.status === 'connected') {
		        	 alert('trueeee if enter');
		        	 alert(JSON.stringify(response));
		        	 FB.Event.subscribe('auth.logout', function (response) {
		        		 alert('FB.Event.subscribe dfd logout');
		        		});
		          
		        	 url= "https://www.facebook.com/logout.php?next=" + 'https://www.musicnoteapp.com/musicnote1/index.html' + "&access_token=" + response.authResponse.accessToken;
		        	 $.ajax({
		     				type : 'POST', url : url,
		     				success : function(responseText){
		     					 document.location.reload();
		     				},error: function(e){
		     					 document.location.reload();
		     				}
		     			});
		     		
		        	 
		        	 
		        	 
		           
		        }
		    });
		  FB.logout(function(response) {
          	alert('FB.logout');
             
              FB.Auth.setAuthResponse(null, 'unknown');
          });
		}*/
		
		
		
		
		
	/*FB.logout(function(response) {
		//alert(response);
		//alert(JSON.stringify(response));
		  FB.Auth.setAuthResponse(null, 'unknown');
    // Person is now logged out
	 });
	}*/
	 if(is_chrome || is_safari)
	 {
		 $.jStorage.set("lkeyX", "");
		 $.jStorage.set("tkeyY", "");
		 $.jStorage.set("ltkeyZ", "");
	 }
	 
	 else
	     document.cookie="";    
	 console.log("Document cookie after logout"+document.cookie);
	 
	 clearInterval(autoFriendRequestGetForHeader);
		 
	 window.location.replace("./index.html");
}

function getRequests(){

	 var url = urlForServer+"friends/requestedfriends";
		var datastr = '{"userId":"'+userId+'","requestId":"1"}';	
		emailStr=datastr;
		var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
			type : 'POST', url : url, data : params,
			success : function(responseText){
			var datasa=responseText.split("&");
			var count=0;
			
				var userses= jQuery.parseJSON(datasa[0]);
				if( userses!=null && userses!=""){
					
				for (var i=0; i<userses.length; i++)
				{
					count=count+1;
					console.log(userses[i]);
					var user=userses[i];
				}
				
				if(count>0)
				{
				$('#fndrequests').parent().prepend('<span class="on"><span>'+count+'</span></span>');
				}else
				{
				$('#fndrequests').parent().children('.on').remove();
				$('#userslists').append('<li><div><label id="addFriend" onclick="javascript:addFriend();" class="offset1" >Add Contacts</label></div></li>');
				}
				
			}
				else
				{
					$('#fndrequests').parent().children('.on').remove();
				}
		}
		});
	
}


//

$("#fndrequests").click(function(){
var url = urlForServer+"friends/requestedfriends";
	var datastr = '{"userId":"'+userId+'","requestId":"1"}';	
	emailStr=datastr;
	//console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST', url : url, data : params,
		success : function(responseText){
		var datasa=responseText.split("&");
		var count=0;
		//document.getElementById("requestlable").innerHTML = datasa[datasa.length-1];
		
			var userses= jQuery.parseJSON(datasa[0]);
			if( userses!=null && userses!=""){
				//console.log("after parse"+userses);
				$('#userslists').find('li').each(function(index){
					$(this).remove();
				});
			for (var i=0; i<userses.length; i++)
			{
				count=count+1;
				var user=userses[i];
				$('#userslists').append('<li id=li'+user.requestedUserId+' class="responseSelectedUser"><a>'+user.name+'</a><div  id="'+user.requestedUserId+'">'
		    			+' <a class="btn btn-primary responseUser" href="javascript:void(0);"> Accept </a> <a class="btn btn-primary requestDecline" href="javascript:void(0);"> Decline </a></div></li>');
			}
			//$('#userslists').append('<a id="addFriend" onclick="javascript:addFriend();" class="offset1" >Add Contacts</a>');
			
		}
			else
			{
				
				$('#userslists').find('label').each(function(index){
					$(this).remove();
				});
				$('#userslists').append('<li><div><label id="addFriend" onclick="javascript:addFriend();" class="offset1" style="margin-left:10px" >Add Contacts</label></div></li>');
			}
	}
	});
});




//
$("#notification").click(function(){
	
	fetchNotifications();
	var notificatIdList= new Array();
	//$('#notificationlists').children().remove();
	$('#notificationlists').children().each(function( index ){
					$(this).remove();
	});
	var contentDivPopNotif = '';
	if(data!=null && data!=''){
		 contentDivPopNotif = '<li><div class="uiHeaderBottomBorder jewelHeader"><label class="">Notifications</label></div></li>';
		for ( var i = 0; i < data.length; i++) {
			var listId=data[i].listId;
			var noteId=data[i].noteId;
			var desc=data[i].logDescription;
			var status=data[i].status;
			//var descdata=data[i].logDescription;
			
			if(data[i].notUserId == userId){
				if(data[i].status =="A"){
					notificatIdList.push(data[i].logId.trim());
				}
				var convertedDate=new Date(data[i].date);
				
				//var convertedDate= new Date(data[i].date).toLocaleFormat("%b/%d/%Y"); 
				
				var dateTex=month[convertedDate.getMonth()];
				var userDetailsName = '';
				var userDetailsInitials='';
				var date=dateTex+" "+convertedDate.getDate()+" "+convertedDate.getFullYear();	
				var userDetailsJsonObj = activeUserObjectMap[data[i].userId];
				var timeString=data[i].time.split(":");
				var timme=convertTwentyFourHourToTwelveHourTime(timeString[0]+":"+timeString[1]);
				if(userDetailsJsonObj != null  && userDetailsJsonObj !=''){
					userDetailsName = userDetailsJsonObj['userFirstName']+userDetailsJsonObj['userLastName'];
					userDetailsInitials=(userDetailsJsonObj['userFirstName']).substring(0,1).toUpperCase()+(userDetailsJsonObj['userLastName']).substring(0,1).toUpperCase();
				}else{
					userDetailsName = userFulName;
					userDetailsInitials=userInitial;
				}
				
				var desc=data[i].logDescription;
				var A='A';
				var desc_lowercase=desc.toLowerCase();
				if(desc_lowercase.match('shared a schedule')){
					if(desc.charAt(0)!='X'){
					desc=data[i].logDescription
					+'<div id="addEve" style="font-size: 12px;height:20px;padding-top:1px" class="'+listId+''+A+''+noteId+' btn btn-success" onclick="javascript:addEve('+listId+','+noteId+');" >Accept</div>'
					+'&nbsp;&nbsp;&nbsp;'
					+'<div id="decEve" style="font-size: 12px;height:20px;padding-top:1px"  class="'+listId+''+A+''+noteId+' btn btn-danger" onclick="javascript:decEve('+listId+','+noteId+');" >Decline</div>';
				}else{
					desc=data[i].logDescription.substring(1);
				}
				}else{
					desc=data[i].logDescription;
				}
				contentDivPopNotif = contentDivPopNotif + '<li id='+data[i].logId+'><div class="phenom clearfix notificationLink '+data[i].pageName+' " style="font-size: 12px;">'
												+'<div class="creator member js-show-mem-menu" style=" left: 5px;">'
													+'<span class="member-initials" title="'+userDetailsName+'">'+userDetailsInitials+'</span>'
												+'</div>'
												+'<div class="phenom-desc" style="white-space: normal;">'
												//if(data[i].pageType == 'schedule' || data[i].pageType == 'music' || data[i].pageType == 'bill'){
												//	contentDivPopNotif = contentDivPopNotif +'<a href="#" class="'+data[i].noteId+'-'+data[i].listId+'">'+data[i].logDescription+'</a>'
												//}else{
													+'<p style="margin: 0 0 0px;">'+desc+'</p>'
												//}
												+'</div>'
												+'<p class="phenom-meta quiet">'
													+'<span class="date" style="font-size: 10px;" dt="'+date+'" title="'+date+' at '+timme+'">'+date+' at '+timme+' </span>'
												+'</p>'
											+'</div></div></li>';
			}
		}
				
		if(data.length > 0){
			var styleAttr=" width: 550%;border-radius:6px;";
			$('#notificationlists').append(contentDivPopNotif);	
			$('#notificationlists').removeAttr("style");
			$('#notificationlists').attr("style", styleAttr);
			if(notificatIdList.length > 0 && notificatIdList[0]!= null ){
				var params = encodeURIComponent(notificatIdList);
				var url = urlForServer+"log/inactiveNotification/"+userId;
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type : 'POST',
					url : url, 
					data : params,
					success : function(responseText){
						if(responseText =='success'){
							$("#notification").parent().find('.on').remove();
						}
						
					},error: function(e){
						console.log("<----  notification fails  -----> "+e);
					}
				});	
			}
		}
		
		
				
	}	
	else
	{
		 contentDivPopNotif = '<li><div class="uiHeaderBottomBorder jewelHeader" style ="border:none;"><label class="">Notifications</label></div></li>';
		$('#notificationlists').append(contentDivPopNotif);	
	}
});

var data;
function fetchNotifications(){
	
	var url = urlForServer+"log/getNotifications/"+userId;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		url : url, 
		success : function(responseText){
			data = jQuery.parseJSON(responseText);
			if(data!=null && data!=''){
			var count = 0;
				for ( var i = 0; i < data.length; i++) {		
					if(data[i].notUserId == userId && data[i].status =="A"){
						count++;
					}
				}
				if(data.length > 0){
					$('#notification').parent().children('.on').remove();
					if(count!= 0 ){
						$('#notification').parent().prepend('<span class="on"><span>'+count+'</span></span>');
					}
				}
				
			}
					
		},
		error: function(e){
			if(userSession=="Active")
			alert("Please try again later");
		}
	});
}
var remData;
function fetchRemainders(){
	$('#reminders').parent().find('.on').remove();
	var url = urlForServer+"log/getRemainder/"+userId;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type: 'POST',
		url : url,
		cache: false,
		contentType: "application/json; charset=utf-8",
		success : function(response){
			remData = jQuery.parseJSON(response);
			var count= 0;
			if(remData!= null && remData!=''){
				for ( var i = 0; i < remData.length; i++) {	
					var inactiveRemainderUserIds =new Array();
					var inactiveRemainderUserIdsWithParam;
					var selectedDate=new Date(remData[i].eventDate);
					var date=month[selectedDate.getMonth()];
					date=date+" "+selectedDate.getDate()+" "+selectedDate.getFullYear();
				
					var today = new Date();
					var toDate = month[today.getMonth()];
					toDate = toDate+" "+today.getDate()+" "+today.getFullYear();
					
					inactiveRemainderUserIdsWithParam=remData[i].inactiveRemainderUserId;
					if(inactiveRemainderUserIdsWithParam!=null){
						if(inactiveRemainderUserIdsWithParam.indexOf("[") != -1){
							inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(inactiveRemainderUserIdsWithParam.indexOf("[")+1,inactiveRemainderUserIdsWithParam.length);
						}
						if(inactiveRemainderUserIdsWithParam.indexOf("]") != -1){
							inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(0,inactiveRemainderUserIdsWithParam.indexOf("]"));
						}
						if(inactiveRemainderUserIdsWithParam.indexOf(",") != -1){
							inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
						}else{
							if(inactiveRemainderUserIdsWithParam.trim() != ''){
								inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
							}
						}
						if(inactiveRemainderUserIds.length > 0 && inactiveRemainderUserIds[0] != null){
							if(inactiveRemainderUserIds.indexOf(userId) >= 0){
							
							}else{
								if(toDate == date)
									count++;
							}
						}else{
							if(toDate == date)
								count++;
						}
					}
					/*if(remData[i].status =="A" && toDate == date){
						
						count++;
					}*/
					if( remData[i].status =="A"){
						remindersMap[remData[i].rId]=remData[i];
					}
				}
				if(count!= 0 ){
					$('#reminders').parent().find('.on').remove();
					$('#reminders').parent().prepend('<span class="on"><span>'+count+'</span></span>');
				}
			}
		},
		error: function(e){
			if(userSession=="Active")
			alert("Please try again later");
		}
	});
}
$("#reminders").click(function(){

	var tempArray= new Array();
	$('#reminderslists').children().remove();
	var contentDivPopRemaind = '';
	if(remData!=null && remData!=''){
		var flag=false;
		contentDivPopRemaind = '<li><div class="uiHeaderBottomBorder jewelHeader"><label class="">Reminders</label></div></li>';
		for ( var i = 0; i < remData.length; i++) {		
		//	if(remData[i].userId == userId){
				var inactiveRemainderUserIds =new Array();
				var inactiveRemainderUserIdsWithParam;
				
				var convertedDate=new Date(remData[i].eventDate);
				var date=month[convertedDate.getMonth()];
				date=date+" "+convertedDate.getDate()+" "+convertedDate.getFullYear();
				
				var today = new Date();
				var toDate = month[today.getMonth()];
				toDate = toDate+" "+today.getDate()+" "+today.getFullYear();
				
				inactiveRemainderUserIdsWithParam=remData[i].inactiveRemainderUserId;
				if(inactiveRemainderUserIdsWithParam!=null){
					if(inactiveRemainderUserIdsWithParam.indexOf("[") != -1){
						inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(inactiveRemainderUserIdsWithParam.indexOf("[")+1,inactiveRemainderUserIdsWithParam.length);
					}
					if(inactiveRemainderUserIdsWithParam.indexOf("]") != -1){
						inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(0,inactiveRemainderUserIdsWithParam.indexOf("]"));
					}
					if(inactiveRemainderUserIdsWithParam.indexOf(",") != -1){
						inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
					}else{
						if(inactiveRemainderUserIdsWithParam.trim() != ''){
							inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
						}
					}
					if(inactiveRemainderUserIds.length > 0 && inactiveRemainderUserIds[0] != null){
						if(inactiveRemainderUserIds.indexOf(userId) >= 0){
							flag= true;
							contentDivPopRemaind = contentDivPopRemaind + '<li id='+remData[i].rId+'>'
							+'<div class="clearfix" style="font-size: 12px;border-bottom: 1px solid #DCDCDC;white-space: normal;margin: 0 9px;">'
								+'<p style="margin: 0 0 0px;" ><h5><B>'+remData[i].rName+'</B></h5> on <B>'+date +'</B> at <B>'+remData[i].eventTime+'</B> added in <B>'+ remData[i].noteName+'</B> </p>'
							+'</div></li>';
						}else{
							if(remData[i].status =="A" && toDate == date){
								if(tempArray.indexOf(remData[i].rId) == -1){
									flag= true;
									tempArray.push(remData[i].rId);
									contentDivPopRemaind = contentDivPopRemaind + '<li id='+remData[i].rId+'>'
									+'<div class="clearfix" style="font-size: 12px;border-bottom: 1px solid #DCDCDC;white-space: normal;margin: 0 9px;">'
										+'<p style="margin: 0 0 0px;" ><h5><B>'+remData[i].rName+'</B></h5> on <B>'+date +'</B> at <B>'+remData[i].eventTime+'</B> added in <B>'+ remData[i].noteName+'</B> </p>'
									+'</div></li>';
								}	
							}
						}
					}else{
						if(remData[i].status =="A" && toDate == date){
							if(tempArray.indexOf(remData[i].rId) == -1){
								flag= true;
								tempArray.push(remData[i].rId);
								contentDivPopRemaind = contentDivPopRemaind + '<li id='+remData[i].rId+'>'
								+'<div class="clearfix" style="font-size: 12px;border-bottom: 1px solid #DCDCDC;white-space: normal;margin: 0 9px;">'
									+'<p style="margin: 0 0 0px;" ><h5><B>'+remData[i].rName+'</B></h5> on <B>'+date +'</B> at <B>'+remData[i].eventTime+'</B> added in <B>'+ remData[i].noteName+'</B> </p>'
								+'</div></li>';
							}	
						}
					}
				}
				
				
				
			//}	
		}
		var styleAttr=" width: 800%;";
		if(flag){
			$('#reminderslists').append(contentDivPopRemaind);	
			$('#reminderslists').removeAttr("style");
			$('#reminderslists').attr("style", styleAttr);
		}else{
			contentDivPopRemaind = '<li><div class="uiHeaderBottomBorder jewelHeader" style="border:none;"><label class="">Reminders</label></div></li>';
			$('#reminderslists').append(contentDivPopRemaind);	
		}
		if(tempArray.length > 0 && tempArray[0]!= null ){
			var params = encodeURIComponent(tempArray);
			var url = urlForServer+"log/inactiveRemainders/"+userId;
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url, 
				data : params,
				success : function(responseText){
					if(responseText =='success'){
						$('#reminders').parent().find('.on').remove();
						//$(this).parent().parent().parent().remove();
						//fetchRemainders();
					}
				},error: function(e){
					console.log("<----  remainder fails  -----> "+e);
				}
			});
		}
	}	
	else
	{
		contentDivPopRemaind = '<li><div class="uiHeaderBottomBorder jewelHeader" style="border:none;"><label class="">Reminders</label></div></li>';
		$('#reminderslists').append(contentDivPopRemaind);	
	}

});


$('#reminderslists').on('click','.js-inactive-remainder',function(){
	var tempArray = new Array();
	tempArray.push($(this).parent().parent().parent().attr('id'));
		if(tempArray.length > 0 && tempArray[0]!= null ){
			var params = encodeURIComponent(tempArray);
			var url = urlForServer+"log/inactiveRemainders/"+userId;
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url, 
				data : params,
				success : function(responseText){
					if(responseText =='success'){
						$('#reminders').parent().find('.on').remove();
						$(this).parent().parent().parent().remove();
						fetchRemainders();
					}
				},error: function(e){
					console.log("<----  remainder fails  -----> "+e);
				}
			});
		}
});
function fetchTrailCount(){
	var datastr = '{"userId":"'+userId+'"}';
	var params = encodeURIComponent(datastr);
	var url = urlForServer+"user/getTrailcount";
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		url : url, data : params,
		success : function(responseText) {
				var data = jQuery.parseJSON(responseText);
				if(data!=null && data!='')
				{
					$('#trailno').text("Trial day "+data);
				}
			
		},
	error : function() 
	{
		console.log("<-------error returned due to trail period expired-------> ");
	}
});

}


function fetchUsersList(){
	var userDetailsJsonObj;
	var datastr = '{"userId":"'+userId+'"}';
	var params = encodeURIComponent(datastr);
	var url = urlForServer+"user/getAllActiveUsersListForSearch";
	
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		url : url, data : params,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
		var dataTextObj;
		 if(data!=null && data!='')
			{
			
				dataTextObj="[";
				for ( var i = 0; i < data.length; i++) {
					var image="";
					userDetailsJsonObj = data[i];
					
					if(userDetailsJsonObj['filePath']!='null' && userDetailsJsonObj['filePath']!="")
						image=userDetailsJsonObj['filePath'];
					else
						image="pics/dummy.jpg";
					
					activeUserDeatilsMap[userDetailsJsonObj['userId']]=userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName'];
					activeUserObjectMap[userDetailsJsonObj['userId']] = userDetailsJsonObj;

					//console.log("get userDetail using user Id-->>>> "+activeUserDeatilsMap[userDetailsJsonObj['userId']]);
					//friendsPhotoMap[userDetailsJsonObj['userId']]=userDetailsJsonObj['filePath'];
					dataTextObj = dataTextObj + "{\"userId\" : \"" + userDetailsJsonObj['userId'] + "\","
					+ "\"userName\" : \"" + userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName'] + "\","
					+ "\"uniquserName\" : \"" + userDetailsJsonObj['userName']+"\","
					+ "\"emailId\" : \"" + userDetailsJsonObj['emailId']+"\","
					+ "\"image\" : \""+image+"\"},";
					 
				}
				if(dataTextObj.lastIndexOf(",")!= -1 && dataTextObj.lastIndexOf(",") !=0 ){
					dataTextObj = dataTextObj.substring(0, dataTextObj.lastIndexOf(","));
				}
				dataTextObj = dataTextObj +"]";
					
			}	
		userDetails = jQuery.parseJSON(dataTextObj);
		
	},
	error : function() {
		console.log("<-------error returned for User details -------> ");
		}
	});   
}

function getrecentActivity(){
	var url = urlForServer+"log/recentActivity/"+userId;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		url : url, 
		success : function(responseText){
			var contentDiv ='<div>';
			var data = jQuery.parseJSON(responseText);
			if(data!=null && data!=''){
					var currentDate = new Date();
					currentDate.setHours(00,-1,00,0);
				for ( var i = 0; i < data.length; i++) {		
					if(data[i].userId == userId && data[i].status =="A"){
						contentDiv = contentDiv + '<p style="font-family: Helvetica Neue;font-size: 14px;"><font class="recentP">';
						if(((data[i].listId!= null && data[i].listId!= '' && data[i].noteId!=null && data[i].noteId!='' )&& 
							(data[i].pageType!= null) && (data[i].pageType=='music' || data[i].pageType=='schedule' || data[i].pageType=='bill' ))){
							contentDiv = contentDiv + '<a style="color:black" id="'+data[i].listId+'~'+data[i].noteId+'" class="js-note-recnt '+data[i].pageType+'" href="javascript:void(0);">';
						}
						var enteredDate = new Date(data[i].date);
						if(currentDate > enteredDate){
							enteredDate=month[enteredDate.getMonth()]+' '+enteredDate.getDate()+' '+enteredDate.getFullYear();
							contentDiv = contentDiv +'<B>'+ enteredDate +': </B>';
						}
						if((data[i].logDescription).indexOf('<br>')!=-1){
							data[i].logDescription=((data[i].logDescription).replace(/\<br\>/g," "));
						}
						
						
						contentDiv = contentDiv +data[i].logDescription+'</font></p>';
						
						if(((data[i].listId!= null && data[i].listId!= '' && data[i].noteId!=null && data[i].noteId!='' )&& 
							(data[i].pageType!= null) && (data[i].pageType=='music' || data[i].pageType=='schedule' || data[i].pageType=='bill' ))){
							contentDiv = contentDiv + '</a>';
						}
					}
				}
			}
			contentDiv = contentDiv + '</div>';
			$('#recentActivity').children().each(function( index ){
					$(this).remove();
			});
			$('#recentActivity').append(contentDiv);
		},error : function(e){
		
		}		
	});
	
}

function registerUser(){
	
	$("#role").append('<option value="Music Teacher">Music Teacher</option>');
	$("#role").append('<option value="Music Student">Music Student</option>');
	$("#role").append('<option value="Musician">Musician</option>');
	$("#role").append('<option value="Other">Other</option>');
	
	
	$("#skillLevel").append('<option value="Beginner">Beginner</option><option value="Intermediate">Intermediate</option><option value="Advanced">Advanced</option>');
	$("#instrument").append('<option value="Bass">Bass</option><option value="Brass">Brass</option><option value="Cello">Cello</option><option value="Drums">Drums</option><option value="Guitar">Guitar</option><option value="Harmonica">Harmonica</option><option value="Harp">Harp</option><option value="Oboe">Oboe</option><option value="Percussion">Percussion</option><option value="Piano/Keys">Piano/Keys</option><option value="Saxophone">Saxophone</option><option value="Trombone">Trombone</option><option value="Trumpet">Trumpet</option><option value="Violin">Violin</option><option value="Vocals">Vocals</option><option value="Woodwinds">Woodwinds</option><option value="Other">Other</option>');
	$("#favoriteMusic").append('<option value="Alternative">Alternative</option><option value="Asian Pop (J-Pop, K-pop)">Asian Pop (J-Pop, K-pop)</option><option value="Bollywood">Bollywood</option><option value="Blues">Blues</option><option value="Carnatic">Carnatic</option><option value="Classical">Classical</option><option value="Country">Country</option><option value="Dance">Dance</option><option value="Easy Listening">Easy Listening</option><option value="Electronic">Electronic</option><option value="European Music (Folk / Pop)">European Music (Folk / Pop)</option><option value="Folk">Folk</option><option value=" Hip Hop / Rap"> Hip Hop / Rap</option><option value="Indian Classical">Indian Classical</option><option value="Indie Pop">Indie Pop</option><option value="Inspirational (incl. Gospel)">Inspirational (incl. Gospel)</option><option value="Jazz">Jazz</option><option value="Latin Music">Latin Music</option><option value="New Age">New Age</option><option value="Opera">Opera</option><option value="Pop">Pop</option><option value="R&B / Soul">R&B / Soul</option><option value="Reggae">Reggae</option><option value="Rock">Rock</option><option value="Singer / Songwriter">Singer / Songwriter</option><option value="World Music">World Music</option><option value="Other">Other</option>');

	$("#main").show();
	$("#sidecontent").hide();	
}


function resetPassword(){
	
	if($("#cfrmpassword").val().trim()!= null && $("#cfrmpassword").val().trim()!= '' && $("#newpassword").val().trim()!= null && $("#newpassword").val().trim()!= '' && $("#oldPassword").val().trim()!= null && $("#oldPassword").val().trim()!= '' && newPassWarningFlag==false){
		$("#cfrmpasswordDiv").attr("style", "dispaly:none");
		$("#cfrmpasswordDiv").text("");
		$("#newpasswordDiv").attr("style", "dispaly:none");
		$("#newpasswordDiv").text("");
		$("#oldPasswordDiv").attr("style", "dispaly:none");
		$("#oldPasswordDiv").text("");
		if($("#newpassword").val().trim()==$("#cfrmpassword").val().trim())
		{
			
			$("#cfrmpasswordDiv1").attr("style", "dispaly:none");
			$("#cfrmpasswordDiv1").text("");
			
			
			  //$('#oldpasswordId').parent().parent().find('.form-signin-heading').remove();
			var url=urlForServer+"user/resetPassword";
			var params = "{\"userId\":\""+userId+"\",\"crfmPassword\":\""+$("#cfrmpassword").val()+"\",\"oldPassword\":\""+$("#oldPassword").val()+"\"}";
			
			params = encodeURIComponent(params);
			
			
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
			    type: 'POST',
			    url : url,
			    cache: false,
			    contentType: "application/json; charset=utf-8",
			    data:params, 
			    dataType: "json",
			    success : function(response){
				 

					if(response!= null && response.status!='' && response.status =='success' ){
						$('#msgModal1').modal('show');
						///$("#schedulestatusicon1").remove();
						//$("#msgfooterbtn1").prepend('<i class="icon-white icon-time" id="schedulestatusicon1"></i>');
						//$("#msgfooterbtn1").attr('class','btn btn-info');
						$("#msg-header-span1").text("Reset Password");
						$("#modal-message1").text("  Password Reset Successfully");
						$("#schedulestatusicon1").remove();
						$("#msgfooterbtn1").prepend('<i class="icon-white icon-ok" id="schedulestatusicon1"></i>');
						$("#msgfooterbtn1").attr('class','btn btn-success');
						$("#oldPasswordDiv1").attr("style", "dispaly:none");
						$("#oldPasswordDiv1").text("");
						$("#oldPassword").val("");
						$("#newpassword").val("");
						$("#cfrmpassword").val("");
						setTimeout(function(){ $('#msgModal1').modal('hide');},3000);
						/*setTimeout(function(){
							$('#main').hide();
							$('#sidecontent').show();
							$('#footer').show();
							$('#header').show();
							app.navigate("home", {trigger : true});
						},55002);*/
						
						
					}
					//block resetpassword in not mailId and Security Question
					else if(response!= null && response.status!='' && response.status =='notSuccess' )
					{
						$("#oldPassword").val("");
						$("#newpassword").val("");
						$("#cfrmpassword").val("");
						$("#resetPasswordNotSupport").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;margin-left: 18px;");
						$("#resetPasswordNotSupport").text("You must update your Profile before you can reset your password. Profile must include an email address or security questions to continue. ").css({"color":"red"});
					}
					else{
						
					/*	$("#msgfooterbtn1").prepend('<i class="icon-white icon-time" id="schedulestatusicon1"></i>');
						$("#msgfooterbtn1").attr('class','btn btn-info');
						$("#msg-header-span1").attr('class',"label label-important").text("info");
						$("#modal-message1").attr('class',"label label-important").text("  Password not reset");
						$("#schedulestatusicon1").remove();
						$("#msgfooterbtn1").prepend('<i class="icon-white icon-ok" id="schedulestatusicon1"></i>');
						$("#msgfooterbtn1").attr('class','btn btn-important');*/
						$("#oldPasswordDiv1").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
						$("#oldPasswordDiv1").text("Wrong password").css({"color":"red"});
						
					}
					
				
					
				},error: function(e){
					alert(e);
					var data = jQuery.parseJSON(e);
					alert(data);
					if(e!= null && e =="success"){
						$("#msgfooterbtn1").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon1"></i>');
						$("#msgfooterbtn1").attr('class','btn btn-info');
						$("#msg-header-span1").text("Reset Password");
						$("#modal-message1").text("  Password Reset successfully");
						$("#schedulestatusicon1").remove();
						$("#msgfooterbtn1").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon1"></i>');
						$("#msgfooterbtn1").attr('class','btn btn-success');
						$("#oldPasswordDiv1").attr("style", "dispaly:none");
						$("#oldPasswordDiv1").text("");
						$("#oldPassword").val("");
						$("#newpassword").val("");
						$("#cfrmpassword").val("");
						setTimeout(function(){ $('#msgModal1').modal('hide');},3000);
						setTimeout(function(){
							$('#main').hide();
							$('#sidecontent').show();
							$('#footer').show();
							$('#header').show();
							app.navigate("home", {trigger : true});
						},3002);
						
					}else{
						/*$("#msgfooterbtn1").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon1"></i>');
						$("#msgfooterbtn1").attr('class','btn btn-info');
						$("#msg-header-span1").attr('class',"label label-important").text("info");
						$("#modal-message1").attr('class',"label label-important").text(" Wrong password so password not reset");
						$("#schedulestatusicon1").remove();
						$("#msgfooterbtn1").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon1"></i>');
						$("#msgfooterbtn1").attr('class','btn btn-important');*/
						
						$("#oldPasswordDiv1").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
						$("#oldPasswordDiv1").text("Wrong password").css({"color":"red"});
						
					}
					
				}
			});
			
		}
		else
		{
			$("#cfrmpasswordDiv1").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
			$("#cfrmpasswordDiv1").text("Password mismatch").css({"color":"red"});
		}
      
		
		
	}
	else
	{ 
		if($("#oldPassword").val().trim()!= null && $("#oldPassword").val().trim()!= '')
		{
			$("#oldPasswordDiv").attr("style", "dispaly:none");
			$("#oldPasswordDiv").text("");
			$("#resetPasswordNotSupport").attr("style", "dispaly:none");
			$("#resetPasswordNotSupport").text("");
		}
		else
		{ 
			$("#oldPasswordDiv").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
			$("#oldPasswordDiv").text("Field cannot be blank").css({"color":"red"});
			$("#resetPasswordNotSupport").attr("style", "dispaly:none");
			$("#resetPasswordNotSupport").text("");
		}
		
		if($("#newpassword").val().trim()!= null && $("#newpassword").val().trim()!= '')
		{
			$("#newpasswordDiv").attr("style", "dispaly:none");
			$("#newpasswordDiv").text("");
			$("#resetPasswordNotSupport").attr("style", "dispaly:none");
			$("#resetPasswordNotSupport").text("");
		}
		else
		{
			$("#newpasswordDiv").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
			$("#newpasswordDiv").text("Field cannot be blank").css({"color":"red"});
			$("#resetPasswordNotSupport").attr("style", "dispaly:none");
			$("#resetPasswordNotSupport").text("");
		}
		
		if($("#cfrmpassword").val().trim()!= null && $("#cfrmpassword").val().trim()!= '')
		{
			$("#cfrmpasswordDiv").attr("style", "dispaly:none");
			$("#cfrmpasswordDiv").text("");
			$("#resetPasswordNotSupport").attr("style", "dispaly:none");
			$("#resetPasswordNotSupport").text("");
		}
		else
		{
			$("#cfrmpasswordDiv1").attr("style", "dispaly:none");
			$("#cfrmpasswordDiv1").text("");
			$("#cfrmpasswordDiv").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
			$("#cfrmpasswordDiv").text("Field cannot be blank").css({"color":"red"});
			$("#resetPasswordNotSupport").attr("style", "dispaly:none");
			$("#resetPasswordNotSupport").text("");
		}
		if($("#oldPassword").val().trim()!= null && $("#oldPassword").val().trim()!= '' && $("#cfrmpassword").val().trim()!= null && $("#cfrmpassword").val().trim()!= '' && $("#newpassword").val().trim()!= null && $("#newpassword").val().trim()!= ''){
			if(newPassWarningFlag==true){
				$("#resetPasswordNotSupport").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
				$("#resetPasswordNotSupport").text("Short passwords are easy to guess. Try one with at least 6 characters.").css({"color":"red"});
			}else{
				$("#resetPasswordNotSupport").attr("style", "dispaly:none");
				$("#resetPasswordNotSupport").text("");
			}
		}
	}
}

function convertTwentyFourHourToTwelveHourTime(times)
{
	var timing=times.split(":");
	if(timing[1].length==1)
		timing[1]="0"+timing[1];
	if(parseInt(timing[0])==12)
	{
		cHour=parseInt(timing[0]);
		cMinutes=timing[1]+" PM";
	}
	else if(parseInt(timing[0])>12)
	{
		cHour=parseInt(timing[0])-12;
		cMinutes=timing[1]+" PM";
	} else if(parseInt(timing[0])==0){
		cHour=parseInt(timing[0])+12;
		cMinutes=timing[1]+" AM";
	}
	else{
		cHour=timing[0];
		cMinutes=timing[1]+" AM";
	}
	return cHour+":"+cMinutes;
}
function createFriendAceeptDivByMail(userIdArray,userIdSharedIdSetArray){
	var divFromat = "";
	var text='';
	
	if(userIdSharedIdSetArray.length >0 && userIdSharedIdSetArray[0]!=null ){
		for(var i=0;i<userIdSharedIdSetArray.length;i++){
			var set=userIdSharedIdSetArray[i].split('~');
		}
	}
	
	if(userIdArray.length > 0 && userIdArray[0]!=null ){
		for(var i = 0; i < userIdArray.length; i++){
			var level=userIdArray[i].split('~');
			var userFullName = activeUserDeatilsMap[level[0].trim()];
			if(userFullName=='undefined' || userFullName=='')
				userFullName="User";
			if(level[1]=='copy')
				text='copy of note';
			else if(level[1]=='note')
				text='note';
			else
				text='book';
			divFromat = divFromat +'<div class="span" style="border-bottom: 1px solid #DCDCDC;"><div class="span1"></div>'
										+'<div class="col-md-7">'
											+'<p>'+userFullName+' shared a '+text+' with you. Do you want to see the note(s) accept friend request</p>'
										+'</div><div class="col-md-offset-8">'
											+'<a id="accept~'+level[0].trim()+'~'+level[1]+'" class="'+userIdSharedIdSetArray+'~  btn btn-success mailSharingAcceptDecline" value="Accept">Accept</a>'
											+'&nbsp;&nbsp;<a id="decline~'+level[0].trim()+'~'+level[1]+'" class="'+userIdSharedIdSetArray+'~ btn btn-warning mailSharingAcceptDecline" value="Decline">Decline</a>'
										+'</div></div>';
										
		}
	}
	return divFromat;
}

// method for form a modal panel contents for normal event sharing 
function createEventAceeptDiv(userIdSharedIdSetArray){
	var divFromat = "";
	var text='';
	if(userIdSharedIdSetArray.length > 0 && userIdSharedIdSetArray[0]!=null ){
		
			divFromat = divFromat +'<div class="span" style="border-bottom: 1px solid #DCDCDC;"><div class="span1"></div>'
										+'<div class="span7">'
											+'<p> There was a Event shared with you. Click Ok to see the Event(s) </p>'
										+'</div><div class="offset8">'
											+'<a id="accept" class="btn btn-success eventSharingAcceptDecline" value="Accept" href="javascript:void(0);">OK</a>'
											+'&nbsp;&nbsp;<a id="decline" class="btn btn-danger eventSharingAcceptDecline" value="Decline" href="javascript:void(0);">Cancel</a>'
										+'</div></div>';
	}
	return divFromat;
}



function getTodaySchedule(){
	$("#tempTablke").empty();
	$("#tempTablke").append('<table id="todaySchedules"></table>');
	
	var today = new Date();
	var monthStr=today.getMonth()+1;
	if(monthStr<10 && monthStr>0)
	{
		monthStr="0"+monthStr;
	}
	var dateStr=today.getDate();
	if(dateStr<10)
	{
		dateStr="0"+dateStr;
	}
	today=monthStr+'/'+dateStr+'/'+today.getFullYear();

	var url = urlForServer+"note/fetchTodayScheduleEvents";
	var params = '{"userId":"'+userId+'","date":"'+today+'"}';
    
    params = encodeURIComponent(params);
	
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
//    		loadList(response);
    		
    		var schedules = new todayScheduleList(response);
    		
    			                       // For each schedule, add a row in the table
    			                       var gridData = [];
    			                       schedules.each(function(schedule) {
    			                           var item = schedule.toJSON();
    			                           item.id = $.jgrid.randId();
    			                           gridData.push(item);
    			                       });

    			                       if(gridData.length!=0)
    			                       {
    			                       // Create the table
    			                       var scheduleTable = jQuery("#todaySchedules");
    			                       scheduleTable.jqGrid({ 
    			                           datatype: 'local',
    			                           data: gridData,
    			                           width:'100%',
    			                           height: 'auto',
    			                       	gridview: true,
    			                       	colNames:['Today\'s Schedule'], 
    			                       	colModel:[ 
    			                       		  		{name:'Description',index:'Description', width:190} 
    			                       		  	],
    			                       	loadComplete : function(data) {
    			                               //alert('grid loading completed ' + data);
    			                           },
    			                           loadError : function(xhr, status, error) {
    			                              // alert('grid loading error' + error);
    			                           }
    			                       	
    			                       });
    			                      
    			                       }
    		
        },
        error: function(e) {
           // alert("Please try again later");
        }
    });
}

function doUserAppDetils() {
	var versions = getOS();
	var screenSize = screen.width + 'x' + screen.height;
	var osName = '';
	if (isAndroid) {
		osName = 'Android';
	} else if (isIpod) {
		osName = 'ipod';
	} else if (isIpad) {
		osName = 'ipad';
	} else if (isIphone) {
		osName = 'iphone';
	} else {
		var width = $(window).width();
		var height = $(window).height();
		screenSize = width + "x" + height;
		osName = 'web';
	}
	var browserNameAndVersion = navigator.sayswho;
	var url = urlForServer + "login/userLoginDetils";
	var params = '{"userId":"' + userId + '","versions":"' + versions
			+ '","osName":"' + osName + '","browserNameAndVersion":"'
			+ browserNameAndVersion + '","screenSize":"' + screenSize + '"}';
	params = encodeURIComponent(params);

	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
		},
		type : 'POST',
		url : url,
		cache : false,
		contentType : "application/json; charset=utf-8",
		data : params,
		dataType : "json",
		success : function(response) {

		}
	});
}



function loginauthenticationcheck(){
	var id="";
	if(is_chrome || is_safari)
	id=$.jStorage.get("lkeyX");
	else
	id=document.cookie;

	    if(id !=null && id !="" && id.indexOf("&") > -1) {
	    	var param=id;
	    	console.log("param"+param);
	    	if(param!=null && param!="")
	    	{
	    		var params = encodeURIComponent(param);
	    		var url=urlForServer+"login/logintime";
	    		$.support.cors = true;
	    		$.ajax({
	    			headers: { 
	    			"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
	    			},
	    			type : 'POST',
	    			url : url,
	    			data : params,
	    			success : function(responseText) 
	    			{
	    			

	    				console.log("<-------data returned from url for login js-------> "+ responseText);    				
	    				var response = responseText;
	    				window.location.replace("./index.html");
	    					
	    				
	    			
	    			},
	    			error : function() 
	    			{
	    				console.log("<-------error returned for new ajax request login autonication-------> ");
	    			}
	    		});	
	    	}
	    	else
	    	{
	    		
	    	}
	    }
	    else
	    {
	    	
	    }
}
function addEve(listId,noteId){
	
	var cls=$('.'+listId+''+'A'+''+noteId+'').attr('class').split(' ');
	$('.'+cls[0]+'').attr('disabled',true);
	var logID=$('#addEve').parent().parent().parent().attr('id');

	var url = urlForServer+"note/calendarEventAccess";
	params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'","sharingStatus":"A"}';
	params = encodeURIComponent(params);
	
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn					
    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	//dataType: "json",
	    	success : function(response){
	    	 ////////////////////////////////////////////////////	
	    	var url = urlForServer+"log/setNotificationAcceptDecline";
			params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'","logID":"'+logID+'"}';
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
			    	type: 'POST',
			    	url : url,
			    	cache: false,
			    	contentType: "application/json; charset=utf-8",
			    	data:params, 
			    	dataType: "json",
			    	success : function(response){
			 },
			 error : function(e)
			    {
			    }
			    
			    });
	  //////////////////////////////////////////////  
	    },
	    error : function(e)
	    {
	    	alert("please try again later");
	    }
	    
	    });
}

function decEve(listId,noteId){
		var cls=$('.'+listId+''+'A'+''+noteId+'').attr('class').split(' ');
		$('.'+cls[0]+'').attr('disabled',true);
		var url = urlForServer+"note/calendarEventAccess";
		var logID=$('#decEve').parent().parent().parent().attr('id');

		params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'","sharingStatus":"D"}';
		params = encodeURIComponent(params);
			
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
		    	type: 'POST',
		    	url : url,
		    	cache: false,
		    	contentType: "application/json; charset=utf-8",
		    	data:params, 
		    	//dataType: "json",
		    	success : function(response){
		    ////////////////////////////////////////////////////	
		    	var url = urlForServer+"log/setNotificationAcceptDecline";
				params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'","logID":"'+logID+'"}';
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
				    	type: 'POST',
				    	url : url,
				    	cache: false,
				    	contentType: "application/json; charset=utf-8",
				    	data:params, 
				    	dataType: "json",
				    	success : function(response){
				 },
				 error : function(e)
				    {
				    }
				    
				    });
		  //////////////////////////////////////////////  	
		    },
		    error : function(e)
		    {
		    }
		    
		    });
	}
//$("#verifyResetButtons").click(function(){
function resendVerify(){
	$('#verifyResetButtons').attr('disabled',true);
		var email=mailIdForResetcode;
			var url = urlForServer+"login/resetVerificationCode";
			var datastr = '{"emailId":"'+email+'"}';
			emailStr=datastr;
			//console.log("<-------Sdatastr -------> "+ datastr);
			var params = encodeURIComponent(datastr);
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST', url : url, data : params,
				success : function(responseText){
					$('#emailVerifyModal').modal('hide');
					$('#signinspan').empty();
				
			},error:function(){
				$('#verifyResetButtons').attr('disabled',false);
			}
			});
}
function validateNewPassword()
{
	$( "#resetPasswordNotSupport" ).empty();
	$( "#resetPasswordNotSupport" ).attr('style','display : none;'); 
	var newPassword=$("#newpassword").val();
	var length=newPassword.length;
	if(newPassword!=""){
		if(length<6)  
		{ 
			$( "#resetPasswordNotSupport" ).attr('style','display : block;color:red;'); 
			$( "#resetPasswordNotSupport" ).text("Short passwords are easy to guess. Try one with at least 6 characters."); 
			newPassWarningFlag=true;
		}  
		else  
		{  
			newPassWarningFlag=false;
		}
		}
} 
//});
		/*else{
			$('#emailverifysubmitButton').attr('disabled',false);
			$("#resetemptyerror").text("Entered email is incorrect").css({"color":"red","font-weight": "bold","margin-left":"10px"});
		}
	}else{
		if(email==""){
			$('#emailverifysubmitButton').attr('disabled',false);
			$("#resetemptyerror").text("Please enter email").css({"color":"red","font-weight": "bold","margin-left":"10px"});
		}else if((email.indexOf('@')==-1)&&(email.indexOf('.')==-1)){
			$('#emailverifysubmitButton').attr('disabled',false);
			$("#resetemptyerror").text("Please enter valid email").css({"color":"red","font-weight": "bold","margin-left":"10px"});
		}
	}*/

//}
/*function onload()	{
	$('#username').focus();
}
function onKeyPress(e)	{
    if(e.keyCode === 13)//for enter key
        $("#login").focus();
}  	*/

