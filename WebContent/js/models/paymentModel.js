var userName="";
var userId;
var userFirstName='';
var userEmail='';
var offer='';
var amt='';
var urlForPayment='http://musicnoteapp.com/musicnotetrial/index.html=';
var urlForPayment1='http://musicnoteapp.com/musicnotetrial/index.html';
var urlForServer='http://musicnoteapp.com/mnTrialApi/mn1.0/';
//var urlForPayment='http://localhost:8080/musicnote/index.html=';
//var urlForPayment1='http://localhost:8080/musicnote/index.html';
//var urlForServer='http://localhost:8080/mnApi/mn1.0/';
var festivOffer="";
var param=[];
var url="";

$(function(){
	
		//to toggle pay for option
		var location=window.location.href;
		var urlpath=location.substring(location.indexOf('?')+1,location.indexOf('?')+13);	
		if(urlpath=='HomePageLink')
		{
		$('.payFor').attr('style','display:block');
		}else{
			$('.payFor').attr('style','display:none');
			$("#payNo").val(1);
		}
		
	
		$('input[name="payment"]').click(function(){
			var location=window.location.href;
			$('#error_msg1').attr('style','display:none;');
			$('#paymentViewOffer').val('0-0');
			$('.emailUpdate').attr('style','display:none;');
			$('#paymentViewOption').val('0-0');
			//$('#payViewNo').val(0);
			$('#payViewTotAmt').val(0);
			
			$('#offer').val('0-0');
			$('#option').val('0-0');
			//$('#payNo').val(0);
			$('#totamt').val(0);
			$('#error_msg').attr('style','display:none');
			$('#offerBox').attr('style','display:none');
			
			if(location.indexOf('HomePageLink')!=-1){
				$('#payViewNo').val(0);
				$('#payNo').val(0);
			}
			
		});
	
	$('.emailUpdate').hide();
	$('#viewEvents').hide();
	$("#description").hide();
	$('.subscribe').attr('style','display:none');
	$('#subscribeImage').attr('style','display:none');
	$('#subsriptionLabel').attr('style','display:none');
	$('#note').attr('style','display:none');
	
	var url = urlForServer + "payment/getOfferMessage";
	
	var name="id";
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( window.location.href );
	if( results == null ) userEmail= "";
	else 
		userId= results[1];
	
	if(userId.indexOf("Renewal")!=-1)
	{
		userId=userId.split('~');
		userId=userId[1];
	}
	
	var datastr = '{"userId":"' + userId + '"}';
	console.log("<-------Sdatastr -------> " + datastr);
	var params = encodeURIComponent(datastr);
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn					
    	},
				type : 'POST',
				url : url,
				data : params,
				success : function(responseText) 
				{
				$("#paymentViewOffer").empty();
				$("#offer").empty();
				var data='<option value="0-0">None</option>';
				$("#paymentViewOffer").append(data);
				$("#offer").append(data);
				var selectNames=jQuery.parseJSON(responseText);
				for (var i = 0;i < selectNames.length ;i++){
					var offerValueSplit=(selectNames[i].amountDays).split('-');
					offerValue=offerValueSplit[0];
					var eventData='<option value=' + selectNames[i].amountDays+ '>'+selectNames[i].subject+' </option>';
					$('.emailUpdate').hide();
					$('#viewEvents').hide();
					$("#paymentViewOffer").append(eventData);
					$("#offer").append(eventData);
					
				}
				},
				error:function(){
					console.log("<-------Error returned while welcome user mail-------> ");
				}
				});
	
	
	//for payment page selecting offer change
	$('#offer').change(function()
			{
		$('#option').attr('disabled','disabled');
		var festivOffer=$('#offer').val();
		var festivofferValueSplit=festivOffer.split('-');
		festivalOffer=festivofferValueSplit[0];
		
		var url = urlForServer + "payment/getOfferMessage";
		var datastr = '{"userId":"' + userId + '"}';
		console.log("<-------Sdatastr -------> " + datastr);
		var params = encodeURIComponent(datastr);
	   
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
					var selectNames=jQuery.parseJSON(responseText);
					for (var i = 0;i < selectNames.length ;i++){
						var offerValueSplit=(selectNames[i].amountDays).split('-');
						offerValue=offerValueSplit[0];
						if(festivalOffer==offerValue){
							$('.emailUpdate').show();
							$('#viewEvents').show();
							$('.emailUpdate').text(selectNames[i].offerMsg);
						}
						else if(festivalOffer==0){
							$('.emailUpdate').hide();
							$('#viewEvents').hide();
							$('#option').removeAttr('disabled');
						}
					}
					},
					error:function(){
						console.log("<-------Error returned while welcome user mail-------> ");
					}
					});
		amountCalculation();
	});
	
	
	
	//for payment page option change
	$('#option').change(function(){
		$('.emailUpdate').hide();
		$('#viewEvents').hide();
		$('#offer').attr('disabled','disabled');
		if($('#option').val()==0){
			$('#offerBox').show();
			$('#offer').removeAttr('disabled');	
		}
		amountCalculation();
	});
	$('#standardPay').click(function(){
		$("#cmd").val("_xclick");
		$('#subsriptionLabel').attr('style','display:none');
		$('#paymentLabel').attr('style','display:block');
		$('#paypalPayment').attr('style','display:block;margin-left:50px;');
		$('#subscribeImage').attr('style','display:none');
		$('#description').attr('style','display:none');
		$('#note').attr('style','display:none');
		$('.emailUpdate').hide();
		$('#viewEvents').hide();
	});
	$('#subscriptionPay').click(function(){
		$('#subsriptionLabel').attr('style','display:block');
		$('#paymentLabel').attr('style','display:none');
		$("#src").val("1");
		$("#sra").val("1");
		$('#note').attr('style','display:block');
		$('#paypalPayment').attr('style','display:none');
		$('#subscribeImage').attr('style','display:block;margin-left:50px;');
		$("#cmd").val("_xclick-subscriptions");	
		$('.emailUpdate').hide();
		$('#viewEvents').hide();
		
	});
	
});
//for payment page amount calculation
function amountCalculation()
  {
	var payment=$('input[name="payment"]:checked').val();
	if(payment=='normal'){
		//alert('normal');
		var payValue = $("#payNo").val();
		var offerValue = "";
		var offerattr = $('#option').attr('disabled');
		if (offerattr != 'disabled') {
			var offerValue1 = $('#option').val();
			offer=offerValue1;
			var offerValueSplit = offerValue1.split('-');
			offerValue = offerValueSplit[0];
		} else {
			var offerValue1 = $('#offer').val();
			offer=offerValue1;
			var offerValueSplit = offerValue1.split('-');
			offerValue = offerValueSplit[0];
		}
		var amt = payValue * offerValue;
		$("#amount").val(amt);
		$("#totamt").val(amt);

		//offer = $('#option').val();
		
	}else{
		//alert('subscribe');
		var payValue = $("#payNo").val();
		var offerValue = "";
		var period="";
		var time="";
		var offerSplit="";
		var offerattr = $('#option').attr('disabled');
		if (offerattr != 'disabled') {
			var offerValue1 = $('#option').val();
			offer=offerValue1;
			var offerValueSplit = offerValue1.split('-');
			offerValue = offerValueSplit[0];
			offerSplit =offerValueSplit[1];
			if(offerSplit==30){
			time='M';
			}else if(offerSplit==365){
			time='Y';
			}else{
			time='0';
			}
			period=1;
		} else {
			var offerValue1 = $('#offer').val();
			offer=offerValue1;
			var offerValueSplit = offerValue1.split('-');
			offerValue = offerValueSplit[0];
			offerSplit =offerValueSplit[1];
			if(offerSplit==30){
			time='M';
			}else if(offerSplit==365){
			time='Y';
			}else{
			time='0';
			}
			period=1;
			//time=offerSplit[1];
		}
		var amt = payValue * offerValue;
		$("#a3").val(amt);
		$("#p3").val(period);
		$("#t3").val(time);
		$("#totamt").val(amt);
		$("#description").show();
		if(time=='M'){time='Month';}else if(time=='Y'){time='Year';}
		$("#description").text("$"+amt+" US Dollar for every "+period+""+time);
		if($("#t3").val()==0){$("#description").hide();}
		
		
	}
	amt = $('#totamt').val();
		var name = "id";
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var payOrRenwal='';
		var results = regex.exec(window.location.href);
		if (results == null)
			userEmail = "";
		else
			userId = results[1];
		
		if(userId.indexOf("Renewal")!=-1)
		{
			userId=userId.split('~');
			payOrRenwal=userId[0];
			userId=userId[1];
		}
		else
		{
			payOrRenwal="Payment";
		}

		var queryString ="External"+payOrRenwal +"="+ userId + "~" + offer + "~" + amt + "~" + payValue;
		var custom = userId + "~" + userId + "~" + payValue +"~" + offer +"~"+ payOrRenwal;
		$("#eMail").val(userId);
		$("#return").val("http://musicnoteapp.com/musicnotetrial/index.html?"+ encodeURIComponent(queryString));
		//$("#return").val("http://localhost:8080/musicnote/index.html?"+ encodeURIComponent(queryString));
		
		$("#payId").val(custom);
		$("#quantity").val(payValue);
		$("#option").val(offer);
}
//for payment view offer change
$('#paymentViewOffer').change(function(){
	$('#paymentViewOption').attr('disabled','disabled');
	
	var festivOffer=$('#paymentViewOffer').val();
	var festivofferValueSplit=festivOffer.split('-');
	festivalOffer=festivofferValueSplit[0];
	
	var url = urlForServer + "payment/getOfferMessage";
	var datastr = '{"userId":"' + userId + '"}';
	console.log("<-------Sdatastr -------> " + datastr);
	var params = encodeURIComponent(datastr);
   
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn					
    	},
				type : 'POST',
				url : url,
				data : params,
				success : function(responseText) 
				{
				var selectNames=jQuery.parseJSON(responseText);
				for (var i = 0;i < selectNames.length ;i++){
					var offerValueSplit=(selectNames[i].amountDays).split('-');
					offerValue=offerValueSplit[0];
					if(festivalOffer==offerValue){
						$('.emailUpdate').show();
						$('#viewEvents').show();
						$('.emailUpdate').text(selectNames[i].offerMsg);
					}
					else if(festivalOffer==0){
						$('.emailUpdate').hide();
						$('#viewEvents').hide();
						$('#paymentViewOption').removeAttr('disabled');
					}
				}
				},
				error:function(){
					console.log("<-------Error returned while welcome user mail-------> ");
				}
				});
	festivOfferCalculation();
});


//for payment view option change
$('#paymentViewOption').change(function(){
	$('#paymentViewOffer').attr('disabled','disabled');
	
	var OfferView=$('#paymentViewOffer').val();
	var OfferViewValueSplit=OfferView.split('-');
	OfferViewValue=OfferViewValueSplit[0];
	if(OfferViewValue==0){
		$('#offerBox').show();
		$('#paymentViewOffer').removeAttr('disabled');	
	}
	festivOfferCalculation();
});

//for payment view page calculation
	function festivOfferCalculation(){
	
	var payment=$('input[name="payment"]:checked').val();
	if(payment=='normal'){
	
	
		var offerValue="";
		var offerattr=	$('#paymentViewOption').attr('disabled');
		var payValue = $("#payViewNo").val();
		if(offerattr!='disabled'){
			var offerValue1=$('#paymentViewOption').val();
			var offerValueSplit=offerValue1.split('-');
			offerValue=offerValueSplit[0];
		}else{
			var offerValue1=$('#paymentViewOffer').val();
			var offerValueSplit=offerValue1.split('-');
			offerValue=offerValueSplit[0];
		}
		var amt=payValue*offerValue;
		$("#amount").val(amt);
		$("#payViewTotAmt").val(amt);
	}
	else{
		//alert('subscribe');
		var payValue = $("#payViewNo").val();
		var offerValue = "";
		var period="";
		var time="";
		var offerSplit="";
		var offerattr = $('#paymentViewOption').attr('disabled');
		if (offerattr != 'disabled') {
			var offerValue1 = $('#paymentViewOption').val();
			offer=offerValue1;
			var offerValueSplit = offerValue1.split('-');
			offerValue = offerValueSplit[0];
			offerSplit =offerValueSplit[1];
			if(offerSplit==30){
			time='M';
			}else if(offerSplit==365){
			time='Y';
			}else{
			time='0';
			}
			period=1;
		} else {
			var offerValue1 = $('#paymentViewOffer').val();
			offer=offerValue1;
			var offerValueSplit = offerValue1.split('-');
			offerValue = offerValueSplit[0];
			offerSplit =offerValueSplit[1];
			if(offerSplit==30){
			time='M';
			}else if(offerSplit==365){
			time='Y';
			}else{
			time='0';
			}
			period=1;
			//time=offerSplit[1];
		}
		var amt = payValue * offerValue;
		$("#a3").val(offerValue);
		$("#p3").val(period);
		$("#t3").val(time);
		$("#payViewTotAmt").val(amt);
		$("#description").show();
		if(time=='M'){time='Month';}else if(time=='Y'){time='Year';}
		$("#description").text("$"+amt+" US Dollar for every "+period+""+time);
		if($("#t3").val()==0){$("#description").hide();}
	}
}
//for payment view page option change
$('#paymentViewOption').change(function(){
	var Option=$('#paymentViewOption').val();
	var OptionValueSplit=Option.split('-');
	valOption=OptionValueSplit[0];
	$('#paymentViewOffer').attr('disabled','disabled');
	$('.emailUpdate').hide();
	$('#viewEvents').hide();
	
	if(valOption==0){
		$('#paymentViewOffer').removeAttr('disabled');
	}
});


$(document).ready( function() {
	
	//method to distinguish whether the grid row is selected or not based on that the params are formed
	
	 function urlFormation(userGridId,Distinguish){
		 if(Distinguish=='A'){
			 
		 if(param.indexOf(userGridId)==-1){
			 param.push(userGridId);
			url=urlForPayment1+'?InternalPayment=?'+param;
		 }else{
			url=urlForPayment1+'?InternalPayment=?'+param;
		 }
		 
		 }else if(Distinguish=='S'){
			 if(param.indexOf(userGridId)!=-1){
				 param.splice(param.indexOf(userGridId),1);
				 url=urlForPayment1+'?InternalPayment=?'+param;
			 } 
		 }
		 queryFormation(url);
		 	
		 }
	 
	 //Method to form the final url to set a return url to paypal
	 
	 function queryFormation(url)
	 {
		 var selectedOption='';
		 	$("#payViewNo").val(param.length);
		 	var offer1=$('#paymentViewOption').val();
		 	var offerSplit=offer1.split('-');
		 	var offerValue=offerSplit[0];
		 	
		 	var festivOffer1=$('#paymentViewOffer').val();
		 	var splitFestivOffer=festivOffer1.split('-');
		 	var festivOffer=splitFestivOffer[0];
		 	
		 	var value=offerCalculation(offerValue,festivOffer);
			$("#payViewTotAmt").val(value*(param.length));
			
			if(offer1!='0-0')
				selectedOption=offer1;
			else
				selectedOption=festivOffer1;
			
		 	var payNo1=$('#payViewNo').val();
		 	var payNo=payNo1.replace("#","");
		 	var amt=$('#payViewTotAmt').val();
		 	$("#amount").val(amt);
		 	var type="test";
		 	
		 	var queryString="~"+userId+"~"+value+"~"+amt+"~"+payNo;
		 	var custom=userId+"~"+param+"~"+param.length+"~"+selectedOption+"~Renewal";
		 	
		 	$("#payId").val(custom);
		 	$("#quantity").val(param.length);
			$("#insidePaymentReturn").val(url+encodeURIComponent(queryString));
	 }
	 
	 function offerCalculation(offerValue,festivOffer)
	 {
		 var value="";
		if(offerValue!=0){
			value=offerValue;
		}else if(festivOffer!=0){
			value=festivOffer;
		}else{
			value=0;
		}
		return value;	
	 }
	 
	 //method to get the selected row data and to get the userId from the data-Deepak
	 function getSelectedRow() {
		 $("#taskTable").find('input[type=checkbox]').each(function() {
			    $(this).change( function(){
			        var colid = $(this).parents('tr:last').attr('id');
			        if( $(this).is(':checked')) {
			           $("#list1").jqGrid('setSelection', colid );
			           $(this).prop('checked',true);
			           var rowData = jQuery('#taskTable').jqGrid ('getRowData', colid);
			           var userGridId=rowData.userId;
			           //When selecting the row 'A' is sent as parameter
			           urlFormation(userGridId,'A');
			        }else{
			        	 var rowData = jQuery('#taskTable').jqGrid ('getRowData', colid);
				           var userGridId=rowData.userId;
				         //When de selecting the row 'S' is sent as parameter
			        	urlFormation(userGridId,'S');
			        }
			        return true;
			    });
			}); 
		
     }
	
	 
					
				
	 
	$(function(){
		var url = urlForServer + "group/checkPaymentGroup";
		var datastr = '{"userId":"' + userId + '"}';
		console.log("<-------Sdatastr -------> " + datastr);
		
		$('#PaidCircles').empty();
		$('#PaidCircles').append('<table id="taskTable" ></table><div id="tablePage"></div>');
		var params = encodeURIComponent(datastr);
	   
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
				var data1 = "";
				var dataLength="";
				var lastsel;
				data1 = jQuery.parseJSON(responseText);
				if(data1!=null && data1!="")
				{
					for(var i=0;i<data1.length;i++){
					}
				dataLength= data1.length ;
				var height;
				if(dataLength>=11)
					{
						 height= 450;
					}
					else
					{
						height=  dataLength * 25+50;
					}
				
				$('#taskTable').jqGrid({
					data:data1,
   	        		datatype: "local",
   	        		sortable: true,       		
   	        		height: 140,
	                width:550,
	                multiselect:true,
	               
   	        		 colNames:['Name','MailId','Level','Expiry-Date','Paid By','userId'],
   	        	   	 colModel:[
   	        	   	    {name:'name',index:'Id', width:50},
   	        	   		{name:'mailId',index:'Id', width:110,editable:true},
   	        	   		{name:'level',index:'level', width:70,hidden:true},
   	        	   		{name:'expiryDate',width:50},
   	        	   		{name:'paidBy',width:50},
   	        	   		{name:'userId',width:50,hidden:true},
   	        	   		
   	        	   ],
   	        	   
   	        	   pager: '#tablePage',
   	        	   sortname: 'taskId', 
   	        	   viewrecords: true, 
   	        	   multiselect: true,
   	        	   multiboxonly: true,
   	        	   sortorder: "desc",
   	        	   grouping:true, 
   	        	   groupingView : { groupField : ['level']},
          	       caption: "User Details"
          	           
   	        	 });
				//$('#taskTable').jqGrid('hideCol', "level");
				$("#taskTable").jqGrid('navGrid',"#tablePage",{edit:false,add:false,del:false});
				getSelectedRow();  
				}
				
		},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});
	});
});
  
function fetchTrialUsers()
{ 
	var url = urlForServer+"user/getUserDetails";
	var datastr = '{"userId":"'+userId+'"}';
	console.log("<-------Sdatastr -------> "+ datastr);
	var params = encodeURIComponent(datastr);
	var role="";
	
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		url : url,
		data : params,
		success : function(responseText) {
		console.log("<-------data returned from url for get User details with reponse as -------> "+ responseText);
		var data = jQuery.parseJSON(responseText);
		if(data!=null && data!='')
		{
		for ( var i = 0; i < data.length; i++) {
			
		}
	}
	},
	error : function() {
		console.log("<-------error returned for User details -------> ");
	}
	}); 
}
	