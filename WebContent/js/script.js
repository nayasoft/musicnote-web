/**
 *
 * Crop Image While Uploading With jQuery
 * 
 * Copyright 2013, Resalat Haque
 * http://www.w3bees.com/
 *
 */

// set info for cropping image using hidden fields
function setInfo(i, e) {
	$('#x').val(e.x1);
	$('#y').val(e.y1);
	$('#w').val(e.width);
	$('#h').val(e.height);
	$('#Image').val(document.getElementById("uploadImage").files[0]);
}

$(document).ready(function() {
	var p = $("#uploadPreview");

	// prepare instant preview
	$("#uploadImage").change(function(){
		// fadeOut or hide preview
	      var v=new Image();
          v.src=document.getElementById("uploadPreview").getAttribute('src');
		  /*if($('#w').val()==0 &&  $('#h').val()==0 && v.width<200 && v.height<200)
		  {
			    $(".imgareaselect-border4").attr("style","display:none;");
				$(".imgareaselect-border1").attr("style","display:none");
				$(".imgareaselect-border2").attr("style","display:none");
				$(".imgareaselect-border3").attr("style","display:none");
				$(".imgareaselect-outer").attr("style","display:none"); 
				var p = $("#uploadPreview");
				p.fadeOut();
				$("modal-backdrop").attr("style","display:none");; 
				$(".imgareaselect-outer").attr("style","display:none");
		 } */
		 
		  
		p.fadeOut();
		
		//document.getElementById("uploadPreview").style.width = "160px";
		//document.getElementById("uploadPreview").style.height = "120px";
		// prepare HTML5 FileReader
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

		oFReader.onload = function (oFREvent) {
	   		p.attr('src', oFREvent.target.result).fadeIn();
		};
	    
	});

	// implement imgAreaSelect plug in (http://odyniec.net/projects/imgareaselect/)
	$('img#uploadPreview').imgAreaSelect({
		// set crop ratio (optional)
		aspectRatio: '1:1',
		onSelectEnd: setInfo
	});
});