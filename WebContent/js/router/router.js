window.Router = Backbone.Router.extend({

    routes: {
	    "home": "home",
	    "": "login",
	    "logout":"logout",
	    "cancel":"cancel",
	    "registration": "registration",
	    "userLogin":"userLogin",
        "profile": "profile",
        "notes": "notes",
        "memos": "notes",
        "music": "notes",
        "schedule": "notes",
        "messages": "messages",
        "reminders": "reminders",
        "recordlesson": "recordlesson",
        "students": "students",
        "invoice": "invoice",
        "employees/:id": "employeeDetails",
		"calendar":"calendar",
		"crowd":"crowd",
		"resetpassword":"resetpassword",
		"help":"help",
		"payment":"payment",
		"trailregistration":"trailregistration",
		"crowdProfiles":"crowdProfiles"
    },

    initialize: function () {
        this.headerView = new HeaderView();
        $("#header").html(this.headerView.render().el);

        this.sideBar = new SideBar();
        $("#sideBar").html(this.sideBar.render().el);
        
        this.footerView = new FooterView();
        $("#footer").html(this.footerView.render().el);
        
        // Close the search dropdown on click anywhere in the UI
        $('body').click(function () {
            $('.dropdown').removeClass("open");
        });
        
    },
    login:function(){
    	
    	   if (!this.loginView) {
               this.loginView = new LoginView();
               this.loginView.render();
           } else {
               this.loginView.delegateEvents(); // delegate events when the view is recycled
           }
    	   document.title = 'Sign In to Musicnote App | Your Music Notebook In The Cloud | Learn, Teach and Play Music Better';
     	  $('meta[name=description]').attr('content', 'Sign In to your Musicnote account. Musicnote helps music teachers and music students to digitally save, share and access all of their music resources from any device. It is your music notebook in the cloud.');
     	  $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
           $("#main").html(this.loginView.el);
            //createUserTest();
          
           //fetchMailMessages("inbox");
    },
    registration:function(){
    	  if (!this.registrationView) {
              this.registrationView = new RegistrationView();
              this.registrationView.render();
          } else {
              this.registrationView.delegateEvents(); // delegate events when the view is recycled
          }
    	  document.title = 'Sign Up for Musicnote | Your Music Notebook In The Cloud | Learn, Teach and Play Music Better';
    	  $('meta[name=description]').attr('content', 'Sign up for Musicnote. Musicnote helps music teachers and music students to digitally save, share and access all of their music resources from any device. It is your music notebook in the cloud and is available in the Apple App Store, Google Play Store and on the web.');
    	  $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
    	  $("#main").attr("style", "dispaly:block");
          $("#main").html(this.registrationView.el);
          $("#role").empty();
          $("#userfirstname").val("");
			$("#userlastname").val("");
			$("#emailid").val("");
			$("#agreeterms").attr('checked', false); 
			$("#fbusername").val("");
			$("#password").val("");
			$("#confirmpassword").val("");
			$("#securityBox").val("");
			$("#dialogempty1").val("");
			registerTokenGenerator();
			
        var url=window.location.href;
       // var location=window.location.href;// for homepage send request
    	//location = decodeURIComponent(location);
      	var checkLinkPaid=url.substring(url.indexOf('?')+1,url.lastIndexOf('?'));
      	if(checkLinkPaid=="MailLink"){
      	//$("#emailid").val('');
        	//$("#password").val('');
      	//}else if(location.indexOf('?viahomepage')!=-1){
      	}
      	else
      	{
      		$("#emailid").val('');
            $("#password").val('');
      	}
          registerUser();
    },
    
    
    trailregistration:function(){
  	  if (!this.registrationView) {
            this.registrationView = new RegistrationView();
            this.registrationView.render();
        } else {
            this.registrationView.delegateEvents(); // delegate events when the view is recycled
        }
  	$("#main").attr("style", "dispaly:block");
        $("#main").html(this.registrationView.el);
        var url=window.location.href;
        var location=window.location.href;// for homepage send request
    	location = decodeURIComponent(location);
      	var checkLinkPaid=url.substring(url.indexOf('?')+1,url.lastIndexOf('?'));
      	if(checkLinkPaid=="MailLink"){
      	//$("#emailid").val('');
        //$("#password").val('');
      	}else if(location.indexOf('?viahomepage')!=-1){
           	}else{
      	$("#emailid").val('');
        $("#password").val('');
      	}
        registerUser();
       
        
  },

    /*createUser:function(){
    	createUser();
    },*/
    userLogin:function(){
    	userLogin();
    },
    logout:function(){
    	userLogout();
    },
    cancel:function(){
    	userLogout();
},
    home: function () {
        // Since the home view never changes, we instantiate it and render it only once
	     //fetchUsers();
        if (!this.homeView) {
        	this.homeView = new HomeView();
            this.homeView.render();
            
             
        } else {
            this.homeView.delegateEvents(); // delegate events when the view is recycled
        }
        document.title = 'Recent Activity | Musicnote App ';
   	    $('meta[name=description]').attr('content', 'Recent activity for your Musicnote App account.');
   	    $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        $("#content").html(this.homeView.el);
        this.headerView.select('home-menu');
		getrecentActivity();
		fetchNotifications();
		fetchRemainders();
		$('#welcomemsg').attr('style','display:none');
		$("#main").attr("style", "display:none");
		$("#spinner").attr('style','display:block;');
		$("#img-spinner").attr('style','display:block;');
		//uesrName();
    },

    profile: function () {
        if (!this.ProfileView) {
            this.ProfileView = new ProfileView();
            this.ProfileView.render();
        }
        document.title = 'Musicnote App Profile';
   	    $('meta[name=description]').attr('content', 'Your Musicnote profile and information. Musicnote helps music teachers and music students to digitally save, share and access all of their music resources from any device.');
   	    $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        $('#content').html(this.ProfileView.el);
        this.headerView.select('profile-menu');
        //$('#msgLoadingModal').modal('show');
        userProfile();
        fetchUsers();
        $("#main").attr("style", "display:none");
        $('#plastnameDiv').attr('style','display:none;');
        $('#pfirstnameDiv').attr('style','display:none;');
        $('#dialogempty1').attr('style','display:none;');
        $('#ValidatepemailidDiv').attr('style','display:none;');
        $('#ExistspemailidDiv').attr('style','display:none;');
        $('#ValidatepemailidDiv').attr('style','display:none;');
        $('#pemailidDiv').attr('style','display:none;');
        $('.profileSecurity').val('0');
        $('.profileSecurityBox').val('');
        $('.profileSecurityBox').attr('disabled',true);
        $("#spinner").attr('style','display:block;');
		$("#img-spinner").attr('style','display:block;');
        $('#welcomemsg').attr('style','display:none');
        $("#bioVarning").attr("style", "dispaly:none");
		$("#bioVarning").text("");
		//uesrName();
    },
	
    notes: function () {
       // if (!this.notesView) {
            this.notesView = new NotesView();
            this.notesView.render();
      //  } 
//            this.notesView.delegateEvents(); // delegate events when the view is recycled
//        }
        if(listType=="music")
        {
        	document.title = 'Notes | Musicnote App';
         	$('meta[name=description]').attr('content', 'Musicnote helps music teachers and music students to digitally save, share and access all of their music resources from any device. It is your music notebook in the cloud.');
         	$('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        }
        else if(listType=="bill")
        {
        	document.title = 'Memos | Musicnote App';
         	$('meta[name=description]').attr('content', 'Share Memos for correspondence Musicnote helps music teachers and music students to digitally save, share and access all of their music resources from any device. It is your music notebook in the cloud.');
         	$('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        }
        else
        {
        	document.title = 'Schedule | Musicnote App';
         	$('meta[name=description]').attr('content', 'Musicnote helps music teachers and music students to digitally save, share and access all of their music resources from any device. It is your music notebook in the cloud.');
         	$('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        }
        $('#content').html(this.notesView.el);
        $('#welcomemsg').attr('style','display:none');
        $("#main").attr("style", "display:none");
        $("#spinner").attr('style','display:block;');
        $("#img-spinner").attr('style','display:block;');
    },
    music: function () {
    	
    	listType="music";
        if (!this.notesView) {
            this.notesView = new NotesView();
            this.notesView.render();
            
        }
        $('#content').html(this.notesView.el);
        $("#main").attr("style", "display:none");
    },
    schedule: function () {
    	
    	listType="schedule";
        if (!this.notesView) {
            this.notesView = new NotesView();
            this.notesView.render();
        }
        $('#content').html(this.notesView.el);
        $("#main").attr("style", "display:none");
    },
    bill: function () {
    	
    	listType="bill";
        if (!this.notesView) {
            this.notesView = new NotesView();
            this.notesView.render();
        }
        $('#content').html(this.notesView.el);
        $("#main").attr("style", "display:none");
    },
    messages: function () {
        if (!this.messagesView) {
            this.messagesView = new MessagesView();
            this.messagesView.render();
        }
        
        $('#content').html(this.messagesView.el);
        $('#welcomemsg').attr('style','display:none');
       
    },
    reminders: function () {
        if (!this.remindersView) {
            this.remindersView = new RemindersView();
            this.remindersView.render();
        }
        $('#content').html(this.remindersView.el);
        $('#welcomemsg').attr('style','display:none');
        $("#main").attr("style", "display:none");
    },
    recordlesson: function () {
       // if (!this.recordlessonView) {
            this.recordlessonView = new RecordLessonView();
            this.recordlessonView.render();
       // }
        document.title = 'Record | Musicnote App';
        $('meta[name=description]').attr('content', 'Record audio, video or upload files to Musicnote.');
     	$('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        $('#content').html(this.recordlessonView.el);
        $('#welcomemsg').attr('style','display:none');
        $("#main").attr("style", "display:none");
        $("#spinner").attr('style','display:block;');
        $("#img-spinner").attr('style','display:block;');
        $('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );	
    	$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarRecord').css( "background-color","rgb(40, 135, 189)" );
    },
    students: function () {
        if (!this.studentsView) {
            this.studentsView = new StudentsView();
            this.studentsView.render();
        }
        document.title = 'Contacts | Musicnote App';
        $('meta[name=description]').attr('content', 'Musicnote helps music teachers and music students to digitally save, share and access all of their music resources from any device. It is your music notebook in the cloud.');
     	$('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        $('#content').html(this.studentsView.el);
        $("#main").attr("style", "display:none");
        $('#welcomemsg').attr('style','display:none');
        $("#searchUser").val("");
        $("#spinner").attr('style','display:block;');
        $("#img-spinner").attr('style','display:block;');
        $('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );	
    	$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarContact').css( "background-color","rgb(40, 135, 189)" );
    },
    invoice: function () {
        if (!this.invoiceView) {
            this.invoiceView = new InvoiceView();
            this.invoiceView.render();
        }
        $('#content').html(this.invoiceView.el);
        $('#welcomemsg').attr('style','display:none');
       
        fetchTeacher();
		fetchStudent();
		var currentDate = new Date()
	    var day = currentDate.getDate()
	    var month = currentDate.getMonth() + 1
	    var year = currentDate.getFullYear()
	    var dateValue=day + "-" + month + "-" + year ;
	    $("#invoiceDate").text(dateValue);
		$("#dateTerm").val(dateValue);
	  	var invoiceNo=1000;
		$("#invoiceNo").text(invoiceNo);

    },
    employeeDetails: function (id) {
        var employee = new Employee({id: id});
        employee.fetch({
            success: function (data) {
                // Note that we could also 'recycle' the same instance of EmployeeFullView
                // instead of creating new instances
                $('#content').html(new EmployeeView({model: data}).render().el);
                $('#welcomemsg').attr('style','display:none');
            }
        });
    },
	calendar:function(){
    	   if (!this.calendarView) {
               this.calendarView = new CalendarView();
               this.calendarView.render();
           } else {
               this.calendarView.delegateEvents(); // delegate events when the view is recycled
           }
           $("#content").html(this.calendarView.el);
           $('#welcomemsg').attr('style','display:none');
           $("#main").attr("style", "display:none");
    },
    crowd:function(){
 	   if (!this.crowdView) {
            this.crowdView = new CrowdView();
            this.crowdView.render();
        } else {
            this.crowdView.delegateEvents(); // delegate events when the view is recycled
        }
 	    document.title = 'Crowd | Musicnote App';
 	    $('meta[name=description]').attr('content', 'The Musicnote Crowd is a place where all Musicnote members can share music resources and learn from others.');
 	    $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        $("#content").html(this.crowdView.el);
        //voteLists();
       	newNotes();
       	$("#main").attr("style", "display:none");
		$('#welcomemsg').attr('style','display:none');
		$("#spinner").attr('style','display:block;');
        $("#img-spinner").attr('style','display:block;');
        //publicNotes();
        /*$('#newNote').empty();
        $('#mostView').empty();
        $('#mostPopular').empty();*/
	},
    help:function(){
	  	  if (!this.helpView) {
	            this.helpView = new HelpView();
	            this.helpView.render();
	        } else {
	            this.helpView.delegateEvents(); // delegate events when the view is recycled
	        }
	  	document.title = 'Help and Feedback For Musicnote | Your Music Notebook In The Cloud';
   	    $('meta[name=description]').attr('content', 'Get help or give feedback to the Musicnote Team.');
   	    $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
	  	$("#main").attr("style", "display:none");
	        $("#content").html(this.helpView.el);
	        $("#userfeedback").val(""); 
	        $("#limitChar1").hide();
			$("#limitChar").attr("style", "dispaly:block;font-family: Helvetica Neue;font-size:14px;");
			$("#limitChar").text("600 characters remaining");
			$("#spinner").attr('style','display:block;');
	        $("#img-spinner").attr('style','display:block;');
			viewHelpVideoUploaded();
	  },
	  payment:function(){
	  	  if (!this.paymentView) {
	            this.paymentView = new PaymentView();
	            this.paymentView.render();
	        } else {
	            this.paymentView.delegateEvents(); // delegate events when the view is recycled
	        }
	        $("#content").html(this.paymentView.el);
	        $("#main").attr("style", "display:none");
	       
	        
	  },
	  
	resetpassword:function(){
		  
		if (!this.resetView) {
            this.resetView = new ResetView();
            this.resetView.render();
        } else {
            this.resetView.delegateEvents(); // delegate events when the view is recycled
        }
		document.title = 'Reset Password | Musicnote App';
   	    $('meta[name=description]').attr('content', 'Reset your Musicnote password');
   	    $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
		$("#main").attr("style", "display:none");
        $("#content").html(this.resetView.el);
        $("#oldPasswordDiv").attr("style", "dispaly:none");
		$("#oldPasswordDiv").text("");
		$("#oldPasswordDiv1").attr("style", "dispaly:none");
		$("#oldPasswordDiv1").text("");
		$("#newpasswordDiv").attr("style", "dispaly:none");
		$("#newpasswordDiv").text("");
		$("#cfrmpasswordDiv").attr("style", "dispaly:none");
		$("#cfrmpasswordDiv").text("");
		$("#newpassword").val("");
		$("#oldPassword").val("");
		$("#cfrmpasswordDiv1").attr("style", "dispaly:none");
		$("#cfrmpasswordDiv1").text("");
		$("#cfrmpassword").val("");
		$('#welcomemsg').attr('style','display:none');
		$("#resetPasswordNotSupport").attr("style", "dispaly:none");
		$("#resetPasswordNotSupport").text("");
	},
	crowdProfiles: function () {
        if (!this.crowdProfile) {
            this.crowdProfile = new crowdProfile();
            this.crowdProfile.render();
        }
        document.title = 'Musicnote App Public Profile | Your Music Notebook In The Cloud';
   	    $('meta[name=description]').attr('content', 'Your Musicnote Public Profile with your bio and recently shared notes to the Musicnote Crowd.');
   	    $('meta[name=keywords]').attr('content', 'musicnote, musicnote app, music notebook, digital music notebook');
        $('#content').html(this.crowdProfile.el);
        this.headerView.select('crowdProfiles-menu');
        $("#main").attr("style", "display:none");
        $("#content").html(this.crowdProfile.el);
    }
});

