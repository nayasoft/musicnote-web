window.RecordLessonView = Backbone.View.extend({
	initialize:function () {
    	console.log('Initializing Record Lesson View');
	},

	render:function () {
		
		if(userId!=null && userId == ''){
			
			var getCookie='';
			if(is_chrome || is_safari)
			{
				userId=$.jStorage.get("lkeyX");
				musicnote=$.jStorage.get("tkeyY");
				musicnoteIn=$.jStorage.get("ltkeyZ");
			}
			else
			{
				getCookie=document.cookie;
				getCookie=getCookie.split("~");
				userId=getCookie[0];
				musicnote=getCookie[1];
				musicnoteIn=getCookie[2];
			} 
			
			
			if(userId==null || userId==undefined || userId == '')
			{
				 window.location.replace("./index.html");
			}else
			{
			var url=$(location).attr('href');
			var str=new Array();
			str=url.split("#");
			listType=str[1];
			
			$('#sidecontent').show();
			$('#header').show();
			$('#footer').show();
			
			fetchUsers();
			fetchUsersList();
			getTodaySchedule();
			}
			
		}else if(userId==null){
			
			var getCookie='';
			if(is_chrome || is_safari)
			{
				userId=$.jStorage.get("lkeyX");
				musicnote=$.jStorage.get("tkeyY");
				musicnoteIn=$.jStorage.get("ltkeyZ");
			}
			else
			{
				getCookie=document.cookie;
				getCookie=getCookie.split("~");
				userId=getCookie[0];
				musicnote=getCookie[1];
				musicnoteIn=getCookie[2];
			}
			
			if(userId==null || userId==undefined ||  userId == '')
			{
				 window.location.replace("./index.html");
			}
			else
			{
			
			var url=$(location).attr('href');
			var str=new Array();
			str=url.split("#");
			listType=str[1];
			
			$('#sidecontent').show();
			$('#header').show();
			$('#footer').show();
			
			fetchUsers();
			fetchUsersList();
			getTodaySchedule();
			}
		}
		 $('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );	
	    	$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarRecord').css( "background-color","rgb(40, 135, 189)" );
	  
		$(this.el).html(this.template());
		return this;
		
	}
   
});
