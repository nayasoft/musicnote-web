window.HomeView = Backbone.View.extend({

    initialize:function () {
        console.log('Initializing Home View');
//        this.template = _.template(directory.utils.templateLoader.get('home'));
//        this.template = templates['Home'];
    },

    events:{
        "click #showMeBtn":"showMeBtnClick"
    },

    render:function () {
    	if(userId!=null && userId == ''){
    		
    		var getCookie='';
			if(is_chrome || is_safari)
			{
				userId=$.jStorage.get("lkeyX");
				musicnote=$.jStorage.get("tkeyY");
				musicnoteIn=$.jStorage.get("ltkeyZ");
			}
			else
			{
				getCookie=document.cookie;
				getCookie=getCookie.split("~");
				userId=getCookie[0];
				musicnote=getCookie[1];
				musicnoteIn=getCookie[2];
			}
    		
    		if(userId==null || userId==undefined || userId == '')
			{
				 window.location.replace("./index.html");
			}else
			{
    		
			$('#sidecontent').show();
			$('#header').show();
			$('#footer').show();
			
			fetchUsers();
			fetchUsersList();
			getTodaySchedule();
			}
		}else if(userId==null){
			
			var getCookie='';
			if(is_chrome || is_safari)
			{
				userId=$.jStorage.get("lkeyX");
				musicnote=$.jStorage.get("tkeyY");
				musicnoteIn=$.jStorage.get("ltkeyZ");
			}
			else
			{
				getCookie=document.cookie;
				getCookie=getCookie.split("~");
				userId=getCookie[0];
				musicnote=getCookie[1];
				musicnoteIn=getCookie[2];
			}
			
			if(userId==null || userId==undefined || userId == '')
			{
				 window.location.replace("./index.html");
			}else
			{
			
			$('#sidecontent').show();
			$('#header').show();
			$('#footer').show();
			
			fetchUsers();
			fetchUsersList();
			}
		}
        $(this.el).html(this.template());
        return this;
    },

    showMeBtnClick:function () {
        app.headerView.search();
    }

});

