window.RegistrationView = Backbone.View.extend( {
	initialize : function() {
		console.log('Initializing Registration View');
		
	},
	events:{
		'submit #contact-form' :'createUser'
	},
	render : function() {
		$(this.el).html(this.template());
		return this;
	}
	,
	createUser:function(event)
	{
		var termscheck=$('#agreeterms').is(':checked');
		if(termscheck){
			$("#termsdecline").attr("style", "display:none");
	        if(FBuserLogin){
	            validateEmail();
	        }
			createUsers();
			event.preventDefault();
		}else{
			$("#termsdecline").attr("style", "display:block;font-weight: normal;line-height: 17px;margin: 0 0 0 28px;margin-top:5px;");
			return false;
		}
	}
	
});