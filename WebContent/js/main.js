  var ENTER_KEY = 13;
templateLoader.load(["RegistrationView","LoginView","HomeView", "HeaderView", "SideBar", "FooterView", "ProfileView", "NotesView", "MessagesView", "RemindersView", 
                     "RecordLessonView", "StudentsView", "InvoiceView", "CalendarView","CrowdView","HelpView","ResetView","crowdProfile"],
    function () {
        app = new Router();
        Backbone.history.start();
    });