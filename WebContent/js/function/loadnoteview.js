	
	var today = new Date();
	
	var yesterday=new Date();
	yesterday.setDate(yesterday.getDate()-1);
	
	var tomor=new Date();
	tomor.setDate(tomor.getDate()+1);
	var notesTagMap={};
	var sharedDetailsMap={};
	var mapTagNotes={};
	var editSubEventId="";
	var editEventId="";
	var editEventListId="";
	var repeatType="";
	var repeateEndDate="";

	$('#notes').on('click','.dropDownStopProgress', function(event){
		event.stopPropagation();
	});
	
	
//Added by Veeraprathaban.R for Tag name auto search /Note Name auto search
	
	$( "#tagName" ).autocomplete({
	    minLength:1,

    source:function(request, response)
     {
		var tagName=$("#tagName").val();
		var searchText=tagName.substring(tagName.lastIndexOf(",")+1,tagName.length);
	  var url = urlForServer + "note/getTagForAutoSearch";
	  var datastr = '{"Searching":"'+searchText.trimLeft()+'","userId":"'+userId+'"}';
        $.ajax({
        	headers: { 
        	"Mn-Callers" : musicnote,
        	"Mn-time" :musicnoteIn		
        	},
          url: url,
          data: datastr,
          dataType: "json",
          type: "POST",
          success: function(data){
             response(data.slice(0, 7));
           }
        });
      },
      open: function(event, ui) {
          $(".ui-autocomplete").css("position", "absolute");
          $(".ui-autocomplete").css("z-index", "2147483647");
      }
	});
	
	$( "#updateTagName" ).autocomplete({
	    minLength:1,

    source:function(request, response)
     {
		//var tagName="";
	  var url = urlForServer + "note/getTagForAutoSearch";
	  var datastr = '{"Searching":"'+$("#updateTagName").val()+'","userId":"'+userId+'"}';
        $.ajax({
        	headers: { 
        	"Mn-Callers" : musicnote,
        	"Mn-time" :musicnoteIn				
        	},
          url: url,
          data: datastr,
          dataType: "json",
          type: "POST",
          success: function(data){
             response(data.slice(0, 7));
           }
        });
      },
      open: function(event, ui) {
          $(".ui-autocomplete").css("position", "absolute");
          $(".ui-autocomplete").css("z-index", "2147483647");
      }
	});
	
	//End Veeraprathaban Search functions
	
	$('#notes').children('.offset').children('#new-note').remove();
	$('#notes').children('.offset').children('#new-event').remove();
	$('#notes').children('.offset').children('#new-event-book').remove();
	if(listType=='schedule')
	{	
		$('#notes').children('.offset').children('#notesearch').after('<div id="new-event" class="col-md-2" style="" >'
			+'<a id="addEvent" class="btn js-event-note-default col-md-9 own-note-add-btn" style="font-family: Helvetica Neue;font-size:14px;" title="Add new event to default calendar" href="javascript:void(0);">'
			+'<span class="glyphicon glyphicon-plus"></span>&nbsp;Event</a>'
		+'</div><div id="new-event-book" class="col-md-2"style="" >'
		+'<a class="btn js-event-book-default col-md-9 own-note-add-btn" style="font-family: Helvetica Neue;font-size:14px;" title="Add Calendar" href="javascript:addNewEventBook();" id="newEventBook">'
		+'<span class="glyphicon glyphicon-plus"></span>&nbsp;Calendar</a>'
	    +'</div>');
		$('#notebookcombo').removeClass('col-md-7');
		$('#notebookcombo').addClass('col-md-5');
		$('#notebookcombo').attr("style",'position: relative; left: -10px;margin-top:-40px;margin-left:600px;');
		$('#notebookcombo').empty();
		var combocontent='<select id="notebookId" class="form-control col-md-7 selectNotes" style="width:44%;display: inline-block;font-family: Helvetica Neue;margin-left:2%;"></select>'
		+'<div id="calendarlist" style="display: inline-block;position:absolute;margin-left:4px;"></div>'
		+'';
		$('#notebookcombo').append(combocontent);
		loadCalenderss();
		//alert("Test Test");
	}
	else
	{
		var titleToolTip='';
		var titleBookTooltip='';
		if(listType=='bill'){
			titleToolTip ='Add new memos to default book';
			titleBookTooltip ="Add new memobook";
		}else{
			titleToolTip ='Add new notes to default book';
			titleBookTooltip ="Add new notebook";
		}
		$('#notebookcombo').removeClass('col-md-5');
		$('#notebookcombo').addClass('col-md-5');
		$('#notebookcombo').attr("style",'left: 5%; position: relative;');
		$('#notes').children('.offset').children('#notesearch').after('<div id="new-note" class="col-md-3" >'
				+'<button type="button" class="btn own-note-add-btn js-add-note-default col-md-5 fontStyle" id="addNote" title="'+titleToolTip+'" style="text-align: left;padding: 4px 6px;" ><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Note</button>'
				+'<button type="button" class="btn own-note-add-btn newBook col-md-6 " id="addBook" title="'+titleBookTooltip+'"style="text-align: left;padding: 4px 6px;" ><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Notebook</button>'		
				/*	+'<a class="note-button-link js-add-note-default col-md-8" style="text-decoration: none; font-weight: 100; border-radius: 7px 7px 7px 7px;color:white;" title="Add new notes to own book" href="javascript:void(0);">'
					+'<span class="glyphicon glyphicon-plus"></span>&nbsp;New Note</a>'*/
				+'</div>');
		$('#notebookcombo').empty();
		var combocontent='<select id="notebookId" class="form-control col-md-5 selectNotes" style="width:44%;display: inline-block;font-family: Helvetica Neue;margin-left:0%;margin-right:10px;"></select> &nbsp;&nbsp;&nbsp;'
		+'<select id="tagBookId" class="form-control col-md-5 selectTagBook" style="width:44%;display: inline-block;font-family: Helvetica Neue;margin-left:0%;"></select>';
		$('#notebookcombo').append(combocontent);
	}
	
	$(".selectNotes").change(function() {
		var url='';
		var flag='';
		var params ='';
		var listId=$('#notebookId').val();
		var tagId=$('#tagBookId').val();
		var noteName=$('#searchNote').val();
		$('#notes').find('.listDiv').each(function( index ){
    		$(this).remove();
    	});
		if(listId !='note'){
			noteShowingViewLevel='book';
			//getSharedNotes();
			url=urlForServer+"note/getListBasedOnListId/"+listId;
			var multiFilterByTagId='';
			var multiFilterByNoteName='';
			
			if(tagId !='tag')
				multiFilterByTagId=tagId;
			else
				multiFilterByTagId='';
			
			if(noteName!='')
				multiFilterByNoteName=noteName;
			else
				multiFilterByNoteName='';
			
			if(listType!=null && listType=="schedule")
				multiFilterByTagId='';
			
			params = '{"userId":"'+userId+'","listType":"'+listType+'","tagId":"'+multiFilterByTagId+'","noteName":"'+multiFilterByNoteName+'"}';
		}else{
			noteShowingViewLevel='note';
			url = urlForServer+"note/fetchList";flag='1';
			params = '{"userId":"'+userId+'","listType":"'+listType+'","fetchEvent":true}';
			//fetchEvent - add due to event by date & time order
			$("#tagBookId").val("tag");
			$('#searchNote').val('');
		}
		 params = encodeURIComponent(params);

		    $.ajax({
		    	headers: { 
		    	"Mn-Callers" : musicnote,
	        	"Mn-time" :musicnoteIn				
		    	},
		    	type: 'POST',
		    	url : url,
		    	cache: false,
		    	contentType: "application/json; charset=utf-8",
		    	data:params, 
		    	dataType: "json",
		    	success : function(response){
	    			//add due to event by date & time order
	    			if(listType=="schedule" && url==urlForServer+"note/fetchList"){
	    				
	    	    		//setTimeout(function(){loadList(response);},2000);
	    				if(response!= null){
		    				loadList(response);
		    				}
	    	    		getListAndNoteNames();
	    	    		loadCalenderss();
	    			}else{
	    				// music & memo || if schedule follows other url 
	    				if(response!= null){
	    					loadListBasedonList(response,flag);
	    				}
		    			if(listType!=null && listType=="schedule")
		    			{
		    				if(multiFilterByNoteName == ''){
		    					if(response!= null)
		    						loadEventList(userId,listId,listType);	
		    				}else{
		    					var totaleventss=[];
		    					if(response!= null){
			    					for ( var i = 0; i < response.length; i++) {
			    						var arrayEventId = new Array();
			    						if(response[i].mnNotesDetails!= undefined){
											for(var j=0;j<response[i].mnNotesDetails.length;j++){
												
												arrayEventId.push(parseInt (response[i].mnNotesDetails[j].eventId.trim()));
											}
										}else{
											arrayEventId.push(parseInt (response[i].eventId.trim()));
										}
			    						if( listId!= undefined && arrayEventId.length > 0 && arrayEventId[0]!=null ){
			    							param='{"userId":"'+userId+'","eventIds":"'+arrayEventId+'","listId":"'+listId+'"}';  
											var calendarUrl=urlForServer+"note/getEventsFromList";
											param= encodeURIComponent(param);
											$.ajax({
												headers: { 
												"Mn-Callers" : musicnote,
									        	"Mn-time" :musicnoteIn			
												},
												type: 'POST',
												url : calendarUrl,
												cache: false,
												contentType: "application/json; charset=utf-8",
												data:param, 
												dataType: "json",
												success : function(data){
													var eve=[];
													if(data!=null && data!="" && data!="0"){
														for(var i=0;i<data.length;i++){
															var startDate=new Date(data[i].start);
															var sdate=startDate.getDate();
															var smonth=startDate.getMonth();
															var syear=startDate.getFullYear();
															var endDate=new Date(data[i].end);
															var edate=endDate.getDate();
															var emonth=endDate.getMonth();
															var eyear=endDate.getFullYear();
															var curDate=new Date();
	
															var allday=false;
															obj = {};
															obj["title"]=data[i].title;
															obj["description"]=data[i].description;
															obj["start"]=data[i].start;
															obj["end"]=data[i].end;
														
															if(data[i].allDay=='true'){
																allday=true;
															}
															obj["ownerId"]=data[i].ownerId;
															obj["subEventId"]=data[i].subEventId;
															obj["repeatType"]=data[i].repeatType;
															obj["listId"]=data[i].listId;
															obj["eventId"]=data[i].eventId;
															obj["location"]=data[i].location;
															obj["repeatEvent"]=data[i].repeatEvent;
															obj["eventEndDate"]=data[i].eventEndDate;
															obj["allDay"]=allday;
															obj.backgroundColor='#2887BD';
															eve.push(obj);
															callfalg=true;
														}
														 if(eve!=null && eve.length!=0){
													    	    for ( var i = 0; i < eve.length; i++) 
													    	    	{
													    	    	totaleventss.push(eve[i]);  
													    	    	}
													    	    }
															
													}
													
													//loadCal(eve);
												},
												error: function(e) {
												}
											});
			    						}
			    					}
		    					}
		    					alert("Fetching Calendar Events");
								loadCal(totaleventss);
		    				}
		    			}
	    			}
		    	}
		    });
	});
	
	$('#editSubEventClick').click(function() {
		if((userEmail!='null')&&(userEmail!='')){
			$('#editSubEventMailCheckbox').attr('style','display:block;');
		}else{
			$('#editSubEventMailCheckbox').attr('style','display:none;');
		}
		$("#editSubEventRepeatEventwarning").attr("style", "display:none");
		$("#editSubEventRepeatEventwarning").text("");
		$("#editSubEventBetweenDate").attr("style", "display:none");
		$("#editSubEventBetweenDate").text("");
        $("#editSubEventTimes").attr("style", "display:none");
		$("#editSubEventTimes").text("");
        $("#editSubEventStartTimes").attr("style", "display:none");
		$("#editSubEventStartTimes").text("");
        $("#editSubEventDescriptions").attr("style", "display:none");
		$("#editSubEventDescriptions").text("");
        $("#editSubEventNames").attr("style", "display:none");
		$("#editSubEventNames").text("");
		
		param='{"eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","listId":"'+editEventListId+'"}';  
		var calendarUrl=urlForServer+"note/fetchSubEvents";
		param= encodeURIComponent(param);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
        	"Mn-time" :musicnoteIn				
			},
			type: 'POST',
			url : calendarUrl,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:param, 
			dataType: "json",
			success : function(data){
			
			var allday=false;
			if(data.allDay=='true'){
				allday=true;
			}
			$('#editSubEventName').val(data.title);
			$('#editSubEventDescription').val(data.description);
			$('#editSubEventLocation').val(data.location);
			
			
			if(allday){
				var startday="";
				var endday="";
				var subEventStartDate="";
				var subEventEndDate="";
				subEventStartDate=new Date(data.eventStartDate);
				var cDay=subEventStartDate.getDate();
				var cMonth=subEventStartDate.getMonth()+1;
			    var cYear=subEventStartDate.getFullYear();
			    if(cDay<10)
				{
					startday="0"+cDay;
				}
			    else
			    {
			    	startday=cDay;
			    }
				var subEventStart=cMonth+"/"+startday+"/"+cYear;
				
				
				
				subEventEndDate=new Date(data.eventEndDate);
				var cDay=subEventEndDate.getDate();
				var cMonth=subEventEndDate.getMonth()+1;
				var cYear=subEventEndDate.getFullYear();
				
				if(cDay<10)
				{
					endday="0"+cDay;
				}
				else
			    {
					endday=cDay;
			    }
				var subEventEndDate=cMonth+"/"+endday+"/"+cYear;
				$("#editSubEventAllDay").attr("checked",true);
				$('#editSubStartDate').show();
				$('#editSubEndDate').show();
				$('#editSubStartTime').hide();
				$('#editSubEndTime').hide();
				$('#editSubEventStartDate').val(convertNumberDateFormatIntoStringFormat(subEventStart,true));
				$('#editSubEventEndDate').val(convertNumberDateFormatIntoStringFormat(subEventEndDate,true));
			}else{
				var startday="";
				var endday="";
				var subEventStartTime="";
				var subEventEndTime="";
				subEventStartTime=new Date(data.eventStartDate);
				var cDay=subEventStartTime.getDate();
				var cMonth=subEventStartTime.getMonth()+1;
			    var cYear=subEventStartTime.getFullYear();
			    var ctime=subEventStartTime.getHours()+":"+subEventStartTime.getMinutes();
			    if(cDay<10)
				{
					startday="0"+cDay;
				}
			    else
			    {
			    	startday=cDay;
			    }
				var subEventSTime=cMonth+"/"+startday+"/"+cYear+" "+ctime;
				
				
				
				subEventEndTime=new Date(data.eventEndDate);
				var cDay=subEventEndTime.getDate();
				var cMonth=subEventEndTime.getMonth()+1;
				var cYear=subEventEndTime.getFullYear();
			    var ctime=subEventEndTime.getHours()+":"+subEventEndTime.getMinutes();
				 if(cDay<10)
					{
						endday="0"+cDay;
					}
				 else
				    {
					 endday=cDay;
				    }
				var subEventETime=cMonth+"/"+endday+"/"+cYear+" "+ctime;
				$("#editSubEventAllDay").attr("checked",false);
				$('#editSubStartTime').show();
				$('#editSubEndTime').show();
				$('#editSubStartDate').hide();
				$('#editSubEndDate').hide();
				$('#editSubEventStartTime').val(convertNumberDateFormatIntoStringFormat(subEventSTime,false));
				$('#editSubEventEndTime').val(convertNumberDateFormatIntoStringFormat(subEventETime,false));
			}
			$('#editSubEventModel').modal('hide');
			$('#editSubEvent').modal('toggle');
		}
		});
	});
	
	$(".selectDueDate").change(function() {
		
	});

	$(".selectTagBook").change(function() {
		var url='';
		var params='';
		var tagId=$('#tagBookId').val();
		var listId=$('#notebookId').val(); 
		var noteName=$('#searchNote').val();
		var flag=tagId;
		$('#notes').find('.listDiv').each(function( index ){
    		$(this).remove();
    	});
		if(tagId!='tag')
		{
			url=urlForServer+"note/fetchTaggedNote";
			var multiFilterByBookId='';
			var multiFilterByNoteName='';
			if(listId !='note')
				multiFilterByBookId=listId;
			else
				multiFilterByBookId='';
			
			if(noteName!='')
				multiFilterByNoteName=noteName;
			else
				multiFilterByNoteName='';
			
			params = '{"userId":"'+userId+'","listType":"'+listType+'","tagId":"'+tagId+'","listId":"'+multiFilterByBookId+'","noteName":"'+multiFilterByNoteName+'"}';
		}
		else
		{
			noteShowingViewLevel='note';
			url = urlForServer+"note/fetchList";
			params = '{"userId":"'+userId+'","listType":"'+listType+'","fetchEvent":false}';
			//fetchEvent - add due to event by date & time order
			flag="0";
			$("#notebookId").val("note");
			$('#searchNote').val('');
		}
		 params = encodeURIComponent(params);

		    $.ajax({
		    	headers: { 
		    	"Mn-Callers" : musicnote,
	        	"Mn-time" :musicnoteIn				
		    	},
		    	type: 'POST',
		    	url : url,
		    	cache: false,
		    	contentType: "application/json; charset=utf-8",
		    	data:params, 
		    	dataType: "json",
		    	success : function(response){
		    		if(response!=null)
	    			loadListBasedonList(response,flag);
		    	}
		    });
	});


	
	$('#notes').find('.listDiv').each(function( index ){
		//alert("remove called");
		$(this).remove();
	});
	
	$("#moreActionsClose").click(function()
			{
		loadNoteAndEvent();
			});
	function loadNoteAndEvent()
	{
		var url = urlForServer+"note/fetchList";
		var params = '{"userId":"'+userId+'","listType":"'+listType+'","fetchEvent":true}';
		//fetchEvent - add due to event by date & time order
	    params = encodeURIComponent(params);
	    
	    
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response)
	    	{
	    		noteLoadResponse=response;
	    	if(listType!='schedule')
	    	{
	    		getTagsForNote();
	    	}
	    	getSharedNotes();
	    		//setTimeout(function(){loadList(response);},3000);
	    	if(response!=null)
	    		loadList(response);
	    	
	    		getListAndNoteNames();
	    	},
	        error: function(e) {
	            alert("Please try again later2");
	        }
	    
	    });
	}
	var url = urlForServer+"note/fetchList";
	var params = '{"userId":"'+userId+'","listType":"'+listType+'","fetchEvent":true}';
	//fetchEvent - add due to event by date & time order
    params = encodeURIComponent(params);
    
    
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response)
    	{
    		noteLoadResponse=response;
    	if(listType!='schedule')
    	{
    		getTagsForNote();
    	}
    	getSharedNotes();
    		//setTimeout(function(){loadList(response);},3000);
    	if(response!=null)
    		loadList(response);
    	
    		getListAndNoteNames();
    	},
        error: function(e) {
            alert("Please try again later2");
        }
    
    });
   
   
    
    
function loadList(response){
	
	var sharedBy='';
	var finaldivAppend=new Array();
    	if(listType=='schedule'){
    		$('#scheduleContent').empty();
    		$('#calendarlist').empty();
 		
    		var calendarUrl=urlForServer+"note/fetchCalendarSharingUsers";
    		var param = '{"userId":"'+userId+'","listType":"'+listType+'"}';
    	    param = encodeURIComponent(param);
    		$.ajax({
    			headers: { 
    			"Mn-Callers" : musicnote,
            	"Mn-time" :musicnoteIn			
    			},
    			type: 'POST',
    	    	url : calendarUrl,
    	    	cache: false,
    	    	contentType: "application/json; charset=utf-8",
    	    	data:param, 
    	    	dataType: "json",
    	    	success : function(sharing){
    			var details='';
    			var listdetails='';
    			
    			if(sharing!=null && sharing!="" && sharing!="0")
	    		{
    				$('.col-md-offset2').remove();
    				details=details+'<div class="col-md-offset2">'
					+'<div class="btn-group" id="calendareventdetails" role="menu" aria-labelledby="dLabel" style="font-family: Helvetica Neue;font-size:14px;" >'
					+'<a class="btn btn-default" ><i class="glyphicon glyphicon-calendar"></i> View </a>'
					+'<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" ><span class="caret"></span></a>'
    				+'<ul class="dropdown-menu userGroup dropDownStopProgress fontStyle" id="calendarul"></div>';
    				if(details!='')
    	    		{
    	    			$('#calendarlist').append(details);
    	    		}
	    			for(var i=0;i<sharing.length;i++)
	    			{
	    				var color="background:"+sharing[i].color;
	    				listdetails=listdetails+'<li  id="'+sharing[i].listId+'" class="userSelectedGroup"><a id="'+sharing[i].listId+'"><canvas width="10" height="10" style="'+color+'"></canvas> '+sharing[i].listName+'</a> </li>';
	    			}
	    			$('#calendarul').append(listdetails);
	    		}
    					
    	    	},
    	        error: function(e) {
    	            alert("Please try again later");
    	        }
    		});
    		
    	}else {
    		$('#scheduleCal').remove();
    		$('#scheduleContent').remove();
    		$('#calendar').remove();
    		$('#calendarlist').remove();
    		
    	}
    	listAccessMap={};
		var arrayList = new Array(); 
		
    	for ( var i = 0; i < response.length; i++) 
		{
    		//if(response[i].status=='A'){
    			listAccessMap[response[i].listId]=response[i].userId;
    			
    			if(response[i].originalListId!=null)
    				var listId=response[i].originalListId;
    			else
    				var listId=response[i].listId;
    			
    			var noteData="";
    			sharedBy='';
    			//$("#notebookId").append('<option value="'+listId+'">'+listName+'</option>');
    			if(listType=='schedule')
    			{
    				
    			if(response[i].ownerId != userId){
					var usname = activeUserDeatilsMap[response[i].ownerId];
					if(usname!=undefined && usname!='')
					 sharedBy='<div class="badge glyphicon glyphicon-sm decs2" title=" Shared by '+usname+'" id="'+response[i].ownerId+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Shared by '+usname+'</span></div>';
					 
				}
    			}
    			
    			
    			//add due to event by date & time order
    			if(response[i].mnNotesDetails == undefined){
    				var desc ="";
    				if(response[i].description.trim() != ""){
    					desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
    				}
					//var allday=response[i].allDay;
					//var location= '<p>Location : '+response[i].location+'</p>'
    				//if(allday=='true'){
						//var start_dt=convertNumberDateFormatIntoStringFormat(response[i].start,true);
	    				//var end_dt=convertNumberDateFormatIntoStringFormat(response[i].end,true);
	    				
	    				var start_dt=convertNumberDateFormatIntoStringFormat1(response[i].start.trim());
	    				var end_dt=convertNumberDateFormatIntoStringFormat1(response[i].end.trim());
					//}else{
					//	var start_dt=convertNumberDateFormatIntoStringFormat(response[i].start,false);
	    			//	var end_dt=convertNumberDateFormatIntoStringFormat(response[i].end,false);
					//}	
					var start='<p>Start : '+start_dt+'</p>'
					var end='<p>End&nbsp;&nbsp; : '+end_dt+'</p>'
					var description=response[i].description
							
					if(listType=='schedule'){
					var repeat_1="";
					var repeat_2="";
					var repeat_data="";
					var loc_data="";
					var repeat=response[i].eventEndDate;
					var location=response[i].location;
					if(repeat!=null && repeat!=''){
					
					repeat_1=response[i].eventEndDate;
					repeat_2=convertNumberDateFormatIntoStringFormat(repeat_1,true);
					repeat_data='<p id="repeat_until">Repeat Until :'+repeat_2+'</p>'
					}else{
					}
					if(location!=null && location!=''){
					//alert(location);	
					loc_data='<p>Location : '+location+'</p>'	
					}	
    				noteData=noteData+'<div class="noteDiv" id="'+response[i].eventId+'">'
    				+ '<div class="todo_description fontStyle15">'
    				//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
    				//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
    				//+'</div>'
    				+'<p><b>'
    				+ response[i].title 
    				+ '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
					+description
					+'</p>'
					+repeat_data
					+loc_data
					+start
					+end
					+'</div>'
    				+'<div class="badges fontStyle">'
    				+sharedBy
    				+desc
    				+'</div>'
    				+ '</div>';
    				
    				var data= '<div class="listDiv col-md-12" style="float:left;" id="'+listId+'">'
	    				+'<div class="listBodyAll">'
						+noteData
						+'</div>'
    				+'</div>';
    				$('#scheduleContent').append(data);
				}else{
    				noteData=noteData+'<div class="noteDiv" id="'+response[i].eventId+'" style="background-color:#F3F3F3;">'
    				+ '<div class="todo_description fontStyle15">'
    				//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
    				//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
    				//+'</div>'
    				+'<p><b>'
    				+ response[i].title 
    				+ '</b>'
					+'</p></div>'
    				+'<div class="badges fontStyle15">'
    				+sharedBy
    				+desc
    				+'</div>'
    				+ '</div>';
    				
    				var data= '<div class="listDiv col-md-12" style="float:left; background-color:#F3F3F3;" id="'+listId+'">'
	    				+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
						+noteData
						+'</div>'
    				+'</div>';
    				$('#scheduleContent').append(data);
				}		
	    		}else{
	    			
	    			var listName = response[i].listName;
	    			for(var j=0;j<response[i].mnNotesDetails.length;j++){
						var classNote="";
						if(response[i].mnNotesDetails[j].listId!=undefined && response[i].mnNotesDetails[j].listId!=""){
							classNote = listId+" ";
						}
						sharedBy="";
	    				var dueDateData="";
						var cmts="";
						var tempDescAfterNote="";
						var votes="";
						var desc="";
						var attach="";
						var reminderDivContent ="";
						var tagss="";
						var datedesc="";
						var date=new Date(response[i].mnNotesDetails[j].startDate);
						var day=date.getDate();
						var months=date.getMonth();
						var year=date.getFullYear();
						var hour=date.getHours();
						var minutes=date.getMinutes();
						var seconds=date.getSeconds();
						dates=month[months]+"/"+day+"/"+year;
						date1=months+1+"/"+day+"/"+year+" "+hour+":"+minutes;
						dates1=convertNumberDateFormatIntoStringFormat(date1,false);
						
						
						var ownerNoteFlag="false";
						if(listType=="schedule"){
							if(response[i].userId == userId){
								if(response[i].mnNotesDetails[j].ownerEventStatus=='A')
									ownerNoteFlag="true";
							}else{
								ownerNoteFlag="true";
							}
							
						}else{
							if(response[i].userId == userId){
								if(response[i].mnNotesDetails[j].ownerNoteStatus=='A')
									ownerNoteFlag="true";
							}else{
								ownerNoteFlag="true";
							}
						}
						if(response[i].mnNotesDetails[j].status =='A' && ownerNoteFlag=='true'){
							var noteData="";
							if(ownerNoteFlag != 'false' ){
								var noteName="";
								var noteId="";
								if(listType=="schedule"){
									noteName=response[i].mnNotesDetails[j].eventName;
									noteId=response[i].mnNotesDetails[j].eventId;	
								}else{
									noteName=response[i].mnNotesDetails[j].noteName;
									noteId=response[i].mnNotesDetails[j].noteId;
								}
	    					//notesSourceList.push(noteName);
	    					//notesDetailMap[noteName]=noteId;
	    					//listDetailMap[noteName]=listId;
	    					//listNameMap[listId]=listName;
	    					
	    					//Added by veera for tag name display with notes
							if(listType!='schedule'){
	    					if(response[i].mnNotesDetails[j].tag !=""){
	    						var tagNames="";
								var tagArray = new Array();
								var tagWithParams= response[i].mnNotesDetails[j].tag;
								if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
									tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
								}
								if(tagWithParams != undefined && tagWithParams.indexOf("]") != -1){
									tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
								}
								if(tagWithParams != undefined && tagWithParams.indexOf(",") != -1){
									tagArray = tagWithParams.split(',');
								}
								else{
									if(tagWithParams != undefined && tagWithParams.trim() != ''){
										tagArray = tagWithParams.split(',');
									}
								}
									
									for(var siz=0;siz<tagArray.length;siz++){
									
										var tagId=tagArray[siz].trim();
										if (tagId in notesTagMap)
											tagNames+=" "+notesTagMap[tagId]+" ,";
									}
									if(tagNames!=''){
									
								var lastIndex  = tagNames.lastIndexOf(",");
								//alert(lastIndex);
								var tagNames = tagNames.substring(0, lastIndex);
								//alert(str);
								//if(listType!="schedule" && response[i].userId.trim()==userId)
								if(listType!="schedule" && listType!="crowd")
		    					{
								tagss ='<div class="badge glyphicon glyphicon-sm tagss" text="'+tagNames+'" style="white-space: normal;display:inline;background-color:#FFAB60";>'
								+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span> </div>';
		    					}
								}
							}//alert(tagNames);
							}
								//End Veera
	    					
								if(response[i].mnNotesDetails[j].dueDate != null && response[i].mnNotesDetails[j].dueDate != 'null' && response[i].mnNotesDetails[j].dueDate != ""){
									dueDateData=loadDueDateContent(response[i].mnNotesDetails[j].dueDate,response[i].mnNotesDetails[j].dueTime);
								}
								if(response[i].mnNotesDetails[j].comments !=""){
									var cmtArray = new Array();
									var cmtWithParams= response[i].mnNotesDetails[j].comments;
									if(cmtWithParams.indexOf("[") != -1){
										cmtWithParams = cmtWithParams.substring(cmtWithParams.indexOf("[")+1,cmtWithParams.length);
									}
									if(cmtWithParams.indexOf("]") != -1){
										cmtWithParams = cmtWithParams.substring(0,cmtWithParams.indexOf("]"));
									}
									if(cmtWithParams.indexOf(",") != -1){
										cmtArray = cmtWithParams.split(',');
									}else{
										if(cmtWithParams.trim() != ''){
											cmtArray = cmtWithParams.split(',');
										}
									}
									if(cmtArray!=null && cmtArray!=0){
										cmts ='<div class="badge glyphicon glyphicon-sm cmts " title="This note has '+cmtArray.length+' comment(s).">'
											+'<span class="glyphicon glyphicon-comment"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+cmtArray.length+'</span></div>';
									}else{
										cmts='';
									}
								}
							
								if(response[i].mnNotesDetails[j].vote !=""){
									var voteArray = new Array();
									var voteWithParams= response[i].mnNotesDetails[j].vote;
								
									if(voteWithParams.indexOf("[") != -1){
										voteWithParams = voteWithParams.substring(voteWithParams.indexOf("[")+1,voteWithParams.length);
									}
									if(voteWithParams.indexOf("]") != -1){
										voteWithParams = voteWithParams.substring(0,voteWithParams.indexOf("]"));
									}
									if(voteWithParams.indexOf(",") != -1){
										voteArray = voteWithParams.split(',');
								
									}else{
										if(voteWithParams.trim() != ''){
											voteArray = voteWithParams.split(',');
										}
									}
									if(listType=='crowd'){
										votes = '<div class="badge vote-badge glyphicon glyphicon-sm"  title="This note has '+voteArray.length+' vote(s).">'
											+'<span class="glyphicon glyphicon-thumbs-up"></span>'
											+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+voteArray.length+'</span>'     
											+'</div>';
									}
								}
							if(response[i].mnNotesDetails[j].attachFilePath!=''){
								var attachArray = new Array();
								var attachWithParams= response[i].mnNotesDetails[j].attachFilePath;
								
								if(attachWithParams.indexOf("[") != -1){
									attachWithParams = attachWithParams.substring(attachWithParams.indexOf("[")+1,attachWithParams.length);
								}
								if(attachWithParams.indexOf("]") != -1){
									attachWithParams = attachWithParams.substring(0,attachWithParams.indexOf("]"));
								}
								if(attachWithParams.indexOf(",") != -1){
									attachArray = attachWithParams.split(',');
								
								}else{
									if(attachWithParams.trim() != ''){
										attachArray = attachWithParams.split(',');
									}
								}
								if(attachArray!=null && attachArray!=0)
								{
								attach = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+attachArray.length+' attachment(s).">'
											+'<span class="glyphicon glyphicon-download-alt"></span>'
											+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+attachArray.length+'</span>'     
										+'</div>';
								}
								else
								{
									attach='';
								}
							}
							if(listType!=null && listType!='schedule'){
							if(response[i].mnNotesDetails[j].remainders.trim() != ""){
								var remainderWithParam=response[i].mnNotesDetails[j].remainders;
								var remainder = new Array();
								
								if(remainderWithParam.indexOf("[") != -1){
									remainderWithParam = remainderWithParam.substring(remainderWithParam.indexOf("[")+1,remainderWithParam.length);
								}
								if(remainderWithParam.indexOf("]") != -1){
									remainderWithParam = remainderWithParam.substring(0,remainderWithParam.indexOf("]"));
								}
								if(remainderWithParam.indexOf(",") != -1){
									remainder = remainderWithParam.split(',');
								}else{
									if(remainderWithParam.trim() != ''){
										remainder = remainderWithParam.split(',');
									}
								}
								if(remainder.length > 0 && remainder[0]!=''){ 
									var remData  = remindersMap[parseInt(remainder[0].trim())];
									if(remData != null && remData!= ''){
										var styleAttr = "";
										var span='';
										if(remData.inactiveRemainderUserId!=null){
											if(remData.inactiveRemainderUserId!=null){
												var inactiveRemainderUserIds =new Array();
												var inactiveRemainderUserIdsWithParam=remData.inactiveRemainderUserId;
												if(inactiveRemainderUserIdsWithParam.indexOf("[") != -1){
													inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(inactiveRemainderUserIdsWithParam.indexOf("[")+1,inactiveRemainderUserIdsWithParam.length);
												}
												if(inactiveRemainderUserIdsWithParam.indexOf("]") != -1){
													inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(0,inactiveRemainderUserIdsWithParam.indexOf("]"));
												}
												if(inactiveRemainderUserIdsWithParam.indexOf(",") != -1){
													inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
												}else{
													if(inactiveRemainderUserIdsWithParam.trim() != ''){
														inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
													}
												}
												if(inactiveRemainderUserIds.length > 0 && inactiveRemainderUserIds[0] != ''){
													if(inactiveRemainderUserIds.indexOf(userId) == -1){
														//active
														span='style="color:black;"';
														styleAttr ='text-decoration: none;background:#DFDD0A;';//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
													}else{
														//inactive
														span='style="color:white;"';
														styleAttr ='text-decoration: none;background: #A888A3;';//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
													}
												}else{
													//active
													span='style="color:black;"';
													styleAttr ='text-decoration: none;background: #DFDD0A;'; //background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
												}
											}	
										}else{
											//inactive
											span='style="color:white;"';
											styleAttr ='text-decoration: none;background: #A888A3;';//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
										}
										var selectedDate=new Date(remData.eventDate);
											
										var condate=new Date(selectedDate);
										var convertedDate="";
										convertedDate=month[condate.getMonth()];
										convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
										var rName = remData.rName;
										if(rName.length > 36){
											rName = rName.substring(0,35)+"...";
										}
										reminderDivContent='<div class="badge reminders glyphicon glyphicon-sm " title="'+rName+' on '+convertedDate+' at '+remData.eventTime+'"'
														+' style="'+styleAttr+'">'
														+'<i class="glyphicon glyphicon-time"></i>'
														+'<span class="badge-text" style="word-spacing:-6px;" '+span+'>&nbsp;&nbsp;'+convertedDate +'</span>'     
													+'</div>';
									}
								}
							}
							}
							if(listType!='schedule')
							{
							if(response[i].userId != userId){
								var shareUserId=sharedDetailsMap[response[i].listId+'~'+response[i].mnNotesDetails[j].noteId];
								 usname = activeUserDeatilsMap[shareUserId];
								 
								 if(usname==undefined)
								 {
									 shareUserId=sharedDetailsMap[response[i].listId.trim()+'~'+0];
									 usname = activeUserDeatilsMap[shareUserId];
								 }
								 
								 if(usname!=undefined && usname!='')
								 sharedBy='<div class="badge glyphicon glyphicon-sm decs2" title=" Shared by '+usname+'" id="'+shareUserId+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Shared by '+usname+'</span></div>';
							}
							
							if(response[i].mnNotesDetails[j].copyFromMember != null && response[i].mnNotesDetails[j].copyFromMember != 'null' && response[i].mnNotesDetails[j].copyFromMember != "")
							{
								var userFromId=response[i].mnNotesDetails[j].copyFromMember;
								if(userFromId != userId && sharedBy=="")
								{
								var usname = activeUserDeatilsMap[userFromId];
								if(usname!=undefined && usname!='')
								sharedBy='<div class="badge glyphicon glyphicon-sm decs2" title=" Copied by '+usname+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Copied by '+usname+'</span></div>';
								}
							}
							}
							
							if(response[i].mnNotesDetails[j].description.trim() != ""){
								var tempDesc = response[i].mnNotesDetails[j].description.trim();
								desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
								if(listType!="schedule" && tempDesc!=""){
									tempDescAfterNote = loadDescContent(noteName,tempDesc);
								}
							}	
							if(listType=='bill')
							{
								datedesc='<div class="badge glyphicon glyphicon-sm decs1" title="This note added on '+dates1+'"><span class="glyphicon glyphicon-play-circle"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+dates+'</span></div>';
							}
							else
							{
								datedesc="";
							}
							
							
							
							
							if(listType!='schedule'){
							noteData=noteData+'<div class="'+classNote+'noteDiv '+response[i].mnNotesDetails[j].shareType+'" id="'+noteId+'">'
	    						+ '<div class="todo_description fontStyle15">'
	    						+'<p><b>'
	    						+ noteName
	    						+ '</b>'
	    						+tempDescAfterNote
								+'</p></div>'
								// displaythe cmt, vote, desc, due date
									+'<div class="badges fontStyle">'
									+sharedBy
									+votes
									+cmts
									+desc
									+attach
									+tagss
									+dueDateData
									+datedesc
									+reminderDivContent
									+'</div>'
	    						+ '</div>';
							}else{
								
								//var location= '<p>Location : '+response[i].mnNotesDetails[j].location+'</p>'
								var start_dt1=convertNumberDateFormatIntoStringFormat1(response[i].mnNotesDetails[j].startDate.trim());
			    				var end_dt1=convertNumberDateFormatIntoStringFormat1(response[i].mnNotesDetails[j].endDate.trim());
//			    				var allday=response[i].mnNotesDetails[j].allDay;
//			    				alert(allday);
//			    				if(allday=='true'){
								//var start_dt1=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].startDate,true);
				    			//var end_dt1=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].endDate,true);
							//	}else{
								//	var start_dt1=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].startDate,false);
				    			//	var end_dt1=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].endDate,false);
								//}
								var start='Start : '+start_dt1+'</p>'
								var end='<p>End&nbsp;&nbsp; : '+end_dt1+'</p>'
								var description=response[i].mnNotesDetails[j].description
								var location=response[i].mnNotesDetails[j].location;
								var loc_data="";
								
								if(location!=null && location!=''){
									//alert(location);	
								loc_data='<p>Location : '+location+'</p>';	
								}
								noteData=noteData+'<div class="'+classNote+'noteDiv '+response[i].mnNotesDetails[j].shareType+'" id="'+noteId+'">'
	    						+ '<div class="todo_description fontStyle15">'
	    						+'<p><b>'
	    						+ noteName
	    						+'</b>&nbsp;&nbsp;&nbsp;&nbsp;'
	    						+description
	    						+'</p>'
								+loc_data
	    						+start
								+end
	    						+'</div>'
	    						+tempDescAfterNote
									+'<div class="badges fontStyle">'
									+sharedBy
									+votes
									+cmts
									+desc
									+attach
									+tagss
									+dueDateData
									+datedesc
									+reminderDivContent
									+'</div>'
	    						+ '</div>';
							}
								
	    					}
							var data='';
							if(listType!='schedule' && noteData!= null && noteData.trim()!=""){
							data=data+ '<div class="listDiv col-md-11" style="float:left; background-color:#F3F3F3;" id="'+listId+'">';
							/*if(response[i].userId != userId ){
								if(arrayList.indexOf(listId) == -1){
									arrayList.push(listId);
									var shardInd = response[i].sharedIndividuals;
									shardInd = shardInd.replace("[","");
									shardInd = shardInd.replace("]","");
									var sharInd ;
									if(shardInd  != '' ){
										sharInd= shardInd.split(",");
									}else{
										sharInd = new Array();
									}
									var shardGrp = response[i].sharedGroups;
									shardGrp = shardGrp.replace("[",""); 
									shardGrp = shardGrp.replace("]","");
									var sharGrp;
									if(shardGrp != ''){
										sharGrp = shardGrp.split(",");
									}else{
										sharGrp = new Array();
									}
									if(((sharInd.length > 0  && sharInd[0]!= '' && sharInd.indexOf(userId) != -1 )
										||(sharGrp.length > 0 && sharGrp[0] != '' ) || response[i].shareAllContactFlag) ) {
											data=data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>';
									}else{
										data=data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom hidden"></b></a>';
									}
								}else{
									data=data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom hidden"></b></a>';
								}
							}else{
								data=data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>';
							}*/
	    				
							data = data /*+'<ul class="dropdown-menu" id="'+listId+'booklists" style="position: absolute;top: 16%;overflow-y: none;width: auto;left:155px;"></ul>'
							+'</div>'
							+'<div><b class="headerListName">'
							+listName
							+'</b></div>'
							+'</div>'*/
							+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
							+noteData
							+'</div>';
							/*+'<div class="js-footer">';*/
						}else{
							
							if(response[i].userId!=userId && noteData!= null && noteData.trim()!="")
							{
								data=data+ '<div class="listDiv col-md-12" style="float:left;background-color:#F3F3F3;" id="'+listId+'">';
								data=data/*+'<div class="modalHeader"><div><a class="close listMenuOthers dropdown-toggle"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
								+'</div>'
								+'<div><b class="headerListName">'
								+listName
								+'</b></div>'
								+'</div>'*/
								+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
								+noteData
								+'</div>';
								/*+'<div class="js-footer">';*/
							}
							else if(noteData!= null && noteData.trim()!="")
							{
								data=data+ '<div class="listDiv col-md-12" style="float:left;background-color:#F3F3F3;" id="'+listId+'">';
								data=data/*+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
								+'<ul class="dropdown-menu" id="'+listId+'booklists" style="position: absolute;top: 16%;overflow-y: none;width: auto;left:155px;"></ul>'
								+'</div>'
								+'<div><b class="headerListName">'
								+listName
								+'</b></div>'
								+'</div>'*/
								+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
								+noteData
								+'</div>';
								/*+'<div class="js-footer">';*/
							}
						}
						if(listType!='schedule'){
							/*if(userId==response[i].userId)
								data=data+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer" style="text-decoration: none;">Add New Note</a>';*/
						}else{
							/*var gotUserId=response[i].userId;
							if(userId==gotUserId)
								data=data+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe schedule-footer" style="text-decoration: none;">Add New Event</a>'
								+'</div>';*/
						}
						+'</div>';
						
						if(listType=='schedule'){
							//$('#scheduleContent').append(data);
							finaldivAppend[i]=data;
						}
						else{
							//$('#notes').append(data);
							finaldivAppend[i]=data;
						}
						
	    			}
	    			}	
	    		}
			}
    	////////////////
    	
    	if(listType=='schedule'){
    		for(n=0;n<finaldivAppend.length;n++)
			{
			$('#scheduleContent').append(finaldivAppend[n]);
			}
		}
		else{
			$('#notes').find('.listDiv').each(function( index ){
				$(this).remove();
			});
			for(n=0;n<finaldivAppend.length;n++)
			{
				$('#notes').append(finaldivAppend[n]);
			}
		}
       ////////////////
    }
    function loadDescContent(noteName,tempDesc){
		var tempDescAfterNote="";
		if(tempDesc.indexOf("<a") == -1 ){
			while(tempDesc.indexOf("<br>") != -1){
				tempDesc = tempDesc.replace("<br>"," ");
			}
			while(tempDesc.indexOf("\n") != -1){
				tempDesc = tempDesc.replace("\n"," ");
			}
			if(noteName.length < 10){	
				if(tempDesc.length > 120){
					tempDesc = tempDesc.substring(0,119)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 10 && noteName.length < 20){
				if(tempDesc.length > 110){
					tempDesc = tempDesc.substring(0,109)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 20 && noteName.length < 30){
				if(tempDesc.length > 100){	
					tempDesc = tempDesc.substring(0,99)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 30 && noteName.length < 40){
				if(tempDesc.length > 90){
					tempDesc = tempDesc.substring(0,89)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 40 && noteName.length < 50){
				if(tempDesc.length > 80){
					tempDesc = tempDesc.substring(0,79)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 50 && noteName.length < 60){
				if(tempDesc.length > 70){
					tempDesc = tempDesc.substring(0,69)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 60 && noteName.length < 70){
				if(tempDesc.length > 60){
					tempDesc = tempDesc.substring(0,59)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 70 && noteName.length < 80){
				if(tempDesc.length > 50){
					tempDesc = tempDesc.substring(0,49)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else if (noteName.length >= 80 && noteName.length < 90){
				if(tempDesc.length > 40){
					tempDesc = tempDesc.substring(0,39)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}else{
				if(tempDesc.length > 36){
					tempDesc = tempDesc.substring(0,35)+"...";
				}
				tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
			}
		}else{
			while(tempDesc.indexOf("<br>") != -1){
				tempDesc = tempDesc.replace("<br>"," ");
			}
			tempDescAfterNote = '&nbsp;&nbsp;<span title="Description">&nbsp;&nbsp;&nbsp;'+tempDesc+'</span>';
		}
		return tempDescAfterNote;
	}
    
    function loadDueDateContent(dueDate,dueTime){
    	var selectedDate=new Date(dueDate);//.toLocaleFormat('%m/%d/%Y');
    	dueTime=dueTime.toLowerCase();
    	if(dueTime.indexOf("a") != -1){
    		if(dueTime.indexOf(" ") == -1){
    			var str=new Array();
    			str=dueTime.split('am');
    			dueTime=str[0]+" "+"am";
    		}
    	}else if(dueTime.indexOf("p") != -1){
    		if(dueTime.indexOf(" ") == -1){
    			var str=new Array();
    			str=dueTime.split('pm');
    			dueTime=str[0]+" "+"pm";
    		}
    	}
    	
    	var selectedDateTime=new Date(dueDate+' '+dueTime);
    	var todayDateTime=new Date();
		var date=new Date(dueDate);
		var dueStatus;
		var title;
		var seconds,minutes=0,hours=0,days;
		if(selectedDate < yesterday)
		{
			dueStatus='due-past';
			title="This note is past due.";
		}
		else if(selectedDate == yesterday)
		{
			dueStatus='due-past';
			title="This note is recently past due!";
		}
		else if(selectedDate == tomor)
		{
			seconds = Math.floor((selectedDateTime - (todayDateTime))/1000);
			 minutes = Math.floor(seconds/60);
			 if(minutes > 60)
			 {
				 hours = Math.floor(minutes/60);
				 minutes =Math.floor(minutes%60);
			 }
			
			dueStatus='due-future';
			title="This note is due in "+hours+" hours and "+minutes +" minutes";
		}
		else if (selectedDate > tomor)
		{
			dueStatus='due-future';
			title="This note is due later.";
		}
		else if(selectedDateTime < todayDateTime)
		{
				dueStatus='due-now';
				title="This note is recently past due.";
			
		}else if(selectedDateTime > todayDateTime){
			
			 seconds = Math.floor((selectedDateTime - (todayDateTime))/1000);
			 minutes = Math.floor(seconds/60);
			 if(minutes > 60)
			 {
				 hours = Math.floor(minutes/60);
				 minutes =Math.floor(minutes%60);
			 }
			 days = Math.floor(hours/24);
			
			dueStatus='due-soon';
			title="This note is due in "+hours+" hours and "+minutes +" minutes";
		}
		
		var condate=new Date(date);
		var convertedDate="";
		convertedDate=month[condate.getMonth()];
		convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
		
		var dueDateDivContent='<div class="badge '+dueStatus+' glyphicon glyphicon-sm " title="'+title+'">'
									+'<i class="glyphicon glyphicon-calendar"></i>'
									+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+convertedDate +'</span>'     
								+'</div>';
		
		return dueDateDivContent;
    }
   

var totalevents=[];
$('#calendarlist').on('click','.userSelectedGroup',function(){
	var ids =$(this).attr('id');
	var param ="";
    $('#calendar').empty();
    
	param='{"userId":"'+userId+'","listType":"'+listType+'","listId":"'+ids+'"}';  	
	var calendarUrl=urlForServer+"note/fetchCalendarEvents";
	var eve=[];
    param = encodeURIComponent(param);
    var backgroundColor=$(this).find("canvas").attr('style');
    var color="";
    if(backgroundColor!=null && backgroundColor!="")
    {
     	var colorarray=backgroundColor.split(":");
     	if(colorarray!=null && colorarray!="")
     		color=colorarray[1];
    }
    
	if(!$(this).find("i").hasClass("glyphicon-ok"))
	{
		 if(!$(this).parent().children().find("i").hasClass("glyphicon-ok"))
 	 	{
 			 totalevents=[];	
 	 	}
		 $(this).find("a").prepend('<i class="glyphicon glyphicon-ok pull-right"></i>');
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
        	"Mn-time" :musicnoteIn				
			},
			type: 'POST',
	    	url : calendarUrl,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:param, 
	    	dataType: "json",
	    	success : function(data){
			if(data!=null && data!='' && data!='0'){
				
	    	    for(var i=0;i<data.length;i++){
	    	    	//alert("data[i].start   :::  "+data[i].start);
	    	    	var startDate=new Date(data[i].start);
	    	    	var sdate=startDate.getDate();
	    	    	var smonth=startDate.getMonth();
	    	    	var syear=startDate.getFullYear();
	    	    	var endDate=new Date(data[i].end);
	    	    	var edate=endDate.getDate();
	    	    	var emonth=endDate.getMonth();
	    	    	var eyear=endDate.getFullYear();
	    	    	var curDate=new Date();

	    	    	var allday=false;

	    	    	if(data[i].allDay=='true'){
	    	    		allday=true;
	    	    	}
	    	    		obj = {};
	    	    		obj["title"]=data[i].title;
		    	    	obj["description"]=data[i].description;
		    	    	obj["start"]=data[i].start;
		    	    	obj["end"]=data[i].end;
	    	    	
	    	    	obj["ownerId"]=data[i].ownerId;
	    	    	obj["subEventId"]=data[i].subEventId;
	    	    	obj["repeatType"]=data[i].repeatType;
	    	    	obj["listId"]=data[i].listId;
	    	    	obj["eventId"]=data[i].eventId;
	    	    	obj["location"]=data[i].location;
	    	    	obj["repeatEvent"]=data[i].repeatEvent;
	    	    	obj["eventEndDate"]=data[i].eventEndDate;
	    	    	
	    	    	obj["allDay"]=allday;
	    	    	obj["color"]=ids;
	    	    	if(color!=null && color!="")
	    	    	obj.backgroundColor=color;
	    	    	eve.push(obj);
	    	    }
	    	   
	    	    if(eve!=null && eve.length!=0){
	    	    for ( var i = 0; i < eve.length; i++) 
	    	    {
	    	    	totalevents.push(eve[i]);  
				}
	    	    }
	    	 // add due to event by date & time order
	    	   // loadList(data);
	    	 }
			loadCal(totalevents);
				
		},
		error:function(e)
		{
			alert("Please try again later");
		}
		});

	}
	else
	{
		$(this).find("i").remove();
		
		 if(totalevents!=null && totalevents.length!=0){
			    for ( var i = totalevents.length-1; i >=0 ; i--) 
	    	    {
			    	if(totalevents[i].color==ids)
	    	    	{
	    	    	    totalevents.splice(i,1);
	    	    	}
				}
	    }
		if(!$(this).parent().children().find("i").hasClass("glyphicon-ok"))
	 	{
			if(totalevents!=null && totalevents.length!=0)
	 	     totalevents=[];	
	 	}
		loadCal(totalevents);
	}
	
	
});

//getListAndNoteNames();loadTags();

//Load list and notes for search functionality

function getListAndNoteNames(){
	var Nvalue=$("#notebookId").val();
	var notesSourceList = new Array();
	var notesDetailMap={};
	var listDetailMap={};
	var listNameMap={};
	
	//alert("My method called");
	$('#notes').find('.input-prepend').each(function( index ){
		$(this).remove();
	});
	var searchContent='<div class="input-prepend col-md-12"><span class="add-on"><i class="glyphicon glyphicon-search"></i></span> <input class="form-control input-large search-query col-md-12"'+
	'data-provide="typeahead" style="border-radius:15px;height:30px;margin-top:-30px;margin-left:27px" id="searchNote" type="text" ';
	if(listType=='bill'){
		searchContent = searchContent +'placeholder="Search by Memo"></div>';
	}else{
		searchContent = searchContent +'placeholder="Search by Note"></div>';
	}
	$('#notesearch').append(searchContent);
	
	
	$("#notebookId").empty();
	
	if(listType != 'schedule'){
		$("#notebookId").append('<option value="note" style="font-family: Helvetica Neue;">&nbsp;&nbsp;Notebooks</option>');
	loadTags();
	}else{
		$("#notebookId").append('<option value="note" style="font-family: Helvetica Neue;">&nbsp;&nbsp;Calendars</option>');
	}
	var totaleventss=[];
	$( "#searchNote" ).keypress(function(e) {
		
		var unicode=e.charCode? e.charCode:e.keyCode; 
		if(unicode==13){
			//alert(ui.item.label);
			if($("#searchNote").val()!=null && $("#searchNote").val()!='')
			{
				noteShowingViewLevel='note';
				var listId=$('#notebookId').val();
				var tagId=$('#tagBookId').val();
				var multiFilterByBookId='';
				var multiFilterByTagId='';
				
				if(listId !='note')
					multiFilterByBookId=listId;
				else
					multiFilterByBookId='';
				
				if(tagId !='tag')
					multiFilterByTagId=tagId;
				else
					multiFilterByTagId='';
				
				if(listType!=null && listType=='schedule')
					multiFilterByTagId='';
					
				var url = urlForServer + "note/getAllNoteByKeyWord";
				var datastr = '{"keyword":"'+$("#searchNote").val()+'","userId":"'+userId+'","listType":"'+listType+'","listId":"'+multiFilterByBookId+'","tagId":"'+multiFilterByTagId+'"}';
				$('#notes').find('.listDiv').each(function( index ){
					$(this).remove();
				});
				totaleventss=[];
				var callfalg=false;
				datastr= encodeURIComponent(datastr);
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
		        	"Mn-time" :musicnoteIn				
					},
					type: 'POST',
					url : url,
					cache: false,
					contentType: "application/json; charset=utf-8",
					data:datastr, 
					dataType: "json",
					success : function(response){
						
						if(listType == 'schedule'){
							for ( var i = 0; i < response.length; i++) {
								var listId = response[i].listId;
								var arrayEventId = new Array();
								if(response[i].mnNotesDetails!= undefined){
									for(var j=0;j<response[i].mnNotesDetails.length;j++){
										
										arrayEventId.push(parseInt (response[i].mnNotesDetails[j].eventId.trim()));
									}
								}else{
									arrayEventId.push(parseInt (response[i].eventId.trim()));
								}
								if( listId!= undefined && arrayEventId.length > 0 && arrayEventId[0]!=null ){
									param='{"userId":"'+userId+'","eventIds":"'+arrayEventId+'","listId":"'+listId+'"}';  
									var calendarUrl=urlForServer+"note/getEventsFromList";
									param= encodeURIComponent(param);
									$.ajax({
										headers: { 
										"Mn-Callers" : musicnote,
							        	"Mn-time" :musicnoteIn			
										},
										type: 'POST',
										url : calendarUrl,
										cache: false,
										contentType: "application/json; charset=utf-8",
										data:param, 
										dataType: "json",
										success : function(data){
											var eve=[];
											if(data!=null && data!="" && data!="0"){
												for(var i=0;i<data.length;i++){
													var startDate=new Date(data[i].start);
													var sdate=startDate.getDate();
													var smonth=startDate.getMonth();
													var syear=startDate.getFullYear();
													var endDate=new Date(data[i].end);
													var edate=endDate.getDate();
													var emonth=endDate.getMonth();
													var eyear=endDate.getFullYear();
													var curDate=new Date();

													var allday=false;
													obj = {};
													obj["title"]=data[i].title;
													obj["description"]=data[i].description;
													obj["start"]=data[i].start;
													obj["end"]=data[i].end;
												
													if(data[i].allDay=='true'){
														allday=true;
													}
													
													obj["ownerId"]=data[i].ownerId;
													obj["subEventId"]=data[i].subEventId;
													obj["repeatType"]=data[i].repeatType;
													obj["listId"]=data[i].listId;
													obj["eventId"]=data[i].eventId;
													obj["location"]=data[i].location;
													obj["repeatEvent"]=data[i].repeatEvent;
													obj["eventEndDate"]=data[i].eventEndDate;
												
													obj["allDay"]=allday;
													obj.backgroundColor='#2887BD';
													eve.push(obj);
													callfalg=true;
												}
												 if(eve!=null && eve.length!=0){
											    	    for ( var i = 0; i < eve.length; i++) 
											    	    	{
											    	    	totaleventss.push(eve[i]);  
											    	    	}
											    	    }
											}
										},
										error: function(e) {
										}
									});
								}
								
							}
							// alerted added for the note search to load in cal.
							alert("Fetching Calendar Events");
							loadCal(totaleventss);
						}	
						
						loadList(response);		
					},
					error: function(e) {
						alert("Please try again later");
					}
			
				});
			}else{
				reloadNotes();
				//alert("Please enter a search term to perform a search");
			}
		}
    });
	var url = urlForServer+"note/fetchList";
	var params = '{"userId":"'+userId+'","listType":"'+listType+'","fetchEvent":false}';
    params = encodeURIComponent(params);

    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
    	 
    		$("#notebookId").empty();
    		if(listType=="schedule"){
    		$("#notebookId").append('<option value="note" style="font-family: Helvetica Neue;">&nbsp;&nbsp;Calendars</option>');
    		}else{
    			$("#notebookId").append('<option value="note" style="font-family: Helvetica Neue;">&nbsp;&nbsp;Notebooks</option>');
    		}
    		
    		var arrayEventId = new Array();
        	for ( var i = 0; i < response.length; i++) 
    		{
        		if(response[i].status=='A'){
        			var listName = response[i].listName;
        			var listId=response[i].listId;
        			var noteData="";
        			if(listType != 'schedule'){
						if(arrayEventId.indexOf(parseInt(listId))== -1){
								arrayEventId.push(parseInt(listId));
								if(listId==Nvalue){
								$("#notebookId").append('<option selected="true" value="'+listId+'" class="'+response[i].userId +' '+response[i].defaultNote+'" style="margin-left: 10px;font-family: Helvetica Neue; font-size:15px; padding:1px;margin-top: 2px">'+listName+'</option>');
								}else{
									$("#notebookId").append('<option value="'+listId+'" class="'+response[i].userId +' '+response[i].defaultNote+'" style="margin-left: 10px;font-family: Helvetica Neue; font-size:15px; padding:1px;margin-top: 2px">'+listName+'</option>');
								}	
						}
        			}else{
        				if(arrayEventId.indexOf(parseInt(listId))== -1){
							arrayEventId.push(parseInt(listId));
        				if(listId==Nvalue){
        				$("#notebookId").append('<option selected="true" value="'+listId+'" class="'+response[i].userId +' '+response[i].defaultNote+'" style="margin-left: 10px;font-family: Helvetica Neue; font-size:15px; padding:1px;margin-top: 2px">'+listName+'</option>');
        				}else{
        				$("#notebookId").append('<option value="'+listId+'" class="'+response[i].userId +' '+response[i].defaultNote+'" style="margin-left: 10px;font-family: Helvetica Neue; font-size:15px; padding:1px;margin-top: 2px">'+listName+'</option>');	
        				}
        				}
        			}
        			for(var j=0;j<response[i].mnNotesDetails.length;j++){
        				var dueDateData="";
    					var cmts="";
    					var votes="";
    					var desc="";
    					var dates="";
						var tagss="";
    					var ownerNoteFlag="false";
    					if(listType=="schedule"){
    						if(response[i].userId == userId){
    							if(response[i].mnNotesDetails[j].ownerEventStatus=='A')
    								ownerNoteFlag="true";
    						}else{
    							ownerNoteFlag="true";
    						}
    						
    					}else{
    						if(response[i].userId == userId){
    							if(response[i].mnNotesDetails[j].ownerNoteStatus=='A')
    								ownerNoteFlag="true";
    						}else{
    							ownerNoteFlag="true";
    						}
    					}
    					
    					if(response[i].mnNotesDetails[j].status=='A' ){
    						if(ownerNoteFlag != 'false' ){
    							var noteName="";
    							var noteId="";
    							if(listType=="schedule"){
    								noteName=response[i].mnNotesDetails[j].eventName;
    								noteId=response[i].mnNotesDetails[j].eventId;	
    							}else{
    								noteName=response[i].mnNotesDetails[j].noteName;
    								noteId=response[i].mnNotesDetails[j].noteId;
    							}
    							notesSourceList.push(noteName);
    							notesDetailMap[noteName]=noteId;
    							listDetailMap[noteName]=listId;
    							listNameMap[listId]=listName;	
    						}
    					}
        			}
        		}
    		}
        	
        	
    }
    });
}


function loadCal(eve)
{
	$('#calendar').empty();
	$('#calendar').fullCalendar({
		theme: true,
		editable: false,
		events:eve ,
		timeFormat: { 
			week: '' 
			},
		
		eventClick: function(event) {
		
		 var ownerId=event.ownerId ;
		 
		 var currentDate = new Date(event.start);
		    var day = currentDate.getDate();
		    var month = currentDate.getMonth() + 1;
		    var year = currentDate.getFullYear();
		    var hour = currentDate.getHours();
		    var minitues =currentDate.getMinutes();
		    
		   
			if(day<10)
			{
				day="0"+day;
			}
		    
			var sdateValue="";
			if(event.allDay==true){
				sdateValue=month + "/" + day + "/" + year;
			}
			else
			{
				sdateValue=month + "/" + day + "/" + year + " " +hour + ":" +minitues;
			}

			if(event.end==null)
			{
				event.end=event.start;
			}
		    var ecurrentDate = new Date(event.end);
		    var eday = ecurrentDate.getDate();
		    var emonth = ecurrentDate.getMonth() + 1;
		    var eyear = ecurrentDate.getFullYear();
		    var ehour = ecurrentDate.getHours();
		    var eminitues = ecurrentDate.getMinutes();
		    
		    var edateValue="";
		   
			if(eday<10)
			{
				eday="0"+eday;
			}
		    
			if(event.allDay==true){
				edateValue=emonth + "/" + eday + "/" + eyear;
			}
			else
			{
				edateValue=emonth + "/" + eday + "/" + eyear + " " +ehour + ":" +eminitues;
			}
			
		
			editSubEventId=event.subEventId;
			editEventId=event.eventId;
			editEventListId=event.listId;
			repeatType="";
			if(event.repeatType!=null && event.repeatType!="")
				repeatType=event.repeatType;
			
			if(event.repeatType!=null && event.repeatType!='')
			{
			$("#viewScheduleRepeatEvent").text(event.repeatType);
			}
			else
			{
				$("#viewScheduleRepeatEvent").text("-");
			}
			$("#editViewSubEventName").text(event.title);
			
			$("#viewScheduleAllDay").text("No");
			$("#viewScheduleTitle").text(event.title);
			$("#viewScheduleDesc").text(event.description);
			$("#viewScheduleLocation").text(event.location);
			if(event.allDay==true){
			$("#viewScheduleAllDay").text("Yes");
			$("#editViewSubEventAllDay").text("Yes");
			$("#editViewSubEventStart").text(convertNumberDateFormatIntoStringFormat(sdateValue,true));
			$("#editViewSubEventEnd").text(convertNumberDateFormatIntoStringFormat(edateValue,true));
			$("#viewScheduleStart").text(convertNumberDateFormatIntoStringFormat(sdateValue,true));
			$("#viewScheduleEnd").text(convertNumberDateFormatIntoStringFormat(edateValue,true));
			}
			else{
				$("#editViewSubEventAllDay").text("No");
				$("#editViewSubEventStart").text(convertNumberDateFormatIntoStringFormat(sdateValue,false));
				$("#editViewSubEventEnd").text(convertNumberDateFormatIntoStringFormat(edateValue,false));
				$("#viewScheduleStart").text(convertNumberDateFormatIntoStringFormat(sdateValue,false));
				$("#viewScheduleEnd").text(convertNumberDateFormatIntoStringFormat(edateValue,false));
			}
			repeateEndDate="";
			    if((event.eventEndDate!=null && event.eventEndDate!='')&&(event.eventEndDate!='undefined/NaN/NaN' || event.eventEndDate!='undefined/undefined/undefined'))
			    {
			    	    var ecurrentEndDate = new Date(event.eventEndDate);
					    var eday = ecurrentEndDate.getDate();
					    var emonth = ecurrentEndDate.getMonth() + 1;
					    var eyear = ecurrentEndDate.getFullYear();
					    var ehour = ecurrentEndDate.getHours();
					    var eminitues =ecurrentEndDate.getMinutes();
					    ecurrentEndDate=emonth + "/" + eday + "/" + eyear;
					    var test=convertNumberDateFormatIntoStringFormat(ecurrentEndDate,true);
			    	    $("#viewScheduleRepeatEnd").text(test);
			    	    repeateEndDate=test;
			    }
			    else
			    {
			    	 $("#viewScheduleRepeatEnd").text('-');
			    }
			    if(event.ownerId==userId){
				    $('#editSubEventModel').modal('toggle');
				  }
				  else{
					  $('#viewScheduleModel').modal('toggle');
				  }
			
			return false;
		},
		
		loading: function(bool) {
			if (bool) {
				$('#loading').show();
			}else{
				$('#loading').hide();
			}
		}
		
	});

}


function loadTags() {
	$("#tagBookId").empty();
	tagFilter=new Array();
	var url = urlForServer + "note/getTags";
	var params = '{"userId":"' + userId + '"}';
	params = encodeURIComponent(params);
	$("#tagBookId").append('<option value="tag" style="margin-left: 10px; font-size:15px;font-family: Helvetica Neue; padding:1px;margin-top: 2px">Tags</option>');
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
		},
		type : 'POST',
		url : url,
		cache : false,
		contentType : "application/json; charset=utf-8",
		data : params,
		dataType : "json",
		success : function(response) {
			if (response != null && response != '') {
				for ( var i = 0; i < response.length; i++) {
					tagFilter.push(response[i].tagName);
					$("#tagBookId").append('<option value="'+response[i].tagId+'" style="margin-left: 10px;font-family: Helvetica Neue; font-size:15px; padding:1px;margin-top: 2px" >'+response[i].tagName+'</option>');
				}
			}
		}
	});
}


function loadListBasedonList(response,flag){
	var sharedBy="";
	for ( var i = 0; i < response.length; i++) 
	{
		var scheduleFlag=false;
		 sharedBy="";
		if(response[i].status=='A'){
			var listId=response[i].listId;
			var noteData="";
			// add due to event by date & time order
			if(response[i].mnNotesDetails == undefined){
				var desc ="";
				if(response[i].description.trim() != ""){
					desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
				}
				
				if(listType=='schedule')
    			{
    			if(response[i].ownerId != userId){
					var usname = activeUserDeatilsMap[response[i].ownerId];
					 sharedBy='<div class="badge glyphicon glyphicon-sm decs2" title=" Shared by '+usname+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Shared by '+usname+'</span></div>';
				}
    			}
				
				noteData=noteData+'<div class="noteDiv" id="'+response[i].eventId+'" style="background-color:#F3F3F3;">'
				+ '<div class="todo_description fontStyle15">'
				//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
				//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
				//+'</div>'
				+'<p><b>'
				+ response[i].title
				+ '</b>'
				+'</p></div>'
				+'<div class="badges fontStyle">'
				+sharedBy
				+desc
				+'</div>'
				+ '</div>';
				
			}else{
				// memos , schedule if by search. && music 	
				var listName = response[i].listName;
				
				for(var j=0;j<response[i].mnNotesDetails.length;j++){
					var classNote="";
					scheduleFlag=true;
					if(response[i].mnNotesDetails[j].listId!=undefined && response[i].mnNotesDetails[j].listId!=""){
						classNote = response[i].mnNotesDetails[j].listId+" ";
					}
					var dueDateData="";
					var cmts="";
					var attach = "";
					var tempDescAfterNote= "";
					var votes="";
					var desc="";
					var tagss="";
					var reminderDivContent="";
					var datedesc="";
					var date=new Date(response[i].mnNotesDetails[j].startDate);
					var day=date.getDate();
					var months=date.getMonth();
					var year=date.getFullYear();
					var hour=date.getHours();
					var minutes=date.getMinutes();
					var seconds=date.getSeconds();
					dates=month[months]+"/"+day+"/"+year;
					date1=months+1+"/"+day+"/"+year+" "+hour+":"+minutes;
					dates1=convertNumberDateFormatIntoStringFormat(date1,false);
					var ownerNoteFlag="false";
					sharedBy="";
					if(listType=="schedule"){
						if(response[i].userId == userId){
							if(response[i].mnNotesDetails[j].ownerEventStatus=='A')
								ownerNoteFlag="true";
						}else{
							ownerNoteFlag="true";
						}
						
					}else{
						if(response[i].userId == userId){
							if(response[i].mnNotesDetails[j].ownerNoteStatus=='A')
								ownerNoteFlag="true";
						}else{
							ownerNoteFlag="true";
						}
					}
					if(response[i].mnNotesDetails[j].status=='A' && ownerNoteFlag=='true'){
						if(ownerNoteFlag != 'false' ){
							var noteName="";
							var noteId="";
							if(listType=="schedule"){
								noteName=response[i].mnNotesDetails[j].eventName;
								noteId=response[i].mnNotesDetails[j].eventId;	
							}else{
								noteName=response[i].mnNotesDetails[j].noteName;
								noteId=response[i].mnNotesDetails[j].noteId;
							}
						
						
						//Added by veera for tag name display with notes
						if(listType!='schedule'){
						if(response[i].mnNotesDetails[j].tag !=""){
							var tagNames="";
							var tagArray = new Array();
							var tagWithParams= response[i].mnNotesDetails[j].tag;
							if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
								tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
							}
							if(tagWithParams != undefined && tagWithParams.indexOf("]") != -1){
								tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
							}
							if(tagWithParams != undefined && tagWithParams.indexOf(",") != -1){
								tagArray = tagWithParams.split(',');
							}else{
								if(tagWithParams != undefined && tagWithParams.trim() != ''){
									tagArray = tagWithParams.split(',');
								}
							}
						//	alert("tagWithParams   "+tagWithParams);
							
							for(var siz=0;siz<tagArray.length;siz++){
								//alert(tagArray[siz]);
								
								var tagId=tagArray[siz].trim();
								if(tagId in notesTagMap)
									tagNames+=" "+notesTagMap[tagId]+" ,";
							}
							
							if(listType!="schedule" && listType!="crowd")
	    					{
								if(tagNames!=''){
									var lastIndex  = tagNames.lastIndexOf(",");
									var tagNames = tagNames.substring(0, lastIndex);	
								tagss ='<div class="badge glyphicon glyphicon-sm tagss" style="white-space: normal;display:inline;background-color:#FFAB60" title="'+tagNames+'">'
										+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span> </div>';
								}
	    					}
						}
						}
						//End Veera
						
						if(response[i].mnNotesDetails[j].dueDate != null && response[i].mnNotesDetails[j].dueDate != 'null' && response[i].mnNotesDetails[j].dueDate != "")
							dueDateData=loadDueDateContent(response[i].mnNotesDetails[j].dueDate,response[i].mnNotesDetails[j].dueTime);
						
							if(response[i].mnNotesDetails[j].comments !=""){
								var cmtArray = new Array();
								var cmtWithParams= response[i].mnNotesDetails[j].comments;
								if(cmtWithParams.indexOf("[") != -1){
									cmtWithParams = cmtWithParams.substring(cmtWithParams.indexOf("[")+1,cmtWithParams.length);
								}
								if(cmtWithParams.indexOf("]") != -1){
									cmtWithParams = cmtWithParams.substring(0,cmtWithParams.indexOf("]"));
								}
								if(cmtWithParams.indexOf(",") != -1){
									cmtArray = cmtWithParams.split(',');
								}else{
									if(cmtWithParams.trim() != ''){
										cmtArray = cmtWithParams.split(',');
									}
								}
								if(cmtArray.length > 0 && cmtArray[0] != ''){
									cmts ='<div class="badge glyphicon glyphicon-sm cmts " title="This note has '+cmtArray.length+' comment(s).">'
										+'<span class="glyphicon glyphicon-comment"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+cmtArray.length+'</span></div>';
								}
							}
							
							if(response[i].mnNotesDetails[j].attachFilePath!=''){
								var attachArray = new Array();
								var attachWithParams= response[i].mnNotesDetails[j].attachFilePath;
								
								if(attachWithParams.indexOf("[") != -1){
									attachWithParams = attachWithParams.substring(attachWithParams.indexOf("[")+1,attachWithParams.length);
								}
								if(attachWithParams.indexOf("]") != -1){
									attachWithParams = attachWithParams.substring(0,attachWithParams.indexOf("]"));
								}
								if(attachWithParams.indexOf(",") != -1){
									attachArray = attachWithParams.split(',');
								
								}else{
									if(attachWithParams.trim() != ''){
										attachArray = attachWithParams.split(',');
									}
								}
								if(attachArray!=null && attachArray.length > 0)
								{
								attach = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+attachArray.length+' attachment(s).">'
											+'<span class="glyphicon glyphicon-download-alt"></span>'
											+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+attachArray.length+'</span>'     
										+'</div>';
								}
								else
								{
									attach='';
								}
							}
							if(listType!=null && listType!='schedule'){
							if(response[i].mnNotesDetails[j].remainders.trim() != ""){
								var remainderWithParam=response[i].mnNotesDetails[j].remainders;
								var remainder = new Array();
								
								if(remainderWithParam.indexOf("[") != -1){
									remainderWithParam = remainderWithParam.substring(remainderWithParam.indexOf("[")+1,remainderWithParam.length);
								}
								if(remainderWithParam.indexOf("]") != -1){
									remainderWithParam = remainderWithParam.substring(0,remainderWithParam.indexOf("]"));
								}
								if(remainderWithParam.indexOf(",") != -1){
									remainder = remainderWithParam.split(',');
								}else{
									if(remainderWithParam.trim() != ''){
										remainder = remainderWithParam.split(',');
									}
								}
								if(remainder.length > 0 && remainder[0]!=''){ 
									var remData  = remindersMap[parseInt(remainder[0].trim())];
									if(remData != null && remData!= ''){
										var styleAttr = "";
										var span='';
										if(remData.inactiveRemainderUserId!=null){
											if(remData.inactiveRemainderUserId!=null){
												var inactiveRemainderUserIds =new Array();
												var inactiveRemainderUserIdsWithParam=remData.inactiveRemainderUserId;
												if(inactiveRemainderUserIdsWithParam.indexOf("[") != -1){
													inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(inactiveRemainderUserIdsWithParam.indexOf("[")+1,inactiveRemainderUserIdsWithParam.length);
												}
												if(inactiveRemainderUserIdsWithParam.indexOf("]") != -1){
													inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(0,inactiveRemainderUserIdsWithParam.indexOf("]"));
												}
												if(inactiveRemainderUserIdsWithParam.indexOf(",") != -1){
													inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
												}else{
													if(inactiveRemainderUserIdsWithParam.trim() != ''){
														inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
													}
												}
												if(inactiveRemainderUserIds.length > 0 && inactiveRemainderUserIds[0] != ''){
													if(inactiveRemainderUserIds.indexOf(userId) == -1){
														//active
														span='style="color:black;"';
														styleAttr ='text-decoration: none;background: #DFDD0A;';//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
													}else{
														//inactive
														span='style="color:white;"';
														styleAttr ='text-decoration: none;background: #A888A3;';//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
													}
												}else{
													//active
													span='style="color:black;"';
													styleAttr ='text-decoration: none;background: #DFDD0A;';//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
												}
											}	
										}else{
											//inactive
											span='style="color:white;"';
											styleAttr ='text-decoration: none;background: #A888A3;';//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
										}
										var selectedDate=new Date(remData.eventDate);
											
										var condate=new Date(selectedDate);
										var convertedDate="";
										convertedDate=month[condate.getMonth()];
										convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
										var rName = remData.rName;
										if(rName.length > 36){
											rName = rName.substring(0,35)+"...";
										}
										reminderDivContent='<div class="badge reminders glyphicon glyphicon-sm " title="'+rName+' on '+convertedDate+' at '+remData.eventTime+'"'
														+' style="'+styleAttr+'">'
														+'<i class="glyphicon glyphicon-time"></i>'
														+'<span class="badge-text" style="word-spacing:-6px;" '+span+'>&nbsp;&nbsp;'+convertedDate +'</span>'     
													+'</div>';
									}
								}
							}
						}
							if(response[i].mnNotesDetails[j].vote !=""){
								var voteArray = new Array();
								var voteWithParams= response[i].mnNotesDetails[j].vote;
							
								if(voteWithParams.indexOf("[") != -1){
									voteWithParams = voteWithParams.substring(voteWithParams.indexOf("[")+1,voteWithParams.length);
								}
								if(voteWithParams.indexOf("]") != -1){
									voteWithParams = voteWithParams.substring(0,voteWithParams.indexOf("]"));
								}
								if(voteWithParams.indexOf(",") != -1){
									voteArray = voteWithParams.split(',');
							
								}else{
									if(voteWithParams.trim() != ''){
										voteArray = voteWithParams.split(',');
									}
								}
							
								if(listType=='crowd'){
									votes = '<div class="badge vote-badge glyphicon glyphicon-sm"  title="This note has '+voteArray.length+' votes(s).">'
										+'<span class="glyphicon glyphicon-thumbs-up"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+voteArray.length+'</span>'     
										+'</div>';
								}
							}
							
							if(listType!='schedule')
							{
							if(response[i].userId != userId){
								var shareId=sharedDetailsMap[response[i].listId+'~'+response[i].mnNotesDetails[j].noteId];
								 usname = activeUserDeatilsMap[shareId];
								 
								 if(usname==undefined)
								 {
									 shareId=sharedDetailsMap[response[i].listId.trim()+'~'+0];
									 usname = activeUserDeatilsMap[shareId];
								 }
								 
								 sharedBy='<div class="badge glyphicon glyphicon-sm decs2" title=" Shared by '+usname+'" id="'+shareId+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Shared by '+usname+'</span></div>';
							}
							
							
							if(response[i].mnNotesDetails[j].copyFromMember != null && response[i].mnNotesDetails[j].copyFromMember != 'null' && response[i].mnNotesDetails[j].copyFromMember != "")
							{
								var userFromId=response[i].mnNotesDetails[j].copyFromMember;
								if(userFromId != userId && sharedBy=="")
								{
								var usname = activeUserDeatilsMap[userFromId];
								sharedBy='<div class="badge glyphicon glyphicon-sm decs2" title=" Copied by '+usname+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Copied by '+usname+'</span></div>';
								}
							}
							}
							
							if(listType=='schedule' && response[i].userId != userId)
			    			{
								var shareId=sharedDetailsMap[response[i].listId+'~'+response[i].mnNotesDetails[j].eventId];
								var usname = activeUserDeatilsMap[response[i].userId];
								sharedBy='<div class="badge glyphicon glyphicon-sm decs2" title=" Shared by '+usname+'"  id="'+shareId+'"><span class="glyphicon glyphicon-user"></span><span class="badge-text" style="word-spacing:-6px;"> Shared by '+usname+'</span></div>';
							}
			    			
							
							if(response[i].mnNotesDetails[j].description.trim() != ""){
								var tempDesc = response[i].mnNotesDetails[j].description.trim();
								desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
								if(listType!="schedule" && tempDesc!=""){
									tempDescAfterNote = loadDescContent(noteName,tempDesc);
								}
							}
							
							if(listType=='bill')
							{
							datedesc='<div class="badge glyphicon glyphicon-sm decs1" title="This note added on '+dates1+'"><span class="glyphicon glyphicon-play-circle"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+dates+'</span></div>';
							}
							else
							{
								datedesc="";
							}
							if(flag!='' && flag!='tag'){
								noteData=noteData+'<div class="'+classNote+'noteDiv '+response[i].mnNotesDetails[j].shareType+'" id="'+noteId+'" style="">';
							}else{
								noteData=noteData+'<div class="'+classNote+'noteDiv '+response[i].mnNotesDetails[j].shareType+' " id="'+noteId+'" style="border: 1px solid #dddddd;">';
							}
							

							// book level shared option
							var shardInd = response[i].sharedIndividuals;
							shardInd = shardInd.replace("[","");
							shardInd = shardInd.replace("]","");
							var sharInd ;
							if(shardInd  != '' ){
								sharInd= shardInd.split(",");
							}else{
								sharInd = new Array();
							}
							var shardGrp = response[i].sharedGroups;
							shardGrp = shardGrp.replace("[",""); 
							shardGrp = shardGrp.replace("]","");
							var sharGrp;
							if(shardGrp != ''){
								sharGrp = shardGrp.split(",");
							}else{
								sharGrp = new Array();
							}

							
							if(listType=='schedule'){
							var repeats_1="";
							var repeats_2="";
							var repeats_data="";
							var start_dt2=convertNumberDateFormatIntoStringFormat1(response[i].mnNotesDetails[j].startDate.trim());
		    				var end_dt2=convertNumberDateFormatIntoStringFormat1(response[i].mnNotesDetails[j].endDate.trim());

							//var location= '<p>Location : '+response[i].mnNotesDetails[j].location+'</p>'
//		    				var allday=response[i].mnNotesDetails[j].allDay;
//		    				alert(allday);
//		    				if(allday=='true'){
		    				//var start_dt2=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].startDate,true);
			    			//var end_dt2=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].endDate,true);
							//}else{
							//	var start_dt2=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].startDate,false);
			    			//	var end_dt2=convertNumberDateFormatIntoStringFormat(response[i].mnNotesDetails[j].endDate,false);
							//}
							var start='Start : '+start_dt2+'</p>'
							var end='<p>End&nbsp;&nbsp; : '+end_dt2+'</p>'
							var description=response[i].mnNotesDetails[j].description
							var location=response[i].mnNotesDetails[j].location;
							var loc_data="";
							
							/*var repeats=response[i].eventEndDate;
							//alert(repeats)
							if(repeats!=null && repeats!=''){
					
							repeats_1=response[i].mnNotesDetails[j].eventStartDate	
							repeats_data='<p id="repeat_until">Repeat Until :'+repeat_1+'</p>'
							}else{
					
							//repeat_1="&nbsp;-";
							//$('#repeats_until').attr("style","display:none");
					
							}*/if(location!=null && location!=''){
								loc_data='<p>Location : '+location+'</p>'	
								}
					
								noteData=noteData
								+ '<div class="todo_description fontStyle15">'
								+'<p><b>'
								+ noteName
								+ '</b>'
								+ '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
								+description
								+'</p>'
								//+repeats_data
								+loc_data
								+start
								+end
								+tempDescAfterNote
								+'</div>'
								// displaythe cmt, vote, desc, due date
								+'<div class="badges fontStyle">'
								+sharedBy
								+votes
								+cmts
								+desc
								+attach
								+tagss
								+dueDateData
								+datedesc
								+reminderDivContent
								+'</div>'
								+ '</div>';
							}else{
								noteData=noteData
								+ '<div class="todo_description fontStyle15">'
								+'<p><b>'
								+ noteName
								+ '</b>'
								+tempDescAfterNote
								+'</p></div>'
								// displaythe cmt, vote, desc, due date
								+'<div class="badges fontStyle">'
								+sharedBy
								+votes
								+cmts
								+desc
								+attach
								+tagss
								+dueDateData
								+datedesc
								+reminderDivContent
								+'</div>'
								+ '</div>';
							}
						
						}
						if(flag!='' && flag!='tag'){
							var data='';
							if(listType!='schedule'){
								data=data+ '<div class="listDiv col-md-11" style="float:left;background-color:#F3F3F3;" id="'+listId+'">';
								/*if(response[i].userId != userId){
									data = data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom hidden"></b></a>';
								}else{
									data = data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>';
								}*/
							}else{
								data = data+ '<div class="listDiv col-md-12" style="float:left;background-color:#F3F3F3;" id="'+listId+'">'
								/*+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'*/;
							}
							data = data /*+'<ul class="dropdown-menu" id="'+listId+'booklists" style="position: absolute;top: 16%;overflow-y: none;width: auto;left:155px;"></ul>'
							+'</div>'
							+'<div><b class="headerListName">'
							+listName
							+'</b></div>'
							+'</div>'*/
							+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
							+noteData
							+'</div>'
							/*+'<div class="js-footer">'*/;
							var gotUserId=response[i].userId;
							if(listType!='schedule'){
								/*if(userId==gotUserId)
								data=data+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer" style="text-decoration: none;">Add New Note</a>';*/
							}else{
								/*if(userId==gotUserId)
								data=data+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe schedule-footer" style="text-decoration: none;">Add New Note</a>'
								+'</div>';*/
							}
							
							+'</div>';
					
							if(listType=='schedule'){
								$('#scheduleContent').append(data);
							}//scheduleContent
							else{
								$('#notes').append(data);
							}
							noteData="";
						}
					}
						
				}
			}
			var size='';
			if(listType!='schedule')
				size='left:687px;';
			else
				size='left:715px;';
			if(flag==''){
				var data='';
				if(listType!='schedule'){
					data=data+ '<div class="listDiv col-md-11" style="float:left;background-color:#F3F3F3;" id="'+listId+'">';
					
					if(((sharInd.length > 0  && sharInd[0]!= '' && sharInd.indexOf(userId) != -1 )
							||(sharGrp.length > 0 && sharGrp[0] != '' ) || (response[i].shareAllContactFlag) || (response[i].userId == userId ))) {
								data=data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>';
						}else{
							data=data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom hidden"></b></a>';
						}
					
					
					/*if(response[i].userId != userId){
						data = data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom hidden"></b></a>';
					}else{
						data = data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>';
					}*/
					
					
				}else{
					
					data = data+ '<div class="listDiv col-md-12" style="float:left; background-color:#F3F3F3;" id="'+listId+'">';
					if(response[i].userId != userId){
						data = data+'<div class="modalHeader"><div><a class="close listMenuOthers dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>';
						size='left:715px;';
					}else{
						data = data+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>';
						size='left:655px;';
					}
					
				}
				data = data +'<ul class="dropdown-menu" id="'+listId+'booklists" style="top: auto;overflow-y: none;width: auto;'+size+'"></ul>'
				+'</div>'
				+'<div><b class="headerListName fontStyle15">'
				+listName
				+'</b></div>'
				+'</div>'
				+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
				+noteData
				+'</div>'
				+'<div class="js-footer">';
				var gotUserId=response[i].userId;
				if(listType!='schedule'){
					if(userId==gotUserId)
					data=data+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer fontStyle" style="text-decoration: none;">Add New Note</a>';
				}else{
					if(userId==gotUserId)
					data=data+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe schedule-footer fontStyle" style="text-decoration: none;">Add New Event</a>';
				}
				+'</div>'
				+'</div>';
				
				$('#notes').find('.listDiv').each(function( index ){
				$(this).remove();
				});
				if(listType=='schedule'){
					if(scheduleFlag)
					$('#scheduleContent').append(data);
				}//scheduleContent
				else{
					$('#notes').append(data);
				}
			}
		}
	}
}

function loadEventList(userId,listId,listType)
{
	$('#calendar').empty();
	
	if( listId!="note")
	{
		param='{"userId":"'+userId+'","listType":"'+listType+'","listId":"'+listId+'"}';  	
		var calendarUrl=urlForServer+"note/fetchCalendarEvents";
	}
	else
	{
		var calendarUrl = urlForServer+"note/fetchScheduleEvents";
		param = '{"userId":"'+userId+'"}';
	}
	

	
	var eve=[];
    param = encodeURIComponent(param);
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
    	},
		type: 'POST',
    	url : calendarUrl,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:param, 
    	dataType: "json",
    	success : function(data){
    	
    	$('#calendar').empty();
		if(data!=null && data!="" && data!="0"){
	    for(var i=0;i<data.length;i++){
	    	var startDate=new Date(data[i].start);
	    	var sdate=startDate.getDate();
	    	var smonth=startDate.getMonth();
	    	var syear=startDate.getFullYear();
	    	var endDate=new Date(data[i].end);
	    	var edate=endDate.getDate();
	    	var emonth=endDate.getMonth();
	    	var eyear=endDate.getFullYear();
	    	var curDate=new Date();

	    	var allday=false;
	    	obj = {};
	    	
	    		obj["title"]=data[i].title;
    	    	obj["description"]=data[i].description;
    	    	obj["start"]=data[i].start;
    	    	obj["end"]=data[i].end;
	    	
	    	
	    	if(data[i].allDay=='true'){
	    		allday=true;
	    	}
	    	
	    	obj["ownerId"]=data[i].ownerId;
	    	obj["subEventId"]=data[i].subEventId;
	    	obj["repeatType"]=data[i].repeatType;
	    	obj["listId"]=data[i].listId;
	    	obj["eventId"]=data[i].eventId;
	    	obj["location"]=data[i].location;
	    	obj["repeatEvent"]=data[i].repeatEvent;
	    	obj["eventEndDate"]=data[i].eventEndDate;
	    	obj.backgroundColor='#2887BD';
	    	obj["allDay"]=allday;
	    	eve.push(obj);
	    }
	    loadCal(eve);
		}
		else
		{
			loadCal(eve);
		}

    	
    },
    error : function(error)
    {
       alert("Pls try againg later");	
    }
    });

}

function getTodaySchedule(){

	$("#tempTablke").empty();
	$("#tempTablke").append('<table id="todaySchedules"></table>');
	
	var today = new Date();
	var monthStr=today.getMonth()+1;
	if(monthStr<10 && monthStr>0)
	{
		monthStr="0"+monthStr;
	}
	var dateStr=today.getDate();
	if(dateStr<10)
	{
		dateStr="0"+dateStr;
	}
	today=monthStr+'/'+dateStr+'/'+today.getFullYear();

	var url = urlForServer+"note/fetchTodayScheduleEvents";
	var params = '{"userId":"'+userId+'","date":"'+today+'"}';
    
    params = encodeURIComponent(params);
	
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
//    		loadList(response);
    
    		
    		var schedules = new todayScheduleList(response);
    		
    			                       // For each schedule, add a row in the table
    			                       var gridData = [];
    			                       schedules.each(function(schedule) {
    			                           var item = schedule.toJSON();
    			                           item.id = $.jgrid.randId();
    			                           gridData.push(item);
    			                       });
    			                   	

    			                       if(gridData.length!=0)
    			                       {
    			                       // Create the table
									   var pageWidth = $("#sideBar").width() - 30;
									   if(pageWidth <100){
											pageWidth = 230;
									   }
    			                       var scheduleTable = jQuery("#todaySchedules");
    			                       scheduleTable.jqGrid({ 
    			                           datatype: 'local',
    			                           data: gridData,
    			                           width:'100%',
    			                           height: 'auto',
    			                       	gridview: true,
    			                       	colNames:['Today\'s Schedule'], 
    			                       	colModel:[ 
    			                       		  		{name:'Description',index:'Description', width:pageWidth} 
    			                       		  	],
    			                       	loadComplete : function(data) {
    			                               //alert('grid loading completed ' + data);
    			                           },
    			                           loadError : function(xhr, status, error) {
    			                              // alert('grid loading error' + error);
    			                           }
    			                       	
    			                       	
    			                       });
    			                      
    			                       }
    			                      
    		
    		
    		
    		
        },
        error: function(e) {
           // alert("Please try again later");
        }
    
    });

	
	
}
function getTagsForNote(){
	notesTagMap={};
	mapTagNotes={};
	var url = urlForServer + "note/getTags";
	var params = '{"userId":"' + userId + '"}';
	params = encodeURIComponent(params);
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
		},
		type : 'POST',
		url : url,
		cache : false,
		contentType : "application/json; charset=utf-8",
		data : params,
		dataType : "json",
		success : function(response) {
			if (response != null && response != '') {
				for ( var i = 0; i < response.length; i++) {
					notesTagMap[response[i].tagId]=response[i].tagName;
					mapTagNotes[response[i].tagName.toLowerCase()]=response[i].tagId;
				}
			}
			loadList(noteLoadResponse);
		}
	});

}

function getTagsForNoteBookView(){
	notesTagMap={};
	mapTagNotes={};
	var url = urlForServer + "note/getTags";
	var params = '{"userId":"' + userId + '"}';
	params = encodeURIComponent(params);
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
		"Mn-time" :musicnoteIn				
		},
		type : 'POST',
		url : url,
		cache : false,
		contentType : "application/json; charset=utf-8",
		data : params,
		dataType : "json",
		success : function(response) {
			if (response != null && response != '') {
				for ( var i = 0; i < response.length; i++) {
					notesTagMap[response[i].tagId]=response[i].tagName;
					mapTagNotes[response[i].tagName.toLowerCase()]=response[i].tagId;
				}
			}
			loadListBasedonList(bookLoadResponse,"");
		}
	});

}



function getSharedNotes(){
	sharedDetailsMap={};
	var url = urlForServer + "note/sharedNotes";
	var params = '{"userId":"' + userId + '","listType":"'+listType+'"}';
	params = encodeURIComponent(params);
	
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
		},
		type : 'POST',
		url : url,
		cache : false,
		contentType : "application/json; charset=utf-8",
		data : params,
		dataType : "json",
		success : function(response) {
			if (response != null && response != '') {
				
				for ( var i = 0; i < response.length; i++) {
					sharedDetailsMap[response[i].listId+'~'+response[i].noteId.trim()]=response[i].userId;
				}
				loadList(noteLoadResponse);

			}
		}
	});

}

function reloadNotes() {
	var searchValue=$('#searchNote').val();
	var tagValue=$('#tagBookId').val();
	if(searchValue=="" && tagValue=="tag"){
	getSharedNotes();
	reLoadList();
	}
}

function loadCalenderss(){

	var eve=[];
	$('#scheduleCal').remove();
	$('#scheduleContent').remove();
	$('#calendar').remove();
	var contentss='<div id="scheduleCal" class="col-md-11"></div>'+
	'<div id="scheduleContent" class="col-md-11"></div>';
	$('#notes').append(contentss);
	$('#scheduleCal').append('<div id="calendar"></div>');
	if(listType=='schedule'){

		  var scheduleUrl = urlForServer+"note/fetchScheduleEvents";
		   
			var scheduleParams = '{"userId":"'+userId+'"}';
			scheduleParams = encodeURIComponent(scheduleParams);

		    $.ajax({
		    	headers: { 
		    	"Mn-Callers" : musicnote,
	        	"Mn-time" :musicnoteIn				
		    	},
		    	
		    	type: 'POST',
		    	url : scheduleUrl,
		    	cache: false,
		    	contentType: "application/json; charset=utf-8",
		    	data:scheduleParams, 
		    	dataType: "json",
		    	success : function(response){
		    		
					if(response!=null && response!=''){
					
			    	    for(var i=0;i<response.length;i++){
			    	    	var startDate=new Date(response[i].start);
			    	    	var sdate=startDate.getDate();
			    	    	var smonth=startDate.getMonth();
			    	    	var syear=startDate.getFullYear();
			    	    	var endDate=new Date(response[i].end);
			    	    	var edate=endDate.getDate();
			    	    	var emonth=endDate.getMonth();
			    	    	var eyear=endDate.getFullYear();
			    	    	var curDate=new Date();

			    	    	var allday=false;

			    	    	if(response[i].allDay=='true'){
			    	    		allday=true;
			    	    	}
			    	    	obj = {};
			    	    	
			    	    		obj["title"]=response[i].title;
				    	    	obj["description"]=response[i].description;
				    	    	obj["start"]=response[i].start;
				    	    	obj["end"]=response[i].end;
				    	    	obj["subEventId"]=response[i].subEventId;
				    	    	obj["repeatType"]=response[i].repeatType;
			    	    		obj["ownerId"]=response[i].ownerId;
			    	    		obj["listId"]=response[i].listId;
			    	    		obj["eventId"]=response[i].eventId;
			    	    		obj["location"]=response[i].location;
			    	    		obj["repeatEvent"]=response[i].repeatEvent;
			    	    		obj["eventEndDate"]=response[i].eventEndDate;
			    	    		obj["allDay"]=allday;
			    	    		obj.backgroundColor='#2887BD';
			    	    		eve.push(obj);
			    	    	
			    	    }
					}
					loadCal(eve);
	          },
	          error: function(e) {
	              alert("Please try again later");
	          }
	      
		    });
		   
		}
}


