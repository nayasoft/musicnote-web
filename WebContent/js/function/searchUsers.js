//var userDetails=[{"userId":"1000","userName":"Rajesh","image":"pics/dummy.jpg"},{"userId":"1001","userName":"Bashid","image":"pics/dummy.jpg"},{"userId":"1002","userName":"Venu Gopal","image":"pics/dummy.jpg"},{"userId":"1003","userName":"Veera Prathaban","image":"pics/dummy.jpg"},{"userId":"1004","userName":"Senthil","image":"pics/dummy.jpg"},{"userId":"1005","userName":"Nandha Kumar","image":"pics/dummy.jpg"},{"userId":"1006","userName":"Ranjeeth Kumar","image":"pics/dummy.jpg"},{"userId":"1007","userName":"Rajeev","image":"pics/dummy.jpg"},{"userId":"1008","userName":"Uthaman","image":"pics/dummy.jpg"}];
var userDetailMap={};
var userSourceList=new Array();
var selectedUserId;
var friends=false;
var notFriends=false;
var requestSend=false;
var responsePending=false;
var followers=false;
for (var i=0; i<userDetails.length; i++)
{
	var user=userDetails[i];
	userSourceList.push(user.uniquserName);
	userDetailMap[user.uniquserName]=user;
	
}

$('.headerMenu').on('click','#userslists', function(event){
	event.stopPropagation();
});

$('#groupMemberContainer').on('click','.dropdown-menu', function(event)
		{
			event.stopPropagation();
		});
$('#fndrequests').on('click', function(event) {
	$('#fndrequests').parent().children('.on').remove();
	document.getElementById("requestlable").innerHTML="";
	
	
});

//to send the friend request

$('#groupMemberContainer').on('click','.selectedUser',function(){
	
	var selectUserId=$(this).parent().parent().attr('id');
	
	 var params = '{"userId":"'+selectUserId+'","requestId":"'+userId+'"}';
	    console.log("params : " + params);
	    params = encodeURIComponent(params);
	    var url = urlForServer+"friends/addfriend";
	    $("#addfriend"+selectUserId).replaceWith( $('<div id="requesting"> <a  href="javascript:void(0);" style="text-decoration: none;font-family: Helvetica Neue;">Sending request... </a></div>') );
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	        type: 'POST',
	        url : url,
	        cache: false,
	        contentType: "application/json; charset=utf-8",
	        
	        data:params, 
	        dataType: "json",
	        complete: function(transport) {
	    	if (transport.status == 200) {
	    		console.log("success");
	    		 $("#requesting").replaceWith( $('<div > <a  href="javascript:void(0);" style="text-decoration: none;font-family: Helvetica Neue;">Request was Sent</a></div>') );
	        }
	    	 else {
	             alert("Please try again later");
	         }
	    }
	    });
});

$('#groupMemberContainer').on('click','.userSelectedGroup',function(){
	if(!$(this).find("i").hasClass("glyphicon-ok"))
	{
		$(this).find("a").prepend('<i class="glyphicon glyphicon-ok pull-right"></i>');
		var groupName=$(this).attr('id');
		var groupId=groupMap[groupName];
		var groupUserId=$(this).parent().attr('id');
		groupUserId=groupUserId.substr(0,groupUserId.length - 2);
		var status="active";
		var criteria=null;
		 var currentDate = new Date();
		 var day = currentDate.getDate();
		 var month = currentDate.getMonth() + 1;
		 var year = currentDate.getFullYear();
		 var dateValue=day + "-" + month + "-" + year ;
		 var endDate=null;
		 var params ='{"groupId":"'+groupId+'","userId":"'+userId+'","groupUserId":"'+groupUserId+'"}';
		//var params = "{\"groupId\":\'"+1+"'\",\"groupName\":\'"+guitar group +"'\",\"groupDetails\":[{\"userId\":\'"+"+ userId +"+"'\",\"startDate\":\'"+ 16-05-2013 +"'\",\"endDate\":\'"+ 17-05-2013+"'\",\"status\":\'"+active+"'\" }]}";
	    
	    console.log("params : " + params);
	    params = encodeURIComponent(params);
	    var url = urlForServer+"group/addGroupdetails";
	
	 $.ajax({
		 headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
        type: 'POST',
        url : url,
        cache: false,
        contentType: "application/json; charset=utf-8",
        
        data:params, 
        dataType: "json",
        complete: function(transport) {
    	if (transport.status == 200) {
    		fetchFriendsList(userId);	
    		console.log("success");
        }
    	 else {
             alert("Please try again later");
         }
     }

	});
	 
	}
	else
	{
		var groupName=$(this).find("i").remove();
		var groupName=$(this).attr('id');
		var groupId=groupMap[groupName];
		var startDate=null;
		var groupUserId=$(this).parent().attr('id');
		groupUserId=groupUserId.substr(0,groupUserId.length - 2);
	//	var groupDetails=groupDetailsMap[userId];
		
		var status="inactive";
		var criteria=null;
		 var currentDate = new Date();
		 var day = currentDate.getDate();
		 var month = currentDate.getMonth() + 1;
		 var year = currentDate.getFullYear();
		 var dateValue=day + "-" + month + "-" + year ;
		 var params ='{"groupId":"'+groupId+'","userId":"'+userId+'","groupUserId":"'+groupUserId+'"}';
		//var params = "{\"groupId\":\'"+1+"'\",\"groupName\":\'"+guitar group +"'\",\"groupDetails\":[{\"userId\":\'"+"+ userId +"+"'\",\"startDate\":\'"+ 16-05-2013 +"'\",\"endDate\":\'"+ 17-05-2013+"'\",\"status\":\'"+active+"'\" }]}";
	    
	    console.log("params : " + params);
	    params = encodeURIComponent(params);
	    var url = urlForServer+"group/updateMemberGroupDetails";
	
	 $.ajax({
		 headers: { 
		 "Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
        type: 'POST',
        url : url,
        cache: false,
        contentType: "application/json; charset=utf-8",
        
        data:params, 
        dataType: "json",
        complete: function(transport) {
    	if (transport.status == 200) {
    		fetchFriendsList(userId);	
    		console.log("success");
        }
    	 else {
             alert("Please try again later");
         }
     }

	});


	}
	
});

//  response friend request

$('#userslists').on('click', '.responseUser', function() {
	$(".responseUser").attr("disabled","disbaled");
	var requestedId = $(this).parent().attr('id');
	var parentId = $(this).parent().parent().attr('id');
	var haveFriendRequest=false;
	var datastr='{"userId":"'+userId+'","requestId":"'+requestedId+'"}';
	console.log(datastr);
	 var url = urlForServer+"friends/acceptFriendRequest";
		var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST', url : url, data : params,
			success : function(responseText){
			$("#"+parentId).remove();
			console.log(responseText);
			$('#userslists').find('li').each(function(index){
				haveFriendRequest=true;
			});
			if(!haveFriendRequest){
				$('#userslists').append('<li><div><label id="addFriend" onclick="javascript:addFriend();" class="offset1" >Add Contacts</label></div></li>');
			}
		}
		});
});

//Click the requests

$('#userslists').on('click', '.requestDecline', function() {
	$(".requestDecline").attr("disabled","disbaled");
	var requestedId = $(this).parent().attr('id');
	var parentId = $(this).parent().parent().attr('id');
	var haveFriendRequest=false;
	
	var datastr='{"userId":"'+userId+'","requestId":"'+requestedId+'"}';
	console.log(datastr);
	 var url = urlForServer+"friends/declineFriendRequest";
		var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST', url : url, data : params,
			success : function(responseText){
			$("#"+parentId).remove();
			console.log(responseText);
			$('#userslists').find('li').each(function(index){
				haveFriendRequest=true;
			});
			if(!haveFriendRequest){
				$('#userslists').append('<li><div><label id="addFriend" onclick="javascript:addFriend();" class="offset1" >Add Contacts</label></div></li>');
			}
		}
		});
	
});

//Follow Function Veera

$('#groupMemberContainer').on('click','.followUser',function(){
	
	var selectUserId=$(this).parent().parent().attr('id');
	 var params = '{"userId":"'+selectUserId+'","requestId":"'+userId+'"}';
	    console.log("params : " + params);
	    params = encodeURIComponent(params);
	   var url = urlForServer+"friends/addfollower";
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	        type: 'POST',
	        url : url,
	        cache: false,
	        contentType: "application/json; charset=utf-8",
	        
	        data:params, 
	        dataType: "json",
	        complete: function(transport) {
	    	if (transport.status == 200) {
	    		console.log("success");
	    		$("#follow"+selectUserId).replaceWith( $('<div style="padding-top: 10px;" class="offset2" id=unfollow'
						+ selectUserId
						+ '> <a class="btn btn-primary unFollowUser fontStyle" href="javascript:void(0);">Unfollow</a></div>') );
	    		
	    	}
	    }
	    });
});


$('#groupMemberContainer').on('click','.unFollowUser',function(){
	
	var selectUserId=$(this).parent().parent().attr('id');
	
	 var params = '{"userId":"'+selectUserId+'","requestId":"'+userId+'"}';
	    console.log("params : " + params);
	    params = encodeURIComponent(params);
	   var url = urlForServer+"friends/unfollower";
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
	        type: 'POST',
	        url : url,
	        cache: false,
	        contentType: "application/json; charset=utf-8",
	        
	        data:params, 
	        dataType: "json",
	        complete: function(transport) {
	    	if (transport.status == 200) {
	    		console.log("success");
	    		$("#unfollow"+selectUserId).replaceWith( $('<div style="padding-top: 10px;" class="offset2" id=follow'
						+ selectUserId
						+ '> <a class="btn btn-primary followUser fontStyle" href="javascript:void(0);" title="Get notifications when they share something with the Crowd">Follow</a></div>') );
	    		
	    	}
	    }
	    });
});

$(document).ready(function() {
	$.extend( $.ui.autocomplete.prototype, {
	    _renderItem: function( ul, item ) {
	        var term = this.element.val(),
	        regex = new RegExp( '(' + term + ')', 'gi' );
	            html = item.label.replace( regex , "<font color=black><b>$&</b></font>" );
	        return $( "<li></li>" )
	            .data( "item.autocomplete", item )
	            .append( $("<a></a>").html(html) )
	            .appendTo( ul );
	    }
	});
});
$( "#searchUser" ).autocomplete({
	    minLength:1,

    source:function(request, response)
     {

	  var url = urlForServer + "group/getGroupSearch";
	  var datastr = '{"Searching":"'+$("#searchUser").val()+'","userId":"'+userId+'"}';
        $.ajax({
        	headers: { 
        	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
          url: url,
          data: datastr,
          dataType: "json",
          type: "POST",
          success: function(data){
             response(data.slice(0, 7));
            
           }
        });
      },
      select: function( event, ui ) {
    	  
    	  var searching=ui.item.label;
    	  searching = searching.substring(searching.indexOf(",")+1,searching.length);
    	  var url = urlForServer + "group/getContactSearch";
		  var datastr = '{"Searching":"'+searching+'"}';
            $.ajax({
            	headers: { 
            	"Mn-Callers" : musicnote,
    	    	"Mn-time" :musicnoteIn					
    	    	},
              url: url,
              data: datastr,
              dataType: "json",
              type: "POST",
              success: function(datas){
            	if(datas!=null && datas!='')
				{
            		selectedUserId=datas.userId;
            		var profilePhotLocation;
            		if(datas.filePath!= 'null' && datas.filePath!= '')
            			profilePhotLocation = uploadUrl+datas.filePath;
            		else
            			profilePhotLocation ="pics/dummy.jpg";
            				
            		var userSelected=false;
            		$('#groupMemberContainer').find('.well').each(function(index){
            			var id=$(this).attr('id');
            			if(id==selectedUserId)
            				userSelected=true;
            		});
            		
            		//Added by veera for get selected user in friend list
            		var url1 = urlForServer+"friends/checkfriends";

            		var params1 ='{"userId":"'+userId+'","requestId":"'+selectedUserId+'"}';
            		    
            		console.log(params1);
            		params = encodeURIComponent(params1);

            	    $.ajax({
            	    	headers: { 
            	    	"Mn-Callers" : musicnote,
            	    	"Mn-time" :musicnoteIn				
            	    	},
            	        type: 'POST',
            	        url : url1,
            	        
            	        data:params1, 
            	        success: function(responseText) {

            	    		addfriends=false;
            	    		requestSend=false;
            	    		responsePending=false;
            	    		notFriends=false;
            	    		console.log(" data :" +responseText);
            	    		if(responseText=="not friends"){
            	    			notFriends=true;
            	    		}else if(responseText=="friends"){
            	    			addfriends=true;
            	    		}else if(responseText=="request send"){
            	    			requestSend=true;
            	    		}else if(responseText=="reponse waiting"){
            	    			responsePending=true;
            	    		}
            	    		
            	    		var url1 = urlForServer+"friends/checkfollowers";

            	    		var params1 ='{"userId":"'+selectedUserId+'","requestId":"'+userId+'"}';
            		    
            	    		console.log(params1);
            	    		params = encodeURIComponent(params1);

            	    		$.ajax({
            	    			headers: { 
            	    			"Mn-Callers" : musicnote,
            	    	    	"Mn-time" :musicnoteIn				
            	    	    	},
            	    			type: 'POST',
            	    			url : url1,
            	    			data:params1, 
            	    			success: function(responseText) {
            	    				if(responseText=="follower"){
            	    					console.log(" If part executed");
            	    					followers=true;
            	    				}else if(responseText=="notfollower"){
            	    					console.log("Else part executed");
            	    					followers=false;
            	    				}
            	    				console.log(" data :" +responseText+" Followerse :-->"+followers);
            	    			}
            	    		});
            //end veera's 
            	    		loginUserId=userId;// need to pass dynamically
            	    		groupId="1";  // don't worry 
            	    		criteria="loginUserId";
            		
            	    		var url = urlForServer+"group/getGroupInfo";

            	    		var params = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+loginUserId+"\",\"criteria\":\""+criteria+"\",\"groupName\":\""+ groupName +"\",\"groupDetails\":[{\"userId\":\"userId\",\"startDate\":\"startDate\",\"endDate\":\"endDate\",\"status\":\"status\" }]}";
            			    
            	    		params = encodeURIComponent(params);

            	    		$.ajax({
            	    			headers: { 
            	    			"Mn-Callers" : musicnote,
            	    	    	"Mn-time" :musicnoteIn				
            	    	    	},
            	    			type: 'POST',
            	    			url : url,
            	    			cache: false,
            	    			contentType: "application/json; charset=utf-8",
            	    			data:params, 
            	    			dataType: "json",
            	    			success : function(data) {
            	    				if(!userSelected){
            								
            								var memberContent = '<div id='
            									+ selectedUserId
            									+ ' class="col-md-8 well " style="padding-right: 20px;">'
            									+ '<a class="brand col-md-3" href="#"><div class="thumbnail span" style="width:80px;height:80px;" ><img style="width:70px;height:70px;" src="'
            									+ profilePhotLocation
            									+ '"></div></a>'
            									+ '<div class="col-md-12"><a class="span" href="javascript:void(0);" style="text-decoration: none;font-family: Helvetica Neue;"> '
            									+ datas.name + ' </a></div>'+ '<div class="span" id=groupView'+selectedUserId+'></div>';
            								if (responseText=="not friends") {
            									memberContent = memberContent
            										+ '<div class="offset2" id=addfriend'
            										+ selectedUserId
            										+ '>'
            										+ ' <a class="btn btn-primary selectedUser fontStyle" href="javascript:void(0);"><i class="glyphicon glyphicon-user"></i> Add as Friend </a></div>';

            									if (!followers) {
            										memberContent = memberContent
            											+ '<div style="padding-top: 10px;" class="" id=follow'
            											+ selectedUserId
            											+ '>'
            											+ ' <a class="btn btn-primary followUser fontStyle" href="javascript:void(0);" title="Get notifications when they share something with the Crowd">Follow</a></div></div>';
            									} else {
            										memberContent = memberContent
            											+ '<div class="offset2" style="padding-top: 10px;" id=follow'
            											+ selectedUserId
            											+ '>'
            											+ ' <a class="btn btn-primary unFollowUser  fontStyle" href="javascript:void(0);">Unfollow</a></div> </div>';
            									}
            									$('#test').after(memberContent);
            									//$('#groupMemberContainer').append(memberContent);

            								} else if (responseText=="request send") {
            									memberContent = memberContent + '<div class="offset2"> <a  href="javascript:void(0);" class="fontStyle" style="text-decoration: none;">Request was Sent</a></div>';
            									if (!followers) {
            										memberContent = memberContent
            											+ '<div class="" style="padding-top: 10px;" id=follow'
            											+ selectedUserId
            											+ '>'
            											+ ' <a class="btn btn-primary followUser fontStyle" href="javascript:void(0);" title="Get notifications when they share something with the Crowd">Follow</a></div></div>';
            									} else {
            										memberContent = memberContent
            											+ '<div class="offset2" style="padding-top: 10px;" id=follow'
            											+ selectedUserId
            											+ '>'
            											+ ' <a class="btn btn-primary unFollowUser fontStyle" href="javascript:void(0);">Unfollow</a></div></div>';
            									}
            									$('#test').after(memberContent);
            									//$('#groupMemberContainer').append(memberContent);
            								} else if (responseText=="reponse waiting") {
            									memberContent = memberContent + ' <div class="offset2"><a  href="javascript:void(0);" class=" fontStyle" style="text-decoration: none;">' + 'Waiting for Response </a></div>';
            										if (!followers) {
            											memberContent = memberContent
            												+ '<div style="padding-top: 10px;" class="" id=follow'
            												+ selectedUserId
            												+ '>'
            												+ ' <a class="btn btn-primary followUser fontStyle" href="javascript:void(0);" title="Get notifications when they share something with the Crowd">Follow</a></div></div>';
            										} else {
            											memberContent = memberContent
            												+ '<div style="padding-top: 10px;" class="offset2" id=follow'
            												+ selectedUserId
            												+ '>'
            												+ ' <a class="btn btn-primary unFollowUser fontStyle" href="javascript:void(0);">Unfollow</a></div></div>';
            										}
            										$('#test').after(memberContent);
            										//$('#groupMemberContainer').append(memberContent);
            								} else {
            									memberContent = memberContent
            											+ '<div class="offset2">'
            											+ '<div class="btn-group" id=group'
            											+ selectedUserId
            											+ ' role="menu" aria-labelledby="dLabel" >'
            											+ '<a class="btn btn-default fontStyle15" href="#"><i class="glyphicon glyphicon-user"></i>  Add to Groups  </a>'
            											+ '<a class="btn btn-default dropdown-toggle userGroupMenuFetch" id="'+selectedUserId+'" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>'
            											+ '<ul class="dropdown-menu userGroup" id='
            											+ selectedUserId
            											+ 'ul></div></div>';
            									if (!followers) {
            										memberContent = memberContent
            											+ '<div style="padding-top: 10px;" class="" id=follow'
            											+ selectedUserId
            											+ '>'
            											+ ' <a class="btn btn-primary followUser fontStyle" href="javascript:void(0);" title="Get notifications when they share something with the Crowd">Follow</a></div></div>';
            									} else {
            										memberContent = memberContent
            											+ '<div style="padding-top: 10px;" class="offset2" id=unfollow'
            											+ selectedUserId
            											+ '>'
            											+ ' <a class="btn btn-primary unFollowUser fontStyle" href="javascript:void(0);">Unfollow</a></div></div>';
            									}
            									$('#test').after(memberContent);
            									//$('#groupMemberContainer').append(memberContent);
            									if(data!=null){
            										var groupNames='';
            										for ( var i = 0; i < data.length; i++) {

            											var groupName = data[i].groupName;

            											var groupAdded = false;

            											groupMap[groupName] = data[i].groupId;

            											for ( var j = 0; j < data[i].groupDetails.length; j++) {
            												var groupUserId = data[i].groupDetails[j];

            												if (groupUserId == selectedUserId) {
            													test = $(
            																'#' + selectedUserId + 'ul')
            																.append(
            																		'<li  id="'
            																				+ groupName
            																				+ '" class="userSelectedGroup"><a  ><i class="glyphicon glyphicon-ok pull-right"></i>'
            																				+ groupName
            																				+ '</a></li>');
            														groupAdded = true;
            														if(groupName!=null && groupName!='undefined')
            														{
            														groupNames+='['+groupName+']';
            														
            														}
            														
            												} 
            											}
            											if (!groupAdded)
            												var test = $(
            														'#' + selectedUserId + 'ul')
            															.append(
            																	'<li  id="'
            																			+ groupName
            																			+ '" class="userSelectedGroup"><a >'
            																			+ groupName
            																			+ '</a></li>');
            										}
            										if(groupNames!=null && groupNames!=''){
            											
            											if(groupNames.indexOf(",") != -1){
            											}
            											$("#groupView"+selectedUserId).append('<span class="glyphicon glyphicon-user"></span> &nbsp;<span class="badge-text">'+groupNames+'</span>');
            										}
            									}

            								}
                						}else{
            		    					alert('User already exist');
            		    				}
            					}
            	    		});
            	    	}
            	    });
				}
            	$("#searchUser").val("");
               }
            });
    	  
      }
	 
});	
//});
function monkeyPatchAutocomplete() {

    // Don't really need to save the old fn, 
    // but I could chain if I wanted to
    var oldFn = $.ui.autocomplete.prototype._renderItem;

    $.ui.autocomplete.prototype._renderItem = function( ul, item) {
        var re = new RegExp("^" + this.term, "i") ;
        var t = item.label.replace(re,"<span style='font-weight:bold;color:Blue;'>" + this.term + "</span>");
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
    };
}