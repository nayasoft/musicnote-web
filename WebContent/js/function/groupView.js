var groupMap = {};
var groupDetailsMap ={};
var userGroupIdMap={};
var userGroupNameMap={};
var groupName;
var groupId;
var loginUserId;
var groupId;
var criteria;
var role;
var userId;
var groupArray;
var userIdArray=new Array();
var oldMailId='';
var checkerror;
var addUserWarningFlag;
var addUserPassWarningFlag;
var inviteUserWarningFlag;
window.clearInterval(autoNoteDetailsFetchTimer);
window.clearInterval(autoCrowdDetailsFetchTimer);
function hasDuplicate(arr) {
    var i = arr.length, j, val;
    var arr12=[];
    while (i--) {
    	val = arr[i];
    	j = i;
    	while (j--) {
    		if (arr[j] === val) {
    			arr12.push(arr[j]);
    		}
    	}
    }
    return arr12;
}
function hasDuplicateUserName(arr) {
    var i = arr.length, j, val;
    var arr12=[];
    while (i--) {
    	val = arr[i];
    	if ((((val).indexOf('@')!=-1)&&((val).indexOf('.')!=-1))) {
    		arr12.push(val);
    	}
    }
    return arr12;
}
function validateUserName()
{
$( "#userDialog" ).attr('style','display : none;');    
var arrayValue=[];
var arrayValue1=[];
var arrayValue2=[];
var letters =/^[a-zA-Z0-9._]+$/gm;  
var usernames=$("#addUserNames").val();
var split=usernames.split(',');
for(i=0;i<split.length;i++){
    var userName=split[i];
    var length=userName.length;
    var lastChar=userName.charAt(length-1);
    var firstChar=userName.charAt(0);
    if(userName!=""){
        if(userName.match(letters))  
        { 
            if((lastChar=="_")||(lastChar==".")||(firstChar=="_")||(firstChar==".")){
                arrayValue1.push(userName);
            }
            if((userName.split(".").length-1) > 1  || (userName.split("_").length-1) > 1 ){
            	arrayValue2.push("userName");
            }
        }  
        else  
        {  
            arrayValue.push(userName);
        }
    }else  
    {  
        //arrayValue2.push("empty");
    }
}
    if(arrayValue!=''){
        $( "#userDialog" ).attr('style','display : block;color:red;margin-top:-4px;');    
        $('#userDialog').empty();
        $('#userDialog').append("Please use only letters (a-z), numbers, and periods.");
        addUserWarningFlag=false;
    }else if(arrayValue2!=''){
        $( "#userDialog" ).attr('style','display : block;color:red;margin-top:-4px;');    
        $('#userDialog').empty();
        $('#userDialog').append("Username does not contain more than one period or one underscore.");
        addUserWarningFlag=false;
    }else if(arrayValue1!=''){
        $( "#userDialog" ).attr('style','display : block;color:red;margin-top:-4px;');    
        $('#userDialog').empty();
        $('#userDialog').append("The first and last character of your username should be a letter (a-z) or number.");
        addUserWarningFlag=false;
    }else{
        addUserWarningFlag=true;
    }
}
function validatePassword()
{
$( "#Dialog" ).empty();
$( "#Dialog" ).attr('style','display : none;'); 
var password=$("#addPassword").val();
var length=password.length;
if(password!=""){
if(length<6)  
{ 
     $( "#Dialog" ).attr('style','display : block;color:red;');
        $( "#Dialog" ).text("Short passwords are easy to guess. Try one with at least 6 characters.");
    addUserPassWarningFlag= false;  
}  
else  
{  
    addUserPassWarningFlag= true;  
}
}
}
function validateEmail()
{
 $("#inviteEmailIdDiv").attr("style", "display:none;");
    $("#inviteEmailIdDiv").empty();
var emailText = $("#inviteEmailId").val();
var length=emailText.length;
var lastChar=emailText.charAt(length-1);
var firstChar=emailText.charAt(0);
var pattern =  /^(?!.*__.*)[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
if (pattern.test(emailText)) {
	if((lastChar=="_")||(lastChar==".")||(firstChar=="_")||(firstChar==".")){
		 $("#inviteEmailIdDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
	     $("#inviteEmailIdDiv").text("Please enter a valid Email address").css({"color":"red"}); 
	    inviteUserWarningFlag= false;
	}else{
		inviteUserWarningFlag= true;
	}
} else {
     $("#inviteEmailIdDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
        $("#inviteEmailIdDiv").text("Please enter a valid Email address").css({"color":"red"}); 
    inviteUserWarningFlag= false;
}
}

$(document).ready( function() {
	$("#addPassword").keyup(function (e) {
		  $(this).val(function(i, v) { return v.replace(/ /g,""); });
		});
	// coding for loading symbol
	$("#spinner").bind("ajaxSend", function() {
        $(this).show();
    }).bind("ajaxStop", function() {
        $(this).hide();
    }).bind("ajaxError", function() {
        $(this).hide();
    }).bind("ajaxComplete", function() {
        $(this).hide();
    });
	
	$(function() {
    var url = urlForServer+"user/getUserDetails";
	var datastr = '{"userId":"'+userId+'"}';
	var params = encodeURIComponent(datastr);
	var role="";
$.ajax({
	headers: { 
	"Mn-Callers" : musicnote,
	"Mn-time" :musicnoteIn				
	},
	type : 'POST',
	url : url,
	data : params,
	success : function(responseText) {
	var data = jQuery.parseJSON(responseText);
	  if(data!=null && data!='')
		{
			for ( var i = 0; i < data.length; i++) {
				
				 userDetailsJsonObj = data[i];
				 
				 var preference =userDetailsJsonObj['addUserFlag'];
				 
				 if(preference=="true"){
					 $("#addUsers").show();
				 }else{
					 $("#addUsers").hide();
				 }
			}
		}
	  //$('#msgLoadingModal').modal('hide');
},
error : function() {
	console.log("<-------error returned for User details -------> ");
	}
});   

});
	$(".sideBarContact").click(function(){
		
		var color = $( this ).css( "background-color" );
		var def='rgb(40, 135, 189)';
		if(color!=def){
		}else{
			window.location.reload(true);
		}
	});
	
	// Fixing height of the page.
	var size=$(window).height() - 90;
    $('#mainDiv').attr('style','height:'+size+'px;overflow: auto;');
	
    $("#addUsers").click(function() {
    	fetchAddusersDetails();
    	$("#addUsersModal").modal('toggle');
    	$("#userDialog").attr('style','display : none');
    	$("#Dialog").attr('style','display : none');
    	$("#addUserNames").val("");
    	$("#addPassword").val("");
    	$('#addUser').attr("disabled", false);
    
    });
    $("#addUser").click(function() {
    	var usersId=userId;
    	var usernameList;
    	var errorEmailName='';
    	 var username=$("#addUserNames").val();
    	 var password=$("#addPassword").val();
    	 
    	var split=username.split(',');
    	usernameList=split.length;
    	var arr1= hasDuplicateUserName(split);
     	if(arr1!=''){
     		$('#addUser').attr("disabled", false);
     		$( "#userDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
     	$('#userDialog').empty();
     	$('#userDialog').append("Invalid Username(s) : ["+arr1+"]");
     	return false;
     	}
     	var arr1= hasDuplicate(split);
     	if(arr1!=''){
     		$('#addUser').attr("disabled", false);
     		$( "#userDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
     	$('#userDialog').empty();
     	$('#userDialog').append("Duplicate value for : "+arr1);
     	return false;
     	}
    	 
    	 if((username!=""&&password!="")&&(checkerror!="error")){
    		 if((addUserWarningFlag)&&(addUserPassWarningFlag)){
    			 $('#addUser').attr("disabled", true);
    		// exitsUsersName();
    		
    	// var role="Music Student";
    	 //var userLevel="Trial-Start";
    	// var termscheck="true";
    	 //   var UserName=username.split(',');
    	  //  for(i=0;i<UserName.length;i++){
    	    	//if(((UserName[i]).indexOf('@')!=-1)&&((UserName[i]).indexOf('.')!=-1)){
    	    	//	alert("if");
    	    		  var url = urlForServer + "user/addingUser";
    	    			//var datastr = '{"userFirstName":"","userLastName":"","emailId":"' + UserName[i] + '","userName":"","userRole":"' + role + '","password":"' + password + '","favoriteMusic":"","skillLevel":"","instrument":"","offSet":"","termscheck":"'+termscheck+'","noteCreateBasedOn":"private","requestedUser":"","userLevel":"'+userLevel+'"}';
    	    			
    	    		  var datastr = '{"userName":"'+username+'","password":"' + password + '","userId":"' + usersId + '"}';
    	    			var params = encodeURIComponent(datastr);
    	    			$
    					.ajax( {
    						headers: { 
    						"Mn-Callers" : musicnote,
    						"Mn-time" :musicnoteIn				
    							},
    						type : 'POST',
    						url : url,
    						data : params,
    						success : function(responseText) {
    						 $('#addUser').attr("disabled", false);
    				     	if(responseText.match('Usernames')){
    						$("#addUsersModal").modal('toggle');
    				     	}else{
    				     		if(responseText.indexOf(',')){
    				     			var userLength=responseText.split(',');
    				     			var userListLength=userLength.length;
    				     			if(usernameList==userListLength){
    				     				$( "#userDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
    	        				     	$('#userDialog').empty();
    				     				$('#userDialog').append("Username(s) already exists : "+responseText);	
    				     			}else{
    				     				$( "#userDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
    	        				     	$('#userDialog').empty();
    	        				     	$('#userDialog').append("Username(s) already exists : "+responseText+" and Other user(s) added successfully");	
    				     			}
    				     		}
    				     		
    				     	}
    					},
    					error : function() {
    						console
    								.log("<-------Error returned while creating user-------> ");
    					}
    				});
    	    		
    		 }
    	 }else{
    		 if(username == ""){
        		 $('#addUser').attr("disabled", false);
        		 $( "#userDialog" ).attr('style','display : block;color:red;');
     			$( "#userDialog" ).text("Field cannot be blank. Separate names with commas to add multiple users");
        	 }else if((username != "")&&(password == ""))
        	 {
        		 $( "#userDialog" ).attr('style','display : none;');
        		 $('#addUser').attr("disabled", false);
        		 $( "#Dialog" ).attr('style','display : block;color:red;');
      				$( "#Dialog" ).text("Field cannot be blank");
        	 }
    	 }
    });
    function fetchAddusersDetails(){
		var url = urlForServer + "user/fetchAddusersDetails";
		var datastr = '{"userId":"' + userId + '"}';
		$('#AddUsersGrid').empty();
		$('#AddUsersGrid').append('<table id="taskTable" ></table><div id="tablePage"></div>');
		var params = encodeURIComponent(datastr);
	   
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
			"Mn-time" :musicnoteIn				
			},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
				var data1 = "";
				var dataLength="";
				var lastsel;
				data1 = jQuery.parseJSON(responseText);
				if(data1!=null && data1!="")
				{
					for(var i=0;i<data1.length;i++){
					}
				dataLength= data1.length ;
				var height;
				if(dataLength>=11)
					{
						 height= 450;
					}
					else
					{
						height=  dataLength * 25+50;
					}
				
				$('#taskTable').jqGrid({
					data:data1,
   	        		datatype: "local",
   	        		sortable: true,       		
   	        		height: 110,
	                width:400,
	                multiselect:false,
	               
   	        		 colNames:['Name','Status'],
   	        	   	 colModel:[
   	        	   	    {name:'userName',index:'Id', width:100, sorttype:"text"},
   	        	   		{name:'status',index:'status', width:100, sorttype:"text"},
   	        	   		/*{name:'level',index:'level', width:70,hidden:true},
   	        	   		{name:'expiryDate',width:50},
   	        	   		{name:'paidBy',width:50},
   	        	   		{name:'userId',width:50,hidden:true},*/
   	        	   		
   	        	   ],
   	        	   
   	        	   	pager: '#tablePage',
   	        	   	pgbuttons: true,
   	        	   	pginput: "true",   
   	        	   	sortorder: "desc",
   	        	   	grouping:true, 
   	        	   	groupingView : { groupField : ['status'],groupText: ['<b>{0} Users</b>']},
   	        		viewrecords: true,
   	        		sortorder: "desc",
   	        		caption: "User Status"
   	        	 });
				$("#taskTable").jqGrid('navGrid',"#tablePage",{edit:false,add:false,del:false,refresh:false,search:false,first:false,end:false});
				//getSelectedRow();  
				}
				
		},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});
	}

	$("#inviteUserId").click(function() {
		$('#inviteEmailId').val('');
		// commented by ramraj for beta testing
		/*var url = urlForServer + "group/getInvitedUser";
		var datastr = '{"userId":"'+userId+'"}';
		console.log("<-------Sdatastr -------> " + datastr);
		
		//$("#inviteUserId").attr('style', "disply:none");
		$('#InvitedUsers').empty();
		$('#trailpremiumDiv').empty();
		$('#userdateselectDiv').empty();
		var params = encodeURIComponent(datastr);
	   
		$.ajax({
				headers: { 
"Ajax-Call" : userTokens,
"Ajax-Time" :userLoginTime				
},
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) 
					{
			if(responseText!='false')
			{
				$('#InvitedUsers').append('<table id="taskTable" ></table><div id="tablePage"></div>');
				$('#trailpremiumDiv').append('<input type="radio" checked="checked" name="users" value="Trail-Start" id="trailusers">&nbsp;Trial&nbsp;'
					+'<input type="radio" name="users" value="Premium" id="premiumusers">&nbsp;Premium');
				$('#userdateselectDiv').append('<input type="radio" name="date" checked="checked" value="Signup-Date" id="signUpDate">&nbsp;Signup-Date&nbsp;'
						+'<input type="radio" name="date" value="Paid-Date" id="paidDate" >&nbsp;Paid-Date');
				var data1 = "";
				var dataLength="";
				var lastsel;
				data1 = jQuery.parseJSON(responseText);
				if(data1!=null && data1!="")
				{
				dataLength= data1.length ;
				var height;
				if(dataLength>=11)
					{
						 height= 250;
					}
					else
					{
						height=  dataLength * 25;
					}
				
				$('#taskTable').jqGrid({
					data:data1,
   	        		datatype: "local",
   	        		sortable: true,       		
   	        		height: height,
	                width:530,
	               
   	        		 colNames:['No','MailId','status','Delete'],
   	        	   	 colModel:[
   	        	   	    {name:'Id',index:'Id', width:20,hidden:true},
   	        	   		{name:'invitedMailID',index:'Id', width:100,editable:true},
   	        	   		{name:'userCreated',index:'userCreated', width:50},
   	        	   		{name:'delet',width:30}
   	        	   ],
   	        	   
   	        	   pager: '#tablePage',
   	        	   sortname: 'taskId', 
   	        	   viewrecords: true, 
   	        	   sortorder: "desc",
   	        	   gridComplete: function()
   	        	   { 
					var ids = $("#taskTable").jqGrid('getDataIDs'); 
					for(var i=0;i < ids.length;i++){
						var cl = ids[i]; 
						delet = "<input style='height:22px;width:20px;' type='button' class='inviteUserDel' value='D' id='"+cl+"' />";
						save = "<input style='height:22px;width:20px;' type='button' class='inviteUserEdit' value='E' id='"+cl+"' />";
						
						$("#taskTable").jqGrid('setRowData',ids[i],{delet:delet+save}); } },
          	           caption: "Invited User"
          	           
   	        	 });
				$("#taskTable").jqGrid('navGrid',"#tablePage",{edit:false,add:false,del:false});
				}
			}
		},
		error:function(){
			console.log("<-------Error returned while welcome user mail-------> ");
		}
		});*/
		
		 $('#inviteUserModel').modal('toggle');
		 $("#inviteEmailIdDiv").attr("style", "display:none");
		 $("#inviteEmailIdDiv").text("");
		 $("#inviteInvalidEmailIdDiv").attr("style", "display:none");
		 $("#inviteInvalidEmailIdDiv").text("");
		 $('#saveInviteUser').attr("disabled", false);	
	});
	
	$("#InvitedUsers").on('click','.inviteUserDel',function(){
		 var rowid=$(this).attr('id');
		 var rowData = $("#taskTable").jqGrid("getRowData", rowid);
		 var mailId=rowData.invitedMailID;
		 var url = urlForServer + "group/deletInvite";
		 var datastr = '{"userId":"'+userId+'","mailId":"'+mailId+'"}';
		
		var params = encodeURIComponent(datastr);
	   
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
			"Mn-time" :musicnoteIn			
			},
			       
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) {
						
						if(responseText=="success")
						{
							$('#inviteUserModel').modal('hide');
							$('#inviteEmailId').val("");
						}
					},
					error : function() {
					
						console
								.log("<-------Error returned while welcome user mail-------> ");
					}

				});
	});
	$("#InvitedUsers").on('click','.inviteUserEdit',function(){
		 var rowid=$(this).attr('id');
		 var rowData = $("#taskTable").jqGrid("getRowData", rowid);
		 oldMailId=rowData.invitedMailID;
		 $('#inviteEmailId').val(oldMailId);
	});
	
	$("#cancelInviteUser").click(function() {
		$('#inviteUserModel').modal('hide');
		$("#inviteEmailIdDiv").attr("style", "display:none");
		$("#inviteEmailIdDiv").text("");
	});
	
	$("#saveInviteUser").click(function() {
		var userMail='';
		var emilFlag=true;
		
		if($('#inviteEmailId').val()!=null && $('#inviteEmailId').val()!='')
		{
			    $("#inviteEmailIdDiv").attr("style", "display:none");
				$("#inviteEmailIdDiv").text(""); 
		}
		else
		{
			    $("#inviteInvalidEmailIdDiv").attr("style", "display:none");
			    $("#inviteInvalidEmailIdDiv").text("");
			    $("#inviteEmailIdDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#inviteEmailIdDiv").text("Email is required. Separate emails with commas to invite multiple users").css({"color":"red"}); 
				
		}
		
   if($('#inviteEmailId').val()!=null && $('#inviteEmailId').val()!='')
   {
	   
	   if(emailcheck($('#inviteEmailId').val())==true)
		{
			emilFlag=true;
			$("#inviteInvalidEmailIdDiv").attr("style", "display:none");
		    $("#inviteInvalidEmailIdDiv").text("");
		}
		else
		{
			emilFlag=false;
			$("#inviteInvalidEmailIdDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#inviteInvalidEmailIdDiv").text("Invalid mail Id").css({"color":"red"});
		}
	   if(emilFlag==true)
	   {
	    $("#inviteEmailIdDiv").attr("style", "display:none");
		$("#inviteEmailIdDiv").text(""); 
		$("#inviteInvalidEmailIdDiv").attr("style", "display:none");
		$("#inviteInvalidEmailIdDiv").text("");
		$('#saveInviteUser').attr("disabled", true);
		
		var userLevel=$('input:radio[name=users]:checked').val();
		var userSelectDate=$('input:radio[name=date]:checked').val();
		if(userLevel==undefined)
			userLevel="Trial";
			
		userSelectDate="Signup-Date";
		var url='';
		var datastr='';
		if(oldMailId!='')
		{
			var mailIds=$('#inviteEmailId').val();
			mailIds=mailIds.split(',');
			mailIds=mailIds[0];
			url = urlForServer + "group/updateInviteEmail";
			datastr = '{"userId":"'+userId+'","userFirstName":"'+userFirstName+'","userLastName":"'+userLastName+'","oldMailId":"'+oldMailId+'","inviteUserMail":"' + mailIds+ '","userLevel":"'+userLevel+'"}';	
		}
		else
		{
			url = urlForServer + "group/welcomeEmail";
			datastr = '{"userId":"'+userId+'","userFirstName":"'+userFirstName+'","userLastName":"'+userLastName+'","inviteUserMail":"' + $('#inviteEmailId').val()+ '","userLevel":"'+userLevel+'","userSelectDate":"'+userSelectDate+'"}';
		}
		
		var params = encodeURIComponent(datastr);
	   
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
			"Mn-time" :musicnoteIn				
			},
			       
					type : 'POST',
					url : url,
					data : params,
					success : function(responseText) {
						if(responseText=="success")
						{
							$('#inviteUserModel').modal('hide');
							$('#inviteEmailId').val("");
							oldMailId="";
							
						}
						else if(responseText=="Your requesting limit is over"){
							$("#inviteInvalidEmailIdDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
							$("#inviteInvalidEmailIdDiv").text("Your requesting limit is over").css({"color":"red"});
							$('#inviteEmailId').val("");
							$('#saveInviteUser').attr("disabled", false);
						}
						
						else
						{
							$("#inviteInvalidEmailIdDiv").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
							$("#inviteInvalidEmailIdDiv").text("User  "+responseText+"  Already Exist. Friend request will be sent for the user who does not exist. ").css({"color":"red"});
							$('#inviteEmailId').val("");
							$('#saveInviteUser').attr("disabled", false);
						}
					
					},
					error : function() {
					
						console
								.log("<-------Error returned while welcome user mail-------> ");
					}

				});
	   }
   }
   else
   {
   }
	});
	
	
	loginUserId=userId;// need to pass dynamically
	groupId="1";  // don't worry 
	criteria="loginUserId";
	
	if(userId!=null && userId!=''){
		fetchGroups(userId,groupId,criteria);
		fetchFriendsList(userId);
		
		if(autoContactDetailsFetchTimer==null || autoContactDetailsFetchTimer==0)
			autoContactDetailsFetchTimer=window.setInterval(function(){
				if(userSession=='Active')
				{
				fetchFriendsList(userId);
				}
				},60000);
		/*setInterval(function(){
			fetchFriendsList(userId);
		},60000);*/

	}

	
	$('#groupdropdown').on('click','.createGroup',function(){
		$('#groupName').val("");
		$("#errormsg").text("");
		$('#createGroupModal').modal('toggle');
	});
	
	
	$('#groupdropdown').on('click','.allGroup',function(){
		loginUserId=userId;// need to pass dynamically
		groupId="1";  // don't worry 
		criteria="loginUserId";
		fetchGroups(userId,groupId,criteria);
		fetchFriendsList(userId);
	});
	
	
	
	$('#groupdropdown').on('click','.groupIconEdit',function(){
			groupName=$(this).attr('id');
			$('#updateGroupModal').modal('toggle');
			$("#updateerrormsg").text("");
			$('#updategroupName').val(groupName);

	});
	

	$('#groupdropdown').on('click','.groupIconDelete',function(){
			groupName='';
			groupName=$(this).attr('id');
			$('#deleteGroupModal').modal('toggle');

	});
	
	
	$('#groupdropdown').on('click','.grouphref',function(){
		groupName='';
		groupName=$(this).attr('id');
		loginUserId=userId;// need to pass dynamically
		groupId=groupName;  // don't worry 
		criteria="groupId";
		var groupId=groupMap[groupName];
			var url = urlForServer+"group/getGroupInfo";
			

			var params = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+loginUserId+"\",\"criteria\":\""+criteria+"\",\"groupName\":\""+ groupName +"\"}";
			params = encodeURIComponent(params);
			
			var memberContent='';
			$('#groupMemberContainer').find('.well').each(function( index ){
				$(this).remove();
			});
			 $.ajax({
				 headers: { 
				 "Mn-Callers" : musicnote,
				 "Mn-time" :musicnoteIn			
				 },
			        type: 'POST',
			        url : url,
			        cache: false,
			        contentType: "application/json; charset=utf-8",
			        
			        data:params, 
			        dataType: "json",
			        success: function(data) {
				 
				 	for ( var i = 0; i < data.length; i++) 
					{
							console.log("Data while choose group name"+data.length+" Data Dertaila"+data);
							var groupName = data[i].groupName;
							
							var status;
							var image;
							for ( var j = 0; j < data[i].groupDetails.length; j++) 
							{
								selectedUserId=data[i].groupDetails[j]+"";
								var userObj = activeUserObjectMap[selectedUserId];
								if(userObj!= undefined && userObj['filePath']!='null' && userObj['filePath']!="")
									image=uploadUrl+userObj['filePath'];
								else
									image="pics/dummy.jpg";
								
							    memberContent='<div id='+selectedUserId+' class="col-md-8 well" style="padding-right: 20px;">'
									+'<a class="brand span3" ><div class="thumbnail span" style="width:80px;height:80px;"><img src="'+image+'" style="width:70px;height:70px;"></div></a>'
									+'<div class="span fontStyle"><a class="col-md-12 fontStyle"  style="text-decoration: none;"> '+activeUserDeatilsMap[selectedUserId]+' </a></div>'
									+ '<div class="col-md-12" id=groupView'+selectedUserId+'></div>'+'<div class="offset2">'
										+'<div class="btn-group" id=group'+selectedUserId+' role="menu" aria-labelledby="dLabel" >'
										+'<a class="btn btn-default fontStyle" ><i class="glyphicon glyphicon-user"></i>  Add to Groups  </a>'
										+'<a class="btn btn-default dropdown-toggle userGroupMenuFetch" id="'+selectedUserId+'" data-toggle="dropdown" ><span class="caret"></span></a>'
										+'<ul class="dropdown-menu userGroup" id='+selectedUserId+'ul></div></div>';
						    				$('#groupMemberContainer').append(memberContent);
							   

							 
								 $('#'+selectedUserId+'ul').append('<li  id="'+groupName+'" class="userSelectedGroup fontStyle"><a id="'+groupName+'"><i class="glyphicon glyphicon-ok pull-right"></i>'+groupName+'</a> </li>');
							
							}
							
				        }}

			    });

});

	$("#updateGroup").click(function() {
		$('#updateGroupModal').modal('toggle');
	});
	
	$('#groupName').autocomplete({
	    minLength:1,

	    source:function(request, response)
	     {

		  var url = urlForServer + "group/getGroupNamesForSearch";
		  var datastr = '{"Searching":"'+$("#groupName").val()+'","userId":"'+userId+'"}';
	        $.ajax({
	        	headers: { 
	        	"Mn-Callers" : musicnote,
	        	"Mn-time" :musicnoteIn			
	        	},
	          url: url,
	          data: datastr,
	          dataType: "json",
	          type: "POST",
	          success: function(data){
	             response(data.slice(0, 7));
	           }
	        });
	      }, open: function(event, ui) {
	          $(".ui-autocomplete").css("position", "absolute");
	          $(".ui-autocomplete").css("z-index", "2147483647");
	      }
	});
	$('#updategroupName').autocomplete({
	    minLength:1,

	    source:function(request, response)
	     {

		  var url = urlForServer + "group/getGroupNamesForSearch";
		  var datastr = '{"Searching":"'+$("#updategroupName").val()+'","userId":"'+userId+'"}';
	        $.ajax({
	        	headers: { 
	        	"Mn-Callers" : musicnote,
	        	"Mn-time" :musicnoteIn				
	        	},
	          url: url,
	          data: datastr,
	          dataType: "json",
	          type: "POST",
	          success: function(data){
	             response(data.slice(0, 7));
	            
	           }
	        });
	      }, open: function(event, ui) {
	          $(".ui-autocomplete").css("position", "absolute");
	          $(".ui-autocomplete").css("z-index", "2147483647");
	      }
	});
	
	
	$("#addGroup").click(function() {
		var url = urlForServer+"group/createGroup";
		var newGroupName=$('#groupName').val();
		if(newGroupName!=null && newGroupName!="" && newGroupName.trim()!="" && newGroupName.indexOf('"')==-1 ){
         var criteria=null;
		var params = "{\"groupId\":\"1\",\"loginUserId\":\""+ userId +"\",\"groupName\":\""+ newGroupName +"\",\"criteria\":\""+criteria+"\"}";
	    
	params = encodeURIComponent(params);

    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
        type: 'POST',
        url : url,
        cache: false,
        contentType: "application/json; charset=utf-8",
        
        data:params, 
        dataType: "json",
        success: function(transport) {
		
    	loginUserId=userId;// need to pass dynamically
    	groupId="1";
    	criteria="loginUserId";
		
		if(transport=="0"){
				$("#errormsg").text("Group name already exists").css({"color":"red"});
				}
				else
				{
            	fetchGroups(loginUserId,groupId,criteria);
            	$('#createGroupModal').modal('hide');
				}

        }
    });

		}
		else
		{
			if(newGroupName.trim()!=""){
				if(newGroupName.indexOf('"')!=-1)
				$("#errormsg").text('Please don\'t use " character').css({"color":"red"});
			}
			else
				$("#errormsg").text("Please enter group name").css({"color":"red"});
		}
//so that the page doesn't post back
return false;
});

	
	$("#editGroup").click(function() {
		var url = urlForServer+"group/updateGroup";
		var updateGroupName=$('#updategroupName').val();
		if(updateGroupName!=null && updateGroupName!="" && updateGroupName.trim()!="" && updateGroupName.indexOf('"')==-1){
		var groupId=groupMap[groupName];
		
		var params = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+ userId +"\",\"groupName\":\""+updateGroupName +"\",\"criteria\":\""+criteria+"\"}";
	    
	    params = encodeURIComponent(params);
	
	
	 $.ajax({
		 headers: { 
		 "Mn-Callers" : musicnote,
		 "Mn-time" :musicnoteIn			
		 },
        type: 'POST',
        url : url,
        cache: false,
        contentType: "application/json; charset=utf-8",
        
        data:params, 
        dataType: "json",
        success: function(responseText) {
	
		
    	if (responseText == '200') {
    		$('#updateGroupModal').modal('hide');
    		loginUserId=userId;// need to pass dynamically
    		groupId="1";
    		criteria="loginUserId";
    		fetchGroups(loginUserId,groupId,criteria);
    		fetchFriendsList(loginUserId);
           	
           	$("#errormsg1").text("");
        }
    	 else {
    			$("#updateerrormsg").text("Group name already exists").css({"color":"red"});
         }
     }

	});
		}
		else
		{

			if(updateGroupName.trim()!=""){
				if(updateGroupName.indexOf('"')!=-1)
				$("#updateerrormsg").text('Please don\'t use " character').css({"color":"red"});
			}
			else
				$("#updateerrormsg").text("Please enter group name").css({"color":"red"});
		
		}
  //so that the page doesn't post back
    return false;
    });
	
	
	
    $("#deleteGroup").click(function() {
		var url = urlForServer+"group/deleteGroup";
		var groupId=groupMap[groupName];
		var params = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+ userId +"\",\"groupName\":\""+groupName+"\",\"criteria\":\"delete\"}";
	    
	params = encodeURIComponent(params);
	

    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
    	},
        type: 'POST',
        url : url,
        cache: false,
        contentType: "application/json; charset=utf-8",
        
        data:params, 
        dataType: "json",
        complete: function(transport) {
    	$('#deleteGroupModal').modal('hide');
            if (transport.status == 200) {
            	loginUserId=userId;// need to pass dynamically
        		groupId="1";
        		criteria="loginUserId";

            	fetchGroups(loginUserId,groupId,criteria);
            	fetchFriendsList(loginUserId);
            	
            }
            else {
                alert("Please try again later");
            }
        }
    });

//so that the page doesn't post back
return false;
});
    
	$("#fetchgroup").click(function() {
		loginUserId=userId;// need to pass dynamically
		groupId="1";  // don't worry 
		criteria="loginUserId";
		fetchGroups(loginUserId,groupId,criteria);
	});
    
	
	$("#fetchGroupMembers").click(function() {
		loginUserId=userId;// need to pass dynamically
		groupId="1";  // don't worry 
		criteria="groupId";
		fetchGroups(loginUserId,groupId,criteria);
	});
	
	
	$('#groupMemberContainer').on('click','.userGroupMenuFetch',function(){
		$('#' + selectedUserId + 'ul').hide();
		selectedUserId=$(this).attr('id');
		loginUserId=userId;// need to pass dynamically
		groupId="1";  // don't worry 
		criteria="loginUserId";
		var groupDetailss;
		
		$(this).parent().find('li').each(function( index ){
			$(this).remove();
		});
		
			
			var url = urlForServer+"group/getGroupInfo";
			
			var params = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+loginUserId+"\",\"criteria\":\""+criteria+"\",\"groupName\":\""+ groupName +"\",\"groupDetails\":[{\"userId\":\"userId\",\"startDate\":\"startDate\",\"endDate\":\"endDate\",\"status\":\"status\" }]}";
			    
			
			params = encodeURIComponent(params);
			
		    $.ajax({
		    	headers: { 
		    	"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
		        type: 'POST',
		        url : url,
		        cache: false,
		        contentType: "application/json; charset=utf-8",
		        
		        data:params, 
		        dataType: "json",
		        success : function(data) {
		    		groupDetailss=data;
                    
		    		if (groupDetailss != null && groupDetailss!='') {
		    			
		    			for ( var j = 0; j < groupDetailss.length; j++) {
		    				$('#' + selectedUserId + 'ul').show();
		    				var groupName = groupDetailss[j].groupName;
		    				
		    				var groupAdded = false;

		    				groupMap[groupName] = groupDetailss[j].groupId;
		    				
		    				for ( var k = 0; k < groupDetailss[j].groupDetails.length; k++) {
		    					var groupUserId = groupDetailss[j].groupDetails[k];
		    					if (groupUserId == selectedUserId) {
		    									$('#' + selectedUserId + 'ul').prepend('<li  id="'+ groupName+ '" class="userSelectedGroup fontStyle"><a id="'
												+ groupName+ '"><i class="glyphicon glyphicon-ok pull-right"></i>'+ groupName+ '</a> </li>');
		    						groupAdded = true;
		    					}
		    				}
		    				if (!groupAdded) {
		    					$('#' + selectedUserId + 'ul').append('<li  id="'+ groupName
		    							+ '" class="userSelectedGroup fontStyle"><a id="'+ groupName+ '">'+ groupName+ '</a> </li>');
		    				}
		    				
		    			}
		    			
		    		}
		    		else
		    		{
		    			$('#' + selectedUserId + 'ul').hide();
		    		}
		    	}
		    });
	});
	

});


function fetchGroups(loginUserId,groupId,criteria)
{
	var url = urlForServer+"group/getGroupInfo";
	groupArray = new Array();
	var params = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+userId+"\",\"criteria\":\""+criteria+"\",\"groupName\":\""+ groupName +"\"}";
	params = encodeURIComponent(params);

    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
    	},
        type: 'POST',
        url : url,
        cache: false,
        contentType: "application/json; charset=utf-8",
        
        data:params, 
        dataType: "json",
        success: function(data) {
    	groupMap={};
    	if(criteria!='groupId')
    	{
    	$("#groupUl li").remove();
    	if(data!=null){
		for ( var i = 0; i < data.length; i++) 
		{
			var groupId=data[i].groupId;
			var defaultGroup=data[i].defaultGroup;
			var groupName = data[i].groupName;
			var groupDetails=data[i].groupDetails;
			groupArray.push(groupName);
			groupMap[groupName]=data[i].groupId;
			userGroupNameMap[groupId]=data[i].groupName;
			
			userGroupIdMap[data[i].groupId]=userIdArray;
			if(defaultGroup=='false'){
				$("#groupUl").append('<li class="grouphref" id="'+groupName+'"><a style="font-family: Helvetica Neue;"> <i class="glyphicon glyphicon-edit groupIconEdit" id="'+groupName+'" title="edit"></i> &nbsp; <i class="glyphicon glyphicon-trash groupIconDelete" title="delete" id="'+groupName+'"></i> &nbsp;<i> ' + groupName + '</i></a></li>');
				}else
					$("#groupUl").append('<li><a style="font-family: Helvetica Neue;"> <i class="grouphref" id="'+groupName+'"> ' + groupName + '</i></a></li>');
			}
        
		$("#groupUl").append( '<li class="divider"></li>');
    	}
    	$("#groupUl").append( '<li><a class="createGroup" id="creategroup" style="font-family: Helvetica Neue;"><i class="glyphicon glyphicon-plus createGroup"></i>&nbsp;&nbsp; Create New Group</a></li>');
    	$("#groupUl").append( '<li><a class="allGroup" id="allgroup" style="font-family: Helvetica Neue;"><i class="glyphicon glyphicon-user allGroup"></i>&nbsp;&nbsp; All Contacts</a></li>');

    }
    	else{
    		
    		console.log("group data " +data[0].groupName);

    	}
    	
    }
    });

return false;
}
function fetchFriendsList(loginUserId){
	loginUserId=userId;// need to pass dynamically
	groupId="1";  // don't worry 
	criteria="loginUserId";
	var groupDetailss;

		var url = urlForServer+"group/getGroupInfo";
//		$('#groupMemberContainer').find('.well').each(function( index ){
//			$(this).remove();
//		});

		var params = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+loginUserId+"\",\"criteria\":\""+criteria+"\",\"groupName\":\""+ groupName +"\",\"groupDetails\":[{\"userId\":\"userId\",\"startDate\":\"startDate\",\"endDate\":\"endDate\",\"status\":\"status\" }]}";
		    
		
		params = encodeURIComponent(params);
		
		
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	        type: 'POST',
	        url : url,
	        cache: false,
	        contentType: "application/json; charset=utf-8",
	        
	        data:params, 
	        dataType: "json",
	        success : function(data) {
	    	groupDetailss=data;
	    	
	    	var url = urlForServer+"friends/getFriendsList";

	    	var params = '{"userId":"'+userId+'","requestId":"1"}';
	    	params = encodeURIComponent(params);
	    	
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
    	},
        type: 'POST',
        url : url,
        data:params, 
        dataType: "json",
        success: function(data) {
    	$('#groupMemberContainer').find('.well').each(function( index ){
			$(this).remove();
		});
    	if (data != null && data!='') {
    		var image;
						for ( var i = 0; i < data.length; i++) {
							var follower=false;
										 friendsPhotoMap[data[i].userId]=data[i].filePath;
										selectedUserId = data[i].userId;
										
										if(data[i].filePath !='null' && data[i].filePath != "")
											image = uploadUrl+data[i].filePath;
										else
											image = "pics/dummy.jpg";
										
										var memberContent = '<div id='+ selectedUserId
												+ ' class="col-md-8 well" style="padding-right: 20px;">'
												+ '<a class="brand col-md-3" ><div class="thumbnail span" style="width:80px;height:80px;"><img src="'+ image+ '" style="width:70px;height:70px;"></div></a>'
												+ '<div class="span fontStyle"><a class="col-md-12" text-decoration: none;"> '
												+ data[i].userName+ ' </a></div>'
												+ '<div class="col-md-12" id=groupView'+selectedUserId+'>'
												+'</div>'
												+ '<div class="offset2">'
												+ '<div class="btn-group" id=group'+ selectedUserId+ ' role="menu" aria-labelledby="dLabel" >'
												+ '<a class="btn btn-default fontStyle" ><i class="glyphicon glyphicon-user"></i> Add to Groups </a>'
												+ '<a class="btn btn-default dropdown-toggle userGroupMenuFetch" id="'+selectedUserId+'" data-toggle="dropdown" ><span class="caret"></span></a>'
												+ '<ul class="dropdown-menu userGroup" id='+ selectedUserId+ 'ul></div></div>';
										
										if (data[i].followers != null) {
											for ( var foll = 0; foll < data[i].followers.length; foll++) {
												if (data[i].followers[foll]== loginUserId) {
													follower=true;
												}
											}
										}
										
										if (!follower) {
											memberContent = memberContent
													+ '<div style="padding-top: 10px;" class="" id=follow'
													+ selectedUserId
													+ '>'
													+ ' <a title="Get notifications when they share something with the Crowd" class="btn btn-primary followUser fontStyle" href="javascript:void(0);">Follow</a></div>';

										} else {

											memberContent = memberContent
													+ '<div class="offset2" style="padding-top: 10px;" id=unfollow'
													+ selectedUserId
													+ '>'
													+ ' <a class="btn btn-primary unFollowUser fontStyle" href="javascript:void(0);">Unfollow</a></div> </div>';
										}

										$('#groupMemberContainer').append(memberContent);
										
										if (groupDetailss != null && groupDetailss!='') {
											var groupNames='';
											for ( var j = 0; j < groupDetailss.length; j++) {
												
												var groupName = groupDetailss[j].groupName;

												var groupAdded = false;

												groupMap[groupName] = groupDetailss[j].groupId;

												for ( var k = 0; k < groupDetailss[j].groupDetails.length; k++) {
													var groupUserId = groupDetailss[j].groupDetails[k];
												
													if (groupUserId == selectedUserId) {
														$('#' + selectedUserId + 'ul').append('<li  id="'+ groupName+ '" class="userSelectedGroup fontStyle"><a id="'
																				+ groupName+ '"><i class="glyphicon glyphicon-ok pull-right"></i>'+ groupName+ '</a> </li>');
														groupAdded = true;
														
														if(groupName!=null && groupName!='undefined')
														{
														groupNames+='['+groupName+']';
														
														}
													}
												}
												if (!groupAdded) {
													$('#' + selectedUserId + 'ul').append('<li  id="'+ groupName
															+ '" class="userSelectedGroup fontStyle"><a id="'+ groupName+ '">'+ groupName+ '</a> </li>');
												}
											}
											
										}
										if(groupNames!=null && groupNames!=''){
											
											if(groupNames.indexOf(",") != -1){
											}
											$("#groupView"+selectedUserId).append('<span class="glyphicon glyphicon-user"></span>&nbsp;<span class="badge-text fontStyle">'+groupNames+'</span>');
										}
									}
					}
    	 
    },
    complete: function(response) 
	{
	}
    });
    
	    }
	    });
	}
function exitsUsersName(){
	
	var userName;
	var emailId;
    var username=$("#addUserNames").val();
    var UserName=username.split(',');
    var length=UserName.length;
   
    for (i=0; i<length; i++) {                        
    	for (j=i+1; j<length; j++) { 
    		if (UserName[i]==UserName[j]){ 
    			$( "#userDialog" ).attr('style','display : block;color:red;');
    			$( "#userDialog" ).text(UserName[i]+' already entered');
    			checkerror="error";
    			return false;
    		}
    		else{
    			checkerror="";
    		}
    	}
    }
    for(i=0;i<UserName.length;i++){
    	if(((UserName[i]).indexOf('@')!=-1)&&((UserName[i]).indexOf('.')!=-1)){
    		var url = urlForServer+"user/getExistsEmail";
    		var datastr = '{"emailId":"'+UserName[i]+'"}';
    		var params = encodeURIComponent(datastr);
    	$.ajax({
    		headers: { 
    		"Mn-Callers" : musicnote,
    		"Mn-time" :musicnoteIn			
    		},
    		type : 'POST',
    		url : url,
    		data : params,
    		success : function(responseText) {
    		var data =jQuery.parseJSON(responseText);
    		  if(data!=null && data!='')
    			{
    			  for ( var i = 0; i < data.length; i++) {
 					 userDetail = data[i];
    	    	 $( "#userDialog" ).attr('style','display : block;color:red;');
    	    	  $( "#userDialog" ).text(userDetail['emailId']+' already exists');
    	    	  checkerror="error";
    			}
    			}else
    			{
    				checkerror="";
    				 $( "#userDialog" ).attr('style','display : none');
    			}
    		
    	},
    	error : function() {
    		console.log("<-------error returned for User details -------> ");
    		}
    	});
    	}else{
    	var url = urlForServer+"user/ExistsUser";
    	var datastr = '{"userName":"'+UserName[i]+'"}';
    	var params = encodeURIComponent(datastr);
    	
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn			
    	},
    	type : 'POST',
    	url : url,
    	data : params,
    	success : function(responseText) {
    	var data = jQuery.parseJSON(responseText);
    	if(data!=null && data!='')
    		{
    		 for ( var i = 0; i < data.length; i++) {
					 userDetail = data[i];
    		$( "#userDialog" ).attr('style','display : block;color:red;');
    		$( "#userDialog" ).text((userDetail['userName'])+' already exists');
    		checkerror="error";
    		 }
    		 }else
    		{
    			 checkerror="";
    			 $( "#userDialog" ).attr('style','display : none');
    		}
    	
    },
    error : function() {
    	console.log("<-------error returned for User details -------> ");
    	}
    });   

    	}
    }	  
    
}



function emailcheck(str) {

	var at="@";
	var dot=".";
	var emailIds=str.split(",");
	
	for(var i=0;i<emailIds.length;i++){
		 var pattern = /^(?!.*__.*)[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
		 if (pattern.test(emailIds[i].trim())) {
			 var length=emailIds[i].length;
			 var lastChar=emailIds[i].charAt(length-1);
			 var firstChar=emailIds[i].charAt(0);
			 if((lastChar=="_")||(lastChar==".")||(firstChar=="_")||(firstChar==".")){
				 return false;
			}else{
				return true;
		    }
		 }
		 else{
			 return false;
		 }
	}
	/*var lat=emailIds[i].trim().indexOf(at);
	var lstr=emailIds[i].trim().length;
	var ldot=emailIds[i].trim().indexOf(dot);
	if (emailIds[i].trim().indexOf(at)==-1){
	   return false;
	}

	if (emailIds[i].trim().indexOf(at)==-1 || emailIds[i].trim().indexOf(at)==0 || emailIds[i].trim().indexOf(at)==lstr){
	   return false;
	}

	if (emailIds[i].trim().indexOf(dot)==-1 || emailIds[i].trim().indexOf(dot)==0 || emailIds[i].trim().indexOf(dot)==lstr){
	    return false;
	}

	 if (emailIds[i].trim().indexOf(at,(lat+1))!=-1){
	    return false;
	 }

	 if (emailIds[i].trim().substring(lat-1,lat)==dot || emailIds[i].trim().substring(lat+1,lat+2)==dot){
	    return false;
	 }

	 if (emailIds[i].trim().indexOf(dot,(lat+2))==-1){
	    return false;
	 }
	
	 if (emailIds[i].trim().indexOf(" ")!=-1){
	    return false;
	 }
     if ((emailIds[i].trim().split(".").length-1) > 1 ){
         return false;
     }
	}
		 return true;	*/				
}
