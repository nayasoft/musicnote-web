window.clearInterval(autoNoteDetailsFetchTimer);
window.clearInterval(autoContactDetailsFetchTimer);
window.clearInterval(autoCrowdDetailsFetchTimer);

$(document).ready( function() {		
	 var size=$(window).height() - 210;
	 var width=$(window).width() - 500;
	
	// coding for processing symbol
		$("#spinner").bind("ajaxSend", function() {
	        $(this).show();
	    }).bind("ajaxStop", function() {
	        $(this).hide();
	    }).bind("ajaxError", function() {
	        $(this).hide();
	    }).bind("ajaxComplete", function() {
	        $(this).hide();
	    });

    // $('#recentActivity').attr('style','min-height:'+size+'px;max-height:'+size+'px;');
	
	$('#add_todo').click( function() {
		var todoDescription = $('#todo_description').val();
		
		$('.todo_list').prepend('<div class="todo">'
			+ '<div class="todo_description">'
				+ todoDescription
			+ '</div>'
		+ '</div>');
		
		$('#todo_form')[0].reset();
		
		$('.check_todo').unbind('click');
		$('.check_todo').click( function() {
			var todo = $(this).parent().parent();
			todo.toggleClass('checked');
		});
		return false;
					
	});
	$("#Messages").tooltip('show');
	
	$('#mailSharingModel').on('click','.mailSharingAcceptDecline',function(){
		var id=$(this).attr('id');
		var status = id.split("~");
		var class1=$(this).attr('class');
		var cls1=class1.split('~');
		var shareUserId=cls1[0];
		var listId=cls1[1];
		var noteId=cls1[2];
		
		if(noteId==undefined || noteId==""){
			noteId=0;
		}
		if(listId==undefined || listId=="" ||  listId.match('mailSharingAcceptDecline')){
			listId=0;
		}
		
		var url = urlForServer+"friends/mailSharingAceept";
		var finalflag=false;
			var count= 0;
			
					$('#mailSharingModel').children('.modalBody').children('.control-group').children('.span').each(function( index ){
						count++;
					});
					if(count > 1)
					{
					finalflag=false;
					$(this).parent().parent().remove();
					}
					else
					{
					finalflag=true;
					if(userIdSharedIdSetArray.length > 0 && userIdSharedIdSetArray[0]!=null ){
						
							var divFrdES = createEventAceeptDiv(userIdSharedIdSetArray);
							$('#mailSharingModel').children('.modalBody').children().empty();
							$('#mailSharingModel').children('.modalBody').children().append("<br>"+divFrdES+"<br><br>");
					}else
					$('#mailSharingModel').modal('hide');
					}
		
			var datastr = '{"userId":"'+userId+'","status":"'+status[0]+'","friendUserId":"'+status[1]+'","finalflag":"'+finalflag+'","level":"'+status[2]+'","noteId":"'+noteId+'","listId":"'+listId+'"}';	
			console.log("<-------Sdatastr -------> "+ datastr);
		var params = encodeURIComponent(datastr);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
			"Mn-time" :musicnoteIn			
			},
			type : 'POST', 
			url : url,
			data : params,
			success : function(responseText){
				if(responseText =="accept" || responseText =="decline"){
					
					var count= 0;		
					$('#mailSharingModel').children('.modalBody').children('.control-group').children('.span').each(function( index ){
						count++;
					});
				}
			},error:function(){
				
			}
		});
	});
	
	
	
});
$(function(){
$("#showMsgs").click(function(){
	$("#alertMessages").modal('hide');
	app.navigate("help", {trigger: true});
	showRecentFile();
});
});
function  showRecentFile(){
	var url=urlForServer+"admin/getUploadedFilesForHelp";
	$.ajax(
		 	{
		 		headers:
		 		{ 
		 			"Mn-Callers" : musicnote,
		 	    	"Mn-time" :musicnoteIn				
		 		},
		 		url:url,
		 		type:'post',
		 		success:function(responseText)
		 		{
		 			if(responseText=='failed')
		 			{
		    			//$("#noVideoFound").attr('style','display:block;color:red;margin-left:170px;margin-top:60px;');	
		 			}
		 			else
		 			{
		 				var data1 = jQuery.parseJSON(responseText);
		 				if(data1!=null && data1!="")
		 				{
		 					var exactFile=data1.length-1;
		 					$('#collapseRecentVideos'+exactFile).attr("class","in collapse");
		 					//$('#collapseRecentVideos1').attr("class","in collapse");
		 				}
		 			}
		 		}
		 	});
}
/*$(function(){
	
	var location=window.location.href;
	var urlpath=location.substring(location.indexOf('?')+1,location.indexOf('?')+15);
	alert(urlpath);
	if(urlpath=='SuccessPayment'){
		var getVales='';
			
			PaymentConfirmEmailID=getVales[0];
			offerForPayment=getVales[1];
			totalAmountForPayment=getVales[2];
			
		alert(PaymentConfirmEmailID);
		alert(offerForPayment);
		alert(totalAmountForPayment);
		
		var params='{"mailId":"'+PaymentConfirmEmailID+'","amount":"'+totalAmountForPayment+'","offer":"'+offerForPayment+'","type":"'+offerFestival+'"}';
		var url=urlForServer+"payment/payment";
		$.ajax({
				headers: { 
"Ajax-Call" : userTokens,
"Ajax-Time" :userLoginTime				
},
			type : 'POST', 
			url : url,
			data : params,
			success : function(responseText){
				
			},error:function(){
				
			}
		});
		}
	
});*/
