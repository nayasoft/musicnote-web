function reportComment(comment){
	$("#moreActions").modal('hide');
	$('#complaintReportModalForComment').modal('show');
	$('#complaintReportModalForComment').children('.modalBody').children('.control-group').empty();
	$('.modalFooter').attr('style','display:block');
	$('.modalHeader').attr('style','display:block');
	$('#complaintReportModalForComment').children('.modalBody').children('.control-group').append('<textarea class="control-label-text userComplaintForComment" id="'+comment+'" style="font-size: 14px;height: 70px;margin-left: 1px; width: 96%;"></textarea>');
	$('#complaintReportModalForComment').children('.modalBody').children('.control-group').append('<div id="repDialog" style="font-family: Helvetica Neue;font-size:14px;display:none" title="Basic dialog"><font color="red"></font></div>');

}
$(document).ready( function() {
	
	// coding for loading symbol
	$("#spinner").bind("ajaxSend", function() {
        $(this).show();
    }).bind("ajaxStop", function() {
        $(this).hide();
    }).bind("ajaxError", function() {
        $(this).hide();
    }).bind("ajaxComplete", function() {
        $(this).hide();
    });

	
	var routes=Backbone.history.fragment;
	if(routes=='music'){
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );	
	$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarNotes').css( "background-color","rgb(40, 135, 189)" );	
	}else if(routes=='schedule'){
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarMenuSchedule').css( "background-color","rgb(40, 135, 189)" );
	}else if(routes=='memos'){
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarMemos').css( "background-color","rgb(40, 135, 189)" );
	}else if(routes=='recordlesson'){
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarRecord').css( "background-color","rgb(40, 135, 189)" );
	}else if(routes=='crowd'){
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarCrowd').css( "background-color","rgb(40, 135, 189)" );
	}else if(routes=='students'){
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.sideBarContact').css( "background-color","rgb(40, 135, 189)" );
	}else if(routes=='crowdProfiles'){
		$('body').children('#sidecontent').children('.row-fluid').children('#sideBar').children('div').children('#frontdiv').children('#sideBarMenu').children('.webside').css( "background-color","" );
	}else{
		alert("Invalid route"+routes);
	}
	
});


$(document).ready( function() {
$(".sideBarNotes").click(function(){
	
	var color = $( this ).css( "background-color" );
	var def='rgb(40, 135, 189)';
	
	if(color!=def){
	}else{
		window.location.reload(true);
	}
	
});

$(".sideBarMenuSchedule").click(function(){
	
	var color = $( this ).css( "background-color" );
	var def='rgb(40, 135, 189)';
	
	if(color!=def){
	}else{
		window.location.reload(true);
	}
});

$(".sideBarMemos").click(function(){
	var color = $( this ).css( "background-color" );
	var def='rgb(40, 135, 189)';
	
	if(color!=def){
	}else{
		window.location.reload(true);
	}
});

$(".sideBarContact").click(function(){
	
	var color = $( this ).css( "background-color" );
	var def='rgb(40, 135, 189)';
	
	if(color!=def){
	}else{
		window.location.reload(true);
	}
});

$(".sideBarCrowd").click(function(){
	
	var color = $( this ).css( "background-color" );
	var def='rgb(40, 135, 189)';
	
	if(color!=def){
	}else{
		window.location.reload(true);
	}
	
});

$(".sideBarRecord").click(function(){
		
	var color = $( this ).css( "background-color" );
	var def='rgb(40, 135, 189)';
	
	if(color!=def){
	}else{
		window.location.reload(true);
	}
});




});
$(".datepick").on("show", function () {
    $("body").on("touchmove", false);
    $('body').attr('style','overflow-y:auto;');
});
$(".datepick").on("hide", function () {
   $("body").unbind("touchmove");
   $('body').attr('style','margin-top:20px');
});
window.clearInterval(autoContactDetailsFetchTimer);
setInterval(function(){myTimerNote();},120000);

function myTimerNote(){
	if(userSession=='Active')
	{
	reloadNotes();
	}
}

	
$(document).ready( function() {
	// Fixing height of the page.
	var size=$(window).height() - 90;
    $('#notes').attr('style','height:'+size+'px;overflow: auto;');
    var windowSize=$(window).width();
    $(window).bind('resize', function() {
    	if(windowSize > $(window).width()){
    		$('#notes').find('.listDiv').each(function( index ){
    			$(this).css('width', $('#content').width()-30);
    		});
    	}else{ 
    		$('#notes').find('.listDiv').each(function( index ){
    			$(this).removeAttr('style').attr('style','background-color: #F3F3F3; float: left;');
    		});
    	}
	}).trigger('resize');
    var copyListId;
	var moveAllNotesListId;
	var moveNotesId;
	var moveNotesName;
	var addDueDateId;
	var addMembersId;
	var addGroupsId;
	var moveListId;
	var noteJsMoveJsCopyType;
	var moreActionBasedId;
	var moreActionEvent;
	var memberShareAllCon;
	var copyNoteAllCon;
	var scheduleListId;
	var tagId;
	var tagButton;
	var incrementForPopover=1;
	var listOwnerFlagForAddBook;
	var publicNoteCopyFlag;
	var publicShareClickId;
	var publicShareListId;
	var publicShareNoteId;
	var publicShareAccess;
	var publicSharemoreactionFlag;
	var publicShareWarnMsgFlag;
	var archiveNoteShareType;
	var addNoteClickId;
	var sahreUserId;
	var bookName;
	var localSharedBookId=0;
	var listOwnerUserId ;
	var emailIds='';
	//var userIds='';
	var type='';
	var shareType='';
	var fromCrowd;
	var crowdShareMsg;
	var selectCheckbox="";
	/*$('#notes').on('click','.modalHeader', function(e){
		if (e.target !== this) return;
		
		if($(this).parent().children('.listBodyAll').attr('style') == 'display:none'){
			$(this).parent().children('.listBodyAll').attr('style','display:block');
		}else{
			$(this).parent().children('.listBodyAll').attr('style','display:none');
		}
		if($(this).parent().children('.js-footer').attr('style') == 'display:none'){
			$(this).parent().children('.js-footer').attr('style','display:block');
		}else{
			$(this).parent().children('.js-footer').attr('style','display:none');
		}
	});*/
	
	
$('#recordwithmic').click(function(){
		
		document.getElementById("audio_recording").style.display="block";
	});
	
	
	$('#notes').on('click','.dropDownStopProgress', function(event){
				event.stopPropagation();
	});
	
	
	var today = new Date();
	var yesterday=new Date();
	yesterday.setDate(yesterday.getDate()-1);
	
	var tomor=new Date();
	tomor.setDate(tomor.getDate()+1);
	
	//Open Add List Modal panel (Book functionality changing purpose commented below line please don't Delete)
	if(!isIphone && !isAndroid){
		$('#notes').on('dblclick',function(e){
			/*if (e.target !== this) return;*/
			if($(e.target).hasClass('noteDiv'))return;
			
			if($(e.target).hasClass('todo_description'))return;
			
			if($(e.target).hasClass('badges'))return;

			if($(e.target).hasClass('listDiv'))return;
			
			if($(e.target).hasClass('listBodyAll'))return;
			
			if($(e.target).hasClass('fc-button'))return;
			
			if($(e.target).hasClass('glyphicon glyphicon-arrow-right'))return;
			
			if($(e.target).hasClass('glyphicon glyphicon-arrow-left'))return;
			
			var flag=false;
			$('#notes').find('.modal').each(function( index ){
				  if($(this).attr('class').split(' ').pop() == 'in')
					  flag=true;
			});
			/*if(listType == 'schedule'){
				$("#bookWarningId").text("");
				if(!flag)
					$('#addListModal').modal('toggle');
			}*/
		});
	}else{
		
		/*$('#notes').doubletap(function(e){
			if (e.target !== this) return;
			if(listType == 'schedule'){
				$("#bookWarningId").text("");
				$('#addListModal').modal('toggle');
			}
		});*/
	}
	$('#notes').on('click','.newBook',function(){
		
		$("#noteBookWarningId").text("");
		$('#addBookModal').modal('show');
	});
	
	$('#notes').on('click','.createBook',function(){
		$("#moreActions").modal('hide');
		$("#bookWarningId").text("");
		$('#addListModal').modal('show');
	});
	
	var mouseOverEventflag="";
	/* Note Mouse Over And Out Event Fire Method */
	$('#notes').on('mouseover','.noteDiv',function(){
		$($(this).parent().parent()).find('.modalHeader').each(function( index ){
				mouseOverEventflag=true;
		});
		if(mouseOverEventflag == ''){
			$(this).parent().parent().css({"background-color":"linear-gradient(to bottom, #A4A4A4 0%, #A4A4A4 100%) repeat scroll 0 0 transparent"});
			$(this).parent().css({"background-color":"linear-gradient(to bottom, #A4A4A4 0%, #A4A4A4 100%) repeat scroll 0 0 transparent"});
		}
		$(this).css({"background-color":"linear-gradient(to bottom, #A4A4A4 0%, #A4A4A4 100%) repeat scroll 0 0 transparent"});
	});
	$('#notes').on('mouseout','.noteDiv',function(){
		if(mouseOverEventflag == ''){
			$(this).parent().parent().css({"background-color":"#F3F3F3"});
			$(this).parent().css({"background-color":"#F3F3F3"});
		}
		$(this).css({"background-color":"#F3F3F3"});
		mouseOverEventflag='';
	});
	
	/* Crowd Note Mouse Over And Out Event Fire Method */
	$('#crowd').on('mouseenter','.noteDiv',function(){
		$(this).css({"background-color":"linear-gradient(to bottom, #A4A4A4 0%, #A4A4A4 100%) repeat scroll 0 0 transparent"});
	});
	$('#crowd').on('mouseleave','.noteDiv',function(){
		$(this).css({"background-color":"#F3F3F3"});
	});
	
	
	/*Adding new List for calender*/
	$('#newAddCalender').click( function() {
			

		var listName = trim($('#calenderName').val());
		var listId='';
		var noteId='';
		if(listType != 'schedule'){
			listId=listIdFromList;
			noteId=noteIdFromList;
		}else{
			listId='0';
			noteId='0';
		}
		
		var url = urlForServer+"note/createList/"+listId+"/"+noteId;
		if(listName!=null && listName !=""){
			var params = "{\"listId\":\"1\",\"listName\":\""+ listName +"\",\"listType\":\""+ listType +"\",\"userId\":\""+userId+"\",\"noteAccess\":\""+allNoteCreateBasedOn+"\",\"status\":\"A\",\"notesDetails\":[]}";
			params = encodeURIComponent(params);
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
		        type: 'POST',
		        url : url,
		        cache: false,
		        contentType: "application/json; charset=utf-8",
		        data:params, 
		        dataType: "json",
		        success : function(response){
					var size='';
					if(listType!='schedule')
						size='left:660px;';
					else
						size='left:630px;';
					
					for(var i=0 ; i < response.length; i++){
						listAccessMap[response[i].listId]=response[i].userId;
						var content='';
						if(listType!='schedule'){
							content=content+'<div class="listDiv col-md-5 inline inline-block;" style="float:left;" id="'+response[i].listId+'">';	
						}else{
							content=content+'<div class="listDiv col-md-12 inline inline-block;" style="float:left;" id="'+response[i].listId+'">';
						}
						content=content+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
    						+'<ul class="dropdown-menu" id="'+response[i].listId+'booklists"  style="top: auto;overflow-y: none;width: auto;'+size+'"></ul>'
    						+'</div>'
    						+'<div><b class="headerListName fontStyle15">'
    						+listName
    						+'</b></div>'
    						+'</div>'
    						+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
    						+'</div>'
    						+'<div class="js-footer">';
				 			if(listType!='schedule'){
				 				content=content+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer fontStyle" style="text-decoration: none;">Add New Note</a>';
				 			}else{
				 				content=content+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe schedule-footer fontStyle" style="text-decoration: none;">Add New Event</a>';
				 			}
				 			content=content+'</div></div>';
            	
				 			if(listType=='schedule'){
				 				/*$('#scheduleContent').append(content);	*/
				 			}
				 			else{
				 				//$('#notes').append(content);
				 				var sideActionDiv='';
				 				sideActionDiv=sideActionDiv+'<li><a id="'+response[i].listId+'">';
				 				sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-ok pull-right"></i>';
				 				//sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-edit bookIconEdit" id="'+response[i].listId+'" title="edit"></i> &nbsp; <i class="glyphicon glyphicon-trash bookIconDelete" title="delete" id="'+response[i].listId+'"></i> &nbsp;<i class="noteToTag" id="'+response[i].listId+'" title="' + listName +'">' + listName +'</i></a></li>';
				 				sideActionDiv=sideActionDiv+'<i class="noteToBook" id="'+response[i].listId+'" title="' + listName +'">' + listName +'</i></a></li>';
				 				sideActionDiv=sideActionDiv+'<li class="divider"></li>';
				 				sideActionDiv=sideActionDiv+'<li class="bookAddClass"><a class="createBook" id="createBook"><span class="glyphicon glyphicon-plus createBook"></span>&nbsp;&nbsp; Create New Book</a></li>'
				 				
				 				$("#bookAddingUiId > li").each(function() {
				 					if($(this).hasClass( "divider" )){
				 						$(this).remove();
				 					}
				 					if($(this).hasClass( "bookAddClass" )){
				 						$(this).remove();
				 					}
				 				});
				 				$('#bookAddingUiId').append(sideActionDiv);
				 			}
				 			getListAndNoteNames();
				 			
				 			$('#addCalenderListModal').modal('hide');
				 			$('#calenderName').val("");
				 			
				 			if(listType!='schedule'){
				 				$("#moreActions").modal('show');
				 			}
					}
				},
		        error: function(e) {
		                alert("Please try again later");
		        }
		    });
		}else{
			$("#calenderWarningId").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#calenderWarningId").text("Calendar title required").css({"color":"red"});
		}
	
	});
		
	
	$('#newAddBook').click( function() {
		
		var listName = trim($('#BookName').val());
		var listId='';
		var noteId='';
		if(listType != 'schedule'){
			listId=listIdFromList;
			noteId=noteIdFromList;
		}else{
			listId='0';
			noteId='0';
		}
		var url = urlForServer+"note/createBook";
		if(listName !=""){
			var params = "{\"listId\":\"1\",\"listName\":\""+ listName +"\",\"listType\":\""+ listType +"\",\"userId\":\""+userId+"\",\"noteAccess\":\""+allNoteCreateBasedOn+"\",\"status\":\"A\",\"notesDetails\":[]}";
			params = encodeURIComponent(params);
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
		        type: 'POST',
		        url : url,
		        cache: false,
		        contentType: "application/json; charset=utf-8",
		        data:params, 
		        dataType: "json",
		        success : function(response){
					var size='';
					if(listType!='schedule')
						size='left:660px;';
					else
						size='left:630px;';
					
					for(var i=0 ; i < response.length; i++){
						
				 			getListAndNoteNames();
				 			
				 			$('#addBookModal').modal('hide');
				 			$('#BookName').val("");
				 							 			
					}
					
				},
		        error: function(e) {
		                alert("Please try again later");
		        }
		    });
		}else{
			$("#noteBookWarningId").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#noteBookWarningId").text("Notebook title required").css({"color":"red"});
		}
	});
	
	
	/*Adding new List*/
	$('#newAdd').click( function() {
		var listName = trim($('#modelName').val());
		var listId='';
		var noteId='';
		if(listType != 'schedule'){
			listId=listIdFromList;
			noteId=noteIdFromList;
		}else{
			listId='0';
			noteId='0';
		}
		var url = urlForServer+"note/createList/"+listId+"/"+noteId;
		if(listName !=""){
			var params = "{\"listId\":\"1\",\"listName\":\""+ listName +"\",\"listType\":\""+ listType +"\",\"userId\":\""+userId+"\",\"noteAccess\":\""+allNoteCreateBasedOn+"\",\"status\":\"A\",\"notesDetails\":[]}";
			params = encodeURIComponent(params);
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
		        type: 'POST',
		        url : url,
		        cache: false,
		        contentType: "application/json; charset=utf-8",
		        data:params, 
		        dataType: "json",
		        success : function(response){
					var size='';
					if(listType!='schedule')
						size='left:660px;';
					else
						size='left:630px;';
					
					for(var i=0 ; i < response.length; i++){
						listAccessMap[response[i].listId]=response[i].userId;
						var content='';
						if(listType!='schedule'){
							content=content+'<div class="listDiv col-md-5 inline inline-block;" style="float:left;" id="'+response[i].listId+'">';	
						}else{
							content=content+'<div class="listDiv col-md-12 inline inline-block;" style="float:left;" id="'+response[i].listId+'">';
						}
						content=content+'<div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
    						+'<ul class="dropdown-menu" id="'+response[i].listId+'booklists"  style="top: auto;overflow-y: none;width: auto;'+size+'"></ul>'
    						+'</div>'
    						+'<div><b class="headerListName fontStyle15">'
    						+listName
    						+'</b></div>'
    						+'</div>'
    						+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
    						+'</div>'
    						+'<div class="js-footer">';
				 			if(listType!='schedule'){
				 				content=content+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer fontStyle" style="text-decoration: none;">Add New Note</a>';
				 			}else{
				 				content=content+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe schedule-footer fontStyle" style="text-decoration: none;">Add New Event</a>';
				 			}
				 			content=content+'</div></div>';
            	
				 			if(listType=='schedule'){
				 				$('#scheduleContent').append(content);	
				 			}
				 			else{
				 				//$('#notes').append(content);
				 				var sideActionDiv='';
				 				sideActionDiv=sideActionDiv+'<li><a id="'+response[i].listId+'">';
				 				sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-ok pull-right"></i>';
				 				//sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-edit bookIconEdit" id="'+response[i].listId+'" title="edit"></i> &nbsp; <i class="glyphicon glyphicon-trash bookIconDelete" title="delete" id="'+response[i].listId+'"></i> &nbsp;<i class="noteToTag" id="'+response[i].listId+'" title="' + listName +'">' + listName +'</i></a></li>';
				 				sideActionDiv=sideActionDiv+'<i class="noteToBook" id="'+response[i].listId+'" title="' + listName +'">' + listName +'</i></a></li>';
				 				sideActionDiv=sideActionDiv+'<li class="divider"></li>';
				 				sideActionDiv=sideActionDiv+'<li class="bookAddClass"><a class="createBook" id="createBook"><span class="glyphicon glyphicon-plus createBook"></span>&nbsp;&nbsp; Create New Book</a></li>'
				 				
				 				$("#bookAddingUiId > li").each(function() {
				 					if($(this).hasClass( "divider" )){
				 						$(this).remove();
				 					}
				 					if($(this).hasClass( "bookAddClass" )){
				 						$(this).remove();
				 					}
				 				});
				 				$('#bookAddingUiId').append(sideActionDiv);
				 			}
				 			getListAndNoteNames();
				 			
				 			$('#addListModal').modal('hide');
				 			$('#modelName').val("");
				 			
				 			if(listType!='schedule'){
				 				$("#moreActions").modal('show');
				 			}
					}
					
				},
		        error: function(e) {
		                alert("Please try again later");
		        }
		    });
		}else{
			$("#bookWarningId").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#bookWarningId").text("Notebook title required").css({"color":"red"});
		}
	});
	
	/* Delete list */
	var deleteId="";
	var deleteListId="";
	$('#notes').on('click','.js-close-list',function(){
		deleteId=$(this);
		deleteListId=$(this).parent().parent().parent().parent().parent().attr('id');
		if(listType!='schedule')
			$('.delete-Book-warn').text('Do you want to delete this Book ?');
		else
			$('.delete-Book-warn').text('Do you want to delete this Calendar ?');
		
		$('#deleteBookModal').modal('toggle');
	});
	
	/*Archive List Modal panel Functionality*/
	$('#notes').on('click','.js-delete-list',function(){
		$('#deleteBookModal').modal('hide');
		var url = urlForServer+"note/archiveList/"+userId;
		var params = '{"listId":"'+deleteListId+'","listType":"'+listType+'","sharedType":"'+deleteId.attr('class')+'"}';
	    params = encodeURIComponent(params);
	
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	
	    		for ( var i = 0; i < response.length; i++) 
	    		{
	    			delete listAccessMap[response[i].listId];
	    		}
	    		getListAndNoteNames();
	    		$(deleteId).parent().parent().parent().parent().parent().remove();
	    		if(listType=='schedule'){
	    			$('#calendarlist').empty();
	    			
	    			loadAllEvents();
	    			loadCalendarComboBoxList();
	    		}else{
	    			loadEventsBooks();
	    		}
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });
		
		
	});
	
	//modal panel close functionality for all modal panel
	$('#notes').on('click','.js-modal-close',function(){
		if(moreActionEvent=='dueDate'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='move'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='attach'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='memberIndividual'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='memberGroup'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='memberAllContact'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='copy'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='copyIndividual'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='copyGroup'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
		if(moreActionEvent=='copyToContact'){
			$('#moreActions').modal('toggle');
			moreActionEvent='';
		}
	});
	
	
	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
	
	/*Add note to the list*/
	$('#notes').on('click','#newNoteAdd',function(e){
		var result="";
		var noteName="";
		var datedesc="";
		var listIds='';
		var noteIds='';
		var selectedBookId='0';
		noteName=trim($('#NoteName').val());
		
		var date=new Date();
		var day=date.getDate();
		var months=date.getMonth();
		var year=date.getFullYear();
		var hour=date.getHours();
		var minutes=date.getMinutes();
		var seconds=date.getSeconds();
		dates=month[months]+"/"+day+"/"+year;
		date1=months+1+"/"+day+"/"+year+" "+hour+":"+minutes;
		dates1=convertNumberDateFormatIntoStringFormat(date1,false);
		if(listType=='bill'){
			datedesc='<div class="badge glyphicon glyphicon-sm decs1" title="This note added on '+dates1+'"><span class="glyphicon glyphicon-play-circle"></span><span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;'+dates+'</span></div>';
		}else{
			datedesc="";
		}
		var searchAddNoteFlag = false;
		if($('#notebookId').val() != 'note'){
			searchAddNoteFlag = true;
		}
		//enter key press event
		if(noteName!=""){
		
			if(!searchAddNoteFlag && listType!="schedule" && $(addNoteClickId).parent().attr('id')!= undefined && $(addNoteClickId).parent().attr('id')=="new-note"){
				var url = urlForServer+"note/createNoteInDefualtList/"+listType+"/"+userId+"/"+selectedBookId;
				dates1=dateConvert();
				var params ='';
				while(noteName.indexOf("\n") != -1){
					noteName = noteName.replace("\n"," ");
				}
				while(noteName.indexOf("\t") != -1){
					noteName = noteName.replace("\t"," ");
				}
				while(noteName.indexOf("\"") != -1){
					noteName = noteName.replace("\"", "`*`");
				}
				if(allNoteCreateBasedOn == 'public')
					params = '{"noteName":"'+noteName+'","startDate":"'+date+'","notesMembers":"","noteGroups":"","noteSharedAllContact":0,"noteSharedAllContactMembers":"","status":"A","ownerNoteStatus":"A","tag":"","remainders":"","vote":"","links":"","endDate":"","noteId":"","dueDate":"","dueTime":"","access":"'+allNoteCreateBasedOn+'","attachFilePath":"","description":"","publicDescription":"","comments":"","pcomments":"","copyToMember":"","copyToGroup":"","copyToAllContact":0,"copyFromMember":"","publicUser":"'+userFulName+'","publicDate":"'+dates1+'","shareType":"","privateTags":"","privateAttachFilePath":"","fromCrowd":""}';
				else
					params = '{"noteName":"'+noteName+'","startDate":"'+date+'","notesMembers":"","noteGroups":"","noteSharedAllContact":0,"noteSharedAllContactMembers":"","status":"A","ownerNoteStatus":"A","tag":"","remainders":"","vote":"","links":"","endDate":"","noteId":"","dueDate":"","dueTime":"","access":"'+allNoteCreateBasedOn+'","attachFilePath":"","description":"","publicDescription":"","comments":"","pcomments":"","copyToMember":"","copyToGroup":"","copyToAllContact":0,"copyFromMember":"","publicUser":"","publicDate":"","shareType":"","privateTags":"","privateAttachFilePath":"","fromCrowd":""}';
				
				params = encodeURIComponent(params);
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
			    	},
					type: 'POST',
					url : url,
					cache: false,
					contentType: "application/json; charset=utf-8",
					data:params, 
					dataType: "json",
					success : function(response){
						while(noteName.indexOf("`*`") != -1){
							noteName = noteName.replace("`*`", "\"");
						}
						$('#notes').children('#searchDiv').after('<div id="'+response['listId']+'"class="listDiv col-md-11" style="float:left; background-color:#F3F3F3;" >'
							+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
								+'<div id="'+response['noteId']+'" class="noteDiv " style="background-color:#F3F3F3;">'
									+'<div class="todo_description fontStyle15">'
										+'<p><b>'+noteName+'</b></p>'
									+'</div>'
									+'<div class="badges fontStyle">'+datedesc+'</div>'
								+'</div>'
							+'</div>'
						+'</div>');
						$('#NoteName').val("");
						
						
						getListAndNoteNames();
						$('#addNoteModal').modal('hide');
						
					    listIdFromList=response['listId'];
						noteIdFromList=response['noteId'];
						listId=response['listId'];
						noteId=response['noteId'];
						noteNameFromList=noteName;
						parentDivName= "notes";
						moreActionBasedId = $('#notes').children('#'+response['listId']+'').children('.listBodyAll').children('#'+response['noteId']+'').children('.todo_description');
						checkOwnerOfList();
				        
					},				
					error: function(e) {
						alert("Please try again later");
					}
					
				});
				
				
			}else{
				var id="";
				if(searchAddNoteFlag){
					id=$('#notebookId').val();
				}else{
					id=$(addNoteClickId).parent().parent().attr('id');
				}
				
				if($('#notebookId').val() !='note')
					selectedBookId=$('#notebookId').val();
				
				var url = urlForServer+"note/createNoteInDefualtList/"+listType+"/"+userId+"/"+selectedBookId;
				
				//var url = urlForServer+"note/createNote/"+id+"/"+userId;
				
				/*This code used to separate notes comments and crowd comments. by Venu*/
				/*var params = '{"noteName":"'+noteName+'","startDate":"'+date+'","notesMembers":"","noteGroups":"","noteSharedAllContact":0,"status":"A","tag":"","remainders":"","vote":"","links":"","endDate":"","noteId":"","dueDate":"","dueTime":"","access":"private","attachFilePath":"","description":"","comments":"","pcomments":"","copyToMember":"","copyToGroup":"","copyToAllContact":0}';*/
				
				dates1=dateConvert();
				var params ='';
				while(noteName.indexOf("\n") != -1){
					noteName = noteName.replace("\n"," ");
				}
				while(noteName.indexOf("\t") != -1){
					noteName = noteName.replace("\t"," ");
				}
				while(noteName.indexOf("\"") != -1){
					noteName = noteName.replace("\"", "`*`");
				}
				if(allNoteCreateBasedOn == 'public')
					params = '{"noteName":"'+noteName+'","startDate":"'+date+'","notesMembers":"","noteGroups":"","noteSharedAllContact":0,"noteSharedAllContactMembers":"","status":"A","ownerNoteStatus":"A","tag":"","remainders":"","vote":"","links":"","endDate":"","noteId":"","dueDate":"","dueTime":"","access":"'+allNoteCreateBasedOn+'","attachFilePath":"","description":"","publicDescription":"","comments":"","pcomments":"","copyToMember":"","copyToGroup":"","copyToAllContact":0,"copyFromMember":"","publicUser":"'+userFulName+'","publicDate":"'+dates1+'","shareType":"","privateTags":"","fromCrowd":""}';
				else
					params = '{"noteName":"'+noteName+'","startDate":"'+date+'","notesMembers":"","noteGroups":"","noteSharedAllContact":0,"noteSharedAllContactMembers":"","status":"A","ownerNoteStatus":"A","tag":"","remainders":"","vote":"","links":"","endDate":"","noteId":"","dueDate":"","dueTime":"","access":"'+allNoteCreateBasedOn+'","attachFilePath":"","description":"","publicDescription":"","comments":"","pcomments":"","copyToMember":"","copyToGroup":"","copyToAllContact":0,"copyFromMember":"","publicUser":"","publicDate":"","shareType":"","privateTags":"","fromCrowd":""}';
				
				params = encodeURIComponent(params);

				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type: 'POST',
					url : url,
					cache: false,
					contentType: "application/json; charset=utf-8",
					data:params, 
					dataType: "json",
					success : function(response){
							while(noteName.indexOf("`*`") != -1){
								noteName = noteName.replace("`*`", "\"");
							}
							if(searchAddNoteFlag){
								var noteClass='';
								if(selectedBookId!='0' && response['listId'] != selectedBookId)
									noteClass = response['listId']+' noteDiv';
								else
									noteClass='noteDiv';
								
								var tempFirstInsert = $('#notes').children('.listDiv').attr('class');
								if(tempFirstInsert != undefined){
									$('#notes').children('#'+id+'').children('.listBodyAll').prepend('<div class="'+noteClass+'" id="'+response['noteId']+'" style="border: 1px solid #dddddd;" >'
										+ '<div class="todo_description fontStyle15">'
										//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
										//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
										//+'</div>'
										+'<p><b>'
										+ noteName
										+ '</b></p></div>'
										+'<div class="badges fontStyle">'
										+datedesc
										+ '</div>'
										+ '</div>');
								}else{
									var listNameSelected = '';
									var selectedListId='';
									$('#notebookId').children('option').each(function() {
										var value = $(this).attr('value');
										if(value == $('#notebookId').val()){
											selectedListId=$('#notebookId').val();
											listNameSelected =  $(this).text();
										}
									}
									);
									var data= '<div class="listDiv col-md-11" style="float:left;background-color:#F3F3F3;" id="'+selectedListId+'">'
									+'<div class="modalHeader">'
										+'<div><a class="close listMenu dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
											+'<ul class="dropdown-menu" id="'+selectedListId+'booklists" style="top: auto;overflow-y: none;width: auto;left:715px;"></ul>'
										+'</div>'
										+'<div><b class="headerListName fontStyle15">'
											+listNameSelected
										+'</b></div>'
									+'</div>'
									
									+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
									+'<div class="'+noteClass+'" id="'+response['noteId']+'" style="border: 1px solid rgb(221, 221, 221); background-color: rgb(243, 243, 243);">'
								
									//+'<div class="'+noteClass+'" style="background-color:#F3F3F3;">'
										+'<div class="todo_description fontStyle15">'
											+'<p><b>'
												+ noteName
												+ '</b>'
												+ '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
											+'</p>'
											+'</div>'
											+'<div class="badges fontStyle">'
												+datedesc
											+'</div>'
									+ '</div>'
									+ '</div>'
									+'<div class="js-footer">'
										+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer fontStyle" style="text-decoration: none;">Add New Note</a>'
									+'</div>'
									+'</div>';
									$('#notes').children('#searchDiv').after(data);
								}
							}else{
								$(addNoteClickId).parent().parent().children('.listBodyAll').prepend('<div class="noteDiv" id="'+response['noteId']+'" style="border: 1px solid #dddddd;background-color:#F3F3F3;" >'
									+ '<div class="todo_description fontStyle15">'
									//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
									//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
									//+'</div>'
									+'<p><b>'
									+ noteName
									+ '</b></p></div>'
									+'<div class="badges fontStyle">'
									+datedesc
									+ '</div>'
									+ '</div>');
								}
							
							$('#NoteName').val("");
							getListAndNoteNames();
							$('#addNoteModal').modal('hide');
								
							if(selectedBookId!='0' && response['listId'] != selectedBookId)
							    listIdFromList=response['listId'];
							else
								listIdFromList=id;
							
							noteIdFromList=response['noteId'];
							
							if(selectedBookId!='0' && response['listId'] != selectedBookId)
								listId=response['listId'];
							else
								listId=id;
							
							noteId=response['noteId'];
							noteNameFromList=noteName;
							parentDivName= "notes";
							moreActionBasedId = $('#notes').children('#'+id+'').children('.listBodyAll').children('#'+response['noteId']+'').children('.todo_description');
							checkOwnerOfList();
								
							//loadTags();
					},
					error: function(e) {
						alert("Please try again later");
					}
				});
			}
		}
		else{
			$("#noteWarningId").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#noteWarningId").text("Note title required").css({"color":"red"});
			
		}
	});
	
	 
	/*kishore here */
	var  noteNameFromList;
	var listIdFromList;
	var noteIdFromList;
	var listOwnerFlag = false;
	var sharedNoteFlag = false;
	var sharedHideDelete = false;
	var listOwnerFullName ;
	var listOwnerFirstName;
	var parentDivName='';
	var vote =new Array();
	$('#notes').on('click','.noteDiv ',function(e){
		 if (e.target !== this) return;
		 
		

		 moreActionBasedId=$(this).children('.todo_description');
		 
		 var classStr = $(this).attr('class');
		 classStr=classStr.trim();
		 archiveNoteShareType = classStr.substr( classStr.lastIndexOf(' ') + 1);
		 
		 if(archiveNoteShareType !=null && archiveNoteShareType!=undefined && archiveNoteShareType!='' && archiveNoteShareType !='null')
			 sahreUserId= $(this).children('.badges').children().attr('id');
		 else
			 sahreUserId=null;

		var findListIdCls = $(this).attr('class');
		var clas= findListIdCls.split(" ");
		if(clas[0].trim()== "noteDiv"){
			listIdFromList=$(this).parent().parent().attr('id');
		}else{
			listIdFromList= clas[0].trim();
		}
		 //listIdFromList=$(this).parent().parent().attr('id');
		 noteIdFromList=$(this).attr('id');
		 noteNameFromList =$(this).children('.todo_description').find("p").find("b").text();
		 parentDivName= "notes";
		 checkOwnerOfList();
		 
	});
	
	$('#notes').on('click','.todo_description ',function(e){
		moreActionBasedId=$(this);
		
		var classStr = $(this).parent().attr('class');
		classStr=classStr.trim();
		archiveNoteShareType = classStr.substr( classStr.lastIndexOf(' ') + 1);
		 if(archiveNoteShareType !=null && archiveNoteShareType!=undefined && archiveNoteShareType!='' && archiveNoteShareType !='null')
			 sahreUserId= $(this).parent().children('.badges').children().attr('id');
		 else
			 sahreUserId=null;

		var findListIdCls = $(this).parent().attr('class');
		var clas= findListIdCls.split(" ");
		if(clas[0].trim()== "noteDiv"){
			listIdFromList=$(this).parent().parent().parent().attr('id');
		}else{
			listIdFromList=clas[0].trim();
		}
		noteIdFromList=$(this).parent().attr('id');
		noteNameFromList =$(this).find("p").find("b").text();
		parentDivName= "notes";
		checkOwnerOfList();
	});
	
	$('#notes').on('click','.badges ',function(e){
		moreActionBasedId=$(this).parent().children('.todo_description');
		
		var classStr = $(this).parent().attr('class');
		classStr=classStr.trim();
		archiveNoteShareType = classStr.substr( classStr.lastIndexOf(' ') + 1);
		
		 if(archiveNoteShareType !=null && archiveNoteShareType!=undefined && archiveNoteShareType!='' && archiveNoteShareType !='null')
			 sahreUserId= $(this).children().attr('id');
		 else
			 sahreUserId=null;

		var findListIdCls = $(this).parent().attr('class');
		var clas= findListIdCls.split(" ");
		if(clas[0].trim()== "noteDiv"){
			listIdFromList=$(this).parent().parent().parent().attr('id');
		}else{
			listIdFromList= clas[0].trim();
		}
		noteIdFromList=$(this).parent().attr('id');
		noteNameFromList =$(this).parent().children('.todo_description').find("p").find("b").text();
		parentDivName= "notes";
		checkOwnerOfList();
	});
	
	// crowd page
	
	$('#mostPopular').on('click','.noteDiv ',function(e){
		 if (e.target !== this) return;
		
		   moreActionBasedId=$(this).children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().attr('id');
		   noteIdFromList=$(this).attr('id');
		
		   noteNameFromList =$(this).children('.todo_descriptions').find("p").text();
		   //alert(noteIdFromList+"NoteName"+noteNameFromList);
		   parentDivName= "crowd";
		   checkOwnerOfList();
		   LogWrite(noteIdFromList,listIdFromList,userId,noteNameFromList);
			
	});
	$('#mostPopular').on('click','.todo_descriptions ',function(e){
		
		   moreActionBasedId=$(this).parent().children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().parent().attr('id');
		   noteIdFromList=$(this).parent().attr('id');
		
		   noteNameFromList =$(this).parent().children('.todo_descriptions').find("p").text();
		   //alert(noteIdFromList+"NoteName"+noteNameFromList);
		   parentDivName= "crowd";
		   checkOwnerOfList();
		   LogWrite(noteIdFromList,listIdFromList,userId,noteNameFromList);
			
	});
	
	$('#mostView').on('click','.noteDiv ',function(e){
		 if (e.target !== this) return;
			
		   moreActionBasedId=$(this).children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().attr('id');
		   noteIdFromList=$(this).attr('id');
		
		   noteNameFromList =$(this).children('.todo_descriptions').find("p").text();
		   //alert(noteIdFromList+"NoteName"+noteNameFromList);
		   parentDivName= "crowd";
		   checkOwnerOfList();
		   LogWrite(noteIdFromList,listIdFromList,userId,noteNameFromList);
		 
		
	});
	$('#mostView').on('click','.todo_descriptions ',function(e){
		
		   moreActionBasedId=$(this).parent().children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().parent().attr('id');
		   noteIdFromList=$(this).parent().attr('id');
		
		   noteNameFromList =$(this).parent().children('.todo_descriptions').find("p").text();
		   //alert(noteIdFromList+"NoteName"+noteNameFromList);
		   parentDivName= "crowd";
		   checkOwnerOfList();
		   LogWrite(noteIdFromList,listIdFromList,userId,noteNameFromList);
		 
		
	});
	
	$('#newNote').on('click','.noteDiv ',function(e){
		 if (e.target !== this) return;
		
	
		   moreActionBasedId=$(this).children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().attr('id');
		   noteIdFromList=$(this).attr('id');
		 
		   noteNameFromList =$(this).children('.todo_descriptions').find("p").text();
		   //alert(noteIdFromList+"NoteName"+noteNameFromList);
		   parentDivName= "crowd";
		   checkOwnerOfList();
		   LogWrite(noteIdFromList,listIdFromList,userId,noteNameFromList);
		 
		
	});
	
	$('#crowdUser').on('click','.noteDiv',function(e){
		// if (e.target !== this) return;
		   moreActionBasedId=$(this).children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().attr('id');
		   noteIdFromList=$(this).attr('id');
		 
		   noteNameFromList =$(this).children('.todo_descriptions').find("p").text();
		   checkOwnerOfList();
		   LogWrite(noteIdFromList,listIdFromList,userId,noteNameFromList);
		 
		
	});
	
	
	
	$('#newNote').on('click','.todo_descriptions ',function(e){
		
		   moreActionBasedId=$(this).parent().children('.todo_descriptions');
		   listIdFromList=$(this).parent().parent().parent().attr('id');
		   noteIdFromList=$(this).parent().attr('id');
		
		   noteNameFromList =$(this).parent().children('.todo_descriptions').find("p").text();
		   parentDivName= "crowd";
		   checkOwnerOfList();
		   LogWrite(noteIdFromList,listIdFromList,userId,noteNameFromList);
		 
		
	});
	
	//crowd page 
	function checkOwnerOfList(){
		if(listType!='crowd'){
		document.getElementById('loginListId').value=listIdFromList;
		document.getElementById('loginNoteId').value=noteIdFromList;
		}
		var url = urlForServer+"note/checkOwner/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type: 'POST',
			url : url,     
			cache: false,
			success : function(responseText){
				var data = jQuery.parseJSON(responseText);
				
				if(parentDivName == "notes" ){
					if(data.status == "success"){
						listOwnerFlag = true;
						listOwnerFlagForAddBook=true;
						getTagsForNote();
					}else{
						listOwnerFlag = false;
						listOwnerFlagForAddBook=false;
					}
				}else{
					listOwnerFlag = false;
					if(data.status == "success"){
						listOwnerFlagForAddBook=true;
					}else{
						listOwnerFlagForAddBook=false;
					}
				}
				
				if(data.shared =='shared'){
					sharedHideDelete = true;
				}else{
					sharedHideDelete = false;
				}
				
				if(!listOwnerFlag){
					if(moreActionBasedId.parent().attr('class') == 'noteDiv'){
						sharedNoteFlag = false;
					}else{
						sharedNoteFlag = true;
					}
				}
				if(data.crowdShareMsg==true)
				{
					crowdShareMsg=data.crowdShareMsg;
				}
				else
				{
					crowdShareMsg=false;
				}
				publicShareWarnMsgFlag = data.publiShareWarnMsg;
				listOwnerFullName = data.userName;
				listOwnerFirstName =data.userFirstName;
				listOwnerUserId = data.userId;
				publicNoteCopyFlag = data.publiCopy;
				//setTimeout(function(){createMoreActionModal();},1000);
				createMoreActionModal();
				
			},
			error : function (e){
				console.log("<-------error returned for new ajax request attach file-------> "+e);
			}
		});
		
	}
	
	function createMoreActionModal(){
		
		$('#tagFilterUl li').not('.tagit-new').remove();
		var tagsArray=new Array();
		var userDetailsJsonObj;
		var url = urlForServer+"note/getNote/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
				type: 'POST',
				url : url,
				cache: false,
				success : function(responseText){
					var data = jQuery.parseJSON(responseText);
					var desc='';
					var commentsDiv='';
					var attachDiv='';
					var description='';
					var tagss="";
					var tagNames="";
					var votesWithParam;
					var cmtWithParams;
					var attachWithParams; 
					var noteSharedAllContact;
					var noteOwnerId;
					var noteLevel;
					
					var noteSharedAllContactMembers;
					var noteSharedAllContactMembersIds =new Array();
					var comments = new Array();
					var commentSize='0';
					var remainder = new Array();
					var attachArray = new Array();
					var tagArray = new Array();
					vote =new Array();
					var viteWithUser='vote';
					var voteDivClass = 'js-vote';
					var dueDate = '';
					var voteArchieve = '';
					var location='';
					if(data!=null && data!=''){
						//description=data['description'];
						votesWithParam = data['vote'];
						$('#tagFilterUl').remove();
						$('#tagFilterDiv').append('<ul id="tagFilterUl" class="span"></ul>');
						/*This code used to separate notes comments and crowd comments. by Venu*/
						/*if(listType=='crowd')
							cmtWithParams = data['pcomments'];
						else
							cmtWithParams = data['comments'];*/
						if(listType=='crowd')
							cmtWithParams = data['pcomments'];
						else
							cmtWithParams = data['comments'];
						
						if(listType=='crowd'){
							if(data['publicDescription']==undefined){
								description = data['description'];
							}else{
								description = data['publicDescription'];
							}
						}
						else{
							description = data['description'];
						}
						if(listType=='crowd'){
							attachWithParams =data['privateAttachFilePath'];
						}else{
							attachWithParams =data['attachFilePath'];
						}
						
						
						if(listType=="schedule"){
							noteSharedAllContact=data['eventSharedAllContact'];
							location=data['location'];
						}
						else
							noteSharedAllContact=data['noteSharedAllContact'];
						
						if(listType!="schedule")
						{
						if(data['shared'] =='shared')
							sharedHideDelete=true;
						else
							sharedHideDelete=false;
						}
						
						noteOwnerId=data['userId'];
						noteLevel=data['noteLevel'];
						fromCrowd=data['fromCrowd'];
						
						if(data['userId'] != userId){
							if(listType=="schedule")
								noteSharedAllContactMembers=data['eventSharedAllContactMembers'];
							else
								noteSharedAllContactMembers=data['noteSharedAllContactMembers'];
							
							if(noteSharedAllContactMembers.indexOf("[") != -1){
								noteSharedAllContactMembers = noteSharedAllContactMembers.substring(noteSharedAllContactMembers.indexOf("[")+1,noteSharedAllContactMembers.length);
							}
							if(noteSharedAllContactMembers.indexOf("]") != -1){
								noteSharedAllContactMembers = noteSharedAllContactMembers.substring(0,noteSharedAllContactMembers.indexOf("]"));
							}
							if(noteSharedAllContactMembers.indexOf(",") != -1){
								noteSharedAllContactMembersIds = noteSharedAllContactMembers.split(',');
							}else{
								if(noteSharedAllContactMembers.trim() != ''){
									noteSharedAllContactMembersIds = noteSharedAllContactMembers.split(',');
								}
							}
						}
						
						if(votesWithParam.indexOf("[") != -1){
							votesWithParam = votesWithParam.substring(votesWithParam.indexOf("[")+1,votesWithParam.length);
						}
						if(votesWithParam.indexOf("]") != -1){
							votesWithParam = votesWithParam.substring(0,votesWithParam.indexOf("]"));
						}
						if(votesWithParam.indexOf(",") != -1){
							vote = votesWithParam.split(',');
						}else{
							if(votesWithParam.trim() != ''){
								vote = votesWithParam.split(',');
							}
						}
						
						//Added by veera for tag name display with notes
						if(listType!='crowd'){
							
    						
    					if(data['tag']!=null && data['tag'] !=""){
							var tagWithParams= data['tag'];
							if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
								tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
							}
							if(tagWithParams!= undefined && tagWithParams.indexOf("]") != -1){
								tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
							}
							if(tagWithParams!= undefined && tagWithParams.indexOf(",") != -1){
								tagArray = tagWithParams.split(',');
							}else{
								if(tagWithParams!= undefined && tagWithParams.trim() != ''){
									tagArray = tagWithParams.split(',');
								}
							}
						//	alert("tagWithParams   "+tagWithParams);
							
							for(var siz=0;siz<tagArray.length;siz++){
								//alert(tagArray[siz]);
								var tagId=tagArray[siz].trim();
								if (tagId in notesTagMap){
									tagNames+="["+notesTagMap[tagId]+"]";
									var selectedTag='<li class="tagit-choice"><div class="tagit-label">'+notesTagMap[tagId]+'</div></li>';
									//$("#addveeeraTagFiltes").append(selectedTag);
									tagsArray.push(notesTagMap[tagId]);
									//alert(tagNames);
								}
							}
							
							
						}
    					
    					if(listType!="schedule" && (listOwnerFlag || noteLevel == 'edit'))
    						{
    							tagss ='<div class="badge glyphicon glyphicon-sm tagss" title="'+tagNames+'">'
    								+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span></div>';
    						}
    					}
						
						if(cmtWithParams.indexOf("[") != -1){
							cmtWithParams = cmtWithParams.substring(cmtWithParams.indexOf("[")+1,cmtWithParams.length);
						}
						if(cmtWithParams.indexOf("]") != -1){
							cmtWithParams = cmtWithParams.substring(0,cmtWithParams.indexOf("]"));
						}
						if(cmtWithParams.indexOf(",") != -1){
							comments = cmtWithParams.split(',');
						}else{
							if(cmtWithParams.trim() != ''){
								comments = cmtWithParams.split(',');
							}
						}
						
						if(attachWithParams!=null && attachWithParams!=undefined && attachWithParams!='')
						{
						
						if(attachWithParams.indexOf("[") != -1){
								attachWithParams = attachWithParams.substring(attachWithParams.indexOf("[")+1,attachWithParams.length);
						}
						if(attachWithParams.indexOf("]") != -1){
							attachWithParams = attachWithParams.substring(0,attachWithParams.indexOf("]"));
						}
						if(attachWithParams.indexOf(",") != -1){
								attachArray = attachWithParams.split(',');
							
						}else{
								if(attachWithParams.trim() != ''){
									attachArray = attachWithParams.split(',');
								}
						}
						attachDiv = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+attachArray.length+' attachment(s).">'
										+'<span class="glyphicon glyphicon-download-alt"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+attachArray.length+'</span>'     
									+'</div>';
						}
						
						if(attachArray.length > 0 && attachArray[0]!=null ){
							/*This code used to attachements. by kishore*/
							url= urlForServer+"note/getAttachedByFileId/"+userId;
							
							params = encodeURIComponent(attachArray);
							var attObj;
							$.ajax({	
								headers: { 
								"Mn-Callers" : musicnote,
						    	"Mn-time" :musicnoteIn					
						    	},
								type : 'POST',
								url : url,
								data : params,
								success : function(response) {
									var attachData = jQuery.parseJSON(response);
									if(attachData != null && attachData!= ''){
										
										var attachDiv1 =	'<div class="window-module clearfix" style="margin-bottom: 0;">'
											+'<div class="window-module-title no-divider">'
												+'<span class="window-module-title-icon glyphicon glyphicon-tasks" style="margin-top:22px;"></span>'
												+'<h4 >Attachments</h4>'
											+'</div>';
										
										attachDiv1 = attachDiv1 +'<div class="list-actions1 " style="top: -20px; position: relative;">';
							
										for ( var i = 0; i < attachData.length; i++) {
											attObj = attachData[i];
											var ext;
											
											if(attObj.fileName!= -1){
												ext = attObj.fileName.substring(attObj.fileName.lastIndexOf('.')+1,attObj.fileName.length);
											}
										var playclass='';
										var playIcon='';
										var link='javascript:void(0);';
										var target='';
										
										if(attObj.fileName.contains(".mov") || attObj.fileName.contains(".mp4") || attObj.fileName.contains(".avi") || attObj.fileName.contains(".wmv")  || attObj.fileName.contains(".wma") || attObj.fileName.contains(".mp3") || attObj.fileName.contains(".wav") || attObj.fileName.contains(".amr"))
										{
											playclass="playRecordedMoreActions"
											playIcon='<i class="glyphicon glyphicon-play" title="Click to play" />'
												
										}
										else if(attObj.fileName.contains(".pdf"))
										{
											/*if(is_chrome)
											{
											playclass="openPdf"	
											}
											else
											{*/
											link='pdfviewer.html?'+attObj.fullPath+'';
											target="_blank";
											//}
										}
			
											attachDiv1 = attachDiv1  +'<div class="phenom clearfix" style ="border-bottom:none;">'
											    +'<a id="'+attObj.fullPath+'~'+attObj.userId+'" href="'+link+'" target="'+target+'" class="member '+playclass+'" title="'+attObj.fileName+'" style="width: 48px; height: 40px; left: -20px;">'												
											    +'<span class="ext">'+ext+'</span>'+playIcon
												+'</a>';
											if(listType!='crowd' && userId == attObj.userId){
												attachDiv1 = attachDiv1 +'<p style="margin: 5px 0 0 39px;position: absolute;width:75%;">';
											}else{
												attachDiv1 = attachDiv1 +'<p style="margin: 5px 0 0 39px;position: absolute;width:75%;">';
											}
											if((attObj.fileName).length>70){
											var fileName1=(attObj.fileName).substring(0, 67);
											attObj.fileUploadName=fileName1+"...";
											}
												attachDiv1 = attachDiv1 +'<span class="title ellip" style="display:block;line-height:110%;word-wrap: break-word;white-space:normal;word-break: break-all;">'
													+'<a id="'+attObj.fileName+'" title="'+attObj.fileUploadName+'" style="'+attObj.fileUploadName+'"  class="file-download ~ '+attObj.userId+'" href="javascript:void(0);">'+attObj.fileUploadName+'</a>'
												+'</span>';
												
												if(listType!='crowd' && userId == attObj.userId ){
													attachDiv1 =attachDiv1 +'<br><span style="position: absolute; top: 26px;font-weight: bolder;" class="ellip" id="'+attObj.fileName+'"><a title="Click to remove" class="remove-file-download" href="javascript:void(0);" style="color: #939393;">Remove</a>&nbsp;&nbsp;&nbsp;&nbsp;<a  class="ellip" href="javascript:void(0);" style="color: #939393;">'+attObj.currentTime+'</a></span>'
														+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
														//+'<span id="'+attObj.fileName+'" class="ellip" style="position: absolute; top: 23px;font-weight: bolder;">'
														//	+'<a  title="Click to delete" class="delete-file-download" href="javascript:void(0);" style="color: #939393;" >Delete</a>'
													//+'</span>';
												}
												else
												{
													attachDiv1 =attachDiv1 +'<br><span style="position: absolute; top: 26px;font-weight: bolder;" class="ellip" id="'+attObj.fileName+'"><a  class="ellip" href="javascript:void(0);" style="color: #939393;">'+attObj.currentTime+'</a></span>'
													+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
													//+'<span id="'+attObj.fileName+'" class="ellip" style="position: absolute; top: 23px;font-weight: bolder;">'
													//	+'<a  title="Click to delete" class="delete-file-download" href="javascript:void(0);" style="color: #939393;" >Delete</a>'
												//+'</span>';
												}
											attachDiv1= attachDiv1 +'</p>'
											+'</div>';
										}
										getattachedFilesForNotes(listIdFromList,noteIdFromList,listOwnerFlag,false);
										$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.hide-on-edit').after(attachDiv1);
									}
									
								},error : function() 
								{
									alert("Please try again later");
									console.log("<-------error returned for new ajax request attach file-------> ");
								}
							});
						}
						
						if(comments.length > 0 && comments[0]!=null){
							/*This code used to separate notes comments and crowd comments. by venu*/
							/*url= urlForServer+"note/getComments/"+listType;*/
							
							var cmtsLevel;
							var crowdCmdDefaultDiv='';
							if(listType=='crowd')
								cmtsLevel='P';
							else
								cmtsLevel='I';
							
							url= urlForServer+"note/getComments/"+cmtsLevel+"/"+userId;
							params = encodeURIComponent(comments);
							var comtObj;
							$.ajax({
								headers: { 
								"Mn-Callers" : musicnote,
						    	"Mn-time" :musicnoteIn				
						    	},
								type : 'POST',
								url : url,
								data : params,
								success : function(response) 
								{
									
									var comtData = jQuery.parseJSON(response);
									var linkArray = new Array();

									if(comtData != null && comtData!= ''){
										commentSize=comtData.length;
										for ( var i = 0; i < comtData.length; i++) {
											comtObj = comtData[i];
											var cmtedUserName;
											var cmtInitials;
											var userFirst,userLast;
											if(comtData[i].userId != parseInt(userId)){
												userDetailsJsonObj = activeUserObjectMap[comtData[i].userId] ;
												if(userDetailsJsonObj != undefined && userDetailsJsonObj !=null)
												{
													cmtedUserName = userDetailsJsonObj['userFirstName']+' '+userDetailsJsonObj['userLastName'];
													userFirst=userDetailsJsonObj['userFirstName']; userLast=userDetailsJsonObj['userLastName'];
													cmtInitials=userFirst.substring(0,1).toUpperCase()+userLast.substring(0,1).toUpperCase();
												}
												else
													cmtedUserName = activeUserDeatilsMap[comtData[i].userId];
												
											}else{
												cmtedUserName = userFulName;
												cmtInitials=userInitial;
											}
											
											var condate=new Date(comtObj.cDate);
											var convertedDate="";
											convertedDate=month[condate.getMonth()];
											convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
											var dateTex=convertedDate.split("/");
											var date=dateTex[0]+" "+dateTex[1]+" "+dateTex[2];
											var timestring=comtObj.cTime.split(":");
											var timee=convertTwentyFourHourToTwelveHourTime(timestring[0]+":"+timestring[1]);
												var editedNotes='';
												var comtsFromObj =comtObj.comments;
												var link="";
												var videoDiv="";
												
												while(comtsFromObj.indexOf("youtube.com/watch?v=") != -1){
													comtsFromObj=comtsFromObj.substring(comtsFromObj.indexOf("youtube.com/watch?v=")+20,comtsFromObj.length);
													link = comtsFromObj.substring(0,comtsFromObj.indexOf("\""));
													comtsFromObj = comtsFromObj.substring(comtsFromObj.indexOf("</a>"),comtsFromObj.length);
													var playLink=comtObj.cId.trim()+link.trim();
													videoDiv = videoDiv + '<div id="'+playLink+'"></div>	';
													link=comtObj.cId.trim()+"~"+link.trim();
													if(linkArray.indexOf(link) == -1){
														linkArray.push(link);
													}
													
												}
														if(comtObj.edited == 1){
															
															var eddate=new Date(comtObj.editDate);
															var editDate="";
															editDate=month[eddate.getMonth()];
															editDate=editDate+"/"+eddate.getDate()+"/"+eddate.getFullYear();
															
															//var editDate = new Date(comtObj.editDate).toLocaleFormat("%b/%d/%Y");
															
															var editDateText=editDate.split("/");
															var editDate=editDateText[0]+" "+editDateText[1]+" "+editDateText[2];
															var edittimestring=comtObj.editTime.split(":");
															var edittimee=convertTwentyFourHourToTwelveHourTime(edittimestring[0]+":"+edittimestring[1]);
															editedNotes = ' - edited ' +editDate +' at '+ edittimee ;
														}
														var cmtsWithBr =  comtObj.comments;
														var arrayCmtsBr = cmtsWithBr.split("<br>");
														if(arrayCmtsBr.length > 0 && arrayCmtsBr[0]!="" ){
															cmtsWithBr = '';
															for(var kj =0; kj< arrayCmtsBr.length;kj++ ){
																cmtsWithBr = cmtsWithBr +  arrayCmtsBr[kj].trim() + "<br>\n";   
															}
														}
														if(cmtsWithBr.lastIndexOf("\n") == (cmtsWithBr.trim().length)){
															cmtsWithBr = cmtsWithBr.substring(0, cmtsWithBr.lastIndexOf("\n")-4);
														}
														var cmdDisplayLevel='';
														
														
													if(listType=='crowd' || listType=='music' && fromCrowd=='True')
													{
														cmdDisplayLevel =  comtObj.commLevel;
														if(cmdDisplayLevel=='P/I')
														{
															crowdCmdDefaultDiv = crowdCmdDefaultDiv + '<div class="phenom clearfix"> <div class="creator member js-show-mem-menu">'
															+'<span class="member-initials" title="'+cmtedUserName+'">'+cmtInitials+'</span>'
															+'</div>'
															+'<div class="phenom-desc"> '
																	+'<a class="inline-object js-show-mem-menu" >'+cmtedUserName+'</a> '
																	+'<div class="action-comment">'
																	+'<div class="current-comment" style="overflow:auto;">'
																		+'<p>'+cmtsWithBr +'</p>'
																	+'</div>'
																	//+'<textarea class="js-text">'+comtObj.comments +'</textarea> ' 
																	+'</div>'
															+'</div>'
															+videoDiv
															+'<p class="phenom-meta quiet">'
																+'<span title="'+date+' at '+timee+'" class="date js-hide-on-sending" dt="'+date+' '+timee+'">'+date+' at '+timee+' '+editedNotes+'</span>'
																+'<span id="'+comtObj.cId+'" class="js-hide-on-sending">';
																	if(comtObj.userId == userId){
																		
																		crowdCmdDefaultDiv = crowdCmdDefaultDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;">Edit</a>'
																		+'- <a  class="js-delete-cmt" style="color: #939393;font-weight: normal;">Delete</a>'+'</p>'
																		+'</div>';
																	}
																	//alert(comtObj.cId);
																	
																	else if(listType =='crowd'){
																		
																	//crowdCmdDefaultDiv = crowdCmdDefaultDiv+'- <a  class="js-report-cmt" id="'+comtObj.cId+'" onClick="javascript:reportComment(this.id)" style="color: #939393;font-weight: normal;">Report</a>' +'</p>'
																		crowdCmdDefaultDiv = crowdCmdDefaultDiv+'</p>'
																		+'</div>';
																	}else{
																		crowdCmdDefaultDiv = crowdCmdDefaultDiv+'</p>'
																		+'</div>';
																	}
															
															
														}
														else
														{
															var endTagss='</span>'
																+'</p>'
																+'</div>';
														commentsDiv = commentsDiv + '<div class="phenom clearfix"> <div class="creator member js-show-mem-menu">'
														+'<span class="member-initials" title="'+cmtedUserName+'">'+cmtInitials+'</span>'
														+'</div>'
														+'<div class="phenom-desc"> '
																+'<a class="inline-object js-show-mem-menu" >'+cmtedUserName+'</a> '
																+'<div class="action-comment">'
																+'<div class="current-comment" style="overflow:auto;">'
																	+'<p>'+cmtsWithBr +'</p>'
																+'</div>'
																//+'<textarea class="js-text">'+comtObj.comments +'</textarea> ' 
																+'</div>'
														+'</div>'
														+videoDiv
														+'<p class="phenom-meta quiet">'
															+'<span title="'+date+' at '+timee+'" class="date js-hide-on-sending" dt="'+date+' '+timee+'">'+date+' at '+timee+' '+editedNotes+'</span>'
															+'<span id="'+comtObj.cId+'" class="js-hide-on-sending">';
																if(comtObj.userId == userId){
																commentsDiv = commentsDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;">Edit</a>'
																	+'- <a  class="js-delete-cmt" style="color: #939393;font-weight: normal;">Delete</a>'+endTagss	;
																}
																
																
																		if(listType=='music'){	
																			commentsDiv = commentsDiv+endTagss; 
																		}else if(comtObj.userId != userId){
																			//commentsDiv=commentsDiv+'- <a  class="js-report-cmt" id="'+comtObj.cId+'" onClick="javascript:reportComment(this.id)" style="color: #939393;font-weight: normal;">Report</a>'+endTagss;
																			commentsDiv=commentsDiv+endTagss;
																		}
														}
													}
													else
													{
														var endTagss='</span>'
															+'</p>'
															+'</div>';
													commentsDiv = commentsDiv + '<div class="phenom clearfix"> <div class="creator member js-show-mem-menu">'
													+'<span class="member-initials" title="'+cmtedUserName+'">'+cmtInitials+'</span>'
													+'</div>'
													+'<div class="phenom-desc"> '
															+'<a class="inline-object js-show-mem-menu" >'+cmtedUserName+'</a> '
															+'<div class="action-comment">'
															+'<div class="current-comment" style="overflow:auto;">'
																+'<p>'+cmtsWithBr +'</p>'
															+'</div>'
															//+'<textarea class="js-text">'+comtObj.comments +'</textarea> ' 
															+'</div>'
													+'</div>'
													+videoDiv
													+'<p class="phenom-meta quiet">'
														+'<span title="'+date+' at '+timee+'" class="date js-hide-on-sending" dt="'+date+' '+timee+'">'+date+' at '+timee+' '+editedNotes+'</span>'
														+'<span id="'+comtObj.cId+'" class="js-hide-on-sending">';
															if(comtObj.userId == userId){
															commentsDiv = commentsDiv +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;">Edit</a>'
																+'- <a  class="js-delete-cmt" style="color: #939393;font-weight: normal;">Delete</a>'+endTagss	;
															}
															
															
																	if(listType=='music'){
																		if(comtObj.userId != userId){
																			if(listOwnerUserId!=userId){
																				//commented for hide note report function
																				//commentsDiv=commentsDiv+'- <a  class="js-note-report-cmt" id="'+comtObj.cId+'" onClick="javascript:reportComment(this.id)" style="color: #939393;font-weight: normal;">Report</a>'+endTagss;
																				
																				commentsDiv=commentsDiv+endTagss;
																			}else{
																				commentsDiv=commentsDiv+'- <a  class="js-delete-cmt" style="color: #939393;font-weight: normal;">Delete</a>'+endTagss;
																				//commentsDiv=commentsDiv+endTagss;
																			}
																		}else{
																			commentsDiv = commentsDiv+endTagss; 
																		}
																	}else if(comtObj.userId != userId){
																		if(listOwnerUserId!=userId){
																			//commented for hide the memo report function
																			//commentsDiv=commentsDiv+'- <a  class="js-note-report-cmt" id="'+comtObj.cId+'" onClick="javascript:reportComment(this.id)" style="color: #939393;font-weight: normal;">Report</a>'+endTagss;
																			commentsDiv=commentsDiv+endTagss;
																		}else{
																			commentsDiv=commentsDiv+'- <a  class="js-delete-cmt" style="color: #939393;font-weight: normal;">Delete</a>'+endTagss;
																			//commentsDiv=commentsDiv+endTagss;
																		}
																	}
													}
										}
									}
									
									if(listType=='crowd' || listType=='music' && fromCrowd=='True')
									{
										$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.window-module').children('.list-actions').append(crowdCmdDefaultDiv);
										
										if(crowdCmdDefaultDiv!='')
										{
											$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.window-module').children('.list-actions').after('<div class="phenom-bold"></div>');
										}
										$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.window-module').children('.list-actions-crd').append(commentsDiv);
									}
									else
									{
										$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.window-module').children('.list-actions').append(commentsDiv);
									}
										
									if(linkArray.length > 0 && linkArray[0]!=null){
										for(var i =0; i< linkArray.length;i++ ){
											var playId= linkArray[i].split("~");
											onYouTubeIframeAPIReady(playId[1].trim(),playId[0].trim()+playId[1].trim());
										}
									}
								},
								error : function() 
								{
									alert("Please try again later");
									console.log("<-------error returned for new ajax request note comments -------> ");
								}
							});
						
						}
						//end
						if(listType!='schedule')
						{
						
						if(vote.length > 0 && vote[0] != null){
							viteWithUser=vote.length +' votes';
							if(vote.indexOf(userId) >= 0){
								viteWithUser=viteWithUser +'(with you)';
								voteDivClass = 'js-un-vote';
							}	
						}else{
							viteWithUser='0 votes';
						}
						if(description!=null && description.trim()!=''){
							var array = new Array();
							array = description.split("<br>");
								if(array.length > 0 && array[0]!=null ){
									description ='';
									for(var i =0; i< array.length;i++ ){
										description = description +  array[i] + "<br>\n";   
									}
								} 
							desc='<div class="badge voted glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
						}
						//if(listOwnerFlag){
							if(data['dueDate'] != "")
								dueDate = '';//loadDueDateContent(data['dueDate'],data['dueTime']);
						//}
				
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').find('.js-composer').remove();							
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').children('.js-header').remove();
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.clearfix').remove();
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.hide-on-edit').remove();
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.clearfix').remove();
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.js-scheduleEdit').remove();
				var title='<div class="js-header"><div class="window-header clearfix" ><span class="window-header-icon glyphicon glyphicon-th-large" style="margin-top:27px;"></span><h3 id="'+noteOwnerId+'"';
				if(!listOwnerFlag){
					title= title+' class="" title = "Posted by '+listOwnerFullName+'">'+noteNameFromList+'</h3> </div>';
				}else{
					title= title+' class="js-edit-notes-name" style="cursor: pointer;" title="Click to edit">'+noteNameFromList+'</h3> </div>';
				}
				/*if(listOwnerFlag){
					title = title +'<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><a title="Click to edit the title" class="js-edit-notes-name" style="color: #939393;font-weight: normal;">Edit</a></span></div>';
				}*/			
				title = title +'</div>';	
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').append(title);
				
				var voteView="";
				if(listType=='crowd'){
					voteView='<a class="badge vote-badge clickable voted glyphicon glyphicon-sm btn dropdown-toggle " data-toggle="dropdown"><span class="glyphicon glyphicon-thumbs-up"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+viteWithUser+'</span></a>'
					+'<ul class="dropdown-menu" style="max-height: 400px; overflow: auto; min-width: 180px; max-width: 240;">'+createListForVote()+'</ul>';
				}else{
					voteView="";
				}
				
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').append('<div class="badges clearfix btn-group-vote"> '
						/*We didn't separate notes comments and crowd comments use below line other wise we know need to use these lines. by Venu*/
						/*starting */
						+voteView
						+'<div class="badge voted glyphicon glyphicon-sm cmts" title="This note has '+comments.length +' comment(s)."><span class="glyphicon glyphicon-comment"></span><span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;'+comments.length+'</span></div>'
						+desc
						+attachDiv
						//+tagss
						+dueDate
						/*Ending*/
						
				+'</div>'); 
				
				/*This code used to separate notes comments and crowd comments by Venu*/
				/*if(listType=='crowd'){
					$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.btn-group-vote').append('<a class="badge vote-badge clickable voted glyphicon glyphicon-sm btn dropdown-toggle " data-toggle="dropdown"><span class="glyphicon glyphicon-thumbs-up"></span><span class="badge-text">&nbsp;&nbsp;'+viteWithUser+'</span></a>'
							+'<ul class="dropdown-menu" style="max-height: 400px; overflow: hidden; min-width: 180px; max-width: 240;">'+createListForVote()+'</ul>');
				}
				
				url= urlForServer+"note/getCommentsCount/"+listType;
				var par='{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'"}';
				cparams = encodeURIComponent(par);
				var comtObj;
				$.ajax({		
				headers: { 
		    	"Ajax-Call" : userTokens,
		    	"Ajax-Time" :userLoginTime				
		    	},
					type : 'POST',
					url : url,
					data : cparams,
					success : function(response){
						var data = jQuery.parseJSON(response);
						if(data != null && data!= ''){
								$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.btn-group-vote').append('<div class="badge voted glyphicon glyphicon-sm cmts" title="This note has '+data.len+' comment(s)."><span class="glyphicon glyphicon-comment"></span><span class="badge-text">&nbsp;&nbsp;'+data.len+'</span></div>');
						
						}
					},
					error : function() 
					{
						alert("Please try again later");
					}
				});
					
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.btn-group-vote').append(desc);
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.btn-group-vote').append(attachDiv);
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.btn-group-vote').append(dueDate);*/
				
				
				var descEditDivForUserAlone ='';
				if((listOwnerFlag || noteLevel == 'edit') &&  listType!='crowd'){
					if(description!= ''){
						descEditDivForUserAlone	= '&nbsp;&nbsp;<div class="action-comment" title="Note Description"><div class="current-comment" style="overflow:auto;"><p class="p-decs-test-align">' +description  +'</p></div></div>'
							+'<p><span class="col-md-1"></span>&nbsp;&nbsp;<span><a  title="Click to Edit the Description" class="js-note-edit-desc1" style="color: #939393;font-weight: normal;">Edit</a></span></p>';
					}else{
						descEditDivForUserAlone ='&nbsp;&nbsp;<textarea class="new-comment-input js-note-edit-desc  fontStyle" placeholder="Note description" style="width: 80%; position: relative; top: 10px;" title="Click to edit description"></textarea>';
					}
				}else{
					if(description!= ''){
						descEditDivForUserAlone	= '&nbsp;&nbsp;<div class="action-comment" title="Note Description"><div class="current-comment" style="overflow:auto;"><p>' +description  +'</p></div></div>';
					}else{
						descEditDivForUserAlone	='';
					}
				}
				//description 
				$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').append('<div class="hide-on-edit">'
						+descEditDivForUserAlone
					 +'</div>');
				
				//activity
					var comtDivInActivity = '';
					if(listOwnerFlag || listType =='crowd'|| noteLevel == 'edit' ){
						comtDivInActivity = '<div class="new-comment">'
							+'<div class="member"><span class="member-initials" title="'+userFulName+'">'+userInitial+'</span></div>'
							+'<textarea class="new-comment-input js-cmt-placeholder fontStyle" placeholder="Add comments, links and text "></textarea>'
						+'</div>'
						;
					}
					
					if(listType=='crowd' || listType=='music' && fromCrowd=='True')
					{
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').append('<div class="window-module clearfix" style="">'
								+'<div class="window-module-title no-divider"><span class="window-module-title-icon glyphicon glyphicon-tasks" style="margin-top:22px;"></span><h4>Activity</h4></div>'
								+'<div class="list-actions">'
								+'</div>'
								+comtDivInActivity
								+'<div class="list-actions-crd">'
								+'</div>');
					}
					else
					{
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').append('<div class="window-module clearfix" style="">'
								+'<div class="window-module-title no-divider"><span class="window-module-title-icon glyphicon glyphicon-tasks" style="margin-top:22px;"></span><h4>Activity</h4></div>'
								+comtDivInActivity
								+'<div class="list-actions">'
								+'</div>'
								+'</div>');
					}
				
					}
						else
						{
							
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').find('.js-composer').remove();	
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').children('.js-header').remove();
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.clearfix').remove();
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.hide-on-edit').remove();
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.clearfix').remove();
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.js-scheduleEdit').remove();
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').children().remove();
							var title='<div class="js-header"><div class="window-header clearfix" ><span class="window-header-icon glyphicon glyphicon-th-large" style="margin-top:27px;"></span><h3 ';

							if(listOwnerFlag){
								title = title +'class="js-edit-notes-name" style="cursor : pointer;" title="Click to edit">'+noteNameFromList+'</h3> </div>';
								//title = title +'<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><a title="Click to edit the title" class="js-edit-notes-name" style="color: #939393;font-weight: normal;">Edit</a></span></div>';
							}else{
								title = title +' title = "Posted by '+listOwnerFullName+'" >'+noteNameFromList+'</h3> </div>';
							}
							title = title +'</div>';	
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').append(title);
										//$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').append('<div class="window-header clearfix"><span class="window-header-icon glyphicon glyphicon-th-large"></span><h3 class="js-edit-notes-name">'+noteNameFromList+'</h3> </div>');
							
							var eveStartDate='';
							var eveEndDate='';
							var eventEndDate='';
							var evesDate=data['startDate'];
							var	eveeDate=data['endDate'];
							
							var repeatEvent='';
							
							
								
							
							//alert(eveeDate+"   "+eveeDate);
							var allDay="";
							if(evesDate.indexOf(":")!=-1){
								
							//	evesDate=evesDate.substring(1,evesDate.length);
								eveStartDate=convertNumberDateFormatIntoStringFormat(evesDate,false);
								allDay='No';
							}else{
								
							//	evesDate=evesDate.substring(1,evesDate.length);
								allDay='Yes';
								eveStartDate=convertNumberDateFormatIntoStringFormat(evesDate,true);
							}
							if(eveeDate.indexOf(":")!=-1){
								//eveeDate=eveeDate.substring(1,eveeDate.length);
								allDay='No';
								eveEndDate=convertNumberDateFormatIntoStringFormat(eveeDate,false);
							}else{
								//eveeDate=eveeDate.substring(1,eveeDate.length);
								allDay='Yes';
								eveEndDate=convertNumberDateFormatIntoStringFormat(eveeDate,true);
							}
							
							
							if(data['repeatEvent']!=null && data['repeatEvent']!='')
							{
								repeatEvent=data['repeatEvent'];
							}
							else
							{
								repeatEvent='-';
							}
							var eventsEnd='';
							if((data['eventEndDate']!=null && data['eventEndDate']!='')&&(data['eventEndDate']!='undefined/NaN/NaN' || data['eventEndDate']!='undefined/undefined/undefined'))
							{
								eventsEnd=data['eventEndDate'];
								eventEndDate=convertNumberDateFormatIntoStringFormat(eventsEnd,true);
							}
							else
							{
								eventEndDate='-';
							}
							var scheduleCondents='<div class="clearfix"><div class="row" >'+
							'<div class="col-md-1" style="padding-top:10px;"></div>'+
							'<div class="col-md-3" style="font-family: Helvetica Neue;padding-top:10px;">Description  </div><div class="col-md-1"style="padding-top:10px;">:</div>'+
							'<div class="col-md-7" style="max-height: 70px;overflow: auto;font-family: Helvetica Neue;padding-top:10px; " >'+description+'</div>'+
							'</div> '+
							'<div class="row">'+
							'<div class="col-md-1"style="padding-top:10px;"></div>'+
							'<div class="col-md-3" style="font-family: Helvetica Neue;padding-top:10px;">Location </div><div class="col-md-1"style="padding-top:10px;">:</div>'+
							'<div class="col-md-7" style="font-family: Helvetica Neue;padding-top:10px;">'+location+'</div>'+
							'</div>'+
							'<div class="row">'+
							'<div class="col-md-1"style="padding-top:10px;"></div>'+
							'<div class="col-md-3" style="font-family: Helvetica Neue;padding-top:10px;">All Day </div><div class="col-md-1"style="padding-top:10px;">:</div>'+
							'<div class="col-md-7" style="font-family: Helvetica Neue;padding-top:10px;">'+allDay+'</div>'+
							'</div>'+
							'<div class="row">'+
							'<div class="col-md-1"style="padding-top:10px;"></div>'+
							'<div class="col-md-3" style="font-family: Helvetica Neue;padding-top:10px;">Start </div><div class="col-md-1"style="padding-top:10px;">:</div>'+
							'<div class="col-md-7" style="font-family: Helvetica Neue;padding-top:10px;" >'+eveStartDate+'</div>'+
							'</div>'+
							'<div class="row" id="endrow">'+
							'<div class="col-md-1"style="padding-top:10px;"></div>'+
							'<div class="col-md-3" style="font-family: Helvetica Neue;padding-top:10px;">End </div><div class="col-md-1"style="padding-top:10px;">:</div>'+
							'<div class="col-md-7" style="font-family: Helvetica Neue;padding-top:10px;">'+eveEndDate+'</div>'+
							'</div>'+
							'<div class="row">'+
							'<div class="col-md-1"style="padding-top:10px;"></div>'+
							'<div class="col-md-3" style="font-family: Helvetica Neue;padding-top:10px;">Repeat Event </div><div class="col-md-1"style="padding-top:10px;">:</div>'+
							'<div class="col-md-7" style="font-family: Helvetica Neue;padding-top:10px;" >'+repeatEvent+'</div>'+
							'</div>'+
							
							'<div class="row">'+
							'<div class="col-md-1"style="padding-top:10px;"></div>'+
							'<div class="col-md-3" style="font-family: Helvetica Neue;padding-top:10px;">Repeat Until </div><div class="col-md-1"style="padding-top:10px;">:</div>'+
							'<div class="col-md-7" style="font-family: Helvetica Neue;padding-top:10px;" >'+eventEndDate+'</div>'+
							'</div></div>';
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').append(scheduleCondents);
							if(listOwnerFlag){
								
								$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').append('<a class="button-link js-scheduleEdit" style="text-decoration: none;margin-top:30px" href="javascript:void(0);"><span class="glyphicon glyphicon-edit"></span>&nbsp;Edit</a>');
							}
						}
					}
					
					if(listType!='schedule' && listType!='crowd'){
						//if(noteOwnerId == userId){
						$("#tagFilterDiv").show();
						//}else{
						//$("#tagFilterDiv").hide();
						//}
					}else{
						$("#tagFilterDiv").hide();
					}
				
				if(listType!='schedule'){
					// side bar 
					// members
					$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').children().remove();
					var sideActionDiv  = '';
					var shareAllContact = '';
					var crowdvalues ='';
					if(listType == 'music' || listType == 'bill'){
						if(noteOwnerId == userId){
							if(noteSharedAllContact==0){
								shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">With All Contacts</a></li>';
							}else if(noteSharedAllContact==1){
								shareAllContact='<li><a class="js-members-delcontact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
							}
							
							sideActionDiv ='<div class="window-module clearfix">'
								
								+'<div class="card-detail-members hide clearfix"></div>'
								+'<div class="btn-group" style="display: block;">'
								+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Add or remove members of the Book from the Note" style="min-width:130px;text-align: left;">'
								+'<span class="glyphicon glyphicon-user"></span> &nbsp;Share Note&nbsp;<span class="caret" style="right: 10%;bottom:40%; position: absolute;"></span>'
								+'</a>'
								+'<ul class="dropdown-menu pull-right" style="margin-top: 40px;margin-right: -25px;">'
								+'<li><a class="js-members" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Individual(s)</a></li>'
								+'<li><a class="js-members-group" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Group(s)</a></li>'
								//+'<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Share Contacts</a></li>'
								+shareAllContact
								+'</ul>'
								+'</div>'
								+'</div>';
							
						}else if(noteLevel == 'edit'){
								if(noteSharedAllContactMembersIds.indexOf(userId) != -1){
									shareAllContact='<li><a class="js-members-delcontact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
								}else{
									shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">With All Contacts</a></li>';
								}
							
								sideActionDiv ='<div class="window-module clearfix">'
									
									+'<div class="card-detail-members hide clearfix"></div>'
									+'<div class="btn-group" style="display: block;">'
									+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Add or remove members of the Book from the Note" style="min-width:130px;text-align: left;">'
									+'<span class="glyphicon glyphicon-user"></span> &nbsp;Share Note&nbsp;<span class="caret" style="right: 10%;bottom:40%; position: absolute;"></span>'
									+'</a>'
									+'<ul class="dropdown-menu pull-right" style="margin-top: 40px;margin-right: -25px;">'
									+'<li><a class="js-members" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Individual</a></li>'
									+'<li><a class="js-members-group" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Group(s)</a></li>'
									//+'<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Share Contacts</a></li>'
									+shareAllContact
									+'</ul>'
									+'</div>'
									+'</div>';
						}
					}
					
					//vote
					//This if Condition used for vote functionality need to hide in notes and Memo page. (by Venu)
					if(listType!=null && listType!='music' && listType!='bill'){
						voteArchieve= voteArchieve+'<div class="clearfix">';
					
						voteArchieve=	voteArchieve+'<div class="voted">'
							+'<a class="'+voteDivClass+' button-link is-on side" style="text-decoration: none;min-width:138px;">'
								+'<span class="glyphicon glyphicon-thumbs-up"></span>'
							+'&nbsp;Vote';
							if(vote.indexOf(userId) >= 0){
								voteArchieve = voteArchieve +'<span class="on">'
									+'<span class="glyphicon glyphicon-ok"></span>'
								+'</span>';
								}

							voteArchieve = voteArchieve +'</a>'
						+'</div>';
							voteArchieve= voteArchieve+'<div class="clearfix">';
							
							voteArchieve=	voteArchieve+'<a class="button-link js-crowd-report-note" title="report" style="text-decoration: none;min-width:138px;">'
									+'<span class="glyphicon glyphicon-warning-sign"></span>'
								+'&nbsp;Report';
								voteArchieve = voteArchieve +'</a>'
							+'</div>';
							
							
					}
						//archive
							if(listType != 'crowd' && !sharedHideDelete){
								voteArchieve = voteArchieve + '<a class="button-link js-archive" title="Delete the note from the book" style="text-decoration: none;min-width:130px;">'
								+'<span class="glyphicon glyphicon-remove"></span>'
								+'&nbsp;Delete'
								+'</a>';
							}
							
						if(parentDivName == "notes" ){
							voteArchieve = voteArchieve + '</div>';
						}
					//Actions	
					if(listOwnerFlag ||listType == 'crowd' || noteLevel == 'edit'){
						sideActionDiv = sideActionDiv +'<div class="window-module other-actions clearfix">'
						
						
						
							sideActionDiv = sideActionDiv +'<div class="clearfix">';
						//public or private 
							var access = "";
							var icon_class='';
							if(data['access'] != "" ){
								if(data['access'].trim() == "private"){
									access = "Crowd"; 
									icon_class="glyphicon glyphicon-globe";
								}else{
									access = "Crowd";
									icon_class ="glyphicon glyphicon-globe";
									
								}
							}
							
							var copyAll='';
							
							if(data['copyToAllContact']==1)
								copyAll='<li><a class="js-copied-note-contacts" style="text-decoration: none;" href="javascript:void(0);">Added To Contacts</a> </li>';
							else
								copyAll='<li><a class="js-copy-note-contacts" style="text-decoration: none;" href="javascript:void(0);">With All Contacts</a> </li>';
							
							if(listType == 'music'){
								if(crowdShareMsg==false)
								{
								if(noteOwnerId == userId || noteLevel == 'edit'){
									if(data['access'].trim() == "private"){
										if(publicShareWarnMsgFlag){
											crowdvalues = '<a class="button-link js-access-change true" title="Note in '+data['access'].trim()+' view, change to '+access+'" style="text-decoration: none;min-width:130px;">'
												+'<span class="'+icon_class+'"></span>'
												+'&nbsp;'+access
												+'</a>';
										}else{
											crowdvalues = '<a class="button-link js-access-change false" title="Note in '+data['access'].trim()+' view, change to '+access+'" style="text-decoration: none;min-width:130px;">'
											+'<span class="'+icon_class+'"></span>'
											+'&nbsp;'+access
											+'</a>';
										}
									}else{
										crowdvalues = '<a class="button-link js-access-changed" title="Note already shared with crowd" style="text-decoration: none;min-width:130px;">'
											+'<span class="'+icon_class+'"></span>'
											+'&nbsp;'+access
											+'</a>';
									}
								}
							}
							else
							{
								crowdvalues = '<a class="button-link js-no-access-changed" title="Crowd Sharing Blocked" style="text-decoration: none;min-width:130px;">'
									+'<font color="#cecece"><span class="'+icon_class+'"></span>'
									+'&nbsp;'+access
									+'</font></a>';
							}//crowdshare end
								
							}
							if(listType!='crowd'){
								if(listOwnerFlag || noteLevel == 'edit'){
								// due date
									sideActionDiv = sideActionDiv + '<a class="button-link js-due-date" title="Add a due date for the note and get notification when it reached duedate" style="text-decoration: none;min-width:130px;">'
									+'<span class="glyphicon glyphicon-calendar"></span>'
									+'&nbsp;Due Date'
									+'</a>';
									
								// copy book
									
									sideActionDiv = sideActionDiv /*+'<a class="button-link js-copy-note" title="Add the note to other book" style="text-decoration: none;">'
									+'<span class=" glyphicon glyphicon-book"></span>'
									+'&nbsp;Add To Book'
									+'</a>'*/
									
									var books=new Array();
									var book ='';
									if(data!=null && data!=''){
										book=data['selectedBookId'];
									}
									
									sideActionDiv=sideActionDiv+'<div class="btn-group" style="min-width:130px;max-width: 250px;display: block;">'
										+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Add the book to the note" style="min-width:130px;text-align: left;">'
										+'<span class=" glyphicon glyphicon-book"></span> &nbsp;Notebook&nbsp;<span class="caret" style="right: 10%;bottom:40%; position: absolute;"></span>'
										+'</a>'
										+'<ul class="dropdown-menu pull-right dropDownStopProgress"  style="margin-top: 40px;" id="bookAddingUiId">';
									var dividerFlag=false;
									$("#notebookId > option").each(function() {
										if(this.value != 'note'){
											if($(this).hasClass(userId) /*&& $(this).hasClass('false')*/ && this.value!=listIdFromList){
												sideActionDiv=sideActionDiv+'<li class="noteToBook" id="'+this.value+'" title="' + this.text +'"><a id="'+this.value+'">';
												var bookIds='';
												if(book!= undefined && book.length!=0){
													bookIds=book[0];
													books=bookIds.split(",");
												}
												
												for(var j=0;j<books.length;j++){
													if(this.value.trim()==books[j].trim()){
														sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-ok pull-right"></i>';
													}
												}
												//sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-edit bookIconEdit" id="'+this.value+'" title="edit"></i> &nbsp; <i class="glyphicon glyphicon-trash bookIconDelete" title="delete" id="'+this.value+'"></i> &nbsp;<i class="noteToBook" id="'+this.value+'" title="' + this.text +'">' + this.text +'</i></a></li>';
												//sideActionDiv=sideActionDiv+'<i class="noteToBook" id="'+this.value+'" title="' + this.text +'">' + this.text +'</i></a></li>';
												sideActionDiv=sideActionDiv+'<i>' + this.text +'</i></a></li>';
												dividerFlag=true;
											}
											
										}
									});
									if(dividerFlag){
									sideActionDiv=sideActionDiv+'<li class="divider"></li>';
									}
									sideActionDiv=sideActionDiv+'<li class="bookAddClass"><a class="createBook" id="createBook"><span class="glyphicon glyphicon-plus createBook"></span>&nbsp;&nbsp; Create New Book</a></li>'
									+'</ul>'
									+'</div>';
												
								//move
									/*if(listOwnerFlag)
									{
										sideActionDiv = sideActionDiv +'<a class="button-link js-move" title="Move the note to other book" style="text-decoration: none;">'
										+'<span class="glyphicon glyphicon-move"></span>'
										+'&nbsp;Move'
										+'</a>';
									}*/
							
								//copy
									sideActionDiv = sideActionDiv +'<div class="btn-group" style="min-width:130px;max-width: 250px;display: block;">'
									+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Add the note to other users" style="margin-bottom:6px;text-align: left;min-width:130px;">'
									+'<span class=" glyphicon glyphicon-share"></span>&nbsp;Share Copy&nbsp;<span class="caret" style="right: 10%;bottom:40%; position: absolute;"></span>'
									+'</a>'
									+'<ul class="dropdown-menu pull-right" style="margin-top: 80px;">'
									+'<li><a class="js-copy-note" style="text-decoration: none;" href="javascript:void(0);">Make Copy</a> </li>'
									+'<li><a class="js-copy-note-indiviual" role="button" style="text-decoration: none;" href="#" data-toggle="modal">With Individual(s)</a> </li>'
									+'<li><a class="js-copy-note-group" role="button" style="text-decoration: none;" href="#" data-toggle="modal">With Group(s)</a> </li>'
									+'<li><a class="js-copy-note-contacts" style="text-decoration: none;" href="javascript:void(0);">With All Contacts</a> </li>'
									//+copyAll
									+'</ul>'
									+'</div>';
								}
							}
						//attach
						
						if(listType!='crowd' && (listOwnerFlag || noteLevel == 'edit') ){
				 		sideActionDiv = sideActionDiv +'<a class="button-link js-attach" title="Attach files" style="text-decoration: none;min-width:130px;">'
								+'<span class="glyphicon glyphicon-upload"></span>'
								+'&nbsp;Attach Files'
							+'</a>';
						}else{
							if(listType!='crowd' && !listOwnerFlag && sharedNoteFlag && noteLevel == 'edit'){
								sideActionDiv = sideActionDiv +'<a class="button-link js-attach" title="Attach files" style="text-decoration: none;min-width:130px;">'
									+'<span class="glyphicon glyphicon-upload"></span>'
									+'&nbsp;Attach Files'
								+'</a>';
							}
						}
						if(!listOwnerFlag){
							if(data['publicUser']!=userId){
							if(listType=='crowd' &&  publicNoteCopyFlag == 'false'){
								// copy book
								sideActionDiv = sideActionDiv +'<a class="button-link js-crowd-copy-note" title="Add the note to my book" style="text-decoration: none;min-width:138px;">'
									+'<span class=" glyphicon glyphicon-book"></span>'
									+'&nbsp;Add To Notebook'
									+'</a>';
							}else if(listType=='crowd'  && publicNoteCopyFlag == 'true'){
								// copy book
								sideActionDiv = sideActionDiv +'<a class="button-link js-crowd-copied-note" title="Added the note to my book" style="text-decoration: none;min-width:138px;">'
									+'<span class=" glyphicon glyphicon-book"></span>'
									+'&nbsp;Add To Notebook'
									+'</a>';
							}
							}
						}
						
						//$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').children('.window-module').remove();
						
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').find('.window-module').each(function( index ){
							$(this).remove();
						});
				
						if(listType!='crowd' && (listOwnerFlag || noteLevel == 'edit')){
							//Tag
							var tags=new Array();
							var tag ='';
							var remainderWithParam='';
							if(data!=null && data!=''){
								tag=data['tag'];
								remainderWithParam=data['remainders'];
								if(tag!=null && tag!=''){
									tag=tag.replace("[","");
									tag=tag.replace("]","");
									//var n=tag.indexOf(",");
									
										tags=tag.split(',');
									
								}
								var url = urlForServer+"note/getTags";
								var params = '{"userId":"'+userId+'"}';
								params = encodeURIComponent(params);
					
								$.ajax({
									headers: { 
									"Mn-Callers" : musicnote,
							    	"Mn-time" :musicnoteIn				
							    	},
									type: 'POST',
									url : url,
									cache: false,
									contentType: "application/json; charset=utf-8",
									data:params, 
									dataType: "json",
									success : function(response){/*
										sideActionDiv=sideActionDiv+'<div class="btn-group" style="min-width:105px;max-width: 250px;display: block;">'
										+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Tag the note" style="min-width:105px;font-size: 13px;text-align: left;">'
										+'<span class="glyphicon glyphicon-tags"></span> &nbsp;Tag&nbsp;<span class="caret" style="right: 10%; position: absolute;"></span>'
										+'</a>'
										+'<ul class="dropdown-menu pull-right dropDownStopProgress">';

										if(response!=null && response!=''){
											for ( var i = 0; i < response.length; i++) 
											{
												sideActionDiv=sideActionDiv+'<li><a id="'+response[i].tagId+'">';

												for(var j=0;j<tags.length;j++){
													if(response[i].tagId.trim()==tags[j].trim()){
														sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-ok pull-right"></i>';
													}
												}
												sideActionDiv=sideActionDiv+'<i class="glyphicon glyphicon-edit tagIconEdit" id="'+response[i].tagId+'" title="edit"></i> &nbsp; <i class="glyphicon glyphicon-trash tagIconDelete" title="delete" id="'+response[i].tagId+'"></i> &nbsp;<i class="noteToTag" id="'+response[i].tagId+'" title="' + response[i].tagName +'">' + response[i].tagName +'</i></a></li>';
											}
											sideActionDiv=sideActionDiv+'<li class="divider"></li>';
										}
										sideActionDiv=sideActionDiv+'<li><a class="createTag" id="createTag"><i class="glyphicon glyphicon-plus createTag"></i>&nbsp;&nbsp; Create New Tag</a></li>'
										+'</ul>'
										+'</div>';
										*/
										// remainders
					
										if(remainderWithParam.indexOf("[") != -1){
											remainderWithParam = remainderWithParam.substring(remainderWithParam.indexOf("[")+1,remainderWithParam.length);
										}
										if(remainderWithParam.indexOf("]") != -1){
											remainderWithParam = remainderWithParam.substring(0,remainderWithParam.indexOf("]"));
										}
										if(remainderWithParam.indexOf(",") != -1){
											remainder = remainderWithParam.split(',');
										}else{
											if(remainderWithParam.trim() != ''){
												remainder = remainderWithParam.split(',');
											}
										}
										
										if(remainder.length > 0 && remainder[0]!=''){ 
											var remObj  = remindersMap[parseInt(remainder[0].trim())]; 
											var styleAttr = "";
											var span="";
											if(remObj!= undefined){
												if(remObj.inactiveRemainderUserId!=null){
													var inactiveRemainderUserIds =new Array();
													var inactiveRemainderUserIdsWithParam=remObj.inactiveRemainderUserId;
													if(inactiveRemainderUserIdsWithParam.indexOf("[") != -1){
														inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(inactiveRemainderUserIdsWithParam.indexOf("[")+1,inactiveRemainderUserIdsWithParam.length);
													}
													if(inactiveRemainderUserIdsWithParam.indexOf("]") != -1){
														inactiveRemainderUserIdsWithParam = inactiveRemainderUserIdsWithParam.substring(0,inactiveRemainderUserIdsWithParam.indexOf("]"));
													}
													if(inactiveRemainderUserIdsWithParam.indexOf(",") != -1){
														inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
													}else{
														if(inactiveRemainderUserIdsWithParam.trim() != ''){
															inactiveRemainderUserIds = inactiveRemainderUserIdsWithParam.split(',');
														}
													}
													if(inactiveRemainderUserIds.length > 0 && inactiveRemainderUserIds[0] != ''){
														if(inactiveRemainderUserIds.indexOf(userId) == -1){
															//active
															styleAttr ='text-decoration: none;background:#DFDD0A;min-width:130px;';
																//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
															span='style="color:black;"';
														}else{
															//inactive
															span='style="color:white;"';
															styleAttr ='text-decoration: none;background:#A888A3 ;min-width:130px;';
																//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
														}
													}else{
														//active
														span='style="color:black;"';
														styleAttr ='text-decoration: none;background:#DFDD0A ;min-width:130px;';//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
													}
												}	
											}else{
												//inactive
												span='style="color:white;"';
												styleAttr ='text-decoration: none;background:#DFDD0A ;min-width:130px;';//background: linear-gradient(to bottom, #A888A3 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;';
											}
											sideActionDiv = sideActionDiv 
											+'<a id="'+remainder[0]+'" class="button-link remaindIconEdit" title="Reminder" style="'+styleAttr+'">'
											+'<span class="glyphicon glyphicon-time" '+span+'></span>&nbsp;Reminder&nbsp;'
											+'</a>'
											+'</div>';
										}else{
											sideActionDiv = sideActionDiv 
											+'<a class="button-link addRemainders" title="Reminder" style="text-decoration: none;min-width:130px;">'
											+'<span class="glyphicon glyphicon-time"></span>&nbsp;Reminder&nbsp;'
											+'</a>'
											+'</div>';
										}
										
										sideActionDiv=sideActionDiv+crowdvalues+voteArchieve;
													
										$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').append(sideActionDiv);	

									},
									error: function(e) {
										alert("Please try again later");
									}
								});
							}
						}else{
							sideActionDiv=sideActionDiv+'</div>'+crowdvalues+voteArchieve;
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').append(sideActionDiv);
						}
					}	
				}else{
					if(listOwnerFlag){
						var shareAllContact='';
						
						if(listOwnerUserId == userId){
							if(noteSharedAllContact==0){
								shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">With All Contacts</a></li>';
							}else if(noteSharedAllContact==1){
								shareAllContact='<li><a class="js-members-delcontact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
							}
						}else{
							if(noteSharedAllContactMembersIds.indexOf(userId) != -1){
								shareAllContact='<li><a class="js-members-delcontact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
							}else{
								shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">With All Contacts</a></li>';
							}
						}
						
						sideActionDiv ='<div class="window-module clearfix">'
							
							+'<div class="card-detail-members hide clearfix"></div>'
							+'<div class="btn-group">'
							+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Add or remove members of the Book from the Note" style="min-width:130px;">'
							+'<span class="glyphicon glyphicon-user"></span>Share Event&nbsp;&nbsp;<span class="caret"></span>'
							+'</a>'
							+'<ul class="dropdown-menu pull-right" style="">'
							+'<li><a class="js-members" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Individual</a></li>'
							+'<li><a class="js-members-group" role="button" href="#" data-toggle="modal" style="text-decoration: none;">With Group</a></li>'
							//+'<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Share Contacts</a></li>'
							+shareAllContact
							+'</ul>'
							+'</div>'
							
							+'</div>';
						
						
						sideActionDiv = sideActionDiv +'<a class="button-link js-archive" title="Delete the note from the book" style="text-decoration: none;min-width:130px;">'
						+'<span class="glyphicon glyphicon-remove"></span>'
						+'&nbsp;Delete'
					+'</a>';
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').children('.window-module').remove();
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').append(sideActionDiv);
					}
					else  // event accept ,decline ,later
					{
						if(data['sharingdetails']!=null && data['sharingdetails']!="")
						{
							sideActionDiv='';
							if(data['sharingdetails']=="P")
							{
								sideActionDiv ='<div class="window-module clearfix">'
									+'<h3>Share</h3>'
									+'<div class="card-detail-members hide clearfix"></div>'
									+'<div class="btn-group">'
									+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Accept or Decline Events." style="min-width:130px;">'
									+'<span class="glyphicon glyphicon-user"></span> &nbsp;Request&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span>'
									+'</a>'
									+'<ul class="dropdown-menu pull-right" style="">'
									+'<li><a class="js-acceptEvent" role="button" href="#" data-toggle="modal" style="text-decoration: none;">Accept</a></li>'
									+'<li><a class="js-declineEvent" role="button" href="#" data-toggle="modal" style="text-decoration: none;">Decline</a></li>'
									//+'<li><a class="js-laterEvent" role="button" href="#" data-toggle="modal" style="text-decoration: none;">Later</a></li>'
									+'</ul>'
									+'</div>'
									
									+'</div>';	
								
							}
							
							sideActionDiv = sideActionDiv +'<a class="button-link js-archive" title="Delete the note from the book" style="text-decoration: none;min-width:130px;">'
							+'<span class="glyphicon glyphicon-remove"></span>'
							+'&nbsp;Delete'
						+'</a>';
							
//							else if(data['sharingdetails']=="A")
//							{
//								sideActionDiv ='<div class="window-module clearfix">'
//									+'<h3>Share</h3>'
//									+'<div class="card-detail-members hide clearfix"></div>'
//									+'<div class="btn-group">'
//									+'<a class="btn dropdown-toggle button-link" data-toggle="dropdown" title="Accept or Decline Events." style="min-width:105px;">'
//									+'<span class="glyphicon glyphicon-user"></span> &nbsp;Accepted&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
//									+'</a></div></div>';	
//							}
							
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').children('.window-module').remove();
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-3').append(sideActionDiv);
						}
						
						
					}
				}
				/*if(listType=="schedule" && !listOwnerFlag && data['sharingdetails']!="P")
					$("#moreActions").modal('hide');
				else*/
				if(listType!='schedule'){
					$("#moreActions").attr("style",'top: 5%; min-width: 650px; min-height: 480px; display: block;');
				}else{
					$("#moreActions").attr("style",'top: 10%; min-width: 560px; min-height: 280px; display: block;');
				}
				

				$("#moreActions").modal('toggle');	
				if(!listOwnerFlag){
					$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-header').children('.window-header').attr("title",'Posted by '+listOwnerFullName);
				}
			},
			error: function(e) {
				alert("Please try again later");
			}
    
		});
		
		
		setTimeout(function(){
			//alert(tagsArray.length);
		$('#tagFilterUl').tagit( {
			tagSource :tagFilter,
			triggerKeys : [ 'comma','enter','tab' ],
			placeholderText: 'Tags',
			select: true,
			initialTags:tagsArray,
		    tagsChanged:function (a, b) {
			 var newTagName=a.trim();
			 var tagId=mapTagNotes[newTagName.toLowerCase()];
		         if(b=='added' ){
		        	 if(tagId==undefined){
		        		 var url = urlForServer+"note/createTag";
			        		if(newTagName!=null && newTagName!=""){
			                var criteria=null;
			        		var params = '{"userId":'+userId+',"tagName":"'+newTagName+'","listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","listType":"'+listType+'"}';
			        		params = encodeURIComponent(params);
			        		$.ajax({
			        			headers: { 
			        			"Mn-Callers" : musicnote,
			        	    	"Mn-time" :musicnoteIn				
			    		    	},
			        			type: 'POST',
			        			url : url,
			        			cache: false,
			        			contentType: "application/json; charset=utf-8",
			                
			        			data:params, 
			        			dataType: "json",
			        			success: function(transport) {
			        				if(transport['status']=="0"){
			        					if(transport['tag'].contains(',')){
			        						var orignalCount=(newTagName.split(",").length - 1);
			        						var duplicateCount=(transport['tag'].split(",").length - 1);
			        						if(orignalCount == duplicateCount)
			        							$("#errormsg").text("!!! "+transport['tag']+" are already exists").css({"color":"red","font-weight": "bold"});
			        						else
			        							$("#errormsg").text("!!! "+transport['tag']+" are already exists, Other Tags are added successfully.. ").css({"color":"red","font-weight": "bold"});
			        					}else{
			        						var orignalCount=(newTagName.split(",").length - 1);
			        						var duplicateCount=(transport['tag'].split(",").length - 1);
			        						if(orignalCount == duplicateCount)
			        							$("#errormsg").text("!!! "+transport['tag']+" is already exists").css({"color":"red","font-weight": "bold"});
			        						else
			        							$("#errormsg").text("!!! "+transport['tag']+" is already exists, Other Tags are added successfully.. ").css({"color":"red","font-weight": "bold"});
			        						
			        					}
			        					loadTags();
			        					reLoadList();
			        				}else{
			        					loadTags();
			        					reLoadList();
			        				}

			                	}
			        		});
			        		if(listType!='schedule'){
			        		loadTagsForCrowd();
			        		}
			        		}else{
			        			$("#tagWarningId").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			        			$("#tagWarningId").text("Field cannot be blank").css({"color":"red"});
			        		}
			        		return false;
		        	 }else{

		        			var url = urlForServer+"note/noteToTag";
		        			var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+listType+'","tagId":"'+tagId+'"}';
		        		    params = encodeURIComponent(params);
		        		    var count;
		        		    $.ajax({
		        		    	headers: { 
		        		    	"Mn-Callers" : musicnote,
		        		    	"Mn-time" :musicnoteIn					
		        		    	},
		        		    	type: 'POST',
		        		    	url : url,
		        		    	cache: false,
		        		    	contentType: "application/json; charset=utf-8",
		        		    	data:params, 
		        		    	dataType: "json",
		        		    	success : function(response){
		        		    		if(response['status']=='A'){
		        		    			loadTags();
			        					reLoadList();
		        		    		}else if(response['status']=='R') {
		        		    			loadTags();
			        					reLoadList();
		        		    		}	
		        		    	},
		        	            error: function(e) {
		        	                alert("Please try again later");
		        	            }
		        		    });
		        	 }
		        	} else if(b=='popped'){
		    			var url = urlForServer+"note/noteToTag";
		    			var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+listType+'","tagId":"'+tagId+'"}';
		    		    params = encodeURIComponent(params);
		    		    var count;
		    		    $.ajax({
		    		    	headers: { 
		    		    	"Mn-Callers" : musicnote,
		    		    	"Mn-time" :musicnoteIn					
		    		    	},
		    		    	type: 'POST',
		    		    	url : url,
		    		    	cache: false,
		    		    	contentType: "application/json; charset=utf-8",
		    		    	data:params, 
		    		    	dataType: "json",
		    		    	success : function(response){
		    		    		if(response['status']=='A'){
		    		    			loadTags();
		        					reLoadList();
		    		    		}else if(response['status']=='R') {
		    		    			loadTags();
		        					reLoadList();
		    		    		}	
		    		    	},
		    	            error: function(e) {
		    	                alert("Please try again later");
		    	            }
		    		    });
		        	}
		}
		});
		},700);
	}
	
	$('#moreActions').on('click','.ext',function(){
		
	 	var fileName = $(this).parent().attr('id');
	 	var fileName1=fileName.substring(fileName.lastIndexOf('.')+1);
	 	var fileName2=fileName1.split('~');
	 	var finalExtName=fileName2[0];
	 	
	 	var array = fileName.split("~");
		var lessonName =array[0];
		var fileOwnerId=array[1];
	 	
	 	if(finalExtName=='pdf'){
	 		var viewHref=uploadUrl+uploadUrl2+lessonName;
	 		//$(this).parent().parent().after('<object data="'+viewHref+'" type="application/pdf" width="100%" height="100%">');
	 	}
	 	else if(finalExtName=='jpg' || finalExtName=='jpeg' || finalExtName=='gif' || finalExtName=='png')
	 	{
	 		$('#moreActions').children('.modal-dialog').children('.modal-content').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
			$(this).parent().parent().after('<div class="more-player"><li><img height="400" src="'+uploadUrl+lessonName+'" style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid" width="400" id="'+lessonName+'" alt="'+lessonName+'" />');
	 	}
	});
	
	//play audio or video with in morActions
	$('#moreActions').on('click','.playRecordedMoreActions',function(){

		var tempClass = $(this).attr('id');
		var array = tempClass.split("~");
		var lessonName =array[0];
		var fileOwnerId=array[1];
		
		if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
		{
			
			$('#moreActions').children('.modal-dialog').children('.modal-content').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
			$(this).parent().after('<div class="more-player"><li><embed width="260" height="282" src="'+uploadUrl+lessonName+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/></li></div>');
		}
		else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
		{
			$('#moreActions').children('.modal-dialog').children('.modal-content').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
			
			$(this).parent().after('<div class="more-player"><li><embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> </li></div>');
		}
	

	});
	
	// open pdf for chorme browser only
	$('#moreActions').on('click','.openPdf',function()
		{
		var tempClass = $(this).attr('id');
		var array = tempClass.split("~");
		var lessonName =array[0];
		var fileOwnerId=array[1];
		
	
		
		$('#moreActions').children('.modal-body').children('#content').children('.clearfix').children('.list-actions1').children('.more-player').remove();
		$(this).parent().after('<div class="more-player"><li><iframe src="'+uploadUrl+lessonName+'" style="width: 10px; height: 10px;" type="application/x-google-chrome-print-preview-pdf" frameborder="0" scrolling="no" name="extensions" extensionspage="https://chrome.google.com/webstore/detail/oemmndcbldboiebfnladdacbdfmadadm/">'
			+' </iframe><p>To view PDFs please ensure your browser has a PDF viewer (PDF.js) installed.</p></li></div>');
		
	});
	
	
	//remainder
	$('#notes').on('click','.addRemainders',function(){
	
		if((userEmail!='null')&&(userEmail!='')){
			$('#ReminderMailCheckbox').attr('style','display:block;');
		}else{
			$('#ReminderMailCheckbox').attr('style','display:none;');
		}
		$('input:checkbox[name=sendReminderMailCheckbox]').attr('checked',false);
		$('#addRemain').removeAttr("disabled");
		$('#addRemainderModal').children('.modal-dialog').children('.modal-content').children('.modalBody').children('.form-group').children('.row').children('.col-md-2').find('.error-msg').remove();
		$('#remainderName').val("");
		$("#eventStartDate1").val("");
		$('#moreActions').modal('hide');
		$('#addRemainderModal').modal('toggle');
	});
	//file download in crowd
	$('#moreActions').on('click','.file-download',function(){
		var tempClass = $(this).attr('class');
		var array = tempClass.split("~");
		var temp=$(this).attr('style');
		getFileFor($(this).attr('id'),array[1],temp);
	});
	$('#moreActions').on('click','.delete-file-download',function(){
		attachId = $(this);
		$('.delete-Attach-warn').text('Do you want to delete this file and remove from this note?');
		$('#moreActions').modal('hide');
		$('#deleteAttachedModal').modal('toggle');
	});
	
/*	$('#recentActivity').on('click','.js-note-recnt',function(e){
		var ids = $(this).attr('id');
		var idArray = ids.split("~");
		var cls = $(this).attr('class');
		var pageType = cls.split(" ");
//		e.preventDefault();
		listType=pageType[1];
		listIdFromList= idArray[0];
		noteIdFromList= idArray[1];
		parentDivName= "notes";
		app.navigate(''+pageType[1]+'', {
			trigger : true
		});
		setTimeout(function(){
			$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').each(function( index ){
	    		moreActionBasedId = $(this).children('.todo_description');
	    		listType=pageType[1];
				listIdFromList= idArray[0];
				noteIdFromList= idArray[1];
				parentDivName= "notes";
				alert(listIdFromList);
				checkOwnerOfList();
	    	});
		},2000);
			
		
	});*/
	
	$('#moreActions').on('click','.remove-file-download',function(){
		//if(listOwnerFlag){
			var fileName = $(this).parent().attr('id');
			if(selectedFiles.indexOf(fileName) != -1){
				selectedFiles.splice(selectedFiles.indexOf(fileName),1); 

				var filesToBeSaved= selectedFiles;
			
				var noteId=noteIdFromList;
				var listId=listIdFromList;
				
				var params ='';
				if(filesToBeSaved.length > 0 && filesToBeSaved[0] != null){
					 params = encodeURIComponent(filesToBeSaved);
				}else{
					params = '  ';
					params = encodeURIComponent(params);
				}
				var url = urlForServer+"note/attachFile/"+listId+"/"+noteId+"/"+userId;
				
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type : 'POST',
					url : url,
					data : params,
					success : function(response) 
					{
						listId=$('#notebookId').val();
						if(listId =='note'){
							listId =listIdFromList;
						}
						$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').children('.attach-badge').remove();
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').children().remove();

						if(filesToBeSaved.length > 0 && filesToBeSaved[0] != null){
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').attr("title",'This note has '+filesToBeSaved.length+' attachment(s).');
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').append('<span class="glyphicon glyphicon-download-alt"></span>'
											+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+filesToBeSaved.length+'</span>');
							attach = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+filesToBeSaved.length+' attachment(s).">'
											+'<span class="glyphicon glyphicon-download-alt"></span>'
											+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+filesToBeSaved.length+'</span>'     
											+'</div>';
							var descId = $("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.decs');
							if($(descId).attr('class') == undefined){
								var cmtLen = $("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').children('.cmts').find('.badge-text').text();
								if(vote.length > 0 ){
									if(cmtLen > 0){
										$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.cmts').after(attach);
									}else{
										$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.vote-badge').after(attach);
									}
								}else{
									if(cmtLen > 0){
										$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.cmts').after(attach);
									}else{
										$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').prepend(attach);
											
									}
								}
							}else{
								$(descId).after(attach);
							}
						}else{
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').attr("title",'This note has no attachment(s).');
							$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').append('<span class="glyphicon glyphicon-download-alt"></span>'
											+'<span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;0</span>');
						}
						
						$("#moreActions").modal('hide');
						$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text("  File Detached successfully");
						setTimeout(function(){ 
							$('#moveCopyWarning').modal('hide');
							checkOwnerOfList();
						},1000);
						
					},
					error : function() 
					{
						alert("Please try again later");
						console.log("<-------error returned for new ajax request attach file-------> ");
					}
				});
			}
		//}	
	});
	
	
	
	// event edit
	var eventEdit=null;
	$('#moreActions').on('click','.js-scheduleEdit',function()
			{
		if((userEmail!='null')&&(userEmail!='')){
			$('#editEventMailCheckbox').attr('style','display:block;');
		}else{
			$('#editEventMailCheckbox').attr('style','display:none;');
		}
		$("#sendEditEventMailCheckbox").attr('checked', false); 
		
		eventEdit=1;
		var url = urlForServer+"note/getEvents/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
				type: 'POST',
				url : url,
				cache: false,
				success : function(responseText){
			
					var data = jQuery.parseJSON(responseText);
			
						if(data != "")
						{
							 var currentDate = new Date(data.startDate);
							    var day = currentDate.getDate();
							    var month = currentDate.getMonth() + 1;
							    var year = currentDate.getFullYear();
							    var hour = currentDate.getHours();
							    var minitues =currentDate.getMinutes();
							    
								if(day<10)
								{
									day="0"+day;
								}
								if(hour<10)
								{
									hour="0"+hour;
								}
								if(minitues<10)
								{
									minitues="0"+minitues;
								}
								
								 var ecurrentDate = new Date(data.endDate);
								    var eday = ecurrentDate.getDate();
								    var emonth = ecurrentDate.getMonth() + 1;
								    var eyear = ecurrentDate.getFullYear();
								    var ehour = ecurrentDate.getHours();
								    var eminitues =ecurrentDate.getMinutes();
								
								    if(eday<10)
									{
										eday="0"+eday;
									}
								    if(ehour<10)
									{
								    	ehour="0"+ehour;
									}
									if(eminitues<10)
									{
										eminitues="0"+eminitues;
									}
								
							$('#editscheduleNoteName').val(data.eventName);
							$('#editscheduleNoteDetail').val(data.description);
							$('#editScheduleLocation').val(data.location);
							$('#editalldayevent').attr("checked",false);
							$('#editeventBetweenDate').attr("style", "display:none");
							$('#editscheduleNoteNames').attr("style", "display:none");
							$('#editscheduleNoteDetails').attr("style", "display:none");
							$('#editeventStartTimes').attr("style", "display:none");
							$('#editeventEndTimes').attr("style", "display:none");
							
							
							
							
							   
							if(data.alldayevent=="true")
							{
								
								currentDate=month + "/" + day + "/" + year;
								ecurrentDate=emonth + "/" + eday + "/" + eyear;
								
								
								$('#editalldayevent').attr("checked",true);
								$('#editstartDate').show();
								$('#editendDate').show();
								$('#editstartTime').hide();
								$('#editendTime').hide();
								$('#editeventStartDate').val(convertNumberDateFormatIntoStringFormat(currentDate,true));
								$('#editeventEndDate').val(convertNumberDateFormatIntoStringFormat(ecurrentDate,true));
								$('#editeventStartTime').val("");
								$('#editeventEndTime').val("");
							}
							else
							{
								currentDate=month + "/" + day + "/" + year + " " +hour + ":" +minitues;
								ecurrentDate=emonth + "/" + eday + "/" + eyear + " " +ehour + ":" +eminitues;
								
								$('#editstartDate').hide();
								$('#editendDate').hide();
								$('#editstartTime').show();
								$('#editendTime').show();
								$('#editeventStartTime').val(convertNumberDateFormatIntoStringFormat(currentDate,false));
								$('#editeventEndTime').val(convertNumberDateFormatIntoStringFormat(ecurrentDate,false));
								$('#editeventStartDate').val("");
								$('#editeventEndDate').val("");
							}
							
							if(data.repeatEvent!=null && data.repeatEvent!='' && data.repeatEvent!='--Select Repeat--')
							{
							
								$("#editEventRepeatEndDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
								$('#editRepeatEventId').val(data.repeatEvent);

								if((data.eventEndDate!=null && data.eventEndDate!='')&&(data.eventEndDate!='undefined/NaN/NaN' || data.eventEndDate!='undefined/undefined/undefined'))
							    {
									
							    	    var ecurrentEndDate = new Date(data.eventEndDate);
									    var eday = ecurrentEndDate.getDate();
									    var emonth = ecurrentEndDate.getMonth() + 1;
									    var eyear = ecurrentEndDate.getFullYear();
									    var ehour = ecurrentEndDate.getHours();
									    var eminitues =ecurrentEndDate.getMinutes();
									    ecurrentEndDate=emonth + "/" + eday + "/" + eyear;
									    var test=convertNumberDateFormatIntoStringFormat(ecurrentEndDate,true);
									    $('#editEventRepeatDate').val(test);
							    }
							    else
							    {
							    	 $('#editEventRepeatDate').val('');
							    }
								
							}
							else
							{
								$('#editRepeatEventId').val('--Select Repeat--');
								$("#editEventRepeatEndDate").attr("style", "display:none");
								$("#editEventRepeatDate").val('');
								
							}
							
							$('#editScheduleModel').modal('toggle');
							$('#moreActions').modal('hide');
						}
					
				},
				 error: function(e) {
	                alert("Please try again later");
	            }
		
		});
	});
	
	$("#cancelRemain").click(function() {
		$(this).parent().parent().children('.modalBody').children().find('.error-msg').remove();
		$('#addRemainderModal').modal('hide');
		checkOwnerOfList();
	});
	
	$("#cancleUpdateRemaind").click(function() {
		$(this).parent().parent().children('.modalBody').children().find('.error-msg').remove();
		$('#updateRemainderModal').modal('hide');
		checkOwnerOfList();
	});
	
	$("#addRemain").click(function() {
		var noteName = $('#moreActions').children('.modal-header').children('.js-header').children('.window-header').find("h3").text();
		var url =urlForServer+"note/addRemainder/";
		var newRemainderName=$('#remainderName').val();

		$(this).parent().parent().children('.modalBody').children().find('.error-msg').remove();
		if(newRemainderName!=null && newRemainderName.trim()!="" && $('#eventStartDate1').val()!= null && $('#eventStartDate1').val()!=""){
			while(newRemainderName.indexOf("\n") != -1){
				newRemainderName = newRemainderName.replace("\n","<br>");
			}
			
			var isMailSend = $('#sendReminderMailCheckbox').attr('checked')?true:false;

			var dateTime=$("#eventStartDate1").val();
			
			// to save 24 hour format
			var eveStartTime=convertStringDateFormatIntoNumberFormat(dateTime,false);
			eveStartTime=eveStartTime.split(' ');
			eveStartTime=eveStartTime[1];
			
			dateTime=dateTime.split(' ');
			var newEventdate=dateTime[0];
			var newEventTime=dateTime[1];
			
			var am=dateTime[2];
			newEventTime = newEventTime+" "+am;
			var	calEventTime=newEventTime;
			
			calEventTime=calEventTime.toLowerCase();
			if(calEventTime.indexOf("a") != -1){
				if(calEventTime.indexOf(" ") == -1){
					var str=new Array();
					str=calEventTime.split('am');
					calEventTime=str[0]+" "+"am";
				}
			}else if(calEventTime.indexOf("p") != -1){
				if(calEventTime.indexOf(" ") == -1){
					var str=new Array();
					str=calEventTime.split('pm');
					calEventTime=str[0]+" "+"pm";
				}
			}
			var tempEventDate = newEventdate;
			if(is_Ie|| is_safari){
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
			}
			var selectedDateTime=new Date(newEventdate+' '+calEventTime);
			var todayDateTime=new Date();
			
			var entedDate = new Date(newEventdate);
			var selectedDate=new Date();
			selectedDate=(selectedDate.getMonth()+1)+'/'+selectedDate.getDate()+'/'+selectedDate.getFullYear();
			if(selectedDateTime > todayDateTime)
			{
				$("#addRemain").attr("disabled",true);
				var month = (entedDate.getMonth()+1);
				var date = entedDate.getDate();
				if(month <10 && month > 0){
					month = "0"+month;
				}
				if(date <10 && date > 0){
					date = "0"+date;
				}
				var entedDateInFormat = month +'/'+date+'/'+entedDate.getFullYear();
				var params='{"rId":"", userId:"'+userId+'", listId:"'+listIdFromList+'", noteId:"'+noteIdFromList+'","rName":"'+newRemainderName.trim()+'",cDate:"'+selectedDate+'", eventDate:"'+entedDateInFormat+'","eventTime":"'+eveStartTime+'","sendMail":'+isMailSend+',"noteName":"'+noteName.trim()+'", status:"A", edited:"0", editDate:""}';
				var tempListId=$('#notebookId').val();
				if(tempListId =='note'){
					tempListId =listIdFromList;
				}
				params = encodeURIComponent(params);
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
			    	},
					type: 'POST',
					url : url,
					cache: false,
					contentType: "application/json; charset=utf-8",
					data:params, 
					dataType: "json",
					success: function(transport) {		
						if(transport!= null && transport != "0"){
							if(newRemainderName.length > 36){
								newRemainderName = newRemainderName.substring(0,35)+'...';
							}
							var reminderDivContent='<div class="badge reminders glyphicon glyphicon-sm " title="'+newRemainderName+' on '+tempEventDate+' at '+newEventTime+'"'
													+' style="text-decoration: none;background: #DFDD0A;">'
													//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;">'
													+'<i class="glyphicon glyphicon-time"></i>'
													+'<span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;'+tempEventDate +'</span>'     
												+'</div>';
							$("#notes").children('#'+tempListId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').append(reminderDivContent);					
							
							fetchRemainders();
							$('#addRemain').removeAttr("disabled");
							$('#addRemainderModal').modal('hide');
							checkOwnerOfList();
						}
					},
					error: function(e) {
						alert("Please try again later");
					}
				});
		
			}else{
				$(this).parent().parent().parent().parent().children('.modal-dialog').children('.modal-content').children('.modalBody').children('.form-group').children('.row').children('.col-md-2').children('.modalName-text').after('<label class="control-label error-msg" style="color:red;width:200px">unable to add reminder for past</label>');
			}
		}else{
			$(this).parent().parent().parent().parent().children('.modal-dialog').children('.modal-content').children('.modalBody').children('.form-group').children('.row').children('.col-md-2').children('.modalName-text').after('<label class="control-label error-msg" style="color:red;width:150px">Field cannot be blank</label>');
		}
		//so that the page doesn't post back	
		return false;
	});
	var raminId;
	$("#editRemaind").click(function() {
		var noteName = $('#moreActions').children('.modal-dialog').children('.modal-content').children('.modal-header').children('.js-header').children('.window-header').find("h3").text();
		var clickId = $(this);
		$(this).parent().parent().children('.modalBody').children().find('.error-msg').remove();
		var url = urlForServer+"note/updateRemainder/"+listIdFromList+"/"+noteIdFromList+"/"+raminId+"/"+noteName;
		var updateRemindName=$('#updateRemainderName').val();
		if(updateRemindName!=null && updateRemindName.trim()!="" && $('#updateStartDate').val()!= null && $('#updateStartDate').val()!=''){

			var isMailSend = $('#sendEditReminderMailCheckbox').attr('checked')?true:false;

			var dateTime=$("#updateStartDate").val();
			
			// tosave 24 hr format
			var eveStartTime=convertStringDateFormatIntoNumberFormat(dateTime,false);
			eveStartTime=eveStartTime.split(' ');
			eveStartTime=eveStartTime[1];
			
			dateTime=dateTime.split(' ');
			var updateEventDate=dateTime[0];
			var newEventTime=dateTime[1];
			var am=dateTime[2];
			newEventTime = newEventTime+" "+am;
			var	calEventTime=newEventTime;

			calEventTime=calEventTime.toLowerCase();
			if(calEventTime.indexOf("a") != -1){
				if(calEventTime.indexOf(" ") == -1){
					var str=new Array();
					str=calEventTime.split('am');
					calEventTime=str[0]+" "+"am";
				}
			}else if(calEventTime.indexOf("p") != -1){
				if(calEventTime.indexOf(" ") == -1){
					var str=new Array();
					str=calEventTime.split('pm');
					calEventTime=str[0]+" "+"pm";
				}
			}
			var tempEventDate = updateEventDate;
			if(is_Ie|| is_safari){
				var splitedDate =  updateEventDate.split('/');
				updateEventDate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
			}
			var selectedDateTime=new Date(updateEventDate+' '+calEventTime);
			var todayDateTime=new Date();

			var entedDate = new Date(updateEventDate);
			var selectedDate=new Date();
			selectedDate=(selectedDate.getMonth()+1)+'/'+selectedDate.getDate()+'/'+selectedDate.getFullYear();

			if(selectedDateTime > todayDateTime)
			{
				$("#editRemaind").attr("disabled",true);
				var month = (entedDate.getMonth()+1);
				var date = entedDate.getDate();
				if(month <10 && month > 0){
					month = "0"+month;
				}
				if(date <10 && date > 0){
					date = "0"+date;
				}
				
				var updtdDateInFormat = month +'/'+date+'/'+entedDate.getFullYear();
				var params='{"rId":"'+raminId+'","rName":"'+updateRemindName.trim()+'", "eventDate":"'+updtdDateInFormat+'","eventTime":"'+eveStartTime+'","sendMail":'+isMailSend+' ,"userId":"'+userId+'", "editDate":"'+selectedDate+'", "edited":"1"}';
				params = encodeURIComponent(params);
				
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type: 'POST',
					url : url,
					cache: false,
					contentType: "application/json; charset=utf-8",
					data:params, 
					dataType: "json",
					success : function(response){
						if(response!= null && response!='0'){
							fetchRemainders();
							if(updateRemindName.length > 36){
								updateRemindName = updateRemindName.substring(0,35)+'...';
							}
							var tempListId=$('#notebookId').val();
							if(tempListId =='note'){
								tempListId =listIdFromList;
							}
							var reminderDivContent='<div class="badge reminders glyphicon glyphicon-sm " title="'+updateRemindName+' on '+tempEventDate+' at '+newEventTime+'"'
										+' style="text-decoration: none;background: #DFDD0A;">'
										//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;
													+'<i class="glyphicon glyphicon-time"></i>'
													+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tempEventDate +'</span>'     
												+'</div>';
							$("#notes").children('#'+tempListId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.reminders').remove();
							$("#notes").children('#'+tempListId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').append(reminderDivContent);					
							
							$("#editRemaind").removeAttr("disabled");
							$('#updateRemainderModal').modal('hide');
							checkOwnerOfList();
						}
					},
					error: function(e) {
						alert("Please try again later");
					}

				});
			
			}else{
				$(clickId).parent().parent().children('.modalBody').children().children('.modalName-text').after('<label class="control-label error-msg" style="color:red;">unable to add reminder for past</label>');
			}
		}
		else
		{
			$(clickId).parent().parent().children('.modalBody').children().children('.modalName-text').after('<label class="control-label error-msg" style="color:red;">Field cannot be blank</label>');
		}
		//so that the page doesn't post back
		return false;
    });
	
	$('#notes').on('click','.remaindIconDelete',function(){
			//raminId=$(this).attr('id');
			$('#updateRemainderModal').modal('hide');
			$('#deleteRemainderModal').modal('toggle');
	});
	
	$("#deleteRemain").click(function() {
		var url = urlForServer+"note/deleteRemainders/"+listIdFromList+"/"+noteIdFromList+"/"+raminId;
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	dataType: "json",
	    	success : function(response){
				if(response!= null && response!='0'){
					var tempListId=$('#notebookId').val();
					if(tempListId =='note'){
						tempListId =listIdFromList;
					}
					$("#notes").children('#'+tempListId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.reminders').remove();
					fetchRemainders();
					$('#deleteRemainderModal').modal('hide');
					checkOwnerOfList();
				}
	    	},
	    	error: function(e) {
	    		alert("Please try again later");
	    	}
	    });
	    //so that the page doesn't post back
	    return false;
    });
	
	
	$('#notes').on('click','.remaindIconEdit',function(){
		if((userEmail!='null')&&(userEmail!='')){
			$('#EditReminderMailCheckbox').attr('style','display:block;');
		}else{
			$('#EditReminderMailCheckbox').attr('style','display:none;');
		}
			raminId = $(this).attr('id');
			var url = urlForServer+"note/getRemainder/"+userId;
											
			var params = encodeURIComponent(raminId);		
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type: 'POST',
				url : url,
				cache: false,
				contentType: "application/json; charset=utf-8",
				data:params, 
				dataType: "json",
				success : function(responseText){
					var remData = responseText;
					if(remData != null && remData!= ''){
						var selectedDate=new Date(remData[0].eventDate);
					
						var condate=new Date(selectedDate);
						var convertedDate="";
						convertedDate=month[condate.getMonth()];
						convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();

						$('#moreActions').modal('hide');
						$('#updateRemainderModal').modal('toggle');
						$('#updateRemainderModal').children('.modalBody').children().find('.error-msg').remove();
						$("#editRemaind").removeAttr("disabled");
						$('#updateRemainderName').val(remData[0].rName);
						$('#updateStartDate').val(convertedDate+" "+remData[0].eventTime );
						
						if(remData[0].sendMail){
							$('input:checkbox[name=sendEditReminderMailCheckbox]').attr('checked',true);
						}else{
							$('input:checkbox[name=sendEditReminderMailCheckbox]').attr('checked',false);
						}
					}
				},
				error: function(e) {
					alert("Please try again later");
				}
			});
			
	});
	//end remainders
	
	//Book Select Function
	$('#notes').on('click','.noteToBook',function(){
		var id=$(this);
		var bookId=$(this).attr('id');
		var bookName=$(this).attr('title');
		var url = urlForServer+"note/noteToBook";
		var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+listType+'","bookId":"'+bookId+'"}';
	    params = encodeURIComponent(params);
	    var count;
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    		if(response['status']=='I'){
	    			$("#bookAddingUiId > li").each(function() {
	    				$(this).find('a').each(function() {
	    					if($(this).attr('id') == bookId){
	    						$(this).find('i').each(function() {
	    							if($(this).hasClass( "glyphicon-ok" ))
	    								$(this).remove();
	    						});
	    					}
	    				});
	 				});
	    		
	    		}else if(response['status']=='A') {
	    			$("#bookAddingUiId > li").each(function() {
	    				$(this).find('a').each(function() {
	    					if($(this).attr("id") == bookId){
	    						$(this).prepend('<i class="glyphicon glyphicon-ok pull-right"></i>');
	    					}
	    				});
	 				});
	    		}	
	    	},
            error: function(e) {
                alert("Please try again later");
            }
	    });
	});
	
	// Tag Select Function
	$('#notes').on('click','.noteToTag',function(){
		var id=$(this);
		var tagId=$(this).attr('id');
		var tagName=$(this).attr('title');
		var url = urlForServer+"note/noteToTag";
		var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+listType+'","tagId":"'+tagId+'"}';
	    params = encodeURIComponent(params);
	    var count;
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	count  = $(id).parent().parent().parent().parent().parent().parent().parent().parent().find('#content').children('.badges').children('.tagss').find('.badge-text').text().trim();
	    		if(response['status']=='A'){
	    			$(id).parent().prepend('<i class="glyphicon glyphicon-ok pull-right"></i>');
	    			var tagNames=$(id).parent().parent().parent().parent().parent().parent().parent().parent().find('#content').children('.badges').children('.tagss').attr('title');
	    			
	    			 $(id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.tagss').remove();
	    			 $(id).parent().parent().parent().parent().parent().parent().parent().parent().find('#content').children('.badges').children('.tagss').remove();
	    			
	    			if(tagNames.length>0)
	    				tagNames+='['+tagName+']';
	    			else
	    				tagNames='['+tagName+']';
	    			
	    			 
	    			//count = parseInt(count) +1;
	    			 
	    			 var tagss ='<div class="badge glyphicon glyphicon-sm tagss" title="'+tagNames+'">'
							+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span></div>';
	    			 var tagDiv='<div class="badge glyphicon glyphicon-sm tagss" title="'+tagNames+'">'
							+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span></div>';
	    			 $(id).parent().parent().parent().parent().parent().parent().parent().parent().find('#content').children('.badges').append(tagss);
	    			// if(count!=0)
	    				 $(id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').append(tagDiv);
	    		
	    		}else if(response['status']=='R') {
	    			$(id).parent().find('.glyphicon-ok').each(function( index ){
	    				var tagNames=$(id).parent().parent().parent().parent().parent().parent().parent().parent().find('#content').children('.badges').children('.tagss').attr('title');
	    			 if(tagNames!=''){
	    				 tagNames=tagNames.replace("["+tagName+"]","");
	    			 }
	    			 
	    			  //Remove already exist value
	    			 $(id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.tagss').remove();
	    			 $(id).parent().parent().parent().parent().parent().parent().parent().parent().find('#content').children('.badges').children('.tagss').remove();
	    			//Add new Value
	    			  var tagss ='<div class="badge glyphicon glyphicon-sm tagss" title="'+tagNames+'">'
							+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span></div>';
	    			  var tagDiv='<div class="badge glyphicon glyphicon-sm tagss" title="'+tagNames+'">'
						+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span></div>';
	    			 $(id).parent().parent().parent().parent().parent().parent().parent().parent().find('#content').children('.badges').append(tagss);
	    			// if(count!=0)
	    				 $(id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').append(tagDiv);
						$(this).remove();
					});
	    		}	
	    	},
            error: function(e) {
                alert("Please try again later");
            }
        
	    });
	
	});
	
	$('#notes').on('click','.createTag',function(){
		$('#tagName').val("");
		$("#errormsg").text("");
		$('#moreActions').modal('hide');
		$("#tagWarningId").text("");
		$('#createTagModal').modal('show');
	});
	
	
	$('#notes').on('click','.tagIconEdit',function(){
			tagId=$(this).attr('id');
			$("#errormsg1").text('');
			$('#moreActions').modal('hide');
			$('#updateTagModal').modal('toggle');
			$("#tagWarningIdEdit").text("");
			$('#updateTagName').val($(this).parent().text().trim());

	});
	

	$('#notes').on('click','.tagIconDelete',function(){
			tagId=$(this).attr('id');
			$('#moreActions').modal('hide');
			$('#deleteTagModal').modal('toggle');

	});
	
	
	$("#addTag").click(function() {
		var url = urlForServer+"note/createTag";
		var newTagName=$('#tagName').val();
		if(newTagName!=null && newTagName!=""){
        var criteria=null;
		var params = '{"userId":'+userId+',"tagName":"'+newTagName+'","listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","listType":"'+listType+'"}';
		params = encodeURIComponent(params);

		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type: 'POST',
			url : url,
			cache: false,
			contentType: "application/json; charset=utf-8",
        
			data:params, 
			dataType: "json",
			success: function(transport) {
				loginUserId=userId;// need to pass dynamically
				groupId="1";
				criteria="loginUserId";
		
				if(transport['status']=="0"){
					if(transport['tag'].contains(',')){
						var orignalCount=(newTagName.split(",").length - 1);
						var duplicateCount=(transport['tag'].split(",").length - 1);
						if(orignalCount == duplicateCount)
							$("#errormsg").text("!!! "+transport['tag']+" are already exists").css({"color":"red","font-weight": "bold"});
						else
							$("#errormsg").text("!!! "+transport['tag']+" are already exists, Other Tags are added successfully.. ").css({"color":"red","font-weight": "bold"});
					}else{
						var orignalCount=(newTagName.split(",").length - 1);
						var duplicateCount=(transport['tag'].split(",").length - 1);
						if(orignalCount == duplicateCount)
							$("#errormsg").text("!!! "+transport['tag']+" is already exists").css({"color":"red","font-weight": "bold"});
						else
							$("#errormsg").text("!!! "+transport['tag']+" is already exists, Other Tags are added successfully.. ").css({"color":"red","font-weight": "bold"});
						
					}
					loadTags();
					reLoadList();
				}else{
					//$('#createTagModal').modal('hide');
					loadTags();
					reLoadList();
					
					setTimeout(function(){checkOwnerOfList();$('#createTagModal').modal('hide');},500);
					//checkOwnerOfList();
					//createMoreActionModal();
				}

        	}
		});
    
		}else{
			$("#tagWarningId").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#tagWarningId").text("Field cannot be blank").css({"color":"red"});
		}
		//so that the page doesn't post back
		return false;
	});
	
	
	$("#editTag").click(function() {
		var url = urlForServer+"note/updateTag/"+listIdFromList+"/"+noteIdFromList;
		var updateTagName=$('#updateTagName').val();
		if(updateTagName!=null && updateTagName!=""){
			var params = '{"userId":"'+userId+'","tagName":"'+updateTagName+'","tagId":'+tagId+'}';
			
			params = encodeURIComponent(params);
			
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type: 'POST',
				url : url,
				cache: false,
				contentType: "application/json; charset=utf-8",
				data:params, 
				dataType: "json",
				success : function(response){
				if(response == '200')
				{
					$("#errormsg1").text("");
					loadTags();
					reLoadList();
					
					setTimeout(function(){checkOwnerOfList();$('#updateTagModal').modal('hide');},500);
					
					//createMoreActionModal();
				}else
				{
					$("#errormsg1").text("!!! Tag name already exist").css({"color":"red","font-weight": "bold"});
				}
				
				},
				error: function(e) {
					alert("Please try again later");
				}

			});
		}else{
			$("#tagWarningIdEdit").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#tagWarningIdEdit").text("Field cannot be blank").css({"color":"red"});
		}
		//so that the page doesn't post back
		return false;
    });
	
	
	
    $("#deleteTag").click(function() {
		var url = urlForServer+"note/deleteTag";
		var params = '{"userId":"'+userId+'","tagId":'+tagId+'}';
	    
	    params = encodeURIComponent(params);

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
        
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
				loadTags();
	    		reLoadList();
				setTimeout(function(){checkOwnerOfList();$('#deleteTagModal').modal('hide');},500);
	    		//createMoreActionModal();
	    	},
	    	error: function(e) {
	    		alert("Please try again later");
	    	}
	    });
	    //so that the page doesn't post back
	    return false;
    });
	
	function createListForVote(){
		var userNameForDiv ;
		var userInitialForDiv;
		var userIDForDiv;
		var castButtonClass= 'js-vote btn btn-success';
		var castedVote ='Vote';
		var contentDiv='';//'<li><center><a class="btn btn-success" > Voters</a></center></li><li class="divider"></li>';
			if(vote.length > 0 && vote[0] != null){
				for (i=0;i<vote.length;i++){
					userNameForDiv = '';
					if(userId == vote[i]){
						userNameForDiv = userFulName;
						userInitialForDiv=userInitial;
						userIDForDiv = userId;
						castedVote ='Voted';
						castButtonClass= 'js-un-vote btn btn-danger';
					}else{
						var userObje = activeUserObjectMap[vote[i]];
						userNameForDiv = userObje['userFirstName']+userObje['userLastName'];
						userInitialForDiv=(userObje['userFirstName']).substring(0,1).toUpperCase()+(userObje['userLastName']).substring(0,1).toUpperCase();
						userIDForDiv = userObje['userId'];
					}
					contentDiv = contentDiv +'<li class="voter '+userIDForDiv+'"> '
								+'<div class="member">'
									+'<span class="member-initials" title="'+userNameForDiv+'">'+userInitialForDiv+'</span>'
								+'</div>'
								+'<p class="popover-user-name title">'+userNameForDiv+'</p>'
							+'</li><br>';
				}
			}
			//This if Condition used for vote functionality need to hide in notes and Memo page. (by Venu)
			if(listType!=null && listType!='music' && listType!='bill'){				
				contentDiv = contentDiv	+'<li class="divider"></li>'
						+'<li class="vote-btns"><center><a class="'+castButtonClass+'">'+castedVote+'</a></center></li>';
			}
		return contentDiv;				
	}
	// uncast votes
	$('#moreActions').on('click','.js-un-vote',function(){
		
		var callFromSide = $(this).attr('class');
		var sideFlag = false; 
		if(callFromSide.match("side")){
			sideFlag = true;
		}
		var votedFlag=false;
		//votedNotes(votedFlag,listIdFromList,noteIdFromList);
				// kishore - added in api. for all public notes
		vote.splice(vote.indexOf(userId),1);
		var clickId = $(this);
		var params ='';
		if(vote.length > 0 && vote[0] != null){
			 params = encodeURIComponent(vote);
		}else{
			params = '  ';
			params = encodeURIComponent(params);
		}
		var url = urlForServer+"note/castUncastVote/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
		//alert('notes uncast'+url);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
		        type: 'POST',
		        url : url,
				data:params, 
		        success : function(response){
					if(response!=null && response == 'success'){
						if(parentDivName != "" && parentDivName != "notes" ){
							voteLists();
						}
						if(sideFlag){
						//side panel 
							$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.vote-badge').children().remove();
								$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.vote-badge').append('<span class="glyphicon glyphicon-thumbs-up"></span>'
									+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'&nbsp;votes </span>');
								
							
								$(clickId).removeClass('js-un-vote');
								$(clickId).addClass("js-vote");
								$(clickId).find('.on').remove();
									
							$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.dropdown-menu').find('.'+userId+'').remove();
							$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.dropdown-menu').children('.vote-btns').find("center").find("a").remove();
							$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.dropdown-menu').children('.vote-btns').find("center").append('<a class="js-vote btn btn-success">Vote</a>');	
							
							if(vote.length > 0 ){
								$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();
								$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(
										'<div class="badge vote-badge glyphicon glyphicon-sm"  title="This note has '+vote.length+' votes(s).">'
										+'<span class="glyphicon glyphicon-thumbs-up"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'</span></div>');
							}else{
								$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();
							}
						}else{
							if(vote.length > 0 ){
								$(clickId).parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();
								$(clickId).parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(
										'<div class="badge vote-badge glyphicon glyphicon-sm" title="This note has '+vote.length+' votes(s).">'
										+'<span class="glyphicon glyphicon-thumbs-up"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'</span></div>');
							}else{
								$(clickId).parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();
							}
						
						
							$(clickId).parent().parent().parent().parent().parent().parent().children('.col-md-3').children('.other-actions').children('.clearfix').find('.voted').find("a").removeClass('js-un-vote');
							$(clickId).parent().parent().parent().parent().parent().parent().children('.col-md-3').children('.other-actions').children('.clearfix').find('.voted').find("a").addClass("js-vote");
							$(clickId).parent().parent().parent().parent().parent().parent().children('.col-md-3').children('.other-actions').children('.clearfix').find('.voted').find("a").find('.on').remove();
							
							$(clickId).parent().parent().parent().children('.'+userId+'').remove();
							$(clickId).parent().parent().parent().parent().children('.vote-badge').find("span").remove();
							$(clickId).parent().parent().parent().parent().children('.vote-badge').append('<span class="glyphicon glyphicon-thumbs-up"></span>'
								+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'&nbsp;votes </span>');
								
							$(clickId).parent().append('<a class="js-vote btn btn-success">Vote</a>');	
							$(clickId).remove();
						}
					}
				},
				error: function(e) {
					alert("Please try again later");
				}
				
			});
	});
	// cast votes
	$('#moreActions').on('click','.js-vote',function(){
		var callFromSide = $(this).attr('class');
		var sideFlag =false; 
		if(callFromSide.match("side")){
			sideFlag = true;
		}
		var votedFlag=true;
		//votedNotes(votedFlag,listIdFromList,noteIdFromList);
			// kishore - added in api. for all public notes
		if(vote.indexOf(userId) == -1){
			vote.push(userId);
			var clickId = $(this);
			var params= encodeURIComponent(vote);
			var url = urlForServer+"note/castUncastVote/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
			//alert('notes cast'+url);
				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type: 'POST',
					url : url,
					data:params, 
					success : function(response){
						if(response!=null && response == 'success'){
							if(parentDivName != "" && parentDivName != "notes" ){
								voteLists();
							}
							if(sideFlag){
							//side panel 
								$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.vote-badge').children().remove();
								$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.vote-badge').append('<span class="glyphicon glyphicon-thumbs-up"></span>'
									+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'&nbsp;votes (with you) </span>');
									
								$(clickId).append('<span class="on">'
																+'<span class="glyphicon glyphicon-ok"></span>'
															+'</span>');
																
								$(clickId).removeClass('js-vote');
								$(clickId).addClass("js-un-vote");
								
								$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.dropdown-menu').prepend('<li class="voter '+userId+'" style="margin-bottom:20px">'
											+'<br><div class="member">'
												+'<span class="member-initials" title="'+userFulName+'">'+userInitial+'</span>'
											+'</div>'
											+'<p class="popover-user-name title">'+userFulName+'</p>'
										+'</li>');
							
								$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.dropdown-menu').children('.vote-btns').find("center").find("a").remove();
								$(clickId).parent().parent().parent().parent().parent().children('.col-md-9').children('.badges').children('.dropdown-menu').children('.vote-btns').find("center").append('<a class="js-un-vote btn btn-danger">Voted</a>');
								
								if(vote.length > 0 ){
									$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();	
									$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(
										'<div class="badge vote-badge glyphicon glyphicon-sm" title="This note has '+vote.length+' votes(s).">'
										+'<span class="glyphicon glyphicon-thumbs-up"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'</span></div>');
									
								}else{
									$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();	
								}
							}else{
							// main panel 
							
							//show in note
								if(vote.length > 0 ){	
									$(clickId).parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();
									
									$(clickId).parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(
										'<div class="badge vote-badge glyphicon glyphicon-sm"  title="This note has '+vote.length+' votes(s).">'
										+'<span class="glyphicon glyphicon-thumbs-up"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'</span></div>');     
								}else{
									
									$(clickId).parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').remove();
								}
							
								// in more action panel 
								$(clickId).parent().parent().parent().parent().parent().parent().children('.col-md-3').children('.other-actions').children('.clearfix').find('.voted').find("a").removeClass('js-vote');
								$(clickId).parent().parent().parent().parent().parent().parent().children('.col-md-3').children('.other-actions').children('.clearfix').find('.voted').find("a").addClass("js-un-vote");
								$(clickId).parent().parent().parent().parent().parent().parent().children('.col-md-3').children('.other-actions').children('.clearfix').find('.voted').find("a").append('<span class="on">'
														+'<span class="glyphicon glyphicon-ok"></span>'
													+'</span>');
													
								$(clickId).parent().parent().parent().prepend('<li class="voter '+userId+'" style="margin-bottom:20px"> '
											+'<br><div class="member">'
												+'<span class="member-initials" title="'+userFulName+'">'+userInitial+'</span>'
											+'</div>'
											+'<p class="popover-user-name title">'+userFulName+'</p>'
										+'</li>');
								$(clickId).parent().parent().parent().parent().children('.vote-badge').children().remove();
								$(clickId).parent().parent().parent().parent().children('.vote-badge').append('<span class="glyphicon glyphicon-thumbs-up"></span>'
									+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+vote.length+'&nbsp;votes (with you) </span>');

								$(clickId).parent().append('<a class="js-un-vote btn btn-danger">Voted</a>');	
								$(clickId).remove();
							}
						}		
					},
					error: function(e) {
						alert("Please try again later");
					}
					
				});		
		}
	});
	//comts
	$('#moreActions').on('click','.js-cmt-placeholder',function(){
		$(this).addClass("new-comment-input-heigth fontStyle");
		$(this).parent().find('.btn-left-align').remove();
		$(this).parent().append('<div class="btn-left-align">'
			
		+'<button class="js-add-note-pholder btn btn-success fontStyle">Save</button>&nbsp;&nbsp;'
		+'<a class="glyphicon glyphicon-remove js-cmt-cancel " ></a></div>');
	});
	
	$('#moreActions').on('click','.js-cmt-cancel',function(){
		$(this).parent().parent().find("textarea").remove();
		$(this).parent().parent().append('<textarea class="new-comment-input js-cmt-placeholder fontStyle" placeholder="Add comments, links and text "></textarea>');
		$(this).parent().remove();
	});
	
	// add cmts 
	$('#moreActions').on('click','.js-add-note-pholder',function(e){
		var unicode=e.charCode? e.charCode:e.keyCode; 
		var clickId=$(this);
		var cmtLevel;
		var cmts =$(this).parent().parent().find("textarea").val();
		//enter key press event
		
		 if(cmts.trim()!=""){
			 $(".js-add-note-pholder").attr('disabled','disabled');
			/*This code used to separate notes comments and crowd comments. by Venu*/
			var url = urlForServer+"note/addComments/"+listIdFromList+"/"+noteIdFromList+"/"+listType;
			
			var seldate=new Date();
			var selectedDate="";
			selectedDate=seldate.getMonth()+1;
			selectedDate=selectedDate+"/"+seldate.getDate()+"/"+seldate.getFullYear();
			var currentdate = new Date();
			var time = currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
			while(cmts.indexOf("\n") != -1){
				cmts = cmts.replace("\n"," <br> ");
			}
			while(cmts.indexOf("\t") != -1){
				cmts = cmts.replace("\t","    ");
			}
			while(cmts.indexOf("\"") != -1){
				cmts = cmts.replace("\"", "`*`");
			}
			if(listType=='crowd')
				cmtLevel='P';
			else
				cmtLevel='I';
			
			var params='{"cId":"", "userId":"'+userId+'", "listId":"'+listIdFromList+'", "noteId":"'+noteIdFromList+'","comments":"'+cmts+'","cDate":"'+selectedDate+'", "cTime":"'+time+'", "status":"A", "edited":"0", "editDate":"", "editTime":"","commLevel":"'+cmtLevel+'"}';
			params = encodeURIComponent(params);
			
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
		        type: 'POST',
		        url : url,
		        cache: false,
		        contentType: "application/json; charset=utf-8",
		        data:params, 
		        dataType: "json",
		        success : function(response){
		        	
						var cid = response.cId; 
																	
						var condate=new Date(response.date);
						var convertedDate="";
						convertedDate=month[condate.getMonth()];
						convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
						var timestring=response.time.split(":");
						var timee=convertTwentyFourHourToTwelveHourTime(timestring[0]+":"+timestring[1]);
						var array = response.comment.split(" <br> ");
						cmts = "";
						var videoDiv="";
						var link ="";
						var playId="";
						var linkArray = new Array();
						if(array.length > 0 && array[0]!=null ){
							for(var i =0; i< array.length;i++ ){
								if(array[i].indexOf("youtube.com/watch?v=") != -1){
									link = getVideoId(array[i]);
									playId=cid.trim()+link.trim();
									videoDiv = videoDiv + '<div id="'+playId+'"></div>	';
									playId=cid.trim()+"~"+link.trim();
									if(linkArray.indexOf(playId) == -1){
										linkArray.push(playId);
									}
								}
								cmts = cmts +  array[i] + "<br>\n";   
							}
						} 
						
						var dateTex=convertedDate.split("/");
						var date=dateTex[0]+" "+dateTex[1]+" "+dateTex[2];	
						var newCommand='';

						newCommand=newCommand+'<div class="phenom clearfix"> <div class="creator member js-show-mem-menu">'
														+'<span class="member-initials" title="'+userFulName+'">'+userInitial+'</span>'
														+'</div>'
														+'<div class="phenom-desc"> '
																+'<a class="inline-object js-show-mem-menu" >'+userFulName+'</a> '
																+'<div class="action-comment">'
																+'<div class="current-comment" style="overflow:auto;">'
																	+'<p>'+cmts+'</p>'
																+'</div> '
																+'</div>'
														+'</div>'
														+videoDiv
														+'<p class="phenom-meta quiet">'
															+'<span title="'+date+' at '+timee+'" class="date js-hide-on-sending" dt="'+date+' '+timee+'">'+date+' at '+timee+'</span>'
															+'<span id="'+cid+'" class="js-hide-on-sending">'
																 +'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;">Edit</a>'
																	+'- <a  class="js-delete-cmt" style="color: #939393;font-weight: normal;">Delete</a>';
																	
																	var endTag='</span>'
																		+'</p>'
																	+'</div>';
																	
																			
																			if(listType=='crowd')
																			$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.window-module').children('.list-actions-crd').prepend(newCommand+endTag);	
																			else if(listType=='music' && fromCrowd=='True' )
																			$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.window-module').children('.list-actions-crd').prepend(newCommand+endTag);
																			else
																			$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.window-module').children('.list-actions').prepend(newCommand+endTag);
						
						
						var count  = $(clickId).parent().parent().parent().parent().children('.badges').children('.cmts').find('.badge-text').text().trim();
						count = parseInt(count) +1;
						if(linkArray.length > 0 && linkArray[0]!=null){
							for(var i =0; i< linkArray.length;i++ ){
								var playId= linkArray[i].split("~");
								onYouTubeIframeAPIReady(playId[1].trim(),playId[0].trim()+playId[1].trim());
							}
						}
							
						// show in note	
						var divCmts	='<div class="badge glyphicon glyphicon-sm cmts " title="This note has '+count+' comment(s).">'
									+'<span class="glyphicon glyphicon-comment"></span>'
									+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+count+'</span></div>';
						var listId=$('#notebookId').val();
						if(listId =='note'){
							listId =listIdFromList;
						}
						$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').remove();
						
						/*if(vote.length > 0 ){	
							$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge ').after(divCmts);
						}else{*/
							//$(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(divCmts);
							($(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(divCmts));
						/*}*/
						// end show in note
						//alert($(clickId).parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').attr('class'));
						//alert($(clickId).parent().parent().parent().parent().parent().parent().parent().children('#20').children('.listBodyAll').children('#'+noteIdFromList+'').attr('class'));
						$(clickId).parent().parent().parent().parent().children('.badges').children('.cmts').find('.badge-text').remove();
						$(clickId).parent().parent().parent().parent().children('.badges').children('.cmts').append('<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+count+'</span>');
						$(clickId).parent().parent().parent().parent().children('.badges').children('.cmts').attr("title",'This note has '+count+' comment(s).');
						$(clickId).parent().parent().find("textarea").remove();
						$(clickId).parent().parent().append('<textarea class="new-comment-input js-cmt-placeholder fontStyle" placeholder="Add comments, links and text "></textarea>');
						$(clickId).parent().remove();
						
				},
				error: function(e) {
					alert("Please try again later");
				}
				
			});
		}
	
	});
	
	function getVideoId(url){
	
		if(url.indexOf("<a") != -1){
			url = url.substring(url.indexOf(">")+1,url.length);
		}
		
		if(url.indexOf("=") != -1){
			url = url.substring(url.indexOf("=")+1,url.length);
		}
		
		if(url.indexOf("</a>") != -1){
			url = url.substring(0,url.indexOf("</a>"));
		}
		return url;
	}
	
    function onYouTubeIframeAPIReady(videoId,playId) {
       var player = new YT.Player(playId, {
			height: '230',
			width: '405',
			videoId: videoId,
			events: {
    	   		'onReady': onPlayerReady,
    	   		'onStateChange': onPlayerStateChange
			}
        });
    }
    
    function onPlayerReady(event) {
        // event.target.playVideo();
     }
 	var done = false;
     function onPlayerStateChange(event) {
         if (event.data == YT.PlayerState.PLAYING && !done) {
           setTimeout(stopVideo, 6000);
           done = true;
         }
     }
     function stopVideo() {
         player.stopVideo();
     }
    
    
	//delete cmts
     var cmtsclickId='';
     var cIdFromDiv='';
	$('#moreActions').on('click','.js-delete-cmt',function(){
		cIdFromDiv= $(this).parent().attr('id');
		cmtsclickId=$(this);
		$('#moreActions').modal('toggle');
		$('#commentsDeletModal').modal("toggle");
		
	});

	//********* dialog message for delet comment *********//
	$('#commentsDeletYes').on('click',function(){
	//var cIdFromDiv= $(this).parent().attr('id');
	//var cmtsclickId=$(this);
		if(listType=='crowd')
			type="crowd";
		else
			type="music";
		
	var url = urlForServer+"note/deleteComments/"+listIdFromList+"/"+noteIdFromList+"/"+cIdFromDiv+"/"+type;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn					
    	},
	        type: 'POST',
	        url : url,
	        success : function(response){
				
				var count =$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').find('.badge-text').text();
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').find('.badge-text').remove();
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').append('<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+(count-1)+'</span>');
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().children('.badges').children('.cmts').attr("title",'This card has '+(count-1)+' comments(s).');
				// show in note
				var listId;
				if(listType!='crowd'){
					listId=$('#notebookId').val();
					if(listId =='note'){
						listId =listIdFromList;
					}
				}else{
					listId =listIdFromList;
				}
				if((count-1) > 0 ){
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').find("span").remove();
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').removeAttr("title");
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').attr("title",'This card has '+(count-1)+' comments(s).');
				$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').append(
							'<span class="glyphicon glyphicon-comment"></span>'
							+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+(count-1)+'</span>');     
				}else{
					$(cmtsclickId).parent().parent().parent().parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').remove();
				}
					// end show in note
					
				$(cmtsclickId).parent().parent().parent().remove();
				$('#moreActions').modal('toggle');
				$('#commentsDeletModal').modal("toggle");
			},
			error: function(e) {
				alert("Please try again later");
			}
			
		});
	});
	
	$('#commentsDeletNo').on('click',function(){
		$('#commentsDeletModal').modal("toggle");
		$('#moreActions').modal('toggle');
	});
	
	// delete comments show in panel mean use it now it not working.
	$('#pop-over').on('click','.js-confirm-delete-card',function(){
		
		var listMenu='<div id="pop-over" class="pop-over clearfix fancy-scrollbar modal hide fade">' 
				+'<div class="header clearfix">'
					+'<a  class="back-btn js-back-view" style="display: none;">'
						+'<span class="glyphicon glyphicon-sm glyphicon glyphicon-leftarrow"></span> '
					+'</a>'
					+'<span class="header-title">Delete Comment?</span>'
					+'<a  class="close-btn js-close-popover"> '
						+'<span class="glyphicon glyphicon-sm glyphicon glyphicon-close"></span> '
					+'</a> '
				+'</div> '
				+'<div class="content clearfix" style="">'
					+'<div>'
						+'<p> Deleting a comment is forever. There is no undo. </p> '
						+'<input id="'+cIdFromDiv+'" type="submit" value="Delete Comment"  class="js-confirm-delete-card negate full"> '
					+'</div>'
				+'</div>'
			+'</div>';	
	
	});
	
	// edit comts
	$('#moreActions').on('click','.js-edit-cmt-action',function(){
		
		var comtsFromDiv = $(this).parent().parent().parent().children('.phenom-desc').children('.action-comment').children().find("p").text();
		var cIdFromDiv= $(this).parent().attr('id');
		var height = $(this).parent().parent().parent().children('.phenom-desc').children('.action-comment').height();
		if(height < 100)
			height = 70;	
		var outerFrame = height+60;
		$(this).parent().parent().parent().children('.phenom-desc').children('.action-comment').children().addClass("hidden");
		$(this).parent().parent().parent().children('.phenom-desc').children('.action-comment').attr("style","height:"+outerFrame+"px;");
		$(this).parent().parent().parent().children('.phenom-desc').children('.action-comment').append('<div class="js-composer paddingBottomStyle"><div><textarea id="'+cIdFromDiv+'" class="span new-comment-input-heigth fontStyle" style="height: '+height+'px;">'+comtsFromDiv+' </textarea></div><div>'
					+'<div class="btn-left-align" style="left:50px;">'
						+'<button class="js-edit-cmt-card btn btn-success fontStyle">Save</button>&nbsp;&nbsp;'
						+'<a class="glyphicon glyphicon-remove js-edit-cmts-cancel " ></a></div>'
				+'</div></div>');
		$(this).parent().parent().addClass("hidden");
	});
	
	$('#moreActions').on('click','.js-edit-cmts-cancel',function(){
		$(this).parent().parent().parent().parent().children('.current-comment').removeClass('hidden');
		$(this).parent().parent().parent().parent().parent().parent().find("p").removeClass('hidden');
		$(this).parent().parent().parent().parent().attr("style",'');
		$(this).parent().parent().parent().remove();	
	});
	//submit edited cmt
	$('#moreActions').on('click','.js-edit-cmt-card',function(e){
		var clickId=$(this);
		var editedCmts =$(this).parent().parent().parent().children().find("textarea").val();
		
		var cIdFromText = $(this).parent().parent().parent().children().find("textarea").attr('id');
		//alert(editedCmts +'  = '+cIdFromText);
		if(editedCmts.trim()!=""){
			$(".js-edit-cmt-card").attr('disabled','disabled');
			var url = urlForServer+"note/updateComments/"+listIdFromList+"/"+noteIdFromList;
			
			var seldate=new Date();
			var selectedDate="";
			selectedDate=seldate.getMonth()+1;
			selectedDate=selectedDate+"/"+seldate.getDate()+"/"+seldate.getFullYear();
			var currentdate = new Date();
			var time = currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
			while(editedCmts.indexOf("\n") != -1){
				editedCmts = editedCmts.replace("\n"," <br> ");
			}
			while(editedCmts.indexOf("\t") != -1){
				editedCmts = editedCmts.replace("\t","    ");
			}
			while(editedCmts.indexOf("\"") != -1){
				editedCmts = editedCmts.replace("\"", "`*`");
			}
			var params='{"cId":"'+cIdFromText+'","comments":"'+editedCmts+'","editDate":"'+selectedDate+'", "editTime":"'+time+'","edited":"1"}"';
			
			params = encodeURIComponent(params);
			
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
		        type: 'POST',
		        url : url,
		        cache: false,
		        contentType: "application/json; charset=utf-8",
		        data:params, 
		        dataType: "json",
		        success : function(response){
					
					var condate=new Date(response.eDate);
					var convertedDate="";
					convertedDate=month[condate.getMonth()];
					convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
					var timestring=response.eTime.split(":");
					var timee=convertTwentyFourHourToTwelveHourTime(timestring[0]+":"+timestring[1]);
					var array = response.comment.split(" <br> ");
						editedCmts = "";
						var videoDiv="";
						var link ="";
						var playId="";
						var linkArray = new Array();
						if(array.length > 0 && array[0]!=null ){
							for(var i =0; i< array.length;i++ ){
								if(array[i].indexOf("youtube.com/watch?v=") != -1){
									link = getVideoId(array[i]);
									playId=cIdFromText.trim()+link.trim();
									videoDiv = videoDiv + '<div id="'+playId+'"></div>	';
									playId=cIdFromText.trim()+"~"+link.trim();
									if(linkArray.indexOf(playId) == -1){
										linkArray.push(playId);
									}
								}
								editedCmts = editedCmts +  array[i] + "<br>\n";   
							}
						} 
						if(editedCmts.lastIndexOf("\n") == (editedCmts.length-1)){
							editedCmts = editedCmts.substring(0, editedCmts.lastIndexOf("\n")-4);
						}
					var dateTex=convertedDate.split("/");
					var date=dateTex[0]+" "+dateTex[1]+" "+dateTex[2];
					
					$(clickId).parent().parent().parent().parent().children('.current-comment').removeClass('hidden');
					$(clickId).parent().parent().parent().parent().children('.current-comment').find("p").remove();
					
					var title= $(clickId).parent().parent().parent().parent().parent().parent().find("p").find('.js-hide-on-sending').attr('title');
					var dt = $(clickId).parent().parent().parent().parent().parent().parent().find("p").find('.js-hide-on-sending').attr('dt');
					
					$(clickId).parent().parent().parent().parent().parent().parent().find("p").remove();
					
					//$(clickId).parent().parent().parent().parent().parent().find("p").find('.js-hide-on-sending').;
					//video
					$(clickId).parent().parent().parent().parent().parent().parent().find("iframe").remove();
					$(clickId).parent().parent().parent().parent().children('.current-comment').append('<p>'+editedCmts.trim()+'</p>');
					$(clickId).parent().parent().parent().parent().children('.current-comment').attr("style","");
					$(clickId).parent().parent().parent().parent().parent().parent().append(videoDiv+'<p class="phenom-meta quiet">'
												+'<span title="'+title+'" class="date js-hide-on-sending" dt="'+dt+'">'+title+' - edited ' +date+' at '+timee+' </span>'
												+'<span id="'+cIdFromText+'" class="js-hide-on-sending">'
														+'- <a  class="js-edit-cmt-action" style="color: #939393;font-weight: normal;">Edit</a>'
														+'- <a  class="js-delete-cmt" style="color: #939393;font-weight: normal;">Delete</a>'
												+'</span>'
											+'</p>');
					if(linkArray.length > 0 && linkArray[0]!=null){
						for(var i =0; i< linkArray.length;i++ ){
							var playId= linkArray[i].split("~");
							onYouTubeIframeAPIReady(playId[1].trim(),playId[0].trim()+playId[1].trim());
						}
					}						
											
					$(clickId).parent().parent().parent().parent().attr("style",'');
					$(clickId).parent().parent().parent().remove();									
				},
				error: function(e) {
					alert("Please try again later");
				}
				
			});
		}
	
	});
	
	//end cmts
	$('#moreActions').on('click','.js-edit-notes-name',function(){
		if(listOwnerFlag){
			
			var noteNameHidden = $(this).text();
		
			var obj=$(this);
			$(this).addClass("hidden");
			$(this).parent().find("span").addClass("hidden");
			$(this).parent().append('<div class="js-composer paddingBottomStyle"><div><textarea class="js-update-card col-md-5 fontStyle">'+noteNameHidden+' </textarea></div><div>'
				+'</br></br></br></br><button class="js-add-note-name btn btn-success fontStyle" >Save</button>&nbsp;&nbsp;'
				+'<a class="glyphicon glyphicon-remove js-edit-notes-cancel" ></a>'
				+'</div></div>');
			//$(this).parent().parent().remove();
		}
	});
	
	$('#moreActions').on('click','.js-edit-notes-cancel',function(){
		$(this).parent().parent().parent().find("h3").removeClass('hidden');
		$(this).parent().parent().parent().find("span").removeClass('hidden');
		$(this).parent().parent().remove();
	});
	
	// update the notes name
	$('#moreActions').on('click','.js-add-note-name',function(e){
		var unicode=e.charCode? e.charCode:e.keyCode; 
		var clickId=$(this);
		var noteUpdatedName='';
		var type='';
		//enter key press event
		
		if((clickId).parent().parent().children().find("textarea").val().trim() == ""){
			noteUpdatedName= '       ';
		}else{
			noteUpdatedName=$(this).parent().parent().children().find("textarea").val();
			while(noteUpdatedName.indexOf("\n") != -1){
				noteUpdatedName = noteUpdatedName.replace("\n"," ");
			}
			while(noteUpdatedName.indexOf("\t") != -1){
				noteUpdatedName = noteUpdatedName.replace("\t"," ");
			}
			while(noteUpdatedName.indexOf("\"") != -1){
				noteUpdatedName = noteUpdatedName.replace("\"", "`*`");
			}
		}			
		if(noteUpdatedName.trim() != ''){
			$(".js-add-note-name").attr('disabled','disabled');
			if(listType=='schedule')
				type="schedule";
			else
				type="notes";
			
				var params = encodeURIComponent(noteUpdatedName);
				var url = urlForServer+"note/updateNote/"+listIdFromList+"/"+noteIdFromList+"/"+userId+"/"+type;
				var updatedName=$(this).val();
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type: 'POST',
				url : url,
				data : params,
				cache: false,
				success : function(response){
					
					if(response!=null && response == 0)
					{
						while(noteUpdatedName.indexOf("`*`") != -1){
							noteUpdatedName = noteUpdatedName.replace("`*`", "\"");
						}
						if(listType=='schedule')
						{
							// modification in main panel 
							//$('#notes').children('#scheduleContent').children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').find("p").remove();
							//$('#notes').children('#scheduleContent').children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').append('<p><b>'+noteUpdatedName+'</b></p>');
							// more actions
							$(clickId).parent().parent().parent().find("h3").remove();
							$(clickId).parent().parent().parent().find("span").removeClass('hidden');
							$(clickId).parent().parent().parent().append('<h3 class="js-edit-notes-name" style="cursor: pointer;" title="Click to edit">'+noteUpdatedName+'</h3>');
							$(clickId).parent().parent().remove();
							
							if($('#scheduleContent').children('.listDiv').children().attr('class')=='modalHeader')
 				        	{
 				        		loadEventList(userId,listIdFromList,listType);
 				        		loadAllEvents();
 				        		getTodaySchedule();
 				        		loadEventsBooks();
 				        		getListAndNoteNames();
 				        		
 				        	}
 				        	else
 				        	{
 				        		loadAllEvents();
 				        		getTodaySchedule();
 				        		loadEventsBooks();
 				        		getListAndNoteNames();
 				        	}
						}
						else
						{
							var listId=$('#notebookId').val();
							if(listId =='note'){
								listId =listIdFromList;
							}
							// modification in main panel 
							$('#notes').children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').find("p").find("b").remove();
							$('#notes').children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').find("p").prepend('<b>'+noteUpdatedName+'</b>');
						
							$(clickId).parent().parent().parent().find("h3").remove();
							$(clickId).parent().parent().parent().find("span").removeClass('hidden');
							$(clickId).parent().parent().parent().append('<h3  class="js-edit-notes-name" style="cursor: pointer;" title="Click to edit">'+noteUpdatedName+'</h3> </div>');
							$(clickId).parent().parent().remove();
						}
					}
				},
				error: function(e) {
					alert("Please try again later");
				}
			
			});
			   
		}
	});
	
	/* edit decsription in note*/
	$('#moreActions').on('click','.js-note-edit-desc1',function(){
		var olddesc=$(this).parent().parent().parent().children('.action-comment').children().find("p").text();
		$(this).parent().parent().parent().children('.action-comment').fadeOut(1);
		$(this).parent().parent().parent().append('&nbsp;&nbsp;<textarea class="new-comment-input js-note-edit-desc2 new-comment-input-heigth  fontStyle" style="width: 80%;">'+olddesc.trim()+'</textarea>');
		$(this).parent().parent().parent().attr("style",'position: relative;height:140px;');
		$(this).parent().parent().parent().find('.btn-left-align').remove();
		$(this).parent().parent().parent().append('<div class="btn-left-align">'
		+'<button class="js-add-note-desc btn btn-success  fontStyle">Save</button>&nbsp;&nbsp;'
		+'<a class="glyphicon glyphicon-remove js-notes-desc-cancel1 " ></a></div>');
		$(this).parent().parent().remove();
	});
	$('#moreActions').on('click','.js-note-edit-desc',function(){	
		if(is_chrome){
			$(this).parent().find('.btn-left-align').remove();
			$(this).parent().append('<div class="btn-left-align span">'
			+'<button class="js-add-note-desc btn btn-success  fontStyle">Save</button>&nbsp;&nbsp;'
			+'<a class="glyphicon glyphicon-remove js-notes-desc-cancel " ></a></div>');
			$(this).parent().attr("style",'height:140px;');
			$(this).addClass("new-comment-input-heigth");
			$(this).attr("style",'width: 80%; top: 0px;');
		}else{
			$(this).addClass("new-comment-input-heigth");
			$(this).parent().attr("style",'position: relative;height:140px;');
			$(this).attr("style",'width: 80%; position: relative; top: 0px;');
			$(this).parent().find('.btn-left-align').remove();
			$(this).parent().append('<div class="btn-left-align">'
			+'<button class="js-add-note-desc btn btn-success  fontStyle">Save</button>&nbsp;&nbsp;'
			+'<a class="glyphicon glyphicon-remove js-notes-desc-cancel " ></a></div>');
		}
	});
	$('#moreActions').on('click','.js-notes-desc-cancel1',function(){
		
		$(this).parent().parent().find("textarea").remove();
		$(this).parent().parent().removeAttr("style");
		$(this).parent().parent().find('.action-comment').fadeIn(1);
		$(this).parent().parent().append('<p><span class="col-md-1"></span>&nbsp;&nbsp;<span><a  title="Click to Edit the Description" class="js-note-edit-desc1" style="color: #939393;font-weight: normal;">Edit</a></span></p>	');
		$(this).parent().remove();	
	});
	
	$('#moreActions').on('click','.js-notes-desc-cancel',function(){
		$(this).parent().parent().find("textarea").removeClass('new-comment-input-heigth');
		$(this).parent().parent().find("textarea").val("");
		$(this).parent().parent().find("textarea").attr("style",'width: 80%; position: relative; top: 10px;');
		$(this).parent().parent().removeAttr("style");
		$(this).parent().remove();	
	});
	
	$('#moreActions').on('click','.js-add-note-desc',function(){
		var clickId=$(this);
		var result="";
		 var description ;
		//enter key press event
			if($(this).parent().parent().find("textarea").val().trim() == ""){
				description= '       ';
			}else{
				description=$(this).parent().parent().find("textarea").val();
			}
			while(description.indexOf("\t") != -1){
				description = description.replace("\t","    ");
			}
			$(".js-add-note-desc").attr('disabled','disabled');
			var url = urlForServer+"note/addDescription/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
			var Vorwsd = new Array();
			var params = encodeURIComponent(description);
			
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
				type: 'POST',
				url : url,
				cache: false,
				data : params, 
				success : function(response){
				var comtData = jQuery.parseJSON(response);
					var array = comtData.desc.split(" <br> ");
					var	desc = "";
						if(array.length > 0 && array[0]!=null && array[0].trim() !=''){
							for(var i =0; i< array.length;i++ ){
								desc = desc +  array[i] + "<br>\n";   
							}
						} 
					var listId=$('#notebookId').val();
					if(listId =='note'){
						listId =listIdFromList;
					}
					$(clickId).parent().parent().find("textarea").remove();
					$(clickId).parent().parent().children('.action-comment').remove();
					$(clickId).parent().parent().removeAttr("style");
											
					if(comtData.desc.trim()==""){
						$(clickId).parent().parent().prepend('&nbsp;&nbsp;<textarea class="new-comment-input js-note-edit-desc  fontStyle" placeholder="Note description" style="width: 80%; position: relative; top: 10px;"></textarea>');						
						$(clickId).parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').children().find("span").remove();
						$(clickId).parent().parent().parent().children('.badges').find('.decs').remove();						
						// show in note										
						$(clickId).parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.decs').remove();
						// end show in note
						
					}else{
						// show in note
						$(clickId).parent().parent().prepend('&nbsp;&nbsp;<div class="action-comment" title="Note Description"><div class="current-comment" style="overflow:auto;"><p class="p-decs-test-align">' +desc.trim() +'</p></div></div>'
								+'<p><span class="col-md-1"></span>&nbsp;&nbsp;<span><a title="Click to Edit the Description" class="js-note-edit-desc1" style="color: #939393;font-weight: normal;">Edit</a></span></p>');						
						
						var dicDesc = '<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
						$(clickId).parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.decs').remove();
						var cmtLen = $(clickId).parent().parent().parent().children('.badges').children('.cmts').find('.badge-text').text();
						
						if(cmtLen > 0){
							$(clickId).parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').after(dicDesc);
						}else{
							$(clickId).parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(dicDesc);	
						}
						
						$(clickId).parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').children().find("span").remove();
						var tempNoteName = $(clickId).parent().parent().parent().parent().parent().parent().children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').children().find("b").text().trim(); 
						var tempDescDiv = loadDescContent(tempNoteName,desc.trim());
						$(clickId).parent().parent().parent().parent().parent().parent().children('#'+listId+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.todo_description').children().find("b").after(tempDescDiv);
						// end show in note
					
						$(clickId).parent().parent().parent().children('.badges').find('.decs').remove();
						$(clickId).parent().parent().parent().children('.badges').find('.cmts').after('<div class="badge voted glyphicon glyphicon-sm decs" '
						+'title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>');
					}
					$(clickId).parent().remove();

				},
				error: function(e) {
					alert("Please try again later");
				}
        
			});	
		
	});
	
	
	
	$('#notes').on('click','.js-access-change',function(){
		
		$( ".js-access-change" ).click(function( event ) {
			event.stopImmediatePropagation();
			});
		$('input:checkbox[name=publicShareWarnCheckbox]').attr('checked',false);
		
		publicShareClickId = $(this);
		publicSharemoreactionFlag = false;
		
		if($(this).parent().parent().parent().parent().attr('id') == 'moreActions'){
			
			publicSharemoreactionFlag = true;
			publicShareAccess=$(this).text();
			publicShareNoteId=$(moreActionBasedId).parent().attr('id');
			publicShareListId=$(moreActionBasedId).parent().parent().parent().attr('id');
		}else{
			publicSharemoreactionFlag = false;
			publicShareAccess=$(this).text();
			publicShareListId=$(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id');
			publicShareNoteId=$(this).parent().parent().parent().parent().parent().parent().parent().parent().attr('id');
		}
	
		if($(this).hasClass('true')){
			$('#moreActions').modal('hide');
			$(".js-publicshare-note").attr('disabled' , false);
			$('.Note-publicshare-warn').text('Do you want share this note with  Crowd?');
		
			$('#NotePublicShareModal').modal('toggle');
		}else if($(this).hasClass('false')){
			shareNoteToPublic();		
		}
	});
	
	
	
	//change access in note
	$('#notes').on('click','.js-publicshare-note',function(){
		$(".js-publicshare-note").attr('disabled' , true);
		var isChecked = $('#publicShareWarnCheckbox').attr('checked')?true:false;
		
		if(isChecked){
			var url = urlForServer+"note/publicShareWarnMsgFlagChange/"+userId;
			var params = '{"userId":"'+userId+'"}';

			params = encodeURIComponent(params);
	
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type: 'POST',
				url : url,
				cache: false,
				contentType: "application/json; charset=utf-8",
				data:params, 
				dataType: "json",
				success : function(respo){
				 
	    			shareNoteToPublic();
            	},
            	error: function(e) {
            		alert("Please try again later");
            	}
			});
		}else{
			shareNoteToPublic();
		}
		
	});
	
	function dateConvert(){
		var date=new Date();
		var day=date.getDate();
		var months=date.getMonth();
		var year=date.getFullYear();
		var hour=date.getHours();
		var minutes=date.getMinutes();
		var seconds=date.getSeconds();
		dates=month[months]+"/"+day+"/"+year;
		date1=months+1+"/"+day+"/"+year+" "+hour+":"+minutes;
		dates1=convertNumberDateFormatIntoStringFormat(date1,false);
		return dates1;
	}
	
	
	function shareNoteToPublic(){
		dates1=dateConvert();
		var url = urlForServer+"note/noteAccessChange/"+userId;
		var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","publicUser":"'+userFulName+'","publicDate":"'+dates1+'","access":"public"}';
	    params = encodeURIComponent(params);
	
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	
	    	if(response['noteId']!="0"){
				if(publicSharemoreactionFlag){
					//alert(response['access']);
						var access1 = "";
						var icon_class='';
							if(response['access'] != "" ){
								if(response['access'].trim() == "private"){
									access1 = "Crowd"; 
									icon_class="glyphicon glyphicon-globe";
								}else if(response['access'].trim() == "public"){
									access1 = "Crowd";
									icon_class ="glyphicon glyphicon-globe";
								}
							}
					/*$(publicShareClickId).parent().children('.btn-group').after('<a class="button-link js-access-changed" title="Note already shared with crowd" style="text-decoration: none;">'
							+'<span class="'+icon_class+'"></span>'
							+'&nbsp;'+publicShareAccess
							+'</a>');*/
							
							$(publicShareClickId).remove();	
							
							$('#moreActions').children('.modal-body').children('#sideBar').children('.other-actions').children('.clearfix').append('<a class="button-link js-access-changed" title="Note already shared with crowd" style="text-decoration: none;">'
									+'<span class="'+icon_class+'"></span>'
									+'&nbsp;'+publicShareAccess
									+'</a>');
							
							
					
					
					
					
				}else{
					$(".noteMenu").popover('hide');
				}
				newNotes();
					$('#NotePublicShareModal').modal('hide');
	    	}
            },
            error: function(e) {
                alert("Please try again later");
            }
	    });
	}
	
/* event request and decline and later functionality */
	
	$('#notes').on('click','.js-acceptEvent',function(){
	
	//$('.js-acceptEvent').click(function(){
			var listId= listIdFromList;
			var noteId= noteIdFromList;
		
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions')
		{
			addMembersId=moreActionBasedId;
			var url = urlForServer+"note/calendarEventAccess";
			params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'","sharingStatus":"A"}';
			params = encodeURIComponent(params);
				
			    $.ajax({
			    	headers: { 
			    	"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
			    	type: 'POST',
			    	url : url,
			    	cache: false,
			    	contentType: "application/json; charset=utf-8",
			    	data:params, 
			    	//dataType: "json",
			    	success : function(response){
			    	 ////////////////////////////////////////////////////	
			    	var url = urlForServer+"log/setNotificationAcceptDecline";
					params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'"}';
					 $.ajax({
						 headers: { 
						 "Mn-Callers" : musicnote,
					    	"Mn-time" :musicnoteIn				
					    	},
					    	type: 'POST',
					    	url : url,
					    	cache: false,
					    	contentType: "application/json; charset=utf-8",
					    	data:params, 
					    	//dataType: "json",
					    	success : function(response){
					 },
					 error : function(e)
					    {
					    }
					    
					    });
			  ////////////////////////////////////////////// 
			    	
			    	$("#moreActions").modal('hide');
			    	$('#calendarlist').empty();
			    	loadEventsBooks();
			    	getListAndNoteNames();
			    	loadAllEvents();
			    	getTodaySchedule();
//			    	loadCalendarComboBoxList();
			    },
			    error : function(e)
			    {
			    	alert("please try again later");
			    }
			    
			    });
			
			
		}else{
			addMembersId=$(this).parent().parent().parent().parent().parent().parent().parent();
			$(".noteMenu").popover('hide');	
		}
	
	});
	
	
	$('#notes').on('click','.js-declineEvent',function(){
		var listId= listIdFromList;
		var noteId= noteIdFromList;
	
	if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions')
	{
		addMembersId=moreActionBasedId;
		var url = urlForServer+"note/calendarEventAccess";
		params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'","sharingStatus":"D"}';
		 params = encodeURIComponent(params);
			
		    $.ajax({
		    	headers: { 
		    	"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
		    	type: 'POST',
		    	url : url,
		    	cache: false,
		    	contentType: "application/json; charset=utf-8",
		    	data:params, 
		    	//dataType: "json",
		    	success : function(response){
		    	 ////////////////////////////////////////////////////	
		    	/*var url = urlForServer+"log/setNotificationAcceptDecline";
				params = '{"listId":"'+listId+'","userId":"'+userId+'","noteId":"'+noteId+'"}';
				 $.ajax({
					 headers: { 
					 "Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn				
				    	},
				    	type: 'POST',
				    	url : url,
				    	cache: false,
				    	contentType: "application/json; charset=utf-8",
				    	data:params, 
				    	dataType: "json",
				    	success : function(response){
				 },
				 error : function(e)
				    {
				    }
				    
				    });*/
		  ////////////////////////////////////////////// 
		    	$("#moreActions").modal('hide');
		    	$('#calendarlist').empty();
		    	loadEventsBooks();
		    	getListAndNoteNames();
		    	loadAllEvents();
		    	getTodaySchedule();
//		    	loadCalendarComboBoxList();
		    },
		    error : function(e)
		    {
		    	alert("please try again later2");
		    }
		    
		    });
		
		
	}else{
		addMembersId=$(this).parent().parent().parent().parent().parent().parent().parent();
		$(".noteMenu").popover('hide');	
	}

});
	
	
	/*Handles list menu functionality*/
	$('#notes').on('click','.listMenu',function(){
				
				var id=$(this).parent().parent().parent().attr('id');
				$("#"+id+"booklists").children().remove();
				var url = urlForServer+"note/checkListAccess";
				var params=""; 
				var defaultNote="";
				bookName=$(this).parent().parent().children().find("b").text();
				if(listType!='schedule')
					params = '{"listId":"'+id+'","userId":"'+userId+'","noteId":"0"}';
				else if(listType=='schedule')
					params = '{"listId":"'+id+'","userId":"'+userId+'","eventId":"0"}';
					bookId=id;
			    
			    params = encodeURIComponent(params);
			    $.ajax({
			    	headers: { 
			    	"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
			    	type: 'POST',
			    	url : url,
			    	cache: false,
			    	contentType: "application/json; charset=utf-8",
			    	data:params, 
			    	dataType: "json",
			    	success : function(response){
			    	if(response['noteAccess']!="0"){
			    		var listMenu="";
			    		var enableMenu='<li><a class="js-enableBook" href="javascript:void(0);"style="text-decoration: none;" >';
			    		
			    			listMenu='';
			    			if(listType=='schedule')
				    		{
				    		if(!response['eventSharedAllContact']){
			    				shareAllContact='<li><a class="js-shareBook-allContact" href="javascript:void(0);" style="text-decoration: none;" >Share All Contacts</a></li>';
			    			}else if(response['eventSharedAllContact']){
			    				shareAllContact='<li><a class="js-sharedBook-allContact" href="javascript:void(0);" style="text-decoration: none;" >Shared Contacts</a></li>';
			    			}
				    			
			    				var flag=response['enabledisableflag'];
					    		var enabledisable="";
					    		if(flag=="false")
					    			enabledisable="Enable";
					    		else
					    			enabledisable="Disable";
					    		enableMenu = enableMenu+ enabledisable +'</a></li><li><hr></li>'; ;
								listMenu=listMenu+enableMenu;
				    		}
				    		else
			    		{
			    		if(!response['noteSharedAllContact']){
		    				shareAllContact='<li><a class="js-shareBook-allContact" href="javascript:void(0);" style="text-decoration: none;" >Share All Contacts</a></li>';
		    			}else if(response['noteSharedAllContact']){
		    				shareAllContact='<li><a class="js-sharedBook-allContact" href="javascript:void(0);" style="text-decoration: none;" >Shared Contacts</a></li>';
		    			}
			    		}
			    			if(!response['defaultNote']){
			    				defaultNote='<li><hr></li>';
			    				defaultNote=defaultNote+'<li><a class="js-close-list" href="javascript:void(0);" style="text-decoration: none;">';
			    				if(listType !='schedule'){
			    					defaultNote=defaultNote+'Delete Book</a></li>';
			    				}else{
			    					defaultNote=defaultNote+'Delete Calendar</a></li>';
			    				}
			    			}

				    		
							if(listType !='schedule'){
								listMenu=listMenu+'<li><a class="js-shareBook-individual" data-toggle="modal" href="#" style="text-decoration: none;" >Share Individual</a></li>'
					    		
					    		
				    			+'<li><a class="js-shareBook-group" href="#" data-toggle="modal" style="text-decoration: none;" >Share Group</a></li>'
				    			//+'<li><a class="js-shareBook-allContact" href="javascript:void(0);" style="text-decoration: none;" >Share All Contacts</a></li>'
				    			+shareAllContact
					    		+'<li><hr></li>';
								listMenu=listMenu+'<li><a class="js-copy-list" href="javascript:void(0);"style="text-decoration: none;" >Add To Notebook</a></li>'
				    			+'<li><hr></li>';
				    			//+'<li><a class="js-move-notes" href="javascript:void(0);" style="text-decoration: none;">Move All Notes from Book</a></li>';
							}
				    			listMenu=listMenu+ '<li><a class="js-archive-notes" href="javascript:void(0);" style="text-decoration: none;">';
								if(listType !='schedule'){
									listMenu=listMenu+ 'Delete All Notes from Book</a></li>';
								}else{
									listMenu=listMenu+ 'Delete All Events from Calendar</a></li>';
								}
				    			listMenu=listMenu+ defaultNote;
								
				    	$("#"+id+"booklists").append(listMenu);
				    	$("#"+id+"booklists").addClass('fontStyle');
			    	}else{
						if(response['shared']!= undefined && response['shared'] !='notshared'){
							defaultNote= '<li><a class="js-close-list '+response['shared']+'" href="javascript:void(0);" style="text-decoration: none;">';
									if(listType !='schedule'){
										defaultNote=defaultNote+'Delete Book</a></li>';
									}
							$("#"+id+"booklists").append(defaultNote);
							$("#"+id+"booklists").addClass('fontStyle');
						}
					}
			        },
		            error: function(e) {
		                alert("Please try again later");
		            }
			    });
	});
	
	
	$('#notes').on('click','.listMenuOthers',function(){
		var id=$(this).parent().parent().parent().attr('id');
		var url = urlForServer+"note/checkListAccess";
		var params=""; 
		if(listType!='schedule')
			params = '{"listId":"'+id+'","userId":"'+userId+'","noteId":"0"}';
		else if(listType=='schedule')
			params = '{"listId":"'+id+'","userId":"'+userId+'","eventId":"0"}';
			bookId=id;
			
	    params = encodeURIComponent(params);
	
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	if(response['noteAccess']=="0" && listType=="schedule")
	    	{
	    		
	    		if(response['sentUserId']!="0")
	    		{
	    			
	    		    var otherId=response['sentUserId'];
	    		    var sharedMenu="";
	    			if(otherId!=userId)
	    			{
	    				//sharedMenu='<li><a class="js-enableBook" href="javascript:void(0);"style="text-decoration: none;" ></a></li>';
	    				var flag=response['enabledisableflag'];
			    		var enabledisable="";
			    		if(flag=="false")
					    		{
			    			enabledisable="Enable";
					    		}
			    		else
					    		{
			    			enabledisable="Disable";
					    		}
			    		
			    		sharedMenu='<li><a class="js-enableBook" href="javascript:void(0);"style="text-decoration: none;" >'+enabledisable+'</a></li>';
			    		
			    		//$('#menu1').find('.js-enableBook').append(enabledisable);
			    		
			    		$("#"+id+"booklists").empty();
			    		$("#"+id+"booklists").append(sharedMenu);
		    			//$(".listMenuOthers").popover({placement : 'bottom', title: 'Book Menu', html : true, content : sharedMenu});
			    					    		
	    			}
	    			
	    		}
	    	}
            },
            error: function(e) {
                alert("Please try again later");
            }
	    });
});

	
	
	
	
	
	
	/*Handles note menu functionality*/
	$('#notes').on('click','.noteMenu',function(){
		
		var id=$(this).parent().parent().parent().parent().parent().attr('id');
		var noteId=$(this).parent().parent().parent().attr('id');
		var url = urlForServer+"note/checkListAccess";
		var params="";
		if(listType=='schedule')
			params = '{"listId":"'+id+'","userId":"'+userId+'","eventId":"'+noteId+'"}';
		else
			params = '{"listId":"'+id+'","userId":"'+userId+'","noteId":"'+noteId+'"}';
	    
	    params = encodeURIComponent(params);
	
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	if(listType=='schedule')
	    	{
	    		if(response['eventAccess']!="0"){
	    			var access='';
	    			var shareAllContact='';
	    			if(response['eventAccess']=="private"){
	    				access='<li><a class="js-access-change" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Public</a> </li>';
	    			}else if(response['eventAccess']=="public"){
	    				access='<li><a class="js-access-change" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Private</a> </li>';
	    			}
	    			if(response['eventSharedAllContact']==0){
	    				shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Share Contacts</a></li>';
	    			}else if(response['eventSharedAllContact']==1){
	    				shareAllContact='<li><a class="js-members-delete-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
	    			}
	    			
	    			var noteMenu='<div class="noteMenuDiv">'
						+'<ul class="noteMenuUl">'
						+'<li><a class="js-members" role="button" href="#memberModel" data-toggle="modal" style="text-decoration: none;">Share Individual(s)</a></li>'
						+'<li><a class="js-members-group" role="button" href="#shareGroupModel" data-toggle="modal" style="text-decoration: none;">Share Group</a></li>'
						+shareAllContact
						+'<li><hr></li>'
						+'<li><a class="js-copy-note" style="text-decoration: none;" href="javascript:void(0);">Add To Notebook</a> </li>'
						+'<li><a class="js-copy-note-indiviual" role="button" style="text-decoration: none;" href="#copyIndiviualModel" data-toggle="modal">Add To Individual(s)</a> </li>'
						+'<li><a class="js-copy-note-group" role="button" style="text-decoration: none;" href="#copyGroupModel" data-toggle="modal">Add To Group</a> </li>'
						+'<li><a class="js-copy-note-contacts" style="text-decoration: none;" href="javascript:void(0);">Add To Contacts</a> </li>'
						+'<li><hr></li>'
						+'<li><a class="js-due-date" role="button" href="#dueDateModel" data-toggle="modal" style="text-decoration: none;">Due Date</a></li>'
						//+'<li><a class="js-move" style="text-decoration: none;" href="javascript:void(0);">Move</a> </li>'
						+'<li><a class="js-attach" role="button" href="#attachFileModel" data-toggle="modal" style="text-decoration: none;">Attach Files</a> </li>'
						+access
						+'<li><hr></li>'
						+'<li><a class="js-archive" style="text-decoration: none;" href="javascript:void(0);">Archive</a></li>'
						+'</ul>'
						+'</div>';
	    			$(".noteMenu").popover({placement : 'bottom', title: 'Note Menu', html : true ,content :noteMenu});
	    		}
	    	}
	    	else
	    	{
	    		if(response['noteAccess']!="0"){
	    			var access='';
	    			var shareAllContact='';
	    			if(response['noteAccess']=="private"){
	    				access='<li><a class="js-access-change" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Public</a> </li>';
	    			}else if(response['noteAccess']=="public"){
	    				access='<li><a class="js-access-change" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Private</a> </li>';
	    			}
	    			if(response['noteSharedAllContact']==0){
	    				shareAllContact='<li><a class="js-members-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Share Contacts</a></li>';
	    			}else if(response['noteSharedAllContact']==1){
	    				shareAllContact='<li><a class="js-members-delete-contact" role="button" href="javascript:void(0);" data-toggle="modal" style="text-decoration: none;">Shared Contact</a></li>';
	    			}
	    			
	    			var noteMenu='<div class="noteMenuDiv">'
						+'<ul class="noteMenuUl">'
						+'<li><a class="js-members" role="button" href="#memberModel" data-toggle="modal" style="text-decoration: none;">Share Individual</a></li>'
						+'<li><a class="js-members-group" role="button" href="#shareGroupModel" data-toggle="modal" style="text-decoration: none;">Share Group</a></li>'
						+shareAllContact
						+'<li><hr></li>'
						+'<li><a class="js-copy-note" style="text-decoration: none;" href="javascript:void(0);">Add To Notebook</a> </li>'
						+'<li><a class="js-copy-note-indiviual" role="button" style="text-decoration: none;" href="#copyIndiviualModel" data-toggle="modal">Add To Individual</a> </li>'
						+'<li><a class="js-copy-note-group" role="button" style="text-decoration: none;" href="#copyGroupModel" data-toggle="modal">Add To Group</a> </li>'
						+'<li><a class="js-copy-note-contacts" style="text-decoration: none;" href="javascript:void(0);">Add To Contacts</a> </li>'
						+'<li><hr></li>'
						+'<li><a class="js-due-date" role="button" href="#dueDateModel" data-toggle="modal" style="text-decoration: none;">Due Date</a></li>'
						//+'<li><a class="js-move" style="text-decoration: none;" href="javascript:void(0);">Move</a> </li>'
						+'<li><a class="js-attach" role="button" href="#attachFileModel" data-toggle="modal" style="text-decoration: none;">Attach Files</a> </li>'
						+access
						+'<li><hr></li>'
						+'<li><a class="js-archive" style="text-decoration: none;" href="javascript:void(0);">Archive</a></li>'
						+'</ul>'
						+'</div>';
	    			$(".noteMenu").popover({placement : 'bottom', title: 'Note Menu', html : true ,content :noteMenu});
	    		}
	    	}
        	},
        	error: function(e) {
        		alert("Please try again later");
        	}
	    });
	});
	/*Share Note For All Contact save to db*/
	$('#memAllConYes').on('click',function(){
		
		//var noteId=$(memberShareAllCon).parent().attr('id');
		//var listId=$(memberShareAllCon).parent().parent().parent().attr('id');
		//var url = urlForServer+"note/shareAllContacts";
		
		if(listType=='schedule')
		{
			type="Schedule";
		}
		else
		{
			type="member";
		}
		shareType="allContacts";
		
		if(listType!='schedule')
		{
		$('#shareAllContactModal').modal('toggle');
		$('.share-note-warn').text('Share note as:');
		$('#shareWarning').modal('toggle');
		}
		
		if(listType=='schedule')
		{
		//var params = '{"listId":"'+listId+'","noteId":"'+noteId+'","userId":"'+userId+'","listType":"'+type+'"}';
		var url = urlForServer+"note/shareAllContacts";
		var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+type+'","noteLevel":"view","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+noteNameFromList+'"}';
	    
	    params = encodeURIComponent(params);
	

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	$('#shareAllContactModal').modal('hide');
	    		if(moreActionEvent=='memberAllContact'){
	    			checkOwnerOfList();
	    			//$('#moreActions').modal('toggle');
	    			moreActionEvent='';
	    		}
	    		
            },
            error: function(e) {
                alert("Please try again later");
            }
	    });
		}
	});
	/*Share Note For All Contact Modal panel Open*/
	$('#notes').on('click','.js-members-contact',function(){
		var listId;
		var noteId;
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			memberShareAllCon=moreActionBasedId;
			noteId=noteIdFromList;//$(memberShareAllCon).parent().attr('id');
			listId=listIdFromList;//$(memberShareAllCon).parent().parent().parent().attr('id');
			$('#moreActions').modal('hide');
			moreActionEvent='memberAllContact';
		}else{
			memberShareAllCon=$(this).parent().parent().parent().parent().parent().parent().parent();
			listId=$(memberShareAllCon).parent().parent().parent().attr('id');
			noteId=$(memberShareAllCon).parent().attr('id');
			$(".noteMenu").popover('hide');
		}
		
		var response="";
		var url = urlForServer+"user/memberList/"+userId;
		
		var Type='';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
		type : 'POST',
		url : url,
		success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null)
			{
				$('#shareAllContactModal').modal('toggle');
				if(listType!='schedule')
					$("#allContact").text("Are you sure you want to share this note with all contact(s)?");
				else
					$("#allContact").text("Are you sure you want to share this event with all contact(s)?");
			}
			else
			{
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text("No contacts exist. Please add contact(s) to share with");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			}
		
		},
		error :function(e)
		{
		}
		});
	});
	
	
	/* Delete Note Alert panel function*/
	var noteDeleteId;
	var noteDeleteListId;
	var noteDeleteNoteId;
	$('#notes').on('click','.js-archive',function(){
		if($(this).parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			noteDeleteNoteId=$(moreActionBasedId).parent().parent().parent().attr('id');
			noteDeleteListId=$(moreActionBasedId).parent().parent().parent().parent().parent().attr('id');
			moreActionEvent='archive';
		}else{
			noteDeleteId=$(this);
			noteDeleteListId=$(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id');
			noteDeleteNoteId=$(this).parent().parent().parent().parent().parent().parent().parent().parent().attr('id');
		}
		$('#moreActions').modal('hide');
		
		if(listType!='schedule')
			$('.delete-Note-warn').text('Are you sure you want to delete this note?');
		else
			$('.delete-Note-warn').text('Are you sure you want to delete this event?');
		
		$('#deleteNoteModal').modal('toggle');
	});
	
	/*Archive Note Modal Panel Functionality*/
	$('#notes').on('click','.js-delete-note',function(){
		$('.js-delete-note').attr("disabled","disabled");
		var listIdFrom=listIdFromList;
		var noteIdFrom=noteIdFromList;
	
		var url = urlForServer+"note/archiveNote/"+userId;
		var params ="";
		if(listType!='schedule')
			params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","shareType":"'+archiveNoteShareType+'","shareUserId":"'+sahreUserId+'"}';
		else
			{
			params = '{"listId":"'+listIdFrom+'","eventId":"'+noteIdFrom+'","shareType":"'+archiveNoteShareType+'","shareUserId":"'+sahreUserId+'"}';
			moreActionEvent='archive';
	    	}
	    params = encodeURIComponent(params);
	

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	getListAndNoteNames();
	    	if(listType=='schedule')
	    	{
	    		
	    		if($('#scheduleContent').children('.listDiv').children().attr('class')=='modalHeader')
	        	{
	        		loadEventList(userId,listIdFrom,listType);
	        	}
	        	else
	        	{
	        		loadAllEvents();
	        	}
	    	getTodaySchedule();
	    	}
			//loadTags();
	    	var archiveNoteShareListDel;
	    		if(moreActionEvent=='archive'){
	    			archiveNoteShareListDel=$(moreActionBasedId).parent().parent();
	    			if(noteShowingViewLevel=='book')
	    				$(moreActionBasedId).parent().remove();
	    			else if(noteShowingViewLevel=='note')
	    				$(moreActionBasedId).parent().parent().parent().remove();
	    			
	    			if(archiveNoteShareType != 'noteDiv'){
	    				var checkFlag='false';
	    				$(archiveNoteShareListDel).find('.noteDiv').each(function( index ){
	    					checkFlag='true';
	    				});
	    				if(checkFlag == 'false')
	    					$(archiveNoteShareListDel).parent().remove();
	    			}
	    			
	    			$('#deleteNoteModal').modal('hide');
	    			
	    		}else{
	    			
	    			$(noteDeleteId).parent().parent().parent().parent().parent().parent().parent().parent().remove();
	    		}
	    		$('.js-delete-note').removeAttr("disabled","disabled");
            },
            error: function(e) {
                alert("Please try again later");
            }
	    });
	    loadCalenderss();
		loadNoteAndEvent();
	});
	
	
	/* handles list level Book functionality */
	
	//book individual
	var bookId='';
	$('#notes').on('click','.js-shareBook-individual',function(){
		bookId=$(this).parent().parent().parent().parent().parent().attr('id');
		//alert(bookId);
			$(".noteMenu").popover('hide');	
				var parameter="Book";
				loadMembers(parameter);
	});
	
	
	$('#addMembersToBook').on('click',function()
		{
		var membersList=showTags($('#selectBookMembers').tagit('tags'));
		enterValue=$('#selectBookMembers').children().find('tester').text();
		var existUserIds=$(this).parent().parent().find('#book-previousEntry').find('#previousMember').find('.js-old-share');
		var userid='';
		shareType="Bookindividual";
		
		if(membersList.length > 0 || enterValue!='')
		{
			
		/*for(var i=0; i<existUserIds.length; i++) 
		{
			if(i!=0)
			userid += ','+existUserIds[i].id ;
			else
			userid += existUserIds[i].id;
		}*/
		 
		emailIds='';
		userIds='';
		nonMailIds='';
		var myown='false';
		
		var flag='true';
		
		/////////
		for (var i=0;i<membersList.length;i++)
		{
			var member=membersList[i];
			var userId=membersUserIdMap[member];
			
			if(userId!=null && userId!='')
			{
				emailIds=emailIds +','+membersEmailMap[member];
				userIds=userIds +','+ userId;
			}else
			{
				if(userMailId!=member)
				{
				nonMailIds=nonMailIds+','+member;
				if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(member))){
					flag='false';
					}
				}
				else
				{
				flag='false';
				myown='true';
				}
			}
		}
		
		if(enterValue!='')
		{
		if(userMailId!=enterValue)
		{
		nonMailIds=nonMailIds+','+enterValue;
		if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(enterValue))){
			flag='false';
			}
		}
		else
		{
		flag='false';
		myown='true';
		}
		}
		
		nonMailIds=nonMailIds.substring(1, nonMailIds.length);
		userIds=userIds.substring(1, userIds.length);
		emailIds=emailIds.substring(1, emailIds.length);
		
		
		if(flag == 'true')
		{
		if(listType=='schedule')
		{
			type="Schedule";
		}
		else
		{
			type="member";
		}
		
		$('#memberModelBook').modal('hide');
		$('.share-book-warn').text('Share book as:');
		$('#shareBookWarning').modal('show');
		}
		else
		{
			$('#memberModelBook').modal('toggle');
			$("#moveCopyWarning").modal('show');
			
			if(myown=='true')
				$("#move-modal-message1").text(" You entered your own email ID");
					else
				$("#move-modal-message1").text(" Please enter valid email ID");
			
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#memberModelBook').modal('toggle');},2000);
		}
		}
		else
		{
			$('#memberModelBook').modal('toggle');
			$("#moveCopyWarning").modal('show');
			$("#move-modal-message1").text("Please add contact(s)");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#memberModelBook').modal('toggle');},2000);
		}
		
	});
	
	
$('.js-share-book').on('click',function(){
		
		var level=$(this).attr('value').toLowerCase();
		
		if(shareType=="Bookindividual")
		{
			var url = urlForServer+"note/bookSharedMembers";
			var params = '{"members":"'+userIds+'","nonMailIds":"'+nonMailIds+'","listType":"'+type+'","listId":"'+bookId+'","userId":"'+userId+'","noteLevel":"'+level+'","fullName":"'+userFullNameMail+'","noteName":"'+bookName+'"}';
		}
		if(shareType=="Bookgroup")
		{
			var url = urlForServer+"note/bookSharedgroups";
			var params = '{"groups":"'+groupIds+'","listType":"'+type+'","listId":"'+bookId+'","userId":"'+userId+'","noteLevel":"'+level+'","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","bookName":"'+bookName+'"}';
		}
		if(shareType=="BookallContacts")
		{
			var url = urlForServer+"note/bookShareAllContacts";
			var params = '{"listId":"'+bookId+'","userId":"'+userId+'","listType":"'+type+'","noteLevel":"'+level+'","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","bookName":"'+bookName+'"}';
		}
		params = encodeURIComponent(params);
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
	    	},
			type: 'POST',
			url : url,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:params, 
			dataType: "json",
			success : function(response){
			if(shareType=="Bookindividual")
			{
				if(emailIds.length>0)
				sendMail(userFullNameMail,userFirstName,bookName,'Hi, You have notification pending...','Book',listType,'','',emailIds);
			}
    			$('#shareBookWarning').modal('hide');
    			
        	},
        	error: function(e) {
        		alert("Please try again later");
        	}
    
		});
	});
	
	
	// book group
	$('#notes').on('click','.js-shareBook-group',function(){
		bookId=$(this).parent().parent().parent().parent().parent().attr('id');
		$(".noteMenu").popover('hide');	
		var parameter="Book";
			loadGroups(parameter);
	});
	
	var groupIds='';
	$('#addGroupsToBook').on('click',function(){
		var groupList=showTags($('#selectBookGroups').tagit('tags'));
		var existGroupIds=$(this).parent().parent().find('#modalBook-previousEntry').find('#previousBookGroup').find('.js-old-group');
		var groupId='';
		var newGroupId='';
		shareType="Bookgroup";
		if(groupList.length>0)
		{
		for(var i=0; i<existGroupIds.length; i++) 
		{
			if(i!=0)
				groupId += ','+existGroupIds[i].id ;
			else
				groupId += existGroupIds[i].id;
		}
		 
		
		if((groupId!=null && groupId.length>0) && (groupList!=null && groupList.length>0))
			groupIds=groupId +',';
		else
			groupIds=groupId;
		////////////
		var flag='true';
		for (var i=0;i<groupList.length;i++)
		{
			if(i!=0)
			{
				groupIds=groupIds+',';
				newGroupId=newGroupId+',';
			}			
			var group=groupList[i];	
			var ids=membersGroupIdMap[group];
			if(ids!=null && ids!="" && ids!=undefined)
			{
				groupIds=groupIds + ids;
				newGroupId=newGroupId+ids;
			}
			else
				flag='false';
		}
		
		if(flag == 'true'){
		
		if(listType=='schedule')
		{
			type="Schedule";
		}
		else
		{
			type="member";
		}
		
		
		var url1 = urlForServer+"group/getGroupMembers/";
		var param = '{"groups":"'+newGroupId+'"}';
   
		param = encodeURIComponent(param);

		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn			
	    	},
			type: 'POST',
			url : url1,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:param, 
			dataType: "json",
			success : function(response)
			{
			//var data = jQuery.parseJSON(response);
			if(response['groupId']==0)
			{
				$('#shareGroupModelBook').modal('toggle');
				$('.share-book-warn').text('Share book as');
				$('#shareBookWarning').modal('toggle');
		
		/*var listId=$(addGroupsId).parent().parent().parent().attr('id');
		var listId=bookId;
		var url = urlForServer+"note/bookSharedgroups";
		var params = '{"groups":"'+groupIds+'","listType":"'+type+'","listId":"'+listId+'","userId":"'+userId+'","fullName":"'+listOwnerFullName+'","firstName":"'+listOwnerFirstName+'","bookName":"'+bookName+'"}';
	    params = encodeURIComponent(params);
	
	    $.ajax({
	    		headers: { 
		    	"Ajax-Call" : userTokens,
		    	"Ajax-Time" :userLoginTime				
		    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    		$('#shareGroupModelBook').modal('hide');
	    		
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });*/
			}else{
				
				var gIds=response['groupId'];
				
				$('#shareGroupModelBook').modal('toggle');
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text(gIds +" is empty. Please select group(s) with contacts");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#shareGroupModelBook').modal('toggle');},4000);
			}
			},
			error: function(e) {
        		alert("Please try again later");
        	}
		});
		}else{
			$('#shareGroupModelBook').modal('toggle');
			$("#moveCopyWarning").modal('show');
			$("#move-modal-message1").text("Please select valid Group");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#shareGroupModelBook').modal('toggle');},2000);
		}
		}
		else
		{
			$('#shareGroupModelBook').modal('toggle');
			$("#moveCopyWarning").modal('show');
			$("#move-modal-message1").text("Please select Group(s) to share with");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#shareGroupModelBook').modal('toggle');},2000);
		}
		
	});
	
	//book all contacts
	$('#notes').on('click','.js-shareBook-allContact',function(){
		
		bookId=$(this).parent().parent().parent().parent().parent().attr('id');
		var response="";
		var url = urlForServer+"user/memberList/"+userId;
		var Type='';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
		type : 'POST',
		url : url,
		success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null)
			{
				$('#copyBookToContactModal').modal('toggle');
			}
			else
			{
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text("Please select contact(s) to share with");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			}
		
		},
		error :function(e)
		{
			alert("please try again later");
		}
		});	
		
		
	});
	
	
	
	$('#bookAllConYes').on('click',function(){
		
		listId=bookId;
		if(listType=='schedule')
		{
			type="Schedule";
		}
		else
		{
			type="member";
		}
		shareType="BookallContacts";
		$('#copyBookToContactModal').modal('toggle');
		$('.share-book-warn').text('Sharing a book with');
		$('#shareBookWarning').modal('toggle');
		
		/*var url = urlForServer+"note/bookShareAllContacts";
		var params = '{"listId":"'+listId+'","userId":"'+userId+'","listType":"'+type+'","fullName":"'+listOwnerFullName+'","firstName":"'+listOwnerFirstName+'","bookName":"'+bookName+'"}';
	    params = encodeURIComponent(params);
	    $.ajax({
	    		headers: { 
		    	"Ajax-Call" : userTokens,
		    	"Ajax-Time" :userLoginTime				
		    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	$('#copyBookToContactModal').modal('hide');
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });*/
	});
	
 // del note shared all contacts	
$('#notes').on('click','.js-members-delcontact',function(){
	var listId;
	var noteId;
	if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
		memberShareAllCon=moreActionBasedId;
		noteId=noteIdFromList;//$(memberShareAllCon).parent().attr('id');
		listId=listIdFromList;//$(memberShareAllCon).parent().parent().parent().attr('id');
		$('#moreActions').modal('hide');
		moreActionEvent='memberAllContact';
	}else{
		memberShareAllCon=$(this).parent().parent().parent().parent().parent().parent().parent();
		listId=$(memberShareAllCon).parent().parent().parent().attr('id');
		noteId=$(memberShareAllCon).parent().attr('id');
		$(".noteMenu").popover('hide');
	}
	$('#shareAllContactDelModal').modal('toggle');
	
});




$('#memAllConDelYes').on('click',function(){
	var noteId=noteIdFromList;//$(memberShareAllCon).parent().attr('id');
	var listId=listIdFromList;//$(memberShareAllCon).parent().parent().parent().attr('id');
	
	var url = urlForServer+"note/delSharedAllConNote";
	
	if(listType=='schedule')
	{
		type="Schedule";
	}
	else
	{
		type="member";
	}
	var params = '{"listId":"'+listId+'","noteId":"'+noteId+'","userId":"'+userId+'","listType":"'+type+'"}';
    params = encodeURIComponent(params);


    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn					
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
    	
    	$('#shareAllContactDelModal').modal('hide');
    		if(moreActionEvent=='memberAllContact'){
    			checkOwnerOfList();
    			//$('#moreActions').modal('toggle');
    			moreActionEvent='';
    		}
    		
        },
        error: function(e) {
            alert("Please try again later");
        }
    });
});

	
	
	// del book all contact shared
$('#notes').on('click','.js-sharedBook-allContact',function(){
		
		bookId=$(this).parent().parent().parent().parent().parent().attr('id');
			//listId=$(memberShareAllCon).parent().parent().parent().attr('id');
			//$(".listMenu").popover('hide');
		$('#copyBookToContactDel').modal('toggle');
		
	});
	

		$('#bookAllConDelYes').on('click',function(){
				listId=bookId;
				if(listType=='schedule')
				{
						type="Schedule";
				}
				else
				{
					type="member";
				}
	
				var url = urlForServer+"note/delBookSharedAllCon";
				var params = '{"listId":"'+listId+'","userId":"'+userId+'","listType":"'+type+'"}';

				params = encodeURIComponent(params);

				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
			    	},
					type: 'POST',
					url : url,
					cache: false,
					contentType: "application/json; charset=utf-8",
					data:params, 
					dataType: "json",
					success : function(response){
					$('#copyBookToContactDel').modal('hide');
    		
				},
				error: function(e) {
					alert("Please try again later");
				}
    
				});
		});

	
	/*Handles add members functionality*/
	var availableMemberTags = new Array();
	var availableMemberCopy =new Array();
	var membersEmailMap = {};
	var membersUserIdMap = {};
	
	$('#notes').on('click','.js-members',function(){
		$('#shareindiviDiv').attr('style','display:none');
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			addMembersId=moreActionBasedId;
			$('#moreActions').modal('hide');
			moreActionEvent='memberIndividual';
			
		}else{
			addMembersId=$(this).parent().parent().parent().parent().parent().parent().parent();
			$(".noteMenu").popover('hide');	
		}
		
		var parameter="Share";
		loadMembers(parameter);
	});
	
	
	
	var availableGroupTags = new Array();
	var availableGroupTagsForCopy = new Array();
	var membersGroupIdMap = {};
	$('#notes').on('click','.js-members-group',function(){
		$('#sharegropDiv').attr('style','display:none');
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			addGroupsId=moreActionBasedId;
			$('#moreActions').modal('hide');
			moreActionEvent='memberGroup';
		}else{
			addGroupsId=$(this).parent().parent().parent().parent().parent().parent().parent();
			$(".noteMenu").popover('hide');	
		}
		var parameter="Share";
		loadGroups(parameter);
	});
	
	
	/*var emailIds='';
	
	var type='';
	var shareType='';*/
	var userIds='';
	var nonMailIds='';
	var enterValue='';
	/*Called when add member is clicked*/
	$('#addMembers').on('click',function(){
		var membersList=showTags($('#selectNoteMembers').tagit('tags'));
		
		enterValue=$('#selectNoteMembers').children().find('tester').text();
		var existUserIds=$(this).parent().parent().find('#modal-previousEntry').find('#previous').find('.js-old-share');
		
		var userid='';
		if(membersList.length > 0 || enterValue!='')
		{
			
			/*for(var i=0; i<existUserIds.length; i++) 
			{
				if(i!=0)
				userid += ','+existUserIds[i].id ;
				else
				userid += existUserIds[i].id;
			}*/
			 
			emailIds='';
			userIds='';
			nonMailIds='';
			var myown='false';
			
			if((userid!=null && userid.length>0) && (membersList!=null && membersList.length>0))
				userIds=userid +',';
			else
				userIds=userid;
			
			var flag='true';
			
			for (var i=0;i<membersList.length; i++)
			{
				var member=membersList[i];	
				var usersId=membersUserIdMap[member];
				if(usersId!=null && usersId!='')
				{
					emailIds=emailIds +','+membersEmailMap[member];
					userIds=userIds +','+ usersId;
				}else
				{
					if(userMailId!=member)
					{
					nonMailIds=nonMailIds+','+member;
					if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(member))){
						flag='false';
						}
					}
					else
					{
					flag='false';
					myown='true';
					}
						
				}
				
				/*if(i!=0)
				{
					emailIds=emailIds+',';
					userIds=userIds+',';
				}	*/		
			}
			
			//////////////
			if(enterValue!='')
			{
			if(userMailId!=enterValue)
			{
			nonMailIds=nonMailIds+','+enterValue;
			if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(enterValue))){
				flag='false';
				}
			}
			else
			{
			flag='false';
			myown='true';
			}
			}
			////////////
			
			nonMailIds=nonMailIds.substring(1, nonMailIds.length);
			userIds=userIds.substring(1, userIds.length);
			emailIds=emailIds.substring(1, emailIds.length);
			
			// flag removed to enter unknown user
			if(flag == 'true'){
				if(listType=='schedule'){
					type="Schedule";
				}else{
					type="member";
				}
				
				shareType="individual";
				
				if(listType!='schedule'){
					$('#memberModel').modal('hide');
					$('.share-note-warn').text('Share note as:');
					$('#shareWarning').modal('show');
				}
				
				if(listType=='schedule'){
					var tempNoteName = noteNameFromList;
					while(tempNoteName.indexOf("\"") != -1){
						tempNoteName = tempNoteName.replace("\"", "`*`");
					}
					var noteId=$(addMembersId).parent().attr('id');
					var listId=$(addMembersId).parent().parent().parent().attr('id');
					var url=urlForServer +"note/members/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
					var params = '{"members":"'+userIds+'","nonMailIds":"'+nonMailIds+'","listType":"'+type+'","noteLevel":"view","fullName":"'+userFullNameMail+'","noteName":"'+tempNoteName+'"}';
					params = encodeURIComponent(params);
		
					$.ajax({
						headers: { 
						"Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn				
				    	},
						type: 'POST',
						url : url,
						cache: false,
						contentType: "application/json; charset=utf-8",
						data:params, 
						dataType: "json",
						success : function(response)
						{
						if(emailIds.length>0)
							sendMail(userFullNameMail,userFirstName,tempNoteName,'Hi, You have notification pending...','Note',listType,listIdFromList,noteIdFromList,emailIds);
			    			
						$('#memberModel').modal('hide');
			    			if(moreActionEvent=='memberIndividual'){
			    				$('#moreActions').modal('toggle');
			    				moreActionEvent='';
			    			}
		            	},
		            	error: function(e) {
		            		alert("Please try again later");
		            	}
		        
					});
				}
			}else{
				
				if(myown=='true')
					$("#shareindiviDiv").text("You entered your own email ID");
						else
					$("#shareindiviDiv").text("Please enter valid email ID");
				$('#shareindiviDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
				
				/*$('#memberModel').modal('toggle');
				$("#moveCopyWarning").modal('show');
				//$("#move-msg-header-col-md-1").attr('class',"label label-info").text("Warning");
				if(myown=='true')
				$("#move-modal-message1").text("You entered your own email ID");
					else
				$("#move-modal-message1").text("Please enter valid email ID");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#memberModel').modal('toggle');},2000);*/
			}
		}
		else
		{
			$('#shareindiviDiv').text(" Please enter contact(s) to share with");
			$('#shareindiviDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
			/*$('#memberModel').modal('toggle');
			$("#moveCopyWarning").modal('show');
			//$("#move-msg-header-col-md-1").attr('class',"label label-info").text("Warning");
			$("#move-modal-message1").text("Please select contact(s) to share with");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#memberModel').modal('toggle');},2000);*/
		}
		
	});
	
	$('.js-share').on('click',function(){
		var level=$(this).attr('value').toLowerCase();
		var tempNoteName = noteNameFromList;
		while(tempNoteName.indexOf("\"") != -1){
			tempNoteName = tempNoteName.replace("\"", "`*`");
		}
		//var noteId=$(addMembersId).parent().attr('id');
		//var listId=$(addMembersId).parent().parent().parent().attr('id');
		if(shareType=="individual")
		{
			var url = urlForServer +"note/members/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
			var params = '{"members":"'+userIds+'","nonMailIds":"'+nonMailIds+'","listType":"'+type+'","noteLevel":"'+level+'","fullName":"'+userFullNameMail+'","noteName":"'+tempNoteName+'"}';
			
		}
		if(shareType=="group")
		{
			var url = urlForServer+"note/groups/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
			var params = '{"groups":"'+groupIds+'","listType":"'+type+'","noteLevel":"'+level+'","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+tempNoteName+'"}';
		}
		if(shareType=="allContacts")
		{
			var url = urlForServer+"note/shareAllContacts";
			var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","userId":"'+userId+'","listType":"'+type+'","noteLevel":"'+level+'","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+tempNoteName+'"}';
		}
		params = encodeURIComponent(params);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
			type: 'POST',
			url : url,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:params, 
			dataType: "json",
			success : function(response){
			if(shareType=="individual")
			{
			if(emailIds.length>0)
				sendMail(userFullNameMail,userFirstName,tempNoteName,'Hi, You have notification pending...','Note',listType,listIdFromList,noteIdFromList,emailIds);
			}
    			$('#shareWarning').modal('hide');
    			
    			if(moreActionEvent=='memberIndividual'){
    				$('#moreActions').modal('show');
    			}
    			if(moreActionEvent=='memberGroup')
					$('#moreActions').modal('show');
    			
    			if(moreActionEvent=='memberAllContact')
    				checkOwnerOfList();
    			
    				moreActionEvent='';
    				
        	},
        	error: function(e) {
        		alert("Please try again later");
        	}
    
		});
	});
	
	var groupIds='';
	/*Called when add group is clicked*/
	$('#addGroups').on('click',function(){
		var groupList=showTags($('#selectNoteGroups').tagit('tags'));
		var existGroupIds=$(this).parent().parent().find('#modal-previousEntry').find('#previousGroup').find('.js-old-group');
		var groupId='';
		var newGroupId='';
		shareType="group";
		if(groupList.length > 0)
		{
		/*for(var i=0; i<existGroupIds.length; i++) 
		{
			if(i!=0)
				groupId +=','+existGroupIds[i].id ;
			else
				groupId += existGroupIds[i].id;
		}*/
		 
		
		if((groupId!=null && groupId.length>0) && (groupList!=null && groupList.length>0))
			groupIds=groupId +',';
		else
			groupIds=groupId;
		////////
		var flag='true';
		for (var i=0;i<groupList.length;i++)
		{
			if(i!=0)
			{
				groupIds=groupIds+',';
				newGroupId=newGroupId+',';
			}			
			var group=groupList[i];	
			var ids=membersGroupIdMap[group];
			if(ids!=null && ids!="")
			{
				groupIds=groupIds + ids;
				newGroupId=newGroupId+ids;
			}
			else
				flag='false';
		}
		if(flag == 'true'){
			if(listType=='schedule'){
				type="Schedule";
			}else{
				type="member";
			}
			
			var url1 = urlForServer+"group/getGroupMembers/";
			var param = '{"groups":"'+newGroupId+'"}';
	   
			param = encodeURIComponent(param);

			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type: 'POST',
				url : url1,
				cache: false,
				contentType: "application/json; charset=utf-8",
				data:param, 
				dataType: "json",
				success : function(response)
				{
				//var data = jQuery.parseJSON(response);
				if(response['groupId']==0)
				{
					if(listType!='schedule')
					{
					$('#shareGroupModel').modal('toggle');
					$('.share-note-warn').text('Share note as:');
					$('#shareWarning').modal('toggle');
					}
					
					if(listType=='schedule')
					{
					var noteId=$(addGroupsId).parent().attr('id');
					var listId=$(addGroupsId).parent().parent().parent().attr('id');
					//var url =urlForServer+"note/groups/"+listId+"/"+noteId+"/"+userId;
					//var params = '{"groups":"'+groupIds+'","listType":"'+type+'","fullName":"'+listOwnerFullName+'","firstName":"'+listOwnerFirstName+'","noteName":"'+noteNameFromList+'"}';
					
					var url = urlForServer+"note/groups/"+listIdFromList+"/"+noteIdFromList+"/"+userId;
					var params = '{"groups":"'+groupIds+'","listType":"'+type+'","noteLevel":"view","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+noteNameFromList+'"}';
					
					params = encodeURIComponent(params);

					$.ajax({
						headers: { 
						"Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn					
				    	},
						type: 'POST',
						url : url,
						cache: false,
						contentType: "application/json; charset=utf-8",
						data:params, 
						dataType: "json",
						success : function(response){
							$('#shareGroupModel').modal('hide');
							if(moreActionEvent=='memberGroup'){
								$('#moreActions').modal('toggle');
								moreActionEvent='';
							}
					},
					error: function(e) {
						alert("Please try again later");
					}
					});
					}
				}else{
					
					var gIds=response['groupId'];
					$('#sharegropDiv').text(gIds +" is empty. Please select group(s) with contacts");
					$('#sharegropDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
					/*$('#shareGroupModel').modal('toggle');
					$("#moveCopyWarning").modal('show');
					$("#move-modal-message1").text(gIds +" is empty");
					setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#shareGroupModel').modal('toggle');},4000);*/
				}
				},
				error: function(e) {
            		alert("Please try again later");
            	}
			});
			
		}else{
			$('#sharegropDiv').text("Please select valid Group");
			$('#sharegropDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
			/*$('#shareGroupModel').modal('toggle');
			$("#moveCopyWarning").modal('show');
			$("#move-modal-message1").text("Please select valid Group");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#shareGroupModel').modal('toggle');},2000);*/
		}
		}else
		{
			$('#sharegropDiv').text("Please select Group(s) to share with");
			$('#sharegropDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
				/*$('#shareGroupModel').modal('toggle');
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text("Please select group(s) for sharing");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#shareGroupModel').modal('toggle');},2000);*/
		}
		
	});
	
	
	
	
	var memberName;
	var usersArray='';
	
	function loadMembers(parameter)
	{
	
		var response="";
		var url = urlForServer+"user/memberList/"+userId;
		
		var Type='';
		
		if(listType == 'schedule')
			Type="schedule";
			else
				Type="notes";	
			
		availableMemberTags=new Array();
		availableMemberCopy=new Array();
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
		type : 'POST',
		url : url,
		success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			membersEmailMap = {};
			membersUserIdMap = {};
			
			if(parameter=="Copy")
			{
				$('#selectCopyNoteIndiviual').remove();
				$('#copyIndiviualModel').children('.modal-dialog').children('.modal-content').children('.modal-body').children().children('.row').append('<ul id="selectCopyNoteIndiviual" style="font-family: Helvetica Neue;font-size:14px;" class="span"></ul>');
				
			}
			else if(parameter=="Book")
			{
				$('#selectBookMembers').remove();
				$('#memberModelBook').children('.modal-dialog').children('.modal-content').children('.modal-body').children().children().append('<ul id="selectBookMembers" class="span"></ul>');
				
			}
			else
			{
				$('#selectNoteMembers').remove();
				$('#memberModel').children('.modal-dialog').children('.modal-content').children('.modal-body').children().children('.row').append('<ul id="selectNoteMembers" style="font-family: Helvetica Neue;font-size:14px;" class="span"></ul>');
			}
			
			
			$('#previous').find('.js-old-share').each(function( index ){
				$(this).remove();
			});
			
			$('#previous').find('.js-clicked').each(function( index ){
				$(this).remove();
			});
			
			$('#previousMember').find('.js-old-share').each(function( index ){
				$(this).remove();
			});
			

			$('#previousMember').find('.js-clicked').each(function( index ){
				$(this).remove();
			});
			
			$('#copyPreviousMember').find('.js-old-share').each(function( index ){
				$(this).remove();
			});
			
			$('#copyPreviousMember').find('.js-clicked').each(function( index ){
				$(this).remove();
			});
			
			
			/////////////////////////////////////////
			//if(data!=null)
			//{
			var addedUserIds='';
			var listId=$('#notebookId').val();
			var noteId=$(addMembersId).parent().attr('id');
			if(listId !='note'){
				listId =listIdFromList;
				noteId =noteIdFromList;
			}else if(listId =='note' && noteId==undefined){
				listId =listIdFromList;
				noteId =noteIdFromList;
			}else{
				listId=$(addMembersId).parent().parent().parent().attr('id');
				noteId=$(addMembersId).parent().attr('id');
			}
			if(parameter=="Book")
			{
				listId=bookId;
				var url1=urlForServer+"note/listDetails";
				var params = '{"listId":"'+listId+'","type":"member"}';
				
			}
			else
			{
				var url1=urlForServer+"note/noteDetails";
				var params = '{"listId":"'+listId+'","noteId":"'+noteId+'","listType":"'+Type+'","userId":"'+userId+'"}';
			}
			params = encodeURIComponent(params);
			var availableuserId=new Array();
			$.ajax({	
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url1,
				data:params,
				
				success:function(responseText) 
				{
					response=responseText;
					if(parameter!="Book")
					{
					var data1 = jQuery.parseJSON(response);
					if(data1!=null)
					{
						if(parameter=="Copy")
						{
						addedUserIds =data1.copyToMember;
						}
						else
						{
							if(listType=='schedule')
								addedUserIds=data1.eventMembers;
								else
								addedUserIds =data1.notesMembers;
						}
					if(addedUserIds.indexOf("[") != -1){
						addedUserIds = addedUserIds.substring(addedUserIds.indexOf("[")+1,addedUserIds.length);
					}
					if(addedUserIds.indexOf("]") != -1){
						addedUserIds = addedUserIds.substring(0,addedUserIds.indexOf("]"));
					}
					addedUserIds=addedUserIds.replace(/\s/g, "");
					usersArray = addedUserIds.split(',');
					
					}
					}else
					{
						if(response.indexOf("[") != -1){
							response = response.substring(response.indexOf("[")+1,response.length);
						}
						if(response.indexOf("]") != -1){
							response = response.substring(0,response.indexOf("]"));
						}
						response=response.replace(/\s/g, "");
						usersArray = response.split(',');
					}
					if(data!=null)
					{
						var p=0;
						for ( var i = 0; i < data.length; i++) 
						{
							memberName = data[i].userFirstName +' '+data[i].userLastName;
							membersEmailMap[memberName] = data[i].emailId;
							membersUserIdMap[memberName] = data[i].userId;
							availableMemberCopy.push(memberName);
							var obj = data[i];	
							
							var datacont=null;
							if(usersArray!=null && usersArray.length>0)
							{
								//////
								if(usersArray.indexOf(data[i].userId) == -1)
								{
									memberName = data[i].userFirstName +' '+data[i].userLastName;
									membersEmailMap[memberName] = data[i].emailId;
									membersUserIdMap[memberName] = data[i].userId;
									availableMemberTags.push(memberName);
								}
								else
								{
									p=p+1;
									memberName = data[i].userFirstName +' '+data[i].userLastName;
									if(p<4)
									datacont='<a class="js-old-share" id='+data[i].userId+' style="color:brown; font-style:italic;" href="javascript:void(0);">' + memberName + ' <a id='+data[i].userId+' class="js-old-share">&times;</a> </a>';
									if(p==3)
										datacont='<a class="js-old-share js-clicked" id='+data[i].userId+' style="color:brown; font-style:italic;" href="javascript:void(0);" >' + memberName + '<a id='+data[i].userId+' class="js-old-share">&times;</a> <label title="click to see more" class="js-clicked">More...</label> </a>';	
								}
								
								//////
								
									if(parameter=="Copy")
										$('#copyPreviousMember').append(datacont);
									else if(parameter=="Book")
										$('#previousMember').append(datacont);
									else
										$('#previous').append(datacont);
							}else
							{
								memberName = data[i].userFirstName +' '+data[i].userLastName;
								membersEmailMap[memberName] = data[i].emailId;
								membersUserIdMap[memberName] = data[i].userId;
								availableMemberTags.push(memberName);
							}
						}
					}
					
					
					if(parameter=="Copy")
					{
						$('#copyIndiviualModel').modal('toggle');
						$(function() {
							$('#selectCopyNoteIndiviual').tagit( {
								tagSource : availableMemberCopy,
								triggerKeys : [ 'enter', 'comma', 'tab' ]
							});
						});
					}
					else if(parameter=="Book")
					{
						$('#memberModelBook').modal('toggle');
						$(function() {
							$('#selectBookMembers').tagit( {
								tagSource : availableMemberTags,
								triggerKeys : [ 'enter', 'comma', 'tab' ]
							});
						});
					
					}
					else
					{
						$('#memberModel').modal('toggle');
					$(function() {
						$('#selectNoteMembers').tagit( {
							tagSource : availableMemberTags,
							triggerKeys : [ 'enter', 'comma', 'tab' ]
						});
					});
					}
					
				},
				error : function() {
					console.log("<-------error returned while retrieving members list-------> ");
				}
			});
			/*}
			else
			{
				$("#moveCopyWarning").modal('show');
				$("#move-msg-header-col-md-1").attr('class',"label label-info").text("Warning");
				$("#move-modal-message1").attr('class',"label label-info").text("  Please Add Member");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			}*/
			
			/////////////////////////////////////////////
		},
		error : function() {
			console.log("<-------error returned while retrieving members list-------> ");
		}
	});
	}
	
	// load all member
	$('#notes').on('click','.js-clicked',function(){
		parameter="Share";
		var response="";
		
		$('#previous').find('.js-old-share').each(function( index ){
			$(this).remove();
		});
		
		$('#previous').find('.js-clicked').each(function( index ){
			$(this).remove();
		});
		
		$('#previousMember').find('.js-old-share').each(function( index ){
			$(this).remove();
		});
		
		$('#previousMember').find('.js-clicked').each(function( index ){
			$(this).remove();
		});
		
		$('#copyPreviousMember').find('.js-old-share').each(function( index ){
			$(this).remove();
		});
		
		$('#copyPreviousMember').find('.js-clicked').each(function( index ){
			$(this).remove();
		});
		
		var url = urlForServer+"user/memberList/"+userId;
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			url : url,
			success : function(responseText) {
				var data = jQuery.parseJSON(responseText);
		
		if(data!=null)
		{
			for ( var i = 0; i < data.length; i++) 
			{
				memberName = data[i].userFirstName +' '+data[i].userLastName;
			
				var datacont=null;
				if(usersArray!=null && usersArray.length>0)
				{
					//////
					if(usersArray.indexOf(data[i].userId) == -1)
					{
						
					}
					else
					{
						memberName = data[i].userFirstName +' '+data[i].userLastName;
						datacont='<a class="js-old-share" id='+data[i].userId+' style="color:brown; font-style:italic;  "  href="javascript:void(0);">' + memberName + ' <a id='+data[i].userId+' class="js-old-share">&times;</a> </a>';
					}
					//////
						
							$('#copyPreviousMember').append(datacont);
					
							$('#previousMember').append(datacont);
					
							$('#previous').append(datacont);
				}else
				{
					
				}
			}
		}
		},error :function(){}
		});
	});
	///////////
		
	var groupName;
	var groupsArray='';
	function loadGroups(parameter)
	{
		var noteId=$(addGroupsId).parent().attr('id');
		var listId=$(addGroupsId).parent().parent().parent().attr('id');
		var response='';
		var Type='';
		
		if(listType=='schedule')
			Type="schedule";
			else
				Type="notes";
		
		
		groupId="1";  
		criteria="loginUserId";
		var param = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+userId+"\",\"criteria\":\""+criteria+"\",\"groupName\":\"groupName\",\"groupDetails\":[{\"userId\":\"userId\",\"startDate\":\"startDate\",\"endDate\":\"endDate\",\"status\":\"status\" }]}";
		param = encodeURIComponent(param);
		var url = urlForServer+"group/getGroupInfo";
		
		availableGroupTags=new Array();
		availableGroupTagsForCopy=new Array();
		
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
		type : 'POST',
		url : url,
		data:param,
		success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			
			if(parameter=="Copy")
			{
				$('#selectCopyNoteGroups').remove();
				$('#copyGroupModel').children('.modal-dialog').children('.modal-content').children(".modal-body").children().children('.row').append('<ul id="selectCopyNoteGroups" style="font-family: Helvetica Neue;font-size:14px;" class="span"></ul>');
			}
			else if(parameter=="Book")
			{
				$('#selectBookGroups').remove();
				$('#shareGroupModelBook').children('.modal-dialog').children('.modal-content').children('.modal-body').children().children().append('<ul id="selectBookGroups" class="span"></ul>');
			}
			else
			{
				$('#selectNoteGroups').remove();
				$('#shareGroupModel').children('.modal-dialog').children('.modal-content').children('.modal-body').children().children('.row').append('<ul id="selectNoteGroups" style="font-family: Helvetica Neue;" class="span"></ul>');
			}
			
			$('#previousGroup').find('.js-old-group').each(function( index ){
				$(this).remove();
			});
			
			$('#previousGroup').find('.js-group-clicked').each(function( index ){
				$(this).remove();
			});
			
			$('#previousBookGroup').find('.js-old-group').each(function( index ){
				$(this).remove();
			});
			
			$('#previousBookGroup').find('.js-group-clicked').each(function( index ){
				$(this).remove();
			});
			
			$('#copyPreviousGroup').find('.js-old-group').each(function( index ){
				$(this).remove();
			});
			
			$('#copyPreviousGroup').find('.js-group-clicked').each(function( index ){
				$(this).remove();
			});
			
			membersGroupIdMap = {};
			
			/////////////////////////////////////////////
			
			//*** for associate note **//
			if(listId !='note'){
				listId =listIdFromList;
				noteId =noteIdFromList;
			}else if(listId =='note' && noteId==undefined){
				listId =listIdFromList;
				noteId =noteIdFromList;
			}else{
				listId=$(addMembersId).parent().parent().parent().attr('id');
				noteId=$(addMembersId).parent().attr('id');
			}
			//** ------------------- **//
			
			if(data!=null)
			{
			var addedGroupIds='';
			if(parameter=="Book")
			{
				listId=bookId;
				var url1=urlForServer+"note/listDetails";
				var params = '{"listId":"'+listId+'","type":"group"}';
			}
			else
			{
				var url1=urlForServer+"note/noteDetails";
				var params = '{"listId":"'+listId+'","noteId":"'+noteId+'","listType":"'+Type+'","userId":"'+userId+'"}';
			}
			
			params = encodeURIComponent(params);
			
			$.ajax({	
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url1,
				data:params,

				success:function(responseText) 
				{
				response=responseText;
				if(parameter!="Book")
				{
					var data1 = jQuery.parseJSON(response);
					if(data1!=null)
					{
						if(parameter=="Copy")
						{
							addedGroupIds = data1.copyToGroup;
						}else
						{
							if(listType=='schedule')
								addedGroupIds = data1.eventGroups;
							else
								addedGroupIds = data1.noteGroups;
						}
					if(addedGroupIds.indexOf("[") != -1){
						addedGroupIds = addedGroupIds.substring(addedGroupIds.indexOf("[")+1,addedGroupIds.length);
					}
					if(addedGroupIds.indexOf("]") != -1){
						addedGroupIds = addedGroupIds.substring(0,addedGroupIds.indexOf("]"));
					}
					addedGroupIds=addedGroupIds.replace(/\s/g, "");
					groupsArray = addedGroupIds.split(',');
					}
				}
					else
					{
						if(response.indexOf("[") != -1){
							response = response.substring(response.indexOf("[")+1,response.length);
						}
						if(response.indexOf("]") != -1){
							response = response.substring(0,response.indexOf("]"));
						}
						response=response.replace(/\s/g, "");
						groupsArray = response.split(',');
					}
				
				if(data!=null)
				{
					var p=0;
				for ( var i = 0; i < data.length; i++) 
				{
					groupName = data[i].groupName;
					membersGroupIdMap[groupName] = data[i].groupId;
					availableGroupTagsForCopy.push(groupName);
					
					var obj = data[i];	
					var datacont=null;
					if(groupsArray!=null && groupsArray.length>0)
					{
					//////
						if(groupsArray.indexOf(data[i].groupId) == -1)
						{
							groupName = data[i].groupName;
							membersGroupIdMap[groupName] = data[i].groupId;
							availableGroupTags.push(groupName);
						}
						else
						{
							p=p+1;
							groupName = data[i].groupName;
							if(p<4)
								datacont='<a class="js-old-group" id='+data[i].groupId+' style="color:brown; font-style:italic;" href="javascript:void(0);">' + groupName + ' <a id='+data[i].groupId+' class="js-old-group">&times;</a> </a>';
							if(p==3)
								datacont='<a class="js-old-group" id='+data[i].groupId+' style="color:brown; font-style:italic;" href="javascript:void(0);" >' + groupName + '<a id='+data[i].groupId+' class="js-old-group">&times;</a> <label title="click to see more" class="js-group-clicked">More...</label> </a>';	
						}
						
						if(parameter=="Copy")
							$('#copyPreviousGroup').append(datacont);
						
						else if(parameter=="Book")
							$('#previousBookGroup').append(datacont);
						
						else
							$('#previousGroup').append(datacont);
						//////
						}else
					{
						groupName = data[i].groupName;
						membersGroupIdMap[groupName] = data[i].groupId;
						availableGroupTags.push(groupName);
					}
				}
				}
				
				if(parameter=="Copy")
				{
					$('#copyGroupModel').modal('toggle');
					$(function() {
						$('#selectCopyNoteGroups').tagit( {
							tagSource : availableGroupTagsForCopy,
							triggerKeys : [ 'enter', 'comma', 'tab' ]
						});
					});
				}
				else if(parameter=="Book")
				{
					$('#shareGroupModelBook').modal('toggle');
					$(function() {
						$('#selectBookGroups').tagit( {
							tagSource : availableGroupTags,
							triggerKeys : [ 'enter', 'comma', 'tab' ]
						});
					});
				}
				else
				{
					$('#shareGroupModel').modal('toggle');
				$(function() {
					$('#selectNoteGroups').tagit( {
						tagSource : availableGroupTags,
						triggerKeys : [ 'enter', 'comma', 'tab' ]
					});
				});
				}
				},
				error : function() {
					console.log("<-------error returned while retrieving Groups list-------> ");
				}
			});
			}else
			{
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text(" No group exists. Please create group(s) on Contacts page");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			
			}
			////////////////////////////////////////////
		},
		error : function() {
			console.log("<-------error returned while retrieving Groups list-------> ");
		}
	});
	}
	
	// load all groups
	$('#notes').on('click','.js-group-clicked',function(){
		var response="";
		
		$('#previousGroup').find('.js-old-group').each(function( index ){
			$(this).remove();
		});
		
		$('#previousGroup').find('.js-group-clicked').each(function( index ){
			$(this).remove();
		});
		
		$('#previousBookGroup').find('.js-old-group').each(function( index ){
			$(this).remove();
		});
		
		$('#previousBookGroup').find('.js-group-clicked').each(function( index ){
			$(this).remove();
		});
		
		$('#copyPreviousGroup').find('.js-old-group').each(function( index ){
			$(this).remove();
		});
		
		$('#copyPreviousGroup').find('.js-group-clicked').each(function( index ){
			$(this).remove();
		});
		
		var param = "{\"groupId\":\""+groupId+"\",\"loginUserId\":\""+userId+"\",\"criteria\":\""+criteria+"\",\"groupName\":\"groupName\",\"groupDetails\":[{\"userId\":\"userId\",\"startDate\":\"startDate\",\"endDate\":\"endDate\",\"status\":\"status\" }]}";
		param = encodeURIComponent(param);
		var url = urlForServer+"group/getGroupInfo";
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			url : url,
			data:param,
			success : function(responseText) {
				var data = jQuery.parseJSON(responseText);
		
		if(data!=null)
		{
			for ( var i = 0; i < data.length; i++) 
			{
				groupName = data[i].groupName;
				
				var datacont=null;
				if(groupsArray!=null && groupsArray.length>0)
				{
				//////
					if(groupsArray.indexOf(data[i].groupId) == -1)
					{}
					else
					{
						groupName = data[i].groupName;
						datacont='<a class="js-old-group" id='+data[i].groupId+' style="color:brown; font-style:italic;" href="javascript:void(0);">' + groupName + ' <a id='+data[i].groupId+' class="js-old-group">&times;</a> </a>';
					}
					
						$('#copyPreviousGroup').append(datacont);
					
						$('#previousBookGroup').append(datacont);
					
						$('#previousGroup').append(datacont);
					//////
					}else
				{}
			}
			}
		},error :function(){}
		});
	});
	///////////
	
	
	//delete for member in book list indijival
	
	$("ul#previousMember").on('click','a',function() {
		
		//var listId=$(addMembersId).parent().parent().parent().attr('id');
		var listId=bookId;
		var url = urlForServer+"note/delBookmembers";
		var userIds=this.id;
		
		
		var params = '{"members":"'+userIds+'","listId":"'+listId+'","userId":"'+userId+'"}';
		//'{"listId":"'+listId+'","noteId":"'+noteId+'"}
		
	   
	    
	    params = encodeURIComponent(params);
		$.support.cors = true;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
			},
				type : 'POST',
				url : url,
				data:params,
				
				success : function(responseText) {
						
						
						$('#memberModelBook').modal('hide');
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
		
		fileDeleted=true;
		});
	
	// delete groups in list level book share to groups
	
	$("ul#previousBookGroup").on('click','a',function() 
			{
		//var listId=$(addGroupsId).parent().parent().parent().attr('id');
		var listId=bookId;
		var url = urlForServer+"note/delBookgroup";
		var groupId=this.id;
		var params = '{"groupid":"'+groupId+'","listId":"'+listId+'","userId":"'+userId+'"}';
		//'{"listId":"'+listId+'","noteId":"'+noteId+'"}
	    
	    params = encodeURIComponent(params);
		$.support.cors = true;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
			},
				type : 'POST',
				url : url,
				data:params,
				
				success : function(responseText) {
						
							$('#shareGroupModelBook').modal('hide');
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
		
		fileDeleted=true;
		});
	
	
	
	//delete for member
	$("ul#previous").on('click','a',function() {
		//deleteFiles(this.id);
		var noteId=$(addMembersId).parent().attr('id');
		var listId=$(addMembersId).parent().parent().parent().attr('id');
		
		var url = urlForServer+"note/delmembers/"+listId+"/"+noteId+"/"+userId;
		var userIds=this.id;
		
		
		var params = '{"members":"'+userIds+'"}';
		//'{"listId":"'+listId+'","noteId":"'+noteId+'"}
		
	   
	    
	    params = encodeURIComponent(params);
		$.support.cors = true;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
			},
				type : 'POST',
				url : url,
				data:params,
				
				success : function(responseText) {
						
						
						$('#memberModel').modal('hide');
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
		
		fileDeleted=true;
		});
	
	
	//delete for group
	$("ul#previousGroup").on('click','a',function() {
		//deleteFiles(this.id);
		var noteId=$(addGroupsId).parent().attr('id');
		var listId=$(addGroupsId).parent().parent().parent().attr('id');
		
		var url = urlForServer+"note/delgroup/"+listId+"/"+noteId+"/"+userId;
		var groupId=this.id;
		
		
		var params = '{"groupid":"'+groupId+'"}';
		//'{"listId":"'+listId+'","noteId":"'+noteId+'"}
		
	   
	    
	    params = encodeURIComponent(params);
		$.support.cors = true;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
			},
				type : 'POST',
				url : url,
				data:params,
				
				success : function(responseText) {
						
							$('#shareGroupModel').modal('hide');
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
		
		fileDeleted=true;
		});
	
	//handles copy functionality for group level
	$('#notes').on('click','.js-copy-note-group',function(){
		$('#groupcopyDiv').attr('style','display:none');
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			addGroupsId=moreActionBasedId;
			$('#moreActions').modal('hide');
			moreActionEvent='copyGroup';
		}else{
			addGroupsId=$(this).parent().parent().parent().parent().parent().parent().parent();
			$(".noteMenu").popover('hide');	
		}
		var parameter="Copy";
		loadGroups(parameter);
	});
	
	//handles copy functionality for member level
	$('#notes').on('click','.js-copy-note-indiviual',function(){
		$('#individualcopyDiv').attr('style','display:none;');
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			addMembersId=moreActionBasedId;
			$('#moreActions').modal('hide');
			moreActionEvent='copyIndividual';
		}else{
			addMembersId=$(this).parent().parent().parent().parent().parent().parent().parent();
			$(".noteMenu").popover('hide');	
		}
		var parameter="Copy";
		loadMembers(parameter);
	});
	
	
	
	
	/*Called when add member is clicked*/
	$('#copyIndiviual').on('click',function(){
		var membersList=showTags($('#selectCopyNoteIndiviual').tagit('tags'));
		enterValue=$('#selectCopyNoteIndiviual').children().find('tester').text();
		var flag=null;
		var flagCheck='true';
		if(membersList.length > 0 || enterValue!='')
		{
			$('#individualcopyDiv').attr('style','display:none;');
			var existUserIds=$(this).parent().parent().find('#modal-copyPreviousMember').find('#copyPreviousMember').find('.js-old-share');
			var userid='';
			emailIds='';
			userIds='';
			nonMailIds='';
			var myown='false';
			
			if((userid!=null && userid.length>0) && (membersList!=null && membersList.length>0))
				userIds=userid +',';
			else
				userIds=userid;
			
			for (var i=0;i<membersList.length;i++)
			{
				if(i!=0)
				{
					emailIds=emailIds+',';
					userIds=userIds+',';
				}			
				var member=membersList[i];	
				var userId1=membersUserIdMap[member];
				
				if(userId1!=null && userId1!=''){
					emailIds=emailIds + membersEmailMap[member];
					userIds=userIds + userId1;
				}else{
					if(userMailId!=member)
					{
					nonMailIds=nonMailIds+','+member;
					if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(member))){
						flagCheck='false';
						}
					}
					else
					{
						flagCheck='false';
						myown='true';
					}
					
				}
				
				
			}
			
			if(enterValue!='')
			{
			if(userMailId!=enterValue)
			{
			nonMailIds=nonMailIds+','+enterValue;
			if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(enterValue))){
				flagCheck='false';
				}
			}
			else
			{
				flagCheck='false';
				myown='true';
			}
			}
			
			
			nonMailIds=nonMailIds.substring(1, nonMailIds.length);
			var listId=$('#notebookId').val();
			var noteId=$(addMembersId).parent().attr('id');
			if(listId !='note'){
				listId =listIdFromList;
				noteId =noteIdFromList;
			}else if(listId =='note' && noteId==undefined){
				listId =listIdFromList;
				noteId =noteIdFromList;
			}else{
				listId=$(addMembersId).parent().parent().parent().attr('id');
				noteId=$(addMembersId).parent().attr('id');
			}
			var url = urlForServer+"note/copyToIndividual/"+listId+"/"+noteId+"/"+userId;
			
			
			
		if(flagCheck == 'true'){
			$('#copyIndiviualModel').modal('hide');
			$('#dialogcopy').dialog({
				width: 400,
				modal: true,
				resizable: false,
				buttons: {
					"With Comments": function() {
						$(this).dialog("close");
						flag=true;
						var params = '{"members":"'+userIds+'","comments":"'+flag+'","nonMailIds":"'+nonMailIds+'","fullName":"'+userFullNameMail+'","noteName":"'+noteNameFromList+'","noteLevel":"copy"}';
						params = encodeURIComponent(params);

						$.ajax({
							headers: { 
							"Mn-Callers" : musicnote,
					    	"Mn-time" :musicnoteIn			
					    	},
							type: 'POST',
							url : url,
							cache: false,
							contentType: "application/json; charset=utf-8",
							data:params, 
							dataType: "json",
							success : function(response){
								
								sendMail(userFullNameMail,userFirstName,noteNameFromList,'Hi, You have notification pending...','Copy',listType,'','',emailIds);
		    					$('#copyIndiviualModel').modal('hide');
		    					if(moreActionEvent=='copyIndividual'){
		    						$('#moreActions').modal('show');
		    						moreActionEvent='';
		    					}
	            			},
	            			error: function(e) {
	            				alert("Please try again later");
	            			}
						});
						return true;
					},
					"Without Comments": function() {
						$(this).dialog("close");
						flag=false;
						var params = '{"members":"'+userIds+'","comments":"'+flag+'","nonMailIds":"'+nonMailIds+'","fullName":"'+userFullNameMail+'","noteName":"'+noteNameFromList+'","noteLevel":"copy"}';
						params = encodeURIComponent(params);
						
						$.ajax({
							headers: { 
							"Mn-Callers" : musicnote,
					    	"Mn-time" :musicnoteIn			
					    	},
							type: 'POST',
							url : url,
							cache: false,
							contentType: "application/json; charset=utf-8",
							data:params, 
							dataType: "json",
							success : function(response){
								sendMail(userFullNameMail,userFirstName,noteNameFromList,'Hi, You have notification pending...','Copy',listType,'','',emailIds);
		    					$('#copyIndiviualModel').modal('hide');
		    					$('#moreActions').modal('show');
	            			},
	            			error: function(e) {
	            				alert("Please try again later");
	            			}
						});
						return true;
					}
				}
			});
		}else{
			$('#individualcopyDiv').text(" Please select valid member");
			$('#individualcopyDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
			/*$('#copyIndiviualModel').modal('toggle');
			$("#moveCopyWarning").modal('show');
			$("#move-modal-message1").text("  Please select valid member");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#copyIndiviualModel').modal('toggle');},2000);*/
		}
		
		
		}
		else
		{
			$('#individualcopyDiv').text("Please add contact(s)");
			$('#individualcopyDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
			/*$('#copyIndiviualModel').modal('toggle');
			$("#moveCopyWarning").modal('show');
			$("#move-modal-message1").text("Please add contact(s) for sharing");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#copyIndiviualModel').modal('toggle');},2000);*/
		}
		
	});
	
	/*Called when add group is clicked*/
	$('#copyGroups').on('click',function(){
		var groupList=showTags($('#selectCopyNoteGroups').tagit('tags'));
		flag=true;
		var flagCheck='true';
		
		var existGroupIds=$(this).parent().parent().find('#modal-copyPreviousGroup').find('#copyPreviousGroup').find('.js-old-group');
		var groupId='';
		var newGroupId='';
		for(var i=0; i<existGroupIds.length; i++) 
		{
			if(i!=0)
				groupId +=','+existGroupIds[i].id ;
			else
				groupId += existGroupIds[i].id;
		}
		 
		
		if((groupId!=null && groupId.length>0) && (groupList!=null && groupList.length>0))
			groupIds=groupId +',';
		else
			groupIds=groupId;
		for (var i=0;i<groupList.length;i++)
		{
			if(i!=0)
			{
				groupIds=groupIds+',';
				newGroupId=newGroupId+',';
			}			
			var group=groupList[i];	
			var groupId=membersGroupIdMap[group];
			if(groupId!=null && groupId!='')
			{
				groupIds=groupIds + groupId;
				newGroupId=newGroupId+groupId;
			}
			else
				flagCheck='false';
		}
		var url1 = urlForServer+"group/getGroupMembers/";
		var param = '{"groups":"'+newGroupId+'"}';
		
				var noteId=$(addGroupsId).parent().attr('id');
				var listId=$(addGroupsId).parent().parent().parent().attr('id');
				var url = urlForServer+"note/copyToGroup/"+listId+"/"+noteId+"/"+userId;
		
		if(groupList.length > 0)
		{
			
			if(flagCheck == 'true')
			{
				param = encodeURIComponent(param);

				$.ajax({
					headers: { 
					"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
					type: 'POST',
					url : url1,
					cache: false,
					contentType: "application/json; charset=utf-8",
					data:param, 
					dataType: "json",
					success : function(response)
					{
					//var data = jQuery.parseJSON(response);
					if(response['groupId']==0)
					{
				
				$('#copyGroupModel').modal('hide');
				$('#dialogcopy').dialog({
					width: 400,
					modal: true,
					resizable: false,
					buttons: {
						"With Comments": function() {
							$(this).dialog("close");
							flag=true;
							var params = '{"groups":"'+groupIds+'","comments":"'+flag+'","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+noteNameFromList+'"}';
							params = encodeURIComponent(params);

							$.ajax({
								headers: { 
								"Mn-Callers" : musicnote,
						    	"Mn-time" :musicnoteIn			
						    	},
								type: 'POST',
								url : url,
								cache: false,
								contentType: "application/json; charset=utf-8",
								data:params, 
								dataType: "json",
								success : function(response){
									$('#copyGroupModel').modal('hide');
									if(moreActionEvent=='copyIndividual'){
										$('#moreActions').modal('toggle');
										moreActionEvent='';
									}
								},
								error: function(e) {
									alert("Please try again later");
								}
	        
							});
							return true;
						},
						"Without Comments": function() {
							$(this).dialog("close");
							flag=false;
							var params = '{"groups":"'+groupIds+'","comments":"'+flag+'","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+noteNameFromList+'"}';
							params = encodeURIComponent(params);
							$.ajax({
								headers: { 
								"Mn-Callers" : musicnote,
						    	"Mn-time" :musicnoteIn				
						    	},
								type: 'POST',
								url : url,
								cache: false,
								contentType: "application/json; charset=utf-8",
								data:params, 
								dataType: "json",
								success : function(response){
									$('#copyGroupModel').modal('hide');
								},
								error: function(e) {
									alert("Please try again later");
								}
							});
							return true;
						}
					}
				});
				
					}else
					{
						var gIds=response['groupId'];
						$('#groupcopyDiv').text(gIds +" is empty. Please select group(s) with contacts");
						$('#groupcopyDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
						/*$('#copyGroupModel').modal('toggle');
						$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text(gIds +" group is Empty");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#copyGroupModel').modal('toggle');},4000);*/
					}
					},error: function(e) {
						alert("Please try again later");
					}
				});
			}else{
				$('#groupcopyDiv').text("  Please select valid Group");
				$('#groupcopyDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
				/*$('#copyGroupModel').modal('toggle');
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text("  Please select valid Group");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#copyGroupModel').modal('toggle');},2000);*/
			}
		
		
			}
		
		else
		{
			$('#groupcopyDiv').text("  Please select Group(s) to share with");
			$('#groupcopyDiv').attr('style','display:block;color: red;font-family: Helvetica Neue;');
			/*$('#copyGroupModel').modal('toggle');
			$("#moveCopyWarning").modal('show');
			$("#move-modal-message1").text("  Please select group(s) for sharing");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');$('#copyGroupModel').modal('toggle');},2000);*/
		}
	});
	
	
	
	/*Copy Note For All Contact save to db*/
	$('#copAllConYes').on('click',function(){
		var noteId=noteIdFromList;//$(copyNoteAllCon).parent().attr('id');
		var listId=listIdFromList;//$(copyNoteAllCon).parent().parent().parent().attr('id');
		
		var url = urlForServer+"note/copyToAllContact";
		var params = '{"listId":"'+listId+'","noteId":"'+noteId+'","userId":"'+userId+'","comments":"true","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+noteNameFromList+'"}';
	    params = encodeURIComponent(params);
	

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	$('#copyToContactModal').modal('hide');
	    		if(moreActionEvent=='copyToContact'){
	    			checkOwnerOfList();
	    			//$('#moreActions').modal('toggle');
	    			moreActionEvent='';
	    		}
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });
	});
	
	/*Copy Note For All Contact with out comments save to db*/
	$('#copAllConNo').on('click',function(){
		
		var noteId=$(copyNoteAllCon).parent().attr('id');
		var listId=$(copyNoteAllCon).parent().parent().parent().attr('id');
		
		var url = urlForServer+"note/copyToAllContact";
		var params = '{"listId":"'+listId+'","noteId":"'+noteId+'","userId":"'+userId+'","comments":"false","fullName":"'+userFullNameMail+'","firstName":"'+userFirstName+'","noteName":"'+noteNameFromList+'"}';
	    
	    params = encodeURIComponent(params);
	

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    		$('#copyToContactModal').modal('hide');
	    		if(moreActionEvent=='copyToContact'){
	    			$('#moreActions').modal('toggle');
	    			moreActionEvent='';
	    		}
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });
	});
	
	
	$('#notes').on('click','.js-copy-note-contacts',function(){
		
		var listId;
		var noteId;
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			copyNoteAllCon=moreActionBasedId;
			noteId=noteIdFromList;//$(copyNoteAllCon).parent().attr('id');
			listId=listIdFromList;//$(copyNoteAllCon).parent().parent().parent().attr('id');
			$('#moreActions').modal('hide');
			moreActionEvent='copyToContact';
		}else{
			copyNoteAllCon=$(this).parent().parent().parent().parent().parent().parent().parent();
			listId=$(copyNoteAllCon).parent().parent().parent().attr('id');
			noteId=$(copyNoteAllCon).parent().attr('id');
			$(".noteMenu").popover('hide');
		}
		
		var response="";
		var url = urlForServer+"user/memberList/"+userId;
		var Type='';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
		type : 'POST',
		url : url,
		success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null)
			{
				$('#copyToContactModal').modal('toggle');
			}
			else
			{
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text("No contacts exist. Please add contact(s) to share a copy with");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			}
		
		},
		error :function(e)
		{
			alert("please try again later");
		}
		});
		
		
	});
	
	$('#notes').on('click','.js-copied-note-contacts',function()
			{
		  var alert='<p>Note already copied to all contacts!</p>';
		   $('#warning').children().find('p').remove();
			$('#warning').children().append(alert);
			$('#warning').modal('toggle');
			setTimeout(function(){
    			$('#warning').modal('hide');	
    		},5000);
			
			});
	
	
	/*handles due date functionality*/
	$('#notes').on('click','.js-due-date',function(){
		//var noteId;
		//var listId;
		$("#dateErrorMsg").text("");
		$("#timeErrorMsg").text("");
		if($(this).parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			addDueDateId=moreActionBasedId;
			
			//noteIdFromList=$(addDueDateId).parent().attr('id');
			//listIdFromList=$(addDueDateId).parent().parent().parent().attr('id');
			
			//alert(listType);
			$('#moreActions').modal('hide');
			$('#saveDueDate').attr('disabled',false);			
			$('#dueDateModel').modal('show');
			moreActionEvent='dueDate';
		}else{
			addDueDateId=$(this).parent().parent().parent().parent().parent().parent().parent();
			$(".noteMenu").popover('hide');		
			
			//noteIdFromList=$(addDueDateId).parent().attr('id');
			//listIdFromList=$(addDueDateId).parent().parent().parent().attr('id');
			
		}
		
		var url = urlForServer+"note/noteDetails";
		
		var params = '{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'","listType":"note","userId":"'+userId+'"}';
		params = encodeURIComponent(params);
		var response="";
		
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url,
				data:params,

				success:function(responseText) 
				{
					response=responseText;
					var data = jQuery.parseJSON(response);
					if(data.dueDate != "")
					{
					var selectedDate=new Date(data.dueDate);
					
					var condate=new Date(selectedDate);
					var convertedDate="";
					convertedDate=month[condate.getMonth()];
					convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
					
						if(data.dueTime != "")
							$("#dueDate").val(convertedDate+" "+data.dueTime);
						else
							$("#dueDate").val(convertedDate);	
					}
					else
					{
					$("#dueDate").val("");
					//$("#dueTime").val("12:00 PM");
					}
				},
				error:function(e) {
					alert("Please try again later");
				}
			});
			
			
		
		/*$("#notedatepicker" ).datepicker({
			onSelect: function(dateText, inst) {
				var date = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);
				var dateText = $.datepicker.formatDate("MM/dd/yy", date, inst.settings);
				var month=dateText.split("/");
				var upmonth=month[0].substring(0,3);
				var date=upmonth+"/"+month[1]+"/"+month[2];
				
				$("#dueDate").val(date);
			}
		});*/
	});
	
	/*Append due date to note functionality*/
	$('#saveDueDate').click(function() {
	
		if($("#dueDate").val()!=null && $("#dueDate").val() !=""){
			$('#saveDueDate').attr('disabled',true);
			var dateTime=$("#dueDate").val();
			dateTime=dateTime.split(' ');
			var tDate=dateTime[0];
			var time=dateTime[1];
			var am=dateTime[2];
			
				time=time+" "+am;
				if(is_Ie|| is_safari){
					var splitedDate =  tDate.split('/');
					tDate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				}
				
			var dueDateDivContent=  loadDueDateContent(tDate,time); 
			//var dueDateDivContent=  loadDueDateContent($("#dueDate").val(),$("#dueTime").val()) 
			var date=new Date(tDate);
			
			var selectedDate="";
			selectedDate=date.getMonth()+1;
			selectedDate=selectedDate+"/"+date.getDate()+"/"+date.getFullYear();
			$(addDueDateId).parent().find('.badges').children('.due-past').remove();
			$(addDueDateId).parent().find('.badges').children('.due-future').remove();
			$(addDueDateId).parent().find('.badges').children('.due-soon').remove();
			$(addDueDateId).parent().find('.badges').children('.due-now').remove();
			$(addDueDateId).parent().find('.badges').append(dueDateDivContent);
		
			var noteId=noteIdFromList;
			var listId=listIdFromList;

			var url = urlForServer+"note/dueDate/"+listId+"/"+noteId+"/"+userId;
			var params = '{"dueDate":"'+selectedDate+'","dueTime":"'+time+'","fullName":"'+listOwnerFullName+'"}';
			params = encodeURIComponent(params);

			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
				type: 'POST',
				url : url,
				cache: false,
				contentType: "application/json; charset=utf-8",
				data:params, 
				dataType: "json",
				success : function(responseText){
	    			$('#dueDateModel').modal('hide');
	    			if(moreActionEvent=='dueDate'){
	    				$('#moreActions').modal('toggle');
	    				moreActionEvent='';
	    			}
            	},
            	error: function(e) {
            		console.log("<--- error in duedate --- >",e);
            		if(e.status == 200 && e.responseText == "success"){
            			$('#dueDateModel').modal('hide');
            			if(moreActionEvent=='dueDate'){
            				$('#moreActions').modal('toggle');
            				moreActionEvent='';
            			}
            		}
            	}
        
			});
			
			
		}else{
			
			$("#dateErrorMsg").text("");
			//$("#timeErrorMsg").text("");
			
			if($("#dueDate").val()!=null && $("#dueDate").val() =="")
				$("#dateErrorMsg").text("Date is required").css({"color":"red","font-weight": "bold","font-size":"14px","font-family": "Helvetica Neue"});
			
			/*if($("#dueTime").val()!=null && $("#dueTime").val() == "")
				$("#timeErrorMsg").text("!!! Need to Select Time").css({"color":"red","font-weight": "bold"});*/
		}
		
	});
	
	/*Remove due date from note*/
	$('#removeDueDate').click(function() {	
	
		/*$(addDueDateId).parent().find('.badges').each(function( index ){
			$(this).remove();
		});*/
		$(addDueDateId).parent().find('.badges').children('.due-future').remove();
		$(addDueDateId).parent().find('.badges').children('.due-past').remove();
		$(addDueDateId).parent().find('.badges').children('.due-soon').remove();
		var noteId=$(addDueDateId).parent().attr('id');
		var listId=$(addDueDateId).parent().parent().parent().attr('id');
		var date="";
		var time="";
		var url = urlForServer+"note/dueDate/"+listId+"/"+noteId+"/"+userId;
		var params = '{"dueDate":"'+date+'","dueTime":"'+time+'","fullName":"'+listOwnerFullName+'"}';
		
		
	    params = encodeURIComponent(params);
	

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    		$('#dueDateModel').modal('hide');
	    		if(moreActionEvent=='dueDate'){
	    			$('#moreActions').modal('toggle');
	    			moreActionEvent='';
	    		}
            },
            error: function(e) {
                //alert("Please try again later");
            }
        
	    });
		$('#dueDateModel').modal('hide');
	});
	
	/*Add List Modal Close Function*/
	$('#notes').on('click','.modalHeaderClose',function(){
		$('#addListModal').modal('hide');
		$('#addCalenderListModal').modal('hide');
		$('#addBookModal').modal('hide');
		
	});
	
	/*Add List Modal Cancel Function*/
	$('#notes').on('click','.modalBtnPrimary',function(){
		$('#addListModal').modal('hide');
		$('#addCalenderListModal').modal('hide');
		$('#addBookModal').modal('hide');
	});
	
	$('#notes').on('click','.js-copy-list',function(){
		$('#copyListName').val($(this).parent().parent().parent().parent().children().find('.headerListName').text());
		copyListId=$(this).parent().parent().parent().parent().parent();
		//$(".listMenu").popover('hide');	
		$('#copyListModal').modal('toggle');
	});
	
	
	$('#closeCopyListModel').click(function(){
		$('#copyListModal').modal('hide');
	});
	
	$('#copyCreate').click(function(){
		
		var listName=$('#copyListName').val();
		var url = urlForServer+"note/copyList";
		var params = '{"listId":"'+$(copyListId).attr('id')+'","newListName":"'+listName+'","listType":"'+listType+'","fetchEvent":true,"userId":"'+userId+'"}';
	    params = encodeURIComponent(params);
	

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    		$('#notes').find('.listDiv').each(function( index ){
	    			$(this).remove();
	    		});
	    		loadList(response);
	    		getListAndNoteNames();
	    		$('#copyListModal').modal('hide');
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });
	});
	
	
	
	
	$('#notes').on('click','.js-cancel',function(){
		$(this).parent().parent().closest("div").fadeOut(1);
		$(this).parent().parent().parent().append('<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer fontStyle" style="text-decoration: none;">Add New Note</a>');
		$(this).parent().parent().closest("div").remove();
	});
	//add new notes to default list
	$('#notes').on('click','.js-add-note-default',function(){
		addNoteClickId=$(this);
		$('#NoteName').val('');
		$("#noteWarningId").text("");
		$('#addNoteModal').modal('toggle');
	});
	
	$('#notes').on('click','.list-footer',function(){
		addNoteClickId=$(this);
		$('#NoteName').val('');
		$("#noteWarningId").text("");
		$('#addNoteModal').modal('toggle');
	});
	
	// Add Schedule note model Panel 	
	$('#notes').on('click','.js-event-note-default',function(){
		if((userEmail!='null')&&(userEmail!='')){
			$('#eventMailCheckbox').attr('style','display:block;');
		}else{
			$('#eventMailCheckbox').attr('style','display:none;');
		}
		addNoteClickId = $(this);
		clearAllValueInEventModal();
	});
	$('#notes').on('click','.schedule-footer',function(){
		scheduleListId=$(this).parent().parent().attr('id');
		clearAllValueInEventModal();
	});
	function clearAllValueInEventModal(){
		$('#scheduleNoteName').val("");
		$('#scheduleNoteDetail').val("");
		$('#eventStartTime').val("");
		$('#eventEndTime').val("");
		$('#eventStartDate').val("");
		$('#eventEndDate').val("");
		$('#scheduleLocation').val("");
		$('#eventRepeatDate').val("");
		$('#eventRepeatEndDate').attr("style","display:none");
		$('#repeatEventId').val("--Select Repeat--");
		
		$('#scheduleNoteNames').text("");
		$('#scheduleNoteDetails').text("");
		$('#eventStartTimes').text("");
		$('#eventEndTimes').text("");
		$("#eventBetweenDate").attr("style", "display:none");
		$("#eventRepeatEventwarning").attr("style", "display:none");
		$("#eventRepeatEventwarning").text("");
		$('input[name=alldayevent]').attr("checked", false);

		$("#eendDate").hide();
		$("#estartDate").hide();
		$("#estartDateTime").show();
		$("#eendDateTime").show();
		$("#sendEventMailCheckbox").attr('checked', false); 
		
		$("#addScheduleNote").prop('disabled', false);
		$('#addScheduleModel').modal('toggle');
	}
	//View Event Details
	
	$('#okviewevent').click( function() {
		$('#viewScheduleModel').modal('hide');
	});
	$('#repeatEventId').change( function() {
		
		var repeatEvent=$('#repeatEventId').val();
		
		if(repeatEvent!=null &&  repeatEvent!='' && repeatEvent!='--Select Repeat--')
		{
			$("#eventRepeatEndDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$('#eventRepeatDate').val("");
			$("#eventRepeatDateWarningMsg").attr("style", "display:none");
			$("#eventRepeatDateWarningMsg").text("");
		}
		else
		{
			$("#eventRepeatDateWarningMsg").attr("style", "display:none");
			$("#eventRepeatDateWarningMsg").text("");
			$("#eventRepeatEndDate").attr("style", "display:none");
			$('#eventRepeatDate').val("");
			
			
		}
	});
	
	//Add schedule note and add to DB
	$('#addScheduleNote').click( function() {
		var selectedBookId='0';
		var noteName = trim($('#scheduleNoteName').val());
		var eventLocation =trim($('#scheduleLocation').val()); 
		if(noteName!=null && noteName!='')
		{
			
			$("#scheduleNoteNames").attr("style", "display:none");
			$("#scheduleNoteNames").text("");
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		else
		{
			$("#scheduleNoteNames").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#scheduleNoteNames").text("Field cannot be blank").css({"color":"red"});
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		var startTime="";
		var endTime="";
		var eventDiscription=trim($('#scheduleNoteDetail').val());
		if(eventDiscription!=null && eventDiscription!='')
		{
			
			$("#scheduleNoteDetails").attr("style", "display:none");
			$("#scheduleNoteDetails").text("");
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		else
		{
			$("#scheduleNoteDetails").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#scheduleNoteDetails").text("Field cannot be blank").css({"color":"red"});
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		var allDayEvent=$('input[name=alldayevent]').is(':checked');
		var sendEmail=$('input[name=sendEventMailCheckbox]').is(':checked');
		var sendEmailFlag="";
		if(sendEmail)
		{
			sendEmailFlag="true";
		}
		else
		{
			sendEmailFlag="false";
		}
			
		var stime="";
		var etime="";
		var allDayFlag=true;
		var eventFlag=true;
		var eventRepeatEndDate="";
		var eventRepeatStartDate="";
		var eventRepeatDate="";
		var eventRepeatStart="";
		var RepeatEventFlag=true;
		var EventEndDateFlag=true;
		if(allDayEvent){
			stime=$('#eventStartDate').val();
			
			if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=stime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					stime = newEventdate ;
				}
				startTime=convertStringDateFormatIntoNumberFormat(stime, true);
			}
			etime=$('#eventEndDate').val();
			if(etime!=null && etime!='' && etime!='NaN/NaN/NaN')
				if(is_Ie|| is_safari){
					var dateTime=etime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					etime = newEventdate ;
				}
				endTime=convertStringDateFormatIntoNumberFormat(etime, true);
			if(noteName != null && noteName != '' && startTime != null
					&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN')
			{
			if (new Date(stime) > new Date(etime)) {
				allDayFlag=false;
				$("#eventRepeatEventwarning").attr("style", "display:none");
				$("#eventRepeatEventwarning").text("");
				$("#eventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#eventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
			}else
			{
				$("#eventRepeatEventwarning").attr("style", "display:none");
				$("#eventRepeatEventwarning").text("");
				$("#eventBetweenDate").attr("style", "display:none");
				$("#eventBetweenDate").text("");
			}
			}
			else
			{
				$("#eventBetweenDate").attr("style", "display:none");
				$("#eventBetweenDate").text("");
			}
			
		
			
			
		}else{
			stime=$('#eventStartTime').val();
			if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=stime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
					
				}
				startTime=convertStringDateFormatIntoNumberFormat(stime,false);
			}
			etime=$('#eventEndTime').val();
			if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
				if(is_Ie || is_safari){
					var dateTime=etime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
				}
				endTime=convertStringDateFormatIntoNumberFormat(etime,false);
			}
			if(noteName != null && noteName != '' && startTime != null
					&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN')
			{
			if (new Date(stime).getTime() > new Date(etime).getTime()) {
			    
				
			     eventFlag=false;
			        $("#eventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#eventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
					$("#eventRepeatEventwarning").attr("style", "display:none");
					$("#eventRepeatEventwarning").text("");
			}
			else
			{
				
				$("#eventBetweenDate").attr("style", "display:none");
				$("#eventBetweenDate").text("");
				$("#eventRepeatEventwarning").attr("style", "display:none");
				$("#eventRepeatEventwarning").text("");
			}
			}
			else
			{
				$("#eventBetweenDate").attr("style", "display:none");
				$("#eventBetweenDate").text("");
			}
			
			
			
		}
		
		
		
		
		
		if(startTime!=null && startTime!='' && startTime!='NaN/NaN/NaN')
		{
			$("#eventStartTimes").attr("style", "display:none");
			$("#eventStartTimes").text("");
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		else
		{
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
			$("#eventStartTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#eventStartTimes").text("Field cannot be blank").css({"color":"red"});
		}
		if(endTime!=null && endTime!=''&& endTime!='NaN/NaN/NaN')
		{
			
			$("#eventEndTimes").attr("style", "display:none");
			$("#eventEndTimes").text("");
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		else
		{
			$("#eventEndTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#eventEndTimes").text("Field cannot be blank").css({"color":"red"});
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		
		var searchAddNoteFlag = false;
		if($('#notebookId').val() != 'note'){
			searchAddNoteFlag = true;
		}
		var repeatEvent=$('#repeatEventId').val();
		if(repeatEvent!=null && repeatEvent!='' && repeatEvent!='--Select Repeat--')
		{
			if($('#eventRepeatDate').val()!=null && $('#eventRepeatDate').val()!='')
			{
				$("#eventRepeatDateWarningMsg").attr("style", "display:none");
				$("#eventRepeatDateWarningMsg").text("");
			}
			else
			{
				EventEndDateFlag=false;
				$("#eventRepeatDateWarningMsg").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#eventRepeatDateWarningMsg").text("Field cannot be blank").css({"color":"red"});
			}
			
		}
		else
		{
			repeatEvent='';
			$("#eventRepeatDateWarningMsg").attr("style", "display:none");
			$("#eventRepeatDateWarningMsg").text("");
			
		}
		
		eventRepeatEndDate=$('#eventRepeatDate').val();
		if(eventRepeatEndDate!=null && eventRepeatEndDate!='' && eventRepeatEndDate!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=eventRepeatEndDate.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				eventRepeatEndDate = newEventdate ;
			}
			eventRepeatDate=convertStringDateFormatIntoNumberFormat(eventRepeatEndDate, true);
		}
		eventRepeatStartDate=$('#eventStartTime').val();
		if(eventRepeatStartDate!=null && eventRepeatStartDate!='' && eventRepeatStartDate!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=eventRepeatStartDate.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				eventRepeatStartDate = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			}
			eventRepeatStart=convertStringDateFormatIntoNumberFormat(eventRepeatStartDate,true);
		}
		if(noteName != null && noteName != '' && startTime != null
				&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN'  && eventFlag==true && allDayFlag==true && $('#eventRepeatDate').val()!=null && $('#eventRepeatDate').val()!='')
		{
		if(allDayEvent){
			if(allDayFlag)
			{
			if($('#repeatEventId').val()!=null && $('#repeatEventId').val()!='' && $('#repeatEventId').val()!='--Select Repeat--')
			{
				   
			
				if ($('#eventStartDate').val() == $('#eventEndDate').val()) {
					$("#eventRepeatEventwarning").attr("style", "display:none");
					$("#eventRepeatEventwarning").text("");
					$("#eventBetweenDate").attr("style", "display:none");
					$("#eventBetweenDate").text("");
				}
				else
				{
					 
					  RepeatEventFlag=false;
					 $("#eventBetweenDate").attr("style", "display:none");
					$("#eventBetweenDate").text("");
					$("#eventRepeatEventwarning").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#eventRepeatEventwarning").text("You can't create overlapping event.").css({"color":"red"});
				}
			}
			}
			
		}
		else
		{
			if(eventFlag)
			{
			if($('#repeatEventId').val()!=null && $('#repeatEventId').val()!='' && $('#repeatEventId').val()!='--Select Repeat--')
			{
				var startrepeat=$('#eventStartTime').val();
				var endRepeat=$('#eventEndTime').val();
			
				if (startrepeat.substring(0, startrepeat.length - 8).trim() == endRepeat.substring(0, endRepeat.length - 8).trim()) {
					$("#eventRepeatEventwarning").attr("style", "display:none");
					$("#eventRepeatEventwarning").text("");
					$("#eventBetweenDate").attr("style", "display:none");
					$("#eventBetweenDate").text("");
					
				}
				else
				{
					RepeatEventFlag=false;
					$("#eventBetweenDate").attr("style", "display:none");
					$("#eventBetweenDate").text("");
					$("#eventRepeatEventwarning").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#eventRepeatEventwarning").text("You can't create overlapping event.").css({"color":"red"});
				}
			}
			}
		}
		}
		else
		{
			$("#eventRepeatEventwarning").attr("style", "display:none");
			$("#eventRepeatEventwarning").text("");
		}
		
		if($('#notebookId').val() !='note')
			selectedBookId=$('#notebookId').val();
		
		if(noteName != null && noteName != '' && startTime != null
			&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN'  && eventFlag==true && allDayFlag==true && RepeatEventFlag==true && EventEndDateFlag==true)
		{
			$("#addScheduleNote").prop('disabled', true);
			
			while(noteName.indexOf("\n") != -1){
				noteName = noteName.replace("\n"," ");
			}
			while(noteName.indexOf("\t") != -1){
				noteName = noteName.replace("\t"," ");
			}
			while(noteName.indexOf("\"") != -1){
				noteName = noteName.replace("\"", "`*`");
			}
			while(eventDiscription.indexOf("\n")!= -1){
				eventDiscription = eventDiscription.replace("\n"," ");
			}
			while(eventDiscription.indexOf("\t")!= -1){
				eventDiscription = eventDiscription.replace("\t","     ");
			}
			while(eventDiscription.indexOf("\"") != -1){
				eventDiscription = eventDiscription.replace("\"", "`*`");
			}
			if(eventLocation!= ""){
				while(eventLocation.indexOf("\"") != -1){
					eventLocation = eventLocation.replace("\"", "`*`");
				}
			}
			var url="";
			if(!searchAddNoteFlag && listType=="schedule" && $(addNoteClickId).parent().attr('id')!= undefined && $(addNoteClickId).parent().attr('id')=="new-event"){
				url = urlForServer+"note/createNoteInDefualtList/"+listType+"/"+userId+"/"+selectedBookId;	
			}else if (searchAddNoteFlag && listType=="schedule" && $(addNoteClickId).parent().attr('id')!= undefined && $(addNoteClickId).parent().attr('id')=="new-event"){
				scheduleListId = $('#notebookId').val();
				url = urlForServer+"note/createNoteInDefualtList/"+listType+"/"+userId+"/"+scheduleListId;
				//url = urlForServer+"note/createNote/"+scheduleListId+"/"+userId;
			}else{
				url = urlForServer+"note/createNoteInDefualtList/"+listType+"/"+userId+"/"+scheduleListId;
				//url = urlForServer+"note/createNote/"+scheduleListId+"/"+userId;
			}		
		var params = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","eventMembers":"","eventGroups":"","eventSharedAllContact":0,"eventSharedAllContactMembers":"","status":"A","ownerEventStatus":"A","vote":"","links":"","endDate":"'+endTime+'","eventId":"","dueDate":"","dueTime":"","access":"private","attachFilePath":"","description":"'+eventDiscription+'","comments":"","location":"'+eventLocation+'","repeatEvent":"'+repeatEvent+'","eventStartDate":"'+startTime+'","eventEndDate":"'+eventRepeatDate+'"}';
	    var eventId="";
	    var listName="";
	    var desc='';
	    params = encodeURIComponent(params);
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	        
	    			if(response.description != ""){
	    				desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
	    				}
	    			/*if(!searchAddNoteFlag && listType=="schedule" && $(addNoteClickId).parent().attr('id')!= undefined && $(addNoteClickId).parent().attr('id')=="new-event"){
						$('#scheduleContent').append('<div id="'+response.listId+'" class="listDiv col-md-12" style="float: left; background-color: rgb(255, 255, 255);">'
								+'<div class="listBodyAll" style="background-color: rgb(255, 255, 255);">'
									+'<div class="noteDiv" id="'+response.eventId+'">'
										+'<div class="todo_description">'
											+'<p><b>'
												+ noteName
											+ '</b></p>'
										+'</div>'
										+'<div class="badges">'
											+desc
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>');
						$('#calendarlist').empty();
						
						scheduleListId = response.listId;
					}else{
						$('#scheduleContent').children('#'+scheduleListId+'').children('.listBodyAll').append('<div class="noteDiv" id="'+response.eventId+'" style="border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);">'
						//$('#'+scheduleListId).children('.listBodyAll').append('<div class="noteDiv" id="'+response.eventId+'">'
	    					+'<div class="todo_description">'
	    					//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
	    					//+ '<div class="todo_description"><div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
	    					//+'</div>'
	    					+'<p><b>'
	    					+ noteName
	    					+ '</b></p></div>'
							+'<div class="badges">'
							+desc
							+ '</div>'
	    					+ '</div>');
	    			     $('#calendarlist').empty();
	    			     
//	    			loadAllEvents();
	    			}*/
	    			
	    			if(response!="0")
	    			{
	    				
	    			    eventId=response.eventId;
	    			    listName=response.listName;
	    			    
	    				  var scheduleUrl = urlForServer+"note/createScheduleNote/"+response.listId;
	    				    if(eventId!="" && eventId!="0")
	    				    {
	    				    	
	    					var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","repeatEvent":"'+repeatEvent+'","eventStartDate":"'+startTime+'","eventEndDate":"'+eventRepeatDate+'","userId":"'+userId+'","listType":"schedule","eventId":"'+eventId+'","allDay":"'+allDayEvent+'","description":"'+eventDiscription+'","listName":"'+listName+'","location":"'+eventLocation+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmailFlag+'"}';
	    					scheduleParams = encodeURIComponent(scheduleParams);

	    				    $.ajax({
	    				    	headers: { 
	    				    	"Mn-Callers" : musicnote,
	    				    	"Mn-time" :musicnoteIn					
	    				    	},
	    				    	type: 'POST',
	    				    	url : scheduleUrl,
	    				    	cache: false,
	    				    	contentType: "application/json; charset=utf-8",
	    				    	data:scheduleParams, 
	    				    	dataType: "json",
	    				    	success : function(response){
	    				         if(response!='0'){
	    				        	 $('#addScheduleModel').modal('hide');
	    				        	 $('#calendarlist').empty();
	    				        	// hide due to event by date & time order
	    				        	/*if($('#scheduleContent').children('.listDiv').children().attr('class')=='modalHeader')
	    				        	{
	    				        		loadEventList(userId,scheduleListId,listType);
	    				        	}
	    				        	else
	    				        	{
	    				        		loadAllEvents();
	    				        	}*/
	    				        	 var listId=$('#notebookId').val();
	    				        	 loadEventList(userId,listId,listType);
    				    	    	 	 //loadAllEvents();
	    				    	    	 getTodaySchedule();
	    				    	    	 loadCalendarComboBoxList();
	    				    	    	// add due to event by date & time order
	    				    	    	 loadEventsBooks();
	    								 getListAndNoteNames();
	    				    	     }
	    				    	     else
	    				    	    	 alert("Please try again later");
	    			          },
	    			          error: function(e) {
	    			              alert("Please try again later");
	    			          }
	    			      
	    				    });
	    				    }
	    			}
	    			else
	    			{
	    				alert("Please try again later");
	    			}
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });
	    
	    
		}else
		{
			$('#addScheduleModel').modal('show');
		}
		
		//alert(scheduleListId+' ?? Event Name :: '+listName+' &&  StartTime ::: '+startTime+' && End Time :: '+endTime +' &&Description ::'+eventDiscription+' Allday Event'+allDayEvent);
		//alert($('input[name=alldayevent]').is(':checked'));
		  //$('#addScheduleModel').modal('hide');
	});
	
	$('#editRepeatEventId').change( function() {
	
		if($('#editRepeatEventId').val()!=null && $('#editRepeatEventId').val()!='' && $('#editRepeatEventId').val()!='--Select Repeat--')
		{
					
		$("#editEventRepeatEndDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#editEventRepeatDate").val('');
		$("#editEventRepeatDateWarningMsg").attr("style", "display:none");
		$("#editEventRepeatDateWarningMsg").text("");
		}
		else
		{
			$("#editEventRepeatDateWarningMsg").attr("style", "display:none");
			$("#editEventRepeatDateWarningMsg").text("");
			$("#editEventRepeatDate").val('');
			$("#editEventRepeatEndDate").attr("style", "display:none");
		}
	});
	///edit schedule note
	$('#editScheduleNote').click( function() {
	var eventId=0;
	var listId=0;
	
	//if(eventEdit==1)  // from more actions
	//{
		eventId=noteIdFromList;
		listId=listIdFromList;
		
	//}
	/*else
	{                       // from calendar
		eventId=$('#eventID').val();
		listId=$('#editModalLabel').val();
		alert("eventId111-->"+eventId);
	}*/
	var listname='';
	var noteName = trim($('#editscheduleNoteName').val());
	var startTime="";
	var endTime="";
	var eventDiscription=trim($('#editscheduleNoteDetail').val());
	var allDayEvent=$('input[id=editalldayevent]').is(':checked');
	var eventLocation =$('#editScheduleLocation').val(); 
	
	/*if(eventId==0 )
		eventId=$('#eventID').val();
	if(listId==0)
		listId=$('#editModalLabel').val();*/
	
	
	
	var stime="";
	var etime="";
	var editallDayFlag=true;
	var editeventFlag=true;
	var editRepeatEventFlag=true;
	var EditEventEndDate=true;
	var sendEmail=$('input[name=sendEditEventMailCheckbox]').is(':checked');
	var sendEmailFlag="";
	if(sendEmail)
	{
		sendEmailFlag="true";
	}
	else
	{
		sendEmailFlag="false";
	}
	 
	if(allDayEvent)
	{
		stime=$('#editeventStartDate').val();
		if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=stime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				stime = newEventdate;
			}
			startTime=convertStringDateFormatIntoNumberFormat(stime, true);
		}
		etime=$('#editeventEndDate').val();
		if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=etime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				etime = newEventdate;
			}
			endTime=convertStringDateFormatIntoNumberFormat(etime, true);
		}
		if(noteName != null && noteName != '' && startTime != null
				&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != '' )
		{
		if (new Date(stime) > new Date(etime)) {
			//alert("Sorry, you can't create an event that ends before it starts.");
			editallDayFlag=false;
			$("#editeventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#editeventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
			$("#editEventRepeatEventwarning").attr("style", "display:none");
			$("#editEventRepeatEventwarning").text("");
		}else
		{
			$("#editeventBetweenDate").attr("style", "display:none");
			$("#editeventBetweenDate").text("");
			$("#editEventRepeatEventwarning").attr("style", "display:none");
			$("#editEventRepeatEventwarning").text("");
		}
		}
		else
		  {
			  $("#editeventBetweenDate").attr("style", "display:none");
				$("#editeventBetweenDate").text("");
		  }
		  
		
		
		
	}
	else
	{     stime=$('#editeventStartTime').val();
	      if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
	    	  if(is_Ie|| is_safari){
					var dateTime=stime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			  }
	    	  startTime=convertStringDateFormatIntoNumberFormat(stime, false);
	      }
	      etime=$('#editeventEndTime').val();
		  if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
			  if(is_Ie|| is_safari){
					var dateTime=etime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			  }
			  endTime=convertStringDateFormatIntoNumberFormat(etime, false);
		  }
		  if(noteName != null && noteName != '' && startTime != null
					&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != '' )
			{
		  
		  if (new Date(stime).getTime() > new Date(etime).getTime()) {
			     //alert("Sorry, you can't create an event that ends before it starts.");
			     editeventFlag=false;
			        $("#editeventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#editeventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
					$("#editEventRepeatEventwarning").attr("style", "display:none");
					$("#editEventRepeatEventwarning").text("");
			}
			else
			{
				$("#editeventBetweenDate").attr("style", "display:none");
				$("#editeventBetweenDate").text("");
				$("#editEventRepeatEventwarning").attr("style", "display:none");
				$("#editEventRepeatEventwarning").text("");
			}
			}
		  else
		  {
			  $("#editeventBetweenDate").attr("style", "display:none");
			  $("#editeventBetweenDate").text("");
		  }
		  
		 
	}
	var editEventRepeatEndDate="";
	var editEventRepeatDate="";
	var editEventRepeatStartDate="";
	var editEventRepeatStart="";
	
	
	
	
	
	
	var repeatEvent=$('#editRepeatEventId').val();
	if(repeatEvent!=null && repeatEvent!='' && repeatEvent!='--Select Repeat--')
	{
	if($('#editEventRepeatDate').val()!=null && $('#editEventRepeatDate').val()!='')
	{
		
		$("#editEventRepeatDateWarningMsg").attr("style", "display:none");
		$("#editEventRepeatDateWarningMsg").text("");
	}
	else
	{
		EditEventEndDate=false;
		$("#editEventRepeatDateWarningMsg").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#editEventRepeatDateWarningMsg").text("Field cannot be blank").css({"color":"red"});
	}
	}
	else
	{
		repeatEvent='';
		$("#editEventRepeatDateWarningMsg").attr("style", "display:none");
		$("#editEventRepeatDateWarningMsg").text("");
		
	}
	if($('#editRepeatEventId').val()!=null && $('#editRepeatEventId').val()!='' && $('#editRepeatEventId').val()!='--Select Repeat--')
	{
	editEventRepeatEndDate=$('#editEventRepeatDate').val();
	if(editEventRepeatEndDate!=null && editEventRepeatEndDate!='' && editEventRepeatEndDate!='NaN/NaN/NaN')
	{
		if(is_Ie|| is_safari){
			var dateTime=editEventRepeatEndDate.split(' ');
			var newEventdate=dateTime[0];
			
			var splitedDate =  newEventdate.split('/');
			newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
			
			editEventRepeatEndDate = newEventdate ;
		}
		editEventRepeatDate=convertStringDateFormatIntoNumberFormat(editEventRepeatEndDate, true);
	}
	}
	else
	{
		editEventRepeatDate='';
	}
	
	if(allDayEvent)
	{
		editEventRepeatStartDate=$('#editeventStartDate').val();
		if(editEventRepeatStartDate!=null && editEventRepeatStartDate!='' && editEventRepeatStartDate!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=editEventRepeatStartDate.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				editEventRepeatStartDate = newEventdate ;
			}
			editEventRepeatStart=convertStringDateFormatIntoNumberFormat(editEventRepeatStartDate,true);
		}
		
	}
	else
	{
	
	editEventRepeatStartDate=$('#editeventStartTime').val();
		if(editEventRepeatStartDate!=null && editEventRepeatStartDate!='' && editEventRepeatStartDate!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=editEventRepeatStartDate.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				editEventRepeatStartDate = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			}
			editEventRepeatStart=convertStringDateFormatIntoNumberFormat(editEventRepeatStartDate,true);
		}
	}
	
	if(noteName!=null && noteName!='')
	{
		
		$("#editscheduleNoteNames").attr("style", "display:none");
		$("#editscheduleNoteNames").text("");
		
	}
	else
	{
		$("#editscheduleNoteNames").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#editscheduleNoteNames").text("Field cannot be blank").css({"color":"red"});

	}
	
	
	if(eventDiscription!=null && eventDiscription!='')
	{
		
		$("#editscheduleNoteDetails").attr("style", "display:none");
		$("#editscheduleNoteDetails").text("");
		
	}
	else
	{
		$("#editscheduleNoteDetails").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#editscheduleNoteDetails").text("Field cannot be blank").css({"color":"red"});
		
	}
	if(startTime!=null && startTime!='' && startTime!='NaN/NaN/NaN')
	{
		$("#editeventStartTimes").attr("style", "display:none");
		$("#editeventStartTimes").text("");
	
	}
	else
	{
		$("#editeventStartTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#editeventStartTimes").text("Field cannot be blank").css({"color":"red"});
		
	}
	if(endTime!=null && endTime!=''&& endTime!='NaN/NaN/NaN')
	{
		
		$("#editeventEndTimes").attr("style", "display:none");
		$("#editeventEndTimes").text("");
		
	}
	else
	{
		$("#editeventEndTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
		$("#editeventEndTimes").text("Field cannot be blank").css({"color":"red"});
		
	}
	
	if(noteName != null && noteName != '' && startTime != null
			&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != '' && editallDayFlag==true &&  editeventFlag==true && $('#editEventRepeatDate').val()!=null && $('#editEventRepeatDate').val()!='')
	{
		
	if(allDayEvent)
	{
		if(editallDayFlag)
		{
		if($('#editRepeatEventId').val()!=null && $('#editRepeatEventId').val()!='' && $('#editRepeatEventId').val()!='--Select Repeat--')
		{
			var startrepeat=$('#editeventStartDate').val();
			var endRepeat=$('#editeventEndDate').val();
		
			if (startrepeat == endRepeat) {
				
				$("#editEventRepeatEventwarning").attr("style", "display:none");
				$("#editEventRepeatEventwarning").text("");
				$("#editeventBetweenDate").attr("style", "display:none");
				$("#editeventBetweenDate").text("");
				
				
					
			}
			else
			{
				editRepeatEventFlag=false;
				$("#editeventBetweenDate").attr("style", "display:none");
				$("#editeventBetweenDate").text("");
				$("#editEventRepeatEventwarning").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#editEventRepeatEventwarning").text("You can't create overlapping event.").css({"color":"red"});
			}
		}
		}
	}
	else
	{
		if(editeventFlag)
		{
		 if($('#editRepeatEventId').val()!=null && $('#editRepeatEventId').val()!='' && $('#editRepeatEventId').val()!='--Select Repeat--')
			{
				var startrepeat=$('#editeventStartTime').val();
				var endRepeat=$('#editeventEndTime').val();
			
				if (startrepeat.substring(0, startrepeat.length - 8).trim() == endRepeat.substring(0, endRepeat.length - 8).trim()) {
					
					$("#editEventRepeatEventwarning").attr("style", "display:none");
					$("#editEventRepeatEventwarning").text("");
					$("#editeventBetweenDate").attr("style", "display:none");
					$("#editeventBetweenDate").text("");
					
				}
				else
				{
					editRepeatEventFlag=false;
					$("#editeventBetweenDate").attr("style", "display:none");
					$("#editeventBetweenDate").text("");
					$("#editEventRepeatEventwarning").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#editEventRepeatEventwarning").text("You can't create overlapping event.").css({"color":"red"});
				}
			}
		}
	}
	}
	else
	{
		$("#editEventRepeatEventwarning").attr("style", "display:none");
		$("#editEventRepeatEventwarning").text("");
	}
	if(noteName != null && noteName != '' && startTime != null
		&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != '' && editallDayFlag==true &&  editeventFlag==true && editRepeatEventFlag==true && EditEventEndDate==true)
	{
		while(noteName.indexOf("\n") != -1){
			noteName = noteName.replace("\n"," ");
		}
		while(noteName.indexOf("\t") != -1){
			noteName = noteName.replace("\t"," ");
		}
		while(noteName.indexOf("\"") != -1){
			noteName = noteName.replace("\"", "`*`");
		}
		while(eventDiscription.indexOf("\n")!= -1){
			eventDiscription = eventDiscription.replace("\n"," ");
		}
		while(eventDiscription.indexOf("\t")!= -1){
			eventDiscription = eventDiscription.replace("\t","     ");
		}
		while(eventDiscription.indexOf("\"") != -1){
			eventDiscription = eventDiscription.replace("\"", "`*`");
		}
		if(eventLocation!= ""){
			while(eventLocation.indexOf("\"") != -1){
				eventLocation = eventLocation.replace("\"", "`*`");
			}
		}
	var url = urlForServer+"note/updateNote/"+listIdFromList+"/"+userId;
	var params = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","eventMembers":"","eventGroups":"","eventSharedAllContact":0,"eventSharedAllContactMembers":"","status":"A","vote":"","links":"","endDate":"'+endTime+'","eventId":"'+eventId+'","dueDate":"","dueTime":"","access":"private","attachFilePath":"","description":"'+eventDiscription+'","comments":"","location":"'+eventLocation+'","repeatEvent":"'+repeatEvent+'","eventStartDate":"'+editEventRepeatStart+'","eventEndDate":"'+editEventRepeatDate+'"}';

	params = encodeURIComponent(params);

    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
    	//alert($('#scheduleContent').children('#'+scheduleListId+'').children('.listBodyAll').children('div').attr('id'));
    	/* cmt due to order by date and time -- kishore.
    	 * $('#scheduleContent').children('#'+scheduleListId+'').children('.listBodyAll').append('<div class="noteDiv" id="'+response.eventId+'" style="border: 1px solid rgb(221, 221, 221); background-color: rgb(255, 255, 255);">'
    			//$('#'+scheduleListId).children('.listBodyAll').append('<div class="noteDiv" id="'+response.eventId+'">'
    					+ '<div class="todo_description">'
    					//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
    					//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
    					//+'</div>'
    					+'<p><b>'
    					+ noteName
    					+ '</b></p></div>'
						+'<div class="badges">'
						+ '</div>'
    					+ '</div>');*/
    			
    			
    			if(response!="0")
    			{	
    				listname=response;
    				  var scheduleUrl = urlForServer+"note/updateScheduleNote/"+listIdFromList;
    				  
    				    if(eventId!="" && eventId!="0")
    				    {
    					var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","repeatEvent":"'+repeatEvent+'","eventStartDate":"'+editEventRepeatStart+'","eventEndDate":"'+editEventRepeatDate+'","userId":"'+userId+'","listType":"schedule","eventId":"'+eventId+'","allDay":"'+allDayEvent+'","description":"'+eventDiscription+'","listName":"'+listname+'","location":"'+eventLocation+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmailFlag+'"}';
    					
    					scheduleParams = encodeURIComponent(scheduleParams);
    					
    				    $.ajax({
    				    	headers: { 
    				    	"Mn-Callers" : musicnote,
    				    	"Mn-time" :musicnoteIn					
    				    	},
    				    	type: 'POST',
    				    	url : scheduleUrl,
    				    	cache: false,
    				    	contentType: "application/json; charset=utf-8",
    				    	data:scheduleParams, 
    				    	dataType: "json",
    				    	success : function(response){
    				    	     if(response!='0'){
    				    	    	// hide due to event by date & time order
 	    				        	/*if($('#scheduleContent').children('.listDiv').children().attr('class')=='modalHeader')
 	    				        	{
 	    				        		//alert(userId +" "+listId +" "+listType);
 	    				        		loadEventList(userId,listId,listType);
 	    				        	}
 	    				        	else
 	    				        	{
 	    				        		loadAllEvents();
 	    				        	}*/
    				    	    	
    				    	    	// loadAllEvents();
    				    	    	 var listId=$('#notebookId').val();
	    				        	 loadEventList(userId,listId,listType);
    				    	    	 getTodaySchedule();
    				    	    	 
    				    	    	 // add due to event by date & time order - kishore
    				    	    	 loadEventsBooks();
    				     			 getListAndNoteNames();
    				    	     }
    				    	     else
    				    	    	 alert("Please try again later");
    			          },
    			          error: function(e) {
    			              alert("Please try again later");
    			          }
    			      
    				    });
    				    }
    			}
    			else
    			{
    				alert("Please try again later");
    			}
        },
        error: function(e) {
            alert("Please try again later");
        }
    
    });
    
    $('#editScheduleModel').modal('hide');
   /* if(eventEdit==1) // from more actions
    $('#moreActions').modal('show');*/
	}else
	{
		$('#editScheduleModel').modal('show');
	}
	eventEdit=null;
});
	
	//// edit end
	
	$('#editcancelScheduleNote').click( function() {
		                    $("#editscheduleNoteNames").attr("style",
									"display:none");
							$("#editscheduleNoteNames").text("");
							$("#editscheduleNoteDetails").attr("style",
									"display:none");
							$("#editscheduleNoteDetails").text("");
							$("#editeventStartTimes").attr("style",
									"display:none");
							$("#editeventStartTimes").text("");
							$("#editeventEndTimes").attr("style",
									"display:none");
							$("#editeventEndTimes").text("");
							$("#editEventRepeatEventwarning").attr("style", "display:none");
							$("#editEventRepeatEventwarning").text("");
							$("#editeventBetweenDate").attr("style", "display:none");
							$("#editeventBetweenDate").text("");
							$('#editRepeatEventId').val('--Select Repeat--');
							$("#editEventRepeatEndDate").attr("style", "display:none");
							$("#editEventRepeatDate").val('');
							$("#editEventRepeatDateWarningMsg").attr("style", "display:none");
							$("#editEventRepeatDateWarningMsg").text("");
							$("#sendEditEventMailCheckbox").attr('checked', false); 
						
							
		 if(eventEdit==1) // from more actions
		 {
			    $('#moreActions').modal('show');
			    eventEdit=null;
		 }
	});
	
	//cancel the Add schedule note and hide add schedule note model panel 
	$('#cancelScheduleNote').click( function() {
		            $("#scheduleNoteNames").attr("style", "display:none");
					$("#scheduleNoteNames").text("");
					$("#scheduleNoteDetails").attr("style", "display:none");
					$("#scheduleNoteDetails").text("");
					$("#eventStartTimes").attr("style", "display:none");
					$("#eventStartTimes").text("");
					$("#eventEndTimes").attr("style", "display:none");
					$("#eventEndTimes").text("");
		            $('#addScheduleModel').modal('hide');
		            $('#eventRepeatDate').val("");
		    		$('#eventRepeatEndDate').attr("style","display:none");
		    		$('#repeatEventId').val("--Select Repeat--");
		    		$("#eventRepeatEventwarning").attr("style", "display:none");
					$("#eventRepeatEventwarning").text("");
					$("#eventBetweenDate").attr("style", "display:none");
					$("#eventBetweenDate").text("");
					$("#sendEventMailCheckbox").attr('checked', false);
	});
	
	var fromListId;
	$('#notes').on('click','.js-move-notes',function(){
		
		var dataCheck="";
		
		moveAllNotesListId=$(this).parent().parent().parent().parent().parent().children('.listBodyAll');

		var listName=$(this).parent().parent().parent().parent().children().find('.headerListName').text();
		fromListId=$(this).parent().parent().parent().parent().parent().attr('id');
		
		$("#moveAllNotes .modalBody").find('p').each(function( index ){
			$(this).remove();
		});
		
		// *************** move to another note ************* //
		
		var url = urlForServer+"note/fetchBookNames";
		var params = '{"userId":"'+userId+'","listType":"'+listType+'"}';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
			type: 'POST',
			url : url,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:params, 
			dataType: "json",
			success : function(response)
			{
			for(var i=0;i<response.length; i++)
			{
				
				if(fromListId != response[i].listId){
			    								
						$("#moveAllNotes .modalBody").append('<p><a id="'+response[i].listId+'" style="text-decoration: none" class="moveSelectListName" href="javascript:void(0);">'+response[i].bookName+'</a></p>');
					dataCheck="1";
			    }
        	}
			
			if(dataCheck==""){
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text("Please Add Book To Move");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			}else{
				$('#moveAllNotes').modal('toggle');
			}
		},
        	error: function(e) {
        		alert("Please try again later");
        	}
		});
		
		//*******************************//
		
		
		/*moveListId=$(this).parent().parent().parent().parent().parent();
		//alert(moveListId);
		$("#moveAllNotes").find('.moveSelectListName').each(function( index ){
			$(this).remove();
		});
		$("#moveAllNotes .modalBody").find('p').each(function( index ){
			$(this).remove();
		});
		
		$('#notes').find('.headerListName').each(function( index ){
			var tempListName=$(this).text();
			var id=$(this).parent().parent().parent().attr('id');
			if(listName != tempListName){
				if(listAccessMap[id]==userId){
					$("#moveAllNotes .modalBody").append('<p><a id="'+id+'" style="text-decoration: none" class="moveSelectListName" href="javascript:void(0);">'+$(this).text()+'</a></p>');
					dataCheck="1";
				}
			}
		});
		if(dataCheck==""){
			$("#moveCopyWarning").modal('show');
			$("#move-msg-header-col-md-1").attr('class',"label label-info").text("Warning");
			$("#move-modal-message1").attr('class',"label label-info").text("  Please Add Book To Move");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
		}else{
			$('#moveAllNotes').modal('toggle');
		}*/
	});
	
	
	$('#moveAllNotesModel').click(function(){
		$('#moveAllNotes').modal('hide');
	});
	
	
	$('#moveAllNotes').on('click','.moveSelectListName',function()
	{
	
		var tempSelectedName=$(this).text();
		var id=$(this).attr('id');
		//var listId=$(moveListId).attr('id');
		
		$('#notes').find('.moveSelectListName').each(function( index ){
			
			var tempListName=$(this).text();
			var SelectListPath=$(this).parent().parent().parent();
			
			if(tempSelectedName == tempListName){
				
				var url = urlForServer+"note/moveAllNotes/"+userId;
				var params = '{"selectedListId":"'+id+'","listId":"'+fromListId+'"}';
					
			    params = encodeURIComponent(params);

			    $.ajax({
			    	headers: { 
			    	"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
			    	},
			    	type: 'POST',
			    	url : url,
			    	cache: false,
			    	contentType: "application/json; charset=utf-8",
			    	data:params, 
			    	dataType: "json",
			    	success : function(response){
					
					
					$('#notes').children('#'+fromListId).children('.listBodyAll').empty();
			    		/*$(SelectListPath).find('.noteDiv').each(function( index ){
			    			$(this).remove();
			    		});
			    		
			    		var data = response;
			    		for(var i=0;i<data.length;i++){
			    			if(data[i].status=='A'){
			    				var dueDateData="";
								var cmts="";
								var votes="";
								var desc="";
			    				if(data[i].dueDate != "" && data[i].dueTime !="")
			    					dueDateData=loadDueDateContent(data[i].dueDate , data[i].dueTime);
									
								if( data[i].comments != "")	
									cmts = loadCmts(data[i].comments);
								
								if(data[i].vote !="")
									votes = loadVote(data[i].vote);
								
								if(data[i].description != "")
									desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text"></span></div>';
										
			    				$(SelectListPath).children('.listBodyAll').append('<div class="noteDiv" id="'+data[i].noteId+'">'
			    						+ '<div class="todo_description">'
			    						//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
			    						//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
			    						//+'</div>'
			    						+'<p>'
			    						+ data[i].noteName
			    						+ '</p></div>'
											+'<div class="badges">'
												+votes
												+cmts
												+desc
												+dueDateData
											+'</div>'
			    						+ '</div>');
			    			}
			    		}
						$(moveAllNotesListId).find(".noteDiv").each(function( index ){
							$(this).remove();
						});*/
						////////////  getlist /////////////
						
				
						
						//$('#notes').find('.listDiv').each(function( index ){
		//alert("remove called");
		//$(this).remove();
					//});
	
	
						$('#moveAllNotes').modal('hide');
		            },
		            error: function(e) {
		                alert("Please try again later");
		            }
			    });
			}
		});
	});
	
	/* Delete All notes Modal Panel show Event */
	var deleteAllNotePath="";
	var deleteAllId="";
	$('#notes').on('click','.js-archive-notes',function(){
		deleteAllNotePath=$(this).parent().parent().parent().parent().parent().children('.listBodyAll');
		deleteAllId=$(this).parent().parent().parent().parent().parent().attr('id');
		
		if(listType!='schedule')
			$('.delete-All-warn').text('Do you want to delete all note(s) from this Notebook ?');
		else
			$('.delete-All-warn').text('Do you want to delete all event(s) from this Calendar ?');
		
		$('#deleteAllNoteModal').modal('toggle');
	});
		
	/* Delete All notes Modal Panel Action Event */
	$('#notes').on('click','.js-delete-allNote',function(){
		
		var url = urlForServer+"note/archieveAllNotes";
		var params = '{"listId":"'+deleteAllId+'","userId":"'+userId+'","listType":"'+listType+'"}';
	    
	    params = encodeURIComponent(params);
	    

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	
	    		$(deleteAllNotePath).find(".noteDiv").each(function( index ){
	    			$(this).remove();
	    		});
	    		$('#deleteAllNoteModal').modal('hide');
	    		if(listType=='schedule'){
	    			$('#calendarlist').empty();
	    			loadAllEvents();
	    			loadCalendarComboBoxList();
	    		}
	    		//$(".listMenu").popover('hide');	
            },
            error: function(e) {
                alert("Please try again later");
            }
        
	    });
	});
	
	
	// enable disable book
$('#notes').on('click','.js-enableBook',function(){
	
	var status=$(this).text();
	if(status=="Enable")
	{
		$('#scheduleEnable').find('.modalBody').find('.control-group').find('.control-label').remove();
		$('#scheduleEnable').find('.modalBody').find('.control-group').append('<label class="control-label" for="name">Are you sure you want to Enable this book?</label>');
	}
	else
	{
		$('#scheduleEnable').find('.modalBody').find('.control-group').find('.control-label').remove();
		$('#scheduleEnable').find('.modalBody').find('.control-group').append('<label class="control-label" for="name">Are you sure you want to Disable this book from calendar?</label>');
	}
	$('#scheduleEnable').modal('toggle');		
	//bookId=$(this).parent().parent().parent().parent().parent().attr('id');
	//bookId=$(this).parent().parent().parent().parent().parent().parent().parent().parent().attr('id');
			});


$('#enableYes').on('click',function(){
		
		var url = urlForServer+"note/bookEnableDisable";
		var params = '{"listId":"'+bookId+'","userId":"'+userId+'"}';
	    params = encodeURIComponent(params);
	

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	$(".listMenu").popover('hide');	
        	$('#scheduleEnable').modal('hide');
        	$(".listMenuOthers").popover('hide');	
        	loadAllEvents();
        	loadCalendarComboBoxList();
        	getTodaySchedule();
            },
            error: function(e) {
            	$(".listMenu").popover('hide');	
            	$('#scheduleEnable').modal('hide');
            	$(".listMenuOthers").popover('hide');	
            	loadAllEvents();
            	loadCalendarComboBoxList();
            	getTodaySchedule();
            }
        
	    });
	});
	
	
	$('#notes').on('click','.moveSelectNoteName',function(){
		
		var tempSelectedName=$(this).text();
		var selectedListId=$(this).attr('id');
		//var listId=$(moveListId).attr('id');
		
		//var noteId=$(this).attr('id');
		var noteId=noteIdFromList;
		var listId=listIdFromList;
		
		
		if(noteJsMoveJsCopyType == 'move')
		{
			var url = urlForServer+"note/moveNote/"+userId;
			var params = '{"selectedListId":"'+selectedListId+'","listId":"'+listId+'","noteId":"'+noteId+'"}';
			params = encodeURIComponent(params);

			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type: 'POST',
				url : url,
				cache: false,
				contentType: "application/json; charset=utf-8",
				data:params, 
				dataType: "json",
				success : function(response)
				{
				
				if($('#notes').children('#'+listId+'').children().attr('class')=='modalHeader')
					$('#notes').children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').remove();
			},
			error: function(e) {
        		alert("Please try again later");
        	}
		});
		}
		else if(noteJsMoveJsCopyType == 'copy')
		{
			var url = urlForServer+"note/copyNote/"+userId;
			var params = '{"selectedListId":"'+selectedListId+'","listId":"'+listId+'","noteId":"'+noteId+'"}';
			params = encodeURIComponent(params);
	

			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type: 'POST',
				url : url,
				cache: false,
				contentType: "application/json; charset=utf-8",
				data:params, 
				dataType: "json",
				success : function(response)
				{
				var dueDateData="";
				var cmts="";
				var votes="";
				var desc="";
				var tags="";
				var reminder="";
				var datedesc="";
				var tempDescAfterNote="";
    			if(response['dueDate'] != "" && response['dueTime'] != "")
    				dueDateData=loadDueDateContent(response['dueDate'],response['dueTime']);
				
				if( response['comments'] != "")	
					cmts = loadCmts(response['comments'] );
			
				if(response['vote'] !="")
					votes = loadVote(response['vote']);
			
				if(response['description'] != "")
					desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
				
				if(response['tag'] !="")
					tags = loadTag(response['tag']);
				
				if(response['remainders'] !="")
					reminder = loadReminder(response['remainders']);
				
				if(listType=='bill')
				{
					var date=response['startDate'];
					datedesc=loadMemoDate(response['startDate']);
				}else{
					datedesc="";
				}
				
				if(response['description'] !="")
				tempDescAfterNote = loadDescContent(response['noteName'],response['description']);
				
				var listId=$('#notebookId').val();
				if(listId !='note'){
					if(selectedListId == listId){
						$('.listBodyAll').prepend('<div class="noteDiv" id="'+response['noteId']+'" style="border: 1px solid #dddddd; background-color:#F3F3F3;" >'
								+ '<div class="todo_description fontStyle15">'
								+'<p><b>'
								+ response['noteName']
								+'</b>&nbsp;&nbsp;&nbsp;&nbsp;'
								+tempDescAfterNote
								+'</p></div>'
								+'<div class="badges fontStyle">'
								+votes
								+cmts
								+desc
								+dueDateData
								+tags
								+reminder
								+datedesc
								+ '</div>'
								+ '</div>');
					}
				}else{
					$('#notes').children('#searchDiv').after('<div class="listDiv col-md-11" id="'+selectedListId+'" style="float:left; background-color:#F3F3F3;">'
    					+'<div class="listBodyAll" style="background-color:#F3F3F3;">'
    					+'<div id="'+response['noteId']+'" class="noteDiv " style="background-color:#F3F3F3;">'
    					+ '<div class="todo_description fontStyle15">'
    					+'<p><b>'
    					+ response['noteName']
    					+ '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
						+tempDescAfterNote
						+'</p></div>'
							+'<div class="badges fontStyle">'
							+votes
							+cmts
							+desc
							+tags
							+dueDateData
							+reminder
							+datedesc
							+'</div>'
    					+ '</div>');
				}
			},
        	error: function(e) {
        		alert("Please try again later");
        	}
		});
		}
		
		
		/*$('#notes').find('.headerListName').each(function( index ){
			
			var tempListName=$(this).text();
			
			var SelectListPath=$(this).parent().parent().parent();
			var tempNoteId = $(moveNotesId).attr('id');
			if(tempSelectedName == tempListName){
				
				$(moveNotesId).parent().find(".noteDiv").each(function( index ){
					
					var tempNoteName=$(this).find("p").text();
					
					var indexNoteId =  $(this).attr('id');
					
					if(moveNotesName == tempNoteName && indexNoteId==tempNoteId){
						
						if(noteJsMoveJsCopyType == 'move'){
							
							var noteId=$(this).attr('id');
						
							var url = urlForServer+"note/moveNote/"+userId;
							var params = '{"selectedListId":"'+selectedListId+'","listId":"'+listId+'","noteId":"'+noteId+'"}';
							alert(url+" "+params);
							params = encodeURIComponent(params);

							$.ajax({
							headers: { 
		    	"Ajax-Call" : userTokens,
		    	"Ajax-Time" :userLoginTime				
		    	},
								type: 'POST',
								url : url,
								cache: false,
								contentType: "application/json; charset=utf-8",
								data:params, 
								dataType: "json",
								success : function(response){
					    			var dueDateData="";
									var cmts="";
									var votes="";
									var desc="";
					    			if(response['dueDate'] != "" && response['dueTime'] != "")
					    				dueDateData=loadDueDateContent(response['dueDate'],response['dueTime']);
									
									if( response['comments'] != "")	
										cmts = loadCmts(response['comments'] );
								
									if(response['vote'] !="")
										votes = loadVote(response['vote']);
								
									if(response['description'] != "")
										desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text"></span></div>';
										
					    			$(SelectListPath).children('.listBodyAll').append('<div class="noteDiv" id="'+response['noteId']+'">'
					    					+ '<div class="todo_description">'
					    					//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
					    					//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
					    					//+'</div>'
					    					+'<p>'
					    					+ response['noteName']
					    					+ '</p></div>'
												+'<div class="badges">'
												+votes
												+cmts
												+desc
												+ dueDateData
												+'</div>'
					    					+ '</div>');
				            	},
				            	error: function(e) {
				            		alert("Please try again later");
				            	}
							});
						}else if(noteJsMoveJsCopyType == 'copy'){
							
							var noteId=$(this).attr('id');
							
							var url = urlForServer+"note/copyNote/"+userId;
							var params = '{"selectedListId":"'+selectedListId+'","listId":"'+listId+'","noteId":"'+noteId+'"}';
					    
							params = encodeURIComponent(params);
					

							$.ajax({
							headers: { 
		    	"Ajax-Call" : userTokens,
		    	"Ajax-Time" :userLoginTime				
		    	},
								type: 'POST',
								url : url,
								cache: false,
								contentType: "application/json; charset=utf-8",
								data:params, 
								dataType: "json",
								success : function(response){
					    			var dueDateData="";
									var cmts="";
									var votes="";
									var desc="";
									var att="";
					    			if(response['dueDate'] != "" && response['dueTime'] != "")
					    				dueDateData=loadDueDateContent(response['dueDate'],response['dueTime']);
									
									if( response['comments']  != "")	
										cmts = loadCmts(response['comments'] );
								
									if(response['vote'] !="")
										votes = loadVote(response['vote'] );
								
									if(response['description'] != "")
										desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text"></span></div>';
									
									if(response['attachFilePath'] != "")	
										att = loadAttached(response['attachFilePath']);
										
					    			$(SelectListPath).children('.listBodyAll').append('<div class="noteDiv" id="'+response['noteId']+'">'
					    					+ '<div class="todo_description">'
					    					//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
					    					//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
					    					//+'</div>'
					    					+'<p>'
					    					+ response['noteName']
					    					+ '</p></div>'
												+'<div class="badges">'
												+votes
												+cmts
												+desc
												+ dueDateData
												+'</div>'
					    					+ '</div>');
				            	},
				            	error: function(e) {
				            		alert("Please try again later");
				            	}
							});
						}
					}
				});
				if(noteJsMoveJsCopyType == 'move'){
					$(moveNotesId).parent().find(".noteDiv").each(function( index ){
						var tempNote=$(this).find("p").text();
						var indexNoteId =  $(this).attr('id');
							if(moveNotesName==tempNote && indexNoteId==tempNoteId){
								$(this).remove();
							}	
					});
				}*/
				
				$('#moveNotes').modal('hide');
				
				if(moreActionEvent=='copy'){
    				$('#moreActions').modal('toggle');
    				moreActionEvent='';
    			}
			/*}
		});*/
	});
	
	
	/*Move note one list to another list*/
	$('#notes').on('click','.js-move',function(){
		
		var listName;
		var dataCheck="";
		
		noteJsMoveJsCopyType="move";
		
		if($(this).parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			moveNotesId=$(moreActionBasedId).parent();
			
			moveListId=$(moreActionBasedId).parent().parent().parent();
		
			moveNotesName=$(moreActionBasedId).find("p").text();

			listName=$(moveNotesId).parent().parent().find('.headerListName').text();
			
			moreActionEvent='move';
			$('#moreActions').modal('hide');
		}else{
			
			$(".noteMenu").popover('hide');
			
			moveNotesId=$(this).parent().parent().parent().parent().parent().parent().parent().parent();
			
			moveListId=$(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent();
		
			moveNotesName=$(this).parent().parent().parent().parent().parent().parent().parent().find("p").text();
		
			listName=$(moveNotesId).parent().parent().find('.headerListName').text();
		}
		
		$("#moveNotes").find('.moveSelectNoteName').each(function( index ){
			$(this).remove();
		});
		$("#moveNotes .modalBody").find('p').each(function( index ){
			$(this).remove();
		});
		
		// *************** move to another note ************* //
		
		var url = urlForServer+"note/fetchBookNames";
		var params = '{"userId":"'+userId+'","listType":"'+listType+'"}';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
			type: 'POST',
			url : url,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:params, 
			dataType: "json",
			success : function(response)
			{
			for(var i=0;i<response.length; i++)
			{
				
				if(listIdFromList != response[i].listId){
			    		$("#moveNotes .modalBody").append('<p><a id="'+response[i].listId+'" style="text-decoration: none" class="moveSelectNoteName" href="javascript:void(0);">'+response[i].bookName+'</a></p>');
			    		dataCheck="1";
			    }
        	}
			
			if(dataCheck==""){
				$("#moveCopyWarning").modal('show');
				$("#move-modal-message1").text("  Please Add Book To Move");
				setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			}else{
				$('#moveNotes').modal('toggle');
			}
		},
        	error: function(e) {
        		alert("Please try again later");
        	}
		});
		
		//*******************************//
		/*$('#notes').find('.headerListName').each(function( index ){
			
			var tempListName=$(this).text();
			var id=$(this).parent().parent().parent().attr('id');
			alert(tempListName);
		    if(listName != tempListName){
		    	if(listAccessMap[id]==userId){
		    		$("#moveNotes .modalBody").append('<p><a id="'+id+'" style="text-decoration: none" class="moveSelectNoteName" href="javascript:void(0);">'+tempListName+'</a></p>');
		    		dataCheck="1";
		    	}
		    }
		});*/

		$('#moveNotes').find('#myModalLabel').each(function( index ){
			$(this).html("Move Note");
		});
		/*if(dataCheck==""){
			$("#moveCopyWarning").modal('show');
			$("#move-msg-header-col-md-1").attr('class',"label label-info").text("Warning");
			$("#move-modal-message1").attr('class',"label label-info").text("  Please Add Book To Move");
			setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
		}else{
			$('#moveNotes').modal('toggle');
		}*/
	});
	
	/*Copy note one list to another list (same move functionality used for copy also)*/
	$('#notes').on('click','.js-copy-note',function(){
		var listName;
		var dataCheck="";
		noteJsMoveJsCopyType="copy";
		
		if($(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			moveNotesId=$(moreActionBasedId).parent();
			
			moveListId=$(moreActionBasedId).parent().parent().parent();
		
			moveNotesName=$(moreActionBasedId).find("p").text();
			
			listName=$(moveNotesId).parent().parent().find('.headerListName').text();
			
			moreActionEvent='copy';
			$('#moreActions').modal('hide');
		}else{
			$(".noteMenu").popover('hide');
		
			/*moveNotesId=$(this).parent().parent().parent().parent().parent().parent().parent().parent();
		
			moveListId=$(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent();
		
			moveNotesName=$(this).parent().parent().parent().parent().parent().parent().parent().find("p").text();
		
			listName=$(moveNotesId).parent().parent().find('.headerListName').text();*/
		}
		
		$("#moveNotes").find('.moveSelectNoteName').each(function( index ){
			$(this).remove();
		});
		$("#moveNotes .modalBody").find('p').each(function( index ){
			$(this).remove();
		});
		
		
		var url = urlForServer+"note/fetchBookNames";
		var params = '{"userId":"'+userId+'","listType":"'+listType+'"}';
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type: 'POST',
			url : url,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:params, 
			dataType: "json",
			success : function(response)
			{
				for(var i=0;i<response.length; i++)
				{
			    		$("#moveNotes .modalBody").append('<p><a id="'+response[i].listId+'" style="text-decoration: none" class="moveSelectNoteName" href="javascript:void(0);">'+response[i].bookName+'</a></p>');
			    		dataCheck="1";
				}
			
				if(dataCheck==""){
					$("#moveCopyWarning").modal('show');
					$("#move-modal-message1").text("  Please Add Book To Move");
					setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
				}else{
					$('#moveNotes').modal('toggle');
				}
			},
        	error: function(e) {
        		alert("Please try again later");
        	}
		});
		
		$('#moveNotes').find('#myModalLabel').each(function( index ){
			$(this).html("Copy Note");
		});
		
	});
	
	
	/*Copy note on public note to our own list*/
	$('#moreActions').on('click','.js-crowd-copy-note',function(){
		var url = urlForServer+"note/copyPublicNote";
		var params = '{"userId":"'+userId+'","listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'"}';
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
			type: 'POST',
			url : url,
			cache: false,
			contentType: "application/json; charset=utf-8",
			data:params, 
			dataType: "json",
			success : function(response){
    				$("#moreActions").modal('hide');	
    				$('#msg-modal-message-modal').text('Successfully copied note to default notes book ');
    				$('#msgNewModalCrowd').modal('toggle');
        	},
        	error: function(e) {
        		alert("Please try again later");
        	}
		});
		setTimeout("$(\"#msgNewModalCrowd\").modal('hide');", 3000);
	});
	$('#moreActions').on('click','.js-crowd-copied-note',function(){
		$("#moreActions").modal('hide');	
		//$( "#msg-header-span-modal" ).removeClass( "label-info" ).addClass( "label-warning" );
		//$('#msg-modal-message-modal').removeClass( "label-info" ).addClass( "label-warning" );
		$('#msg-modal-message-modal').text('Already added this note');
		$('#msgNewModalCrowd').modal('toggle');
		setTimeout("$(\"#msgNewModalCrowd\").modal('hide');", 3000);
	});
	$('#moreActions').on('click','.js-crowd-report-note',function(){
		$("#moreActions").modal('hide');
		$("#userReportDialog").attr('style','display:none;');
		 $('#complaintReportYes').attr("disabled", false);
		$('#complaintReportModal').modal('toggle');
		$('#complaintReportModal').children('.modalBody').children('.control-group').empty();
		$('.modalFooter').attr('style','display:block');
		$('.modalHeader').attr('style','display:block');
		$('.control-group').empty();
		$('#complaintReportModal').children('.modal-dialog').children('.modal-content').children('.modalBody').children('.control-group').append('<div id="reportFor"><input id="reportDesc" class="reportValue" type="checkbox" style="color:white;margin-top:-4px;margin-right: 5px;" value="description" name="reportFor">Description</input><input id="reportAttach" class="reportValue" type="checkbox" style="color:white;margin-left:40px;margin-top:-4px;margin-right: 5px;" value="attachment" name="reportFor">Attachment</input><input id="reportComment" class="reportValue" type="checkbox" style="color:white;margin-left:40px;margin-top:-4px;margin-right: 5px;" value="comment" name="reportFor">Comment</input></div><br><textarea class="control-label-text" id="userComplaint" style="font-size: 14px;height: 70px;margin-left: 1px; width: 96%;"></textarea>');
		$('#complaintReportModal').children('.modal-dialog').children('.modal-content').children('.modalBody').children('.control-group').append('<br><div id="userReportDialog" style="font-family: Helvetica Neue;font-size:14px;display:none" title="Basic dialog"><font color="red"></font></div>');
	});
	
	$("#complaintReportNo").click(function(){
		$('#complaintReportYes').attr("disabled", false);
		$('#complaintReportModal').modal('hide');
		$("#moreActions").modal('show');
	});
	
	$("#reportCancel").click(function(){
		$('#reportYes').attr("disabled", false);
		$('#complaintReportModalForComment').modal('hide');
		$("#moreActions").modal('show');
	});
	
	////////////report function for note and memo/////////////////////
	$("#reportYesForNote").click(function(){
		 var moveNotesName=$(moreActionBasedId).find("p").find("b").text();
		 var moveNotesId=$(moreActionBasedId).parent().attr('id');
		 var moveListId=$(moreActionBasedId).parent().parent().parent().attr('id');
		// var split=$(moreActionBasedId).find("p").attr('id').split('~');
		 var noteUserId=listOwnerUserId;
		 var noteListId=moveListId;
		 var noteNoteId=moveNotesId;
		 var userComplaint=$(".userComplaintForComment").val();
		 var commentId=$(".userComplaintForComment").attr('id');
		 if(userComplaint!=""){
			 $('#repDialog').empty();
			 $('#reportYesForNote').attr("disabled", true);
			 var url = urlForServer+"note/complaintReportMail";
		
			 var params = '{"reportLevel":"comment","commentId":"'+commentId+'","userComplaint":"'+userComplaint+'","userId":"'+userId+'","noteName":"'+moveNotesName+'","noteUserId":"'+noteUserId+'","noteListId":"'+noteListId+'","noteNoteId":"'+noteNoteId+'","reportpage":"'+listType+'"}';
			
			 $.ajax({
				 headers: { 
				 "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
				    	},
				type: 'POST',
				url : url,
				cache: false,
				data:params, 
				success : function(response){
				if(response.match("success")) {
					$('#reportYesForNote').attr("disabled", false);
					//$('#complaintReportModal').modal('hide');
					$('.modalFooter').attr('style','display:none');
					$('.modalHeader').attr('style','display:none');
					$('#complaintReportModalForComment').children('.modalBody').children('.control-group').empty();
					$('#complaintReportModalForComment').children('.modalBody').children('.control-group').append('<div class="fontStyle" style="padding:13px;">Your Request is being processed and will respond to it as soon as possible</div>');
					setTimeout(function(){$('#complaintReportModalForComment').modal('hide');$("#moreActions").modal('show');},4000);
	    			
				}
				},
	        	error: function(e) {
	        		alert("Please try again later");
	        	}
			});
		 }else{
			 $( "#repDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#repDialog').empty();
			 $("#repDialog").text("Field cannot be blank");
		 }
	});
////////////report function for crowd for admin/////////////////////
	$("#reportYes").click(function(){
		//$(this).attr('disabled',true);
		 var moveNotesName=$(moreActionBasedId).find("p").text();
		 var moveNotesId=$(moreActionBasedId).find("p").attr('id');
		 var split=$(moreActionBasedId).find("p").attr('id').split('~');
		 var noteUserId=split[0];
		 var noteListId=split[1];
		 var noteNoteId=split[2];
		 var userComplaint=$(".userComplaintForComment").val();
		 var commentId=$(".userComplaintForComment").attr('id');
		 if(userComplaint!=""){
			 $('#repDialog').empty();
			 $('#reportYes').attr("disabled", true);
			 var url = urlForServer+"note/complaintReportMail";
		
			 var params = '{"reportLevel":"comment","commentId":"'+commentId+'","userComplaint":"'+userComplaint+'","userId":"'+userId+'","noteName":"'+moveNotesName+'","noteUserId":"'+noteUserId+'","noteListId":"'+noteListId+'","noteNoteId":"'+noteNoteId+'","reportpage":"'+listType+'"}';
			 $.ajax({
				 headers: { 
				 "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
				    	},
				type: 'POST',
				url : url,
				cache: false,
				data:params, 
				success : function(response){
				if(response.match("success")) {
					$('#reportYes').attr("disabled", false);
					//$('#complaintReportModal').modal('hide');
					$('.modalFooter').attr('style','display:none');
					$('.modalHeader').attr('style','display:none');
					$('#complaintReportModalForComment').children('.modalBody').children('.control-group').empty();
					$('#complaintReportModalForComment').children('.modalBody').children('.control-group').append('<div class="fontStyle" style="padding:13px;">Your Request is being processed and will respond to it as soon as possible</div>');
					setTimeout(function(){$('#complaintReportModalForComment').modal('hide');$("#moreActions").modal('show');},4000);
	    			
				}
				},
	        	error: function(e) {
	        		alert("Please try again later");
	        	}
			});
		 }else{
			 $( "#repDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#repDialog').empty();
			 $("#repDialog").text("Field cannot be blank");
		 }
	});
	
	
	$("#complaintReportYes").click(function(){
		
		 var chkArray = [];
		 var reportLevel;
		 var moveNotesName=$(moreActionBasedId).find("p").text();
		 var moveNotesId=$(moreActionBasedId).find("p").attr('id');
		 var split=$(moreActionBasedId).find("p").attr('id').split('~');
		 var noteUserId=split[0];
		 var noteListId=split[1];
		 var noteNoteId=split[2];

		 $(".reportValue:checked").each(function() {
		        chkArray.push($(this).val());
		    });
		     
		    reportLevel = chkArray.join(',') + ",";
		     
		    if(reportLevel.length > 1){
		    	var length = reportLevel.length;
		    	var lastChar = reportLevel.substring(length-1, length); 
		    	if (lastChar == ",") { 
		    		reportLevel = reportLevel.substring(0, length-1);
		    	}
		    	else { 
		    		reportLevel = reportLevel;
		    	} 	
		    	
		 var userComplaint=$("#userComplaint").val().trim();
		 userComplaint = userComplaint.replace( /[\s\n\r]+/g, ' ' );
		 while(userComplaint.indexOf("\"") != -1){
			 userComplaint = userComplaint.replace("\"", "`*`");
			}
		 if(userComplaint!=""){
			 $('#complaintReportYes').attr("disabled", true);
			 var url = urlForServer+"note/complaintReportMail";
		
			 var params = '{"reportLevel":"'+reportLevel+'","commentId":"note","userComplaint":"'+escape(userComplaint)+'","userId":"'+userId+'","noteName":"'+moveNotesName+'","noteUserId":"'+noteUserId+'","noteListId":"'+noteListId+'","noteNoteId":"'+noteNoteId+'","reportpage":"'+listType+'"}';
			 $.ajax({
				 headers: { 
				 "Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn				
				    	},
				type: 'POST',
				url : url,
				cache: false,
				data:params, 
				success : function(response){
				if(response.match("success")) {
					$('#complaintReportYes').attr("disabled", false);
					//$('#complaintReportModal').modal('hide');
					$('.modalFooter').attr('style','display:none');
					$('.modalHeader').attr('style','display:none');
					$('#complaintReportModal').children('.modal-dialog').children('.modal-content').children('.modalBody').children('.control-group').empty();
					$('#complaintReportModal').children('.modal-dialog').children('.modal-content').children('.modalBody').children('.control-group').append('<div class="fontStyle" style="padding:13px;">Your Request is being processed and will respond to it as soon as possible</div>');
					setTimeout(function(){$('#complaintReportModal').modal('hide');$("#moreActions").modal('show');},4000);
	    			
				}
				},
	        	error: function(e) {
	        		alert("Please try again later");
	        	}
			});
		 }else{
			 $( "#userReportDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		     	$('#userReportDialog').empty();
			 $("#userReportDialog").text("Field cannot be blank");
		  }
		}else{
		     $( "#userReportDialog" ).attr('style','display : block;color:red;margin-top:-4px;');	
		  	$('#userReportDialog').empty();
			 $("#userReportDialog").text("Please select atleast one of the checkbox");
		 }
	});
	/*Handles attached files to crowds*/
	$('#crowd').on('click','.js-attach',function(){
		$('#moreActions').modal('hide');
		var url = urlForServer+"note/getAttachFile/"+listIdFromList+"/"+noteIdFromList;
		var response="";	
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url,
				success : function(responseText) 
				{
				response = responseText;
				
					if(response.indexOf("[") != -1){
						response = response.substring(response.indexOf("[")+1,response.length);
					}
					if(response.indexOf("]") != -1){
						response = response.substring(0,response.indexOf("]"));
					}
					if(response.trim() !='' ){
						selectedFiles = response.split(',');
					}else{
						selectedFiles = new Array();
					}
					
					if(selectedFiles.length == 0 ){
						$("#moveCopyWarning1").modal('show');
						$("#move-modal-message11").text("No Files attached");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
						
					}else{
						recordedLessons(selectedFiles,listOwnerFlag);
						otherCheckFiles(selectedFiles,listOwnerFlag,listIdFromList,noteIdFromList);
						otherFiles(selectedFiles,listOwnerFlag);
						$('#attachFileModel').modal('toggle');
						$('#accordion').find('.up').addClass("hidden");
					}
					
				},
				error : function() 
				{
					alert("Please try again later");
					console.log("<-------error returned for new ajax request attach file-------> ");
				}
			});
	});	
	var attachCountId;
	/*Handles attaching files to note*/
	$('#notes').on('click','.js-attach',function(){
	
		$('#downloaderrormsg').attr('style','display:none;');
		var noteId;
		var listId;
		if($(this).parent().parent().parent().parent().parent().parent().parent().attr('id') == 'moreActions'){
			addDueDateId=moreActionBasedId;
			noteId=noteIdFromList;
			listId=listIdFromList;
			$('#moreActions').modal('hide');
			//$('#attachFileModel').modal('toggle');
			moreActionEvent='attach';
		}else{
			addDueDateId=$(this).parent().parent().parent().parent().parent().parent().parent();
			$(".noteMenu").popover('hide');				
			listOwnerFlag = true;
			
			noteId=$(addDueDateId).parent().attr('id');
			listId=$(addDueDateId).parent().parent().parent().attr('id');
		}
		$('#fileTabel').find('.template-upload').each(function( index ){
			$(this).remove();
		});
		if(listOwnerFlag){
			$('#accordion').find('.up').removeClass('hidden');
			$('#accordion').find('.checked').removeClass('hidden');
		}else{
			$("#attachFileModel").children('.modal-footer').removeClass('hidden');
			if(sharedNoteFlag){
				$('#accordion').find('.up').removeClass('hidden');
				//$('#accordion').find('.checked').addClass("hidden");
			}else{
				$("#attachFileModel").children('.modal-footer').addClass("hidden");
				$('#accordion').find('.up').addClass("hidden");
				$('#accordion').find('.checked').removeClass('hidden');
			}
		}
		getattachedFilesForNotes(listId,noteId,listOwnerFlag,true);
		recordedLessonsForNote(listId,noteId);
				//$("#myForm").attr('action',urlForServer+'Upload/fileAttachNote/'+userId+'/'+listId+'/'+noteId);
$('#noteuploaddiv').find('.ajax-upload-dragdrop').each(function(index){
		$(this).remove();
	});				
var settings = {
url:urlForServer+'Upload/fileAttachNote/'+userId+'/'+listId+'/'+noteId+'/'+musicnote,
method: "POST",
//allowedTypes:"jpg,png,gif,doc,pdf,zip",
multiple: true,
onSuccess:function(files,data,xhr)
{ 
var x = JSON.parse(data);
	
if(x.status=='success')
{
}
else
{
alert(x.status);
}
$("#status").html("<font color='green'>Upload is success</font>");
						
						
   
},
afterUploadAll:function(files,data,xhr)
{
	//$("#myForm").attr('action',urlForServer+'Upload/fileAttachNote/'+userId+'/'+listId+'/'+noteId);
 var testValue = 	$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').children('.badge-text').text().trim();
		testValue = parseInt(testValue)+1;
				
		$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').children('.attach-badge').remove();
		$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').children().remove();
				
		$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').attr("title",'This note has '+testValue+' attachment(s).');	
		$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').append('<span class="glyphicon glyphicon-download-alt"></span>'
					+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+testValue+'</span>');
		
		attach = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+testValue+' attachment(s).">'
					+'<span class="glyphicon glyphicon-download-alt"></span>'
					+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+testValue+'</span>'     
					+'</div>';
		var descId = $("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.decs');
		if($(descId).attr('class') == undefined){
			var cmtLen = $("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').children('.cmts').find('.badge-text').text();
			if(vote.length > 0 ){
				if(cmtLen > 0){
					$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.cmts').after(attach);
				}else{
					$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.vote-badge').after(attach);
				}
			}else{
				if(cmtLen > 0){
					$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.cmts').after(attach);
				}else{
					$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').prepend(attach);			
				}
			}
		}else{
			$(descId).after(attach);
		}
  
						
						  setTimeout(function(){
							$('#progress').hide();	
							$('#attachFileModel').modal('hide');
							//$("#moveCopyWarning").modal('show');
						},2000);

						//$("#move-modal-message1").text("  File Uploaded successfully");
						setTimeout(function(){ 
							//$('#moveCopyWarning').modal('hide');
							checkOwnerOfList();
						},3000);
                          

},

onError: function(files,status,errMsg)
{      
$("#status").html("<font color='red'>Upload is Failed</font>");
}
}

$("#noteMultiUpload").uploadFile(settings);
	
		
	});	
	
	
	
	function getattachedFilesForNotes(listId,noteId,listOwnerFlag,showFlag){
		
	var url = urlForServer+"note/getAttachFile/"+listId+"/"+noteId;
	var response="";	
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
				type : 'POST',
				url : url,
				success : function(responseText) 
				{
					response = responseText;
				
					if(response.indexOf("[") != -1){
						response = response.substring(response.indexOf("[")+1,response.length);
					}
					if(response.indexOf("]") != -1){
						response = response.substring(0,response.indexOf("]"));
					}
					
					if(response.trim() !='' ){
						selectedFiles = response.split(',');
					}else{
						selectedFiles = new Array();
					}
					/*if(!listOwnerFlag && selectedFiles.length == 0 ){
						$("#moveCopyWarning").modal('show');
						$("#move-msg-header-col-md-1").attr('class',"label label-info").text("Warning");
						$("#move-modal-message1").attr('class',"label label-info").text("  No Files attached");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
						
					}else{*/
						if(listOwnerFlag){
							recordedLessons(selectedFiles,listOwnerFlag);
							otherCheckFiles(selectedFiles,listOwnerFlag,listId,noteId);
							otherFiles(selectedFiles,listOwnerFlag);
							attachCountId = selectedFiles.length;
							if(showFlag){
								$('#attachFileModel').modal('show');
							}
						}else{
							if(sharedNoteFlag){
								recordedLessons(selectedFiles,true);
								otherCheckFiles(selectedFiles,true,listId,noteId);
								otherFiles(selectedFiles,true);
								attachCountId = selectedFiles.length;
								if(showFlag){
									$('#attachFileModel').modal('show');
								}
							}
						}
					//}
				},
				error : function() 
				{
					alert("Please try again later");
					console.log("<-------error returned for new ajax request attach file-------> ");
				}
			});
			return selectedFiles;
		}
	var download=false;
	var deleteAttach=false;
	var playFile=false;
	var selectedFiles=new Array();
	 
	 $("ul#viewRecordedLesson").on('click','.glyphicon-download-alt',function() {
		if(listOwnerFlag){
			getFileFor($(this).parent().attr('id'),userId,$(this).parent().attr('id'));
		}else{
			getFileFor($(this).parent().attr('id'),listOwnerUserId,$(this).parent().attr('id'));
		}
		deleteAttach = true;
		download=true;
	 });
	 
	 /*$("ul#viewOtherFiles").on('click','.glyphicon-download-alt',function() {
			getFileFor($(this).attr('id'),userId,$(this).attr('id'));
			deleteAttach = true;
			download=true;
			playFile=true;
	 });*/
	 $(".downloadicon").on('click','#downloadFile',function() {
    	 var selected = new Array();
    	 var fileNameExt="";
    	 var fileName;
	       var tempfileName;
    	$("input:checkbox[name=downloadfile]:checked").each(function() {
    		  var fileName=$(this).attr('id');
    		 // getFileFor($(this).attr('id'),$(this).attr('id'));
    	       selected.push(fileName);
    	       return fileName;
    	   	});
    		if(selected.length!=0){
    			$("#downloaderrormsg").attr('style','display:none');
    			if((selected.length)>1){
    				getFilesFor(selected,userId);
    				deleteAttach = true;
    				download=true;
    				playFile=true;
    			}else{
    				getFileFor(selected,userId,selected);
    				deleteAttach = true;
    				download=true;
    				playFile=true;
    			}
    		}else{
    			$("#downloaderrormsg").attr('style','display:block');
    		}
     });
  

	var attachId =""; 
	$("ul#viewUploadedFiles").on('click','.glyphicon-remove',function() {
		deleteAttach = true; download=true;playFile=true;
		if(listOwnerFlag){
			attachId = $(this);
			$('.delete-Attach-warn').text('Do you want to delete this file and remove from this note?');
			$('#attachFileModel').modal('hide');
			$('#deleteAttachedModal').modal('toggle');
		}	
	});
	
	// for play audio or video file uploaded file
	$('ul#viewUploadedFiles').on('click','.playRecorded',function(){
			//var lessonName = $(this).attr('id');
		deleteAttach = true; download=true; playFile=true;
			var tempClass = $(this).attr('id');
			var array = tempClass.split("~");
			var lessonName =array[0];
			var fileOwnerId=array[1];
			
			
			if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
			{
				$('ul#viewUploadedFiles').children('li').children('embed').each(function( index ){
					$('ul#viewUploadedFiles').children('li').children('embed').remove();
				});
 			$(this).parent('a').after('<embed width="260" height="282" src="'+uploadUrl+lessonName+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/>');
			}
			else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
			{
				$('ul#viewUploadedFiles').children('li').children('embed').each(function( index ){
					$('ul#viewUploadedFiles').children('li').children('embed').remove();
				});
				
 			$(this).parent('a').after('<embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> ');
			}
		
	});
	$('ul#viewOtherFiles').on('click','input',function(){
		selectCheckbox="checkbox";
		});
	
	// for play audio or video file other file
	$('ul#viewOtherFiles').on('click','.playRecorded',function(){
			//var lessonName = $(this).attr('id');
			deleteAttach = true; download=true; playFile=true;
			var tempClass = $(this).attr('id');
			var array = tempClass.split("~");
			var lessonName =array[0];
			var fileOwnerId=array[1];
			var fileId='';
			
			
			if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
			{
				$('ul#viewOtherFiles').children('li').children('embed').each(function( index ){
					fileId=$('ul#viewOtherFiles').children('li').children('a').attr('id');
					$('ul#viewOtherFiles').children('li').children('embed').remove();
				});
				if(fileId!=lessonName)
					$(this).parent('a').after('<embed width="260" height="282" src="'+uploadUrl+lessonName+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/>');
			}
			else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
			{
				$('ul#viewOtherFiles').children('li').children('embed').each(function( index ){
					$('ul#viewOtherFiles').children('li').children('embed').remove();
				});
				
 			$(this).parent('a').after('<embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> ');
			}
		
	});
	
	// for play audio or video file other file
	$('ul#viewNoteRecordedLesson').on('click','.playRecorded',function(){
			//var lessonName = $(this).attr('id');
			deleteAttach = true; download=true; playFile=true;
			var tempClass = $(this).attr('id');
			var array = tempClass.split("~");
			var lessonName =array[0];
			var fileOwnerId=array[1];
			var fileId='';
			
			
			if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
			{
				$('ul#viewNoteRecordedLesson').children('li').children('embed').each(function( index ){
					fileId=$('ul#viewNoteRecordedLesson').children('li').children('a').attr('id');
					$('ul#viewNoteRecordedLesson').children('li').children('embed').remove();
				});
				if(fileId!=lessonName)
					$(this).parent('a').after('<embed width="260" height="282" src="'+uploadUrl+lessonName+'" showstatusbar="true" showgotobar="true" showdisplay="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" type="application/x-mplayer2" name="rTuner" AUTOSTART="false"/>');
			}
			else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
			{
				$('ul#viewNoteRecordedLesson').children('li').children('embed').each(function( index ){
					$('ul#viewNoteRecordedLesson').children('li').children('embed').remove();
				});
				
 			$(this).parent('a').after('<embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> ');
			}
		
	});
	
	
	// download for view record lesson  
	$("ul#viewNoteRecordedLesson").on('click','.glyphicon-download-alt',function() {
		var tempClass = $(this).attr('class');
		var array = tempClass.split("~");
		getFileFor($(this).parent().attr('id'),array[1],$(this).parent().attr('id'));
		deleteAttach = true;
		download=true;
		playFile=true;
	 });
	
	// remove for view record lesson
	$("ul#viewNoteRecordedLesson").on('click','.glyphicon-remove',function() {
		
		deleteAttach = true;download=true;playFile=true;
			var fileName = $(this).parent().attr('id');
			var url = urlForServer+"Lesson/deleteVideo/"+fileName+"/"+userId;
				
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
				},
				type : 'POST',
				url : url,
				success : function(responseText) {
					$('#attachFileModel').modal('hide');
					var data=jQuery.parseJSON(responseText);
			        if(data[0].status=='success'){
			        	$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text("  File Deleted successfully");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
						
			        	if(selectedFiles.indexOf(fileName) != -1){
							selectedFiles.splice(selectedFiles.indexOf(fileName),1); 
						}
						recordedLessons(selectedFiles,listOwnerFlag);
						otherCheckFiles(selectedFiles,listOwnerFlag,listIdFromList,noteIdFromList);
						otherFiles(selectedFiles,listOwnerFlag);
						recordedLessons();
						
			        }else{
						$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text("  File Attached with some notes");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			        }
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
	});
	
	
	/*Archive List Modal panel Functionality*/
	$('#notes').on('click','.js-delete-Attach',function(){
	
			var fileName = attachId.parent().attr('id');
			var url = urlForServer+"Lesson/deleteUploadFile/"+fileName+"/"+userId;
			var params='{"listId":"'+listIdFromList+'","noteId":"'+noteIdFromList+'"}';
			params = encodeURIComponent(params);
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url,
				data: params,
				success : function(responseText) {
					var data=jQuery.parseJSON(responseText);
					$('#deleteAttachedModal').modal('hide');
			        if(data[0].status=='success'){
			        	$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text("  File Deleted successfully");

						
						if(selectedFiles.indexOf(fileName) != -1){
							selectedFiles.splice(selectedFiles.indexOf(fileName),1); 
						}
						
						$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.attach-badge').remove();
						if(selectedFiles.length > 0 && selectedFiles[0] != null){
							attach = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+selectedFiles.length+' attachment(s).">'
										+'<span class="glyphicon glyphicon-download-alt"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+selectedFiles.length+'</span>'     
										+'</div>';
							var descId = $("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.decs');
							if($(descId).attr('class') == undefined){
								var cmtLen = $("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').children('.cmts').find('.badge-text').text();
								if(vote.length > 0 ){
									if(cmtLen > 0){
										$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').after(attach);
									}else{
										$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.vote-badge').after(attach);
									}
								}else{
									if(cmtLen > 0){
										$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').find('.cmts').after(attach);
									}else{
										$("#notes").children('#'+listIdFromList+'').children('.listBodyAll').children('#'+noteIdFromList+'').children('.badges').prepend(attach);	
									}
								}
							}else{
								$(descId).after(attach);
							}
						}
						setTimeout(function(){ 
							$('#moveCopyWarning').modal('hide');
							checkOwnerOfList();
						},1000);
			        }else if(data[0].status=='error'){
						$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text("  Unable to delete File Attached");
						//setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
						
			        }else if(data[0].status=='otherNote'){
						$("#moveCopyWarning").modal('show');
						$("#move-msg-modal-body1").attr('style','width:330px;');
						$("#move-modal-message1").text("  File Attached with some other notes in same List");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');
							$("#move-msg-modal-body1").attr('style','');
						},2500);
					}else if(data[0].status=='otherList'){
						$("#moveCopyWarning").modal('show');
						$("#move-msg-modal-body1").attr('style','width:330px;');
						$("#move-modal-message1").text("  File Attached with some other List");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');
							$("#move-msg-modal-body1").attr('style','');
							checkOwnerOfList();
						},2500);
					}

				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});	
		
	});
$("ul#viewOtherFiles").on('click','.glyphicon-remove',function() {
		
		deleteAttach = true;download=true;playFile=true;
			var fileName = $(this).attr('id');
			var url = urlForServer+"Lesson/deleteVideo/"+fileName+"/"+userId;
				
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
				},
				type : 'POST',
				url : url,
				success : function(responseText) {
					$('#attachFileModel').modal('hide');
					var data=jQuery.parseJSON(responseText);
			        if(data[0].status=='success'){
			        	$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text("  File Deleted successfully");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
						
			        	if(selectedFiles.indexOf(fileName) != -1){
							selectedFiles.splice(selectedFiles.indexOf(fileName),1); 
						}
						recordedLessons(selectedFiles,listOwnerFlag);
						otherCheckFiles(selectedFiles,listOwnerFlag,listIdFromList,noteIdFromList);
						otherFiles(selectedFiles,listOwnerFlag);
						
			        }else{
						$("#moveCopyWarning").modal('show');
						$("#move-modal-message1").text("  File Attached with some notes");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
			        }
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
	});
	  $("ul#viewUploadedFiles").on('click','.glyphicon-download-alt',function() {
		var tempClass = $(this).attr('class');
		var array = tempClass.split("~");
		getFileFor($(this).parent().attr('id'),array[1],$(this).parent().attr('id'));
		deleteAttach = true;
		download=true;
		playFile=true;
	 });

	 $("ul#viewRecordedLesson").on('click','li a',function() {
			if(listOwnerFlag){
				if(!download || !deleteAttach )
				{
					var id=this.id;
					var selected=false;
					$(this).find('span').each(function( index ){
						selectedFiles.splice(selectedFiles.indexOf(id),1); 
						$(this).remove();
						selected=true;
					});
					if(!selected)
					{
						selectedFiles.push(id);
						$(this).append('<span id="'+id+'" class="close glyphicon glyphicon-ok glyphicon glyphicon-black" />');
					}
				}
				else{
					download=false;			
					deleteAttach = false;
				}
			}		
		});
	 
	 $("ul#viewNoteRecordedLesson").on('click','li a',function() {
			if(listOwnerFlag){
				if(!download || !deleteAttach ||!playFile)
				{
					var id=this.id;
					var selected=false;
					$(this).find('span').each(function( index ){
						selectedFiles.splice(selectedFiles.indexOf(id),1); 
						$(this).remove();
						selected=true;
					});
					if(!selected)
					{
						selectedFiles.push(id);
						$(this).append('<span id="'+id+'" class="close glyphicon glyphicon-ok glyphicon glyphicon-black" />');
					}
				}
				else{
					download=false;			
					deleteAttach = false;
					playFile=false;
				}
			}		
		});
	 
	 $("ul#viewOtherFiles").on('click','li a',function() {
		 if(selectCheckbox!="checkbox")
			 {
			if(listOwnerFlag || sharedNoteFlag){
				if(!download || !deleteAttach ||!playFile)
				{
					var id=this.id;
					var selected=false;
					$(this).find('span').each(function( index ){
						selectedFiles.splice(selectedFiles.indexOf(id),1); 
						$(this).remove();
						selected=true;
					});
					if(!selected)
					{
						selectedFiles.push(id);
						$(this).append('<span id="'+id+'" class="close glyphicon glyphicon-ok" />');
					}
				}
				else{
					download=false;		
					deleteAttach = false;
					playFile=false;
				}
			}
			 }
		 selectCheckbox="";
		});	
		$("ul#viewUploadedFiles").on('click','li a',function() {
			
			var idwithName=$(this).children('.playRecorded').attr('id');
			var id=$(this).id;
			
			var array = idwithName.split("~");
			var fileName =array[0];
			var fileOwnerId=array[1];
			
			if(listOwnerFlag && fileOwnerId == userId){
				if(!download || !deleteAttach || !playFile)
				{
					var id=this.id;
					var selected=false;
					$(this).find('span').each(function( index ){
						selectedFiles.splice(selectedFiles.indexOf(id),1); 
						$(this).remove();
						selected=true;
					});
					if(!selected)
					{
						selectedFiles.push(id);
						$(this).append('<span id="'+id+'" class="close glyphicon glyphicon-ok" />');
					}
				}
				else{
					download=false;		
					deleteAttach = false;
					playFile=false;
				}
			}else if(fileOwnerId == userId)
			{
				if(!download || !deleteAttach || !playFile)
				{
					var id=this.id;
					var selected=false;
					$(this).find('span').each(function( index ){
						selectedFiles.splice(selectedFiles.indexOf(id),1); 
						$(this).remove();
						selected=true;
					});
					if(!selected)
					{
						selectedFiles.push(id);
						$(this).append('<span id="'+id+'" class="close glyphicon glyphicon-ok" />');
					}
				}
				else{
					download=false;		
					deleteAttach = false;
					playFile=false;
				}
			}
		});		
	$('#attachFile').click(function() {
		var filesToBeSaved= selectedFiles;
		var noteId=noteIdFromList;
		var listId=listIdFromList;
		
		var params ='';
		if(filesToBeSaved.length > 0 && filesToBeSaved[0] != null){
			 params = encodeURIComponent(filesToBeSaved);
		}else{
			params = '  ';
			params = encodeURIComponent(params);
		}
			var url = urlForServer+"note/attachFile/"+listId+"/"+noteId+"/"+userId;
			$.ajax({
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url,
				data : params,
				success : function(response) 
				{
					var listId=$('#notebookId').val();
					if(listId =='note'){
						listId =listIdFromList;
					}
					
					$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').children('.attach-badge').remove();
					$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').children().remove();

					if(filesToBeSaved.length > 0 && filesToBeSaved[0] != null){
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').attr("title",'This note has '+filesToBeSaved.length+' attachment(s).');
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').append('<span class="glyphicon glyphicon-download-alt"></span>'
										+'<span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;'+filesToBeSaved.length+'</span>');
						attach = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+filesToBeSaved.length+' attachment(s).">'
										+'<span class="glyphicon glyphicon-download-alt"></span>'
										+'<span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;'+filesToBeSaved.length+'</span>'     
										+'</div>';
						var descId = $("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.decs');
						if($(descId).attr('class') == undefined){
							var cmtLen = $("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').children('.cmts').find('.badge-text').text();
							if(vote.length > 0 ){
								if(cmtLen > 0){
									$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.cmts').after(attach);
								}else{
									$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.vote-badge').after(attach);
								}
							}else{
								if(cmtLen > 0){
									$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').find('.cmts').after(attach);
								}else{
									$("#notes").children('#'+listId+'').children('.listBodyAll').children('#'+noteId+'').children('.badges').prepend(attach);
										
								}
							}
						}else{
							$(descId).after(attach);
						}
					}else{
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').attr("title",'This note has no attachment(s).');
						$("#moreActions").children('.modal-dialog').children('.modal-content').children('.modal-body').children('.col-md-9').children('.badges').children('.attach-badge').append('<span class="glyphicon glyphicon-download-alt"></span>'
										+'<span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;0</span>');
					}
					
					$("#attachFileModel").modal('hide');
					$("#moveCopyWarning").modal('show');
					if(attachCountId < filesToBeSaved.length){
						$("#move-modal-message1").text("  File Attached successfully");
					}else if(attachCountId == filesToBeSaved.length){
						$("#move-modal-message1").text(" No File(s) to Attach");
					}else {
						$("#move-modal-message1").text("  File Detached successfully");
					}
					setTimeout(function(){ 
						$('#moveCopyWarning').modal('hide');
						checkOwnerOfList();
					},1000);
					/*if(moreActionEvent=='attach'){
	    				$('#moreActions').modal('toggle');
	    				moreActionEvent='';
	    			}*/
				},
				error : function() 
				{
					alert("Please try again later");
					console.log("<-------error returned for new ajax request attach file-------> ");
				}
			});
			
		//Use this to save it into DB
		
	});
	$('#editSubEventSubmit').click(function() {
		
		if(repeatType!=null && repeatType!='')
		{
		var startTime="";
		var endTime="";
		var noteName=$("#editSubEventName").val();
		if(noteName!=null && noteName!='')
		{
			
			$("#editSubEventNames").attr("style", "display:none");
			$("#editSubEventNames").text("");
			
		}
		else
		{
			$("#editSubEventNames").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#editSubEventNames").text("Field cannot be blank").css({"color":"red"});
		
		}
		
		
		var eventDiscription=$("#editSubEventDescription").val();
		
		if(eventDiscription!=null && eventDiscription!='')
		{
			
			$("#editSubEventDescriptions").attr("style", "display:none");
			$("#editSubEventDescriptions").text("");
			
		}
		else
		{
			$("#editSubEventDescriptions").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#editSubEventDescriptions").text("Field cannot be blank").css({"color":"red"});
		
		}
		var eventLocation=$("#editSubEventLocation").val();
		
		var allDayEvent=$('input[name=editSubEventAllDay]').is(':checked');
		var stime="";
		var etime="";
		var allDayFlag=true;
		var eventFlag=true;
		var equalEvent=true;
		var sendEmail=$('input[name=editSubEventMailCheckbox]').is(':checked');
		
		if(allDayEvent){
			stime=$('#editSubEventStartDate').val();
			if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=stime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					stime = newEventdate;
				}
				startTime=convertStringDateFormatIntoNumberFormat(stime, true);
			}
			etime=$('#editSubEventEndDate').val();
			if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=etime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					etime = newEventdate;
				}
				endTime=convertStringDateFormatIntoNumberFormat(etime, true);
			}
		
			if (new Date(stime) > new Date(etime)) {
			    
				
			     eventFlag=false;
			        $("#editSubEventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#editSubEventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
					$("#editSubEventRepeatEventwarning").attr("style", "display:none");
					$("#editSubEventRepeatEventwarning").text("");
			}
			else
			{
				
				$("#editSubEventBetweenDate").attr("style", "display:none");
				$("#editSubEventBetweenDate").text("");
				
			}
		}else{
			stime=$('#editSubEventStartTime').val();
			if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=stime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
				}
				startTime=convertStringDateFormatIntoNumberFormat(stime,false);
			}
			etime=$('#editSubEventEndTime').val();
			if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=etime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
				}
				endTime=convertStringDateFormatIntoNumberFormat(etime,false);
			}
			
			if (new Date(stime).getTime() > new Date(etime).getTime()) {
			    
				
			     eventFlag=false;
			        $("#editSubEventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#editSubEventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
					$("#editSubEventRepeatEventwarning").attr("style", "display:none");
					$("#editSubEventRepeatEventwarning").text("");
			}
			else
			{
				
				$("#editSubEventBetweenDate").attr("style", "display:none");
				$("#editSubEventBetweenDate").text("");
				
			}
			
		}
		if(startTime!=null && startTime!='' && startTime!='NaN/NaN/NaN')
		{
			$("#editSubEventStartTimes").attr("style", "display:none");
			$("#editSubEventStartTimes").text("");
		
		}
		else
		{
			$("#editSubEventStartTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#editSubEventStartTimes").text("Field cannot be blank").css({"color":"red"});
			
			
		}
		if(endTime!=null && endTime!=''&& endTime!='NaN/NaN/NaN')
		{
			
			$("#editSubEventTimes").attr("style", "display:none");
			$("#editSubEventTimes").text("");
			
			
			
			
		}
		else
		{
			
			$("#editSubEventBetweenDate").attr("style", "display:none");
			$("#editSubEventBetweenDate").text("");
			$("#editSubEventTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
			$("#editSubEventTimes").text("Field cannot be blank").css({"color":"red"});
			$("#editSubEventRepeatEventwarning").attr("style", "display:none");
			$("#editSubEventRepeatEventwarning").text("");
			
		}
		
		if(allDayEvent)
		{
			if($('#editSubEventStartDate').val()!=null && $('#editSubEventStartDate').val()!=''&& $('#editSubEventStartDate').val()!='NaN/NaN/NaN' && $('#editSubEventEndDate').val()!=null && $('#editSubEventEndDate').val()!=''   && $('#editSubEventEndDate').val()!='NaN/NaN/NaN')
			{
				if(eventFlag)
				{
				if($('#editSubEventStartDate').val()==$('#editSubEventEndDate').val())
				{
				$("#editSubEventRepeatEventwarning").attr("style", "display:none");
				$("#editSubEventRepeatEventwarning").text("");
			
			}
			else
			{
			
				equalEvent=false;
				$("#editSubEventBetweenDate").attr("style", "display:none");
				$("#editSubEventBetweenDate").text("");
				$("#editSubEventRepeatEventwarning").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#editSubEventRepeatEventwarning").text("You can't create overlapping event.").css({"color":"red"});
			}
				}
		}
		}
		else
		{
			var startrepeat=$('#editSubEventStartTime').val();
			var endRepeat=$('#editSubEventEndTime').val();
		if(startrepeat!=null && startrepeat!='' && startrepeat!='NaN/NaN/NaN' && endRepeat!=null && endRepeat!='' && endRepeat!='NaN/NaN/NaN')
		{
			
			if(eventFlag)
			{
			if (startrepeat.substring(0, startrepeat.length - 8).trim() == endRepeat.substring(0, endRepeat.length - 8).trim()) {
				$("#editSubEventRepeatEventwarning").attr("style", "display:none");
				$("#editSubEventRepeatEventwarning").text("");

					
				}
				else
				{
					equalEvent=false;
					$("#editSubEventBetweenDate").attr("style", "display:none");
					$("#editSubEventBetweenDate").text("");
					$("#editSubEventRepeatEventwarning").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
					$("#editSubEventRepeatEventwarning").text("You can't create overlapping event.").css({"color":"red"});

				}
			}
		}
				
		}
		//alert(endTime);
		
		if(noteName != null && noteName != '' && startTime != null
				&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN' && eventFlag==true && equalEvent==true)
			{
			
			if(allDayEvent){
				$("#editSubEvent").modal('hide');
				 $("#overLappingSubEventModal").modal('toggle');
				/*
			 var scheduleUrl = urlForServer+"note/getSubEvents";
			  	var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","alldayevent":"'+allDayEvent+'","description":"'+eventDiscription+'","location":"'+eventLocation+'","listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmail+'","overlappingEvent":"'+overlapping+'"}';
				console.log(scheduleParams);
				scheduleParams = encodeURIComponent(scheduleParams);
				
			    $.ajax({
			    headers: { 
		    	"Ajax-Call" : userTokens,
		    	"Ajax-Time" :userLoginTime				
		    	},
			    	type: 'POST',
			    	url : scheduleUrl,
			    	cache: false,
			    	contentType: "application/json; charset=utf-8",
			    	data:scheduleParams, 
			    	dataType: "json",
			    	success : function(response){
			    	
			    	if(response.status=='sucess')
			    	{
			    		  $("#editSubEvent").modal('hide');
						  $("#overLappingAllDaySubEventModal").modal('toggle');
			    	}
			    	else
			    	{
			    		  $("#editSubEvent").modal('hide');
						  $("#overLappingSubEventModal").modal('toggle');	
			    	}
			    	
			    	
			    },
			       error: function(e) {
			           alert("Please try again later");
			       }
			   
				    });
			    
			*/}
			else
			{
				

				    /*var scheduleUrl = urlForServer+"note/getSubEvents";
				  	var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","alldayevent":"'+allDayEvent+'","description":"'+eventDiscription+'","location":"'+eventLocation+'","listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmail+'","overlappingEvent":"'+overlapping+'"}';
					console.log(scheduleParams);
					scheduleParams = encodeURIComponent(scheduleParams);
					
				    $.ajax({
				    	headers: { 
							"Ajax-Call" : userTokens,
							"Ajax-Time" :userLoginTime				
						},
				    	type: 'POST',
				    	url : scheduleUrl,
				    	cache: false,
				    	contentType: "application/json; charset=utf-8",
				    	data:scheduleParams, 
				    	dataType: "json",
				    	success : function(response){
				    	
				    	if(response.status=='sucess')
				    	{
				    		  $("#editSubEvent").modal('hide');
							  $("#overLappingAllDaySubEventModal").modal('toggle');
				    	}
				    	else
				    	{
				    		  $("#editSubEvent").modal('hide');
							  $("#overLappingSubEventModal").modal('toggle');	
				    	}
				    	
				    	
				    },
				       error: function(e) {
				           alert("Please try again later");
				       }
				   
					    });*/
				    
				
				 $("#editSubEvent").modal('hide');
				 $("#overLappingSubEventModal").modal('toggle');
			}
				
			
			
			}
		}
		else
		{
			var overlapping=false;
			var startTime="";
			var endTime="";
			var noteName=$("#editSubEventName").val();
			if(noteName!=null && noteName!='')
			{
				
				$("#editSubEventNames").attr("style", "display:none");
				$("#editSubEventNames").text("");
				
			}
			else
			{
				$("#editSubEventNames").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#editSubEventNames").text("Field cannot be blank").css({"color":"red"});
			
			}
			
			
			var eventDiscription=$("#editSubEventDescription").val();
			
			if(eventDiscription!=null && eventDiscription!='')
			{
				
				$("#editSubEventDescriptions").attr("style", "display:none");
				$("#editSubEventDescriptions").text("");
				
			}
			else
			{
				$("#editSubEventDescriptions").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#editSubEventDescriptions").text("Field cannot be blank").css({"color":"red"});
			
			}
			var eventLocation=$("#editSubEventLocation").val();
			
			var allDayEvent=$('input[name=editSubEventAllDay]').is(':checked');
			var stime="";
			var etime="";
			var allDayFlag=true;
			var eventFlag=true;
			var sendEmail=$('input[name=editSubEventMailCheckbox]').is(':checked');
			
			if(allDayEvent){
				stime=$('#editSubEventStartDate').val();
				if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
					if(is_Ie|| is_safari){
						var dateTime=stime.split(' ');
						var newEventdate=dateTime[0];
						
						var splitedDate =  newEventdate.split('/');
						newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
						
						stime = newEventdate ;
					}
					startTime=convertStringDateFormatIntoNumberFormat(stime, true);
				}
				etime=$('#editSubEventEndDate').val();
				if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
					if(is_Ie|| is_safari){
						var dateTime=etime.split(' ');
						var newEventdate=dateTime[0];
						
						var splitedDate =  newEventdate.split('/');
						newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
						
						etime = newEventdate ;
					}
					endTime=convertStringDateFormatIntoNumberFormat(etime, true);
				}
			
				if (new Date(stime) > new Date(etime)) {
				     eventFlag=false;
				        $("#editSubEventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
						$("#editSubEventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
						
				}
				else
				{
					
					$("#editSubEventBetweenDate").attr("style", "display:none");
					$("#editSubEventBetweenDate").text("");
					
				}
			
				
				
			}else{
				stime=$('#editSubEventStartTime').val();
				if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
					if(is_Ie|| is_safari){
						var dateTime=stime.split(' ');
						var newEventdate=dateTime[0];
						
						var splitedDate =  newEventdate.split('/');
						newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
						
						stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
					}
					startTime=convertStringDateFormatIntoNumberFormat(stime,false);
				}
				etime=$('#editSubEventEndTime').val();
				if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
					if(is_Ie|| is_safari){
						var dateTime=etime.split(' ');
						var newEventdate=dateTime[0];
						
						var splitedDate =  newEventdate.split('/');
						newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
						
						etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
					}
					endTime=convertStringDateFormatIntoNumberFormat(etime,false);
				}
				
				if (new Date(stime).getTime() > new Date(etime).getTime()) {
				     eventFlag=false;
				        $("#editSubEventBetweenDate").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
						$("#editSubEventBetweenDate").text("Sorry, you can't create an event that ends before it starts.").css({"color":"red"});
						
				}
				else
				{
					
					$("#editSubEventBetweenDate").attr("style", "display:none");
					$("#editSubEventBetweenDate").text("");
					
				}
				
			}
			if(startTime!=null && startTime!='' && startTime!='NaN/NaN/NaN')
			{
				$("#editSubEventStartTimes").attr("style", "display:none");
				$("#editSubEventStartTimes").text("");
			
			}
			else
			{
				$("#editSubEventStartTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#editSubEventStartTimes").text("Field cannot be blank").css({"color":"red"});
				
				
			}
			if(endTime!=null && endTime!=''&& endTime!='NaN/NaN/NaN')
			{
				
				$("#editSubEventTimes").attr("style", "display:none");
				$("#editSubEventTimes").text("");
				
			}
			else
			{
				$("#editSubEventTimes").attr("style", "display:block;font-family: Helvetica Neue;font-size:14px;");
				$("#editSubEventTimes").text("Field cannot be blank").css({"color":"red"});
				
			}
			
			if(noteName != null && noteName != '' && startTime != null
					&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN' && eventFlag==true)
				{
				

		    	
		    	  var scheduleUrl = urlForServer+"note/updateSingleEvents";
				  	var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","alldayevent":"'+allDayEvent+'","description":"'+eventDiscription+'","location":"'+eventLocation+'","listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmail+'","overlappingEvent":"'+overlapping+'"}';
					scheduleParams = encodeURIComponent(scheduleParams);
					
				    $.ajax({
				    	headers: { 
				    	"Mn-Callers" : musicnote,
				    	"Mn-time" :musicnoteIn					
				    	},
				    	type: 'POST',
				    	url : scheduleUrl,
				    	cache: false,
				    	contentType: "application/json; charset=utf-8",
				    	data:scheduleParams, 
				    	dataType: "json",
				    	success : function(response){
				    	     if(response!='0'){
				    	    	
				    	    	 var url = urlForServer+"note/fetchEvents";
					    	 		var params = '{"listId":"'+editEventListId+'","eventId":"'+editEventId+'","userId":"'+userId+'"}';
					    	 	    params = encodeURIComponent(params);
					    	 	   
					    	 	    $.ajax({
					    	 	    	headers: { 
					    	 	    	"Mn-Callers" : musicnote,
					    		    	"Mn-time" :musicnoteIn				
					    		    	},
					    	 	    	type: 'POST',
					    	 	    	url : url,
					    	 	    	cache: false,
					    	 	    	contentType: "application/json; charset=utf-8",
					    	 	    	data:params, 
					    	 	    	dataType: "json",
					    	 	    	success : function(response){
					    	    	
					    	 	   
				    	    	 
				    	    	 while(noteName.indexOf("\n") != -1){
				 					noteName = noteName.replace("\n"," ");
				 				}
				 				while(noteName.indexOf("\t") != -1){
				 					noteName = noteName.replace("\t"," ");
				 				}
				 				while(noteName.indexOf("\"") != -1){
				 					noteName = noteName.replace("\"", "`*`");
				 				}
				 				while(eventDiscription.indexOf("\n")!= -1){
				 					eventDiscription = eventDiscription.replace("\n"," ");
				 				}
				 				while(eventDiscription.indexOf("\t")!= -1){
				 					eventDiscription = eventDiscription.replace("\t","     ");
				 				}
				 				while(eventDiscription.indexOf("\"") != -1){
				 					eventDiscription = eventDiscription.replace("\"", "`*`");
				 				}
				 				if(eventLocation!= ""){
				 					while(eventLocation.indexOf("\"") != -1){
				 						eventLocation = eventLocation.replace("\"", "`*`");
				 					}
				 				}
				 				
				 				var url = urlForServer+"note/updateNote/"+editEventListId+"/"+userId;
				 				var params = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","eventMembers":"","eventGroups":"","eventSharedAllContact":0,"eventSharedAllContactMembers":"","status":"A","vote":"","links":"","endDate":"'+endTime+'","eventId":"'+editEventId+'","dueDate":"","dueTime":"","access":"private","attachFilePath":"","description":"'+eventDiscription+'","comments":"","location":"'+eventLocation+'","repeatEvent":"'+repeatType+'","eventStartDate":"","eventEndDate":""}';

				 			    params = encodeURIComponent(params);

				 			    $.ajax({
				 			    	headers: { 
				 			    	"Mn-Callers" : musicnote,
				 			    	"Mn-time" :musicnoteIn				
				 			    	},
				 			    	type: 'POST',
				 			    	url : url,
				 			    	cache: false,
				 			    	contentType: "application/json; charset=utf-8",
				 			    	data:params, 
				 			    	dataType: "json",
				 			    	success : function(response){
				 			    	
				 			    	   if(response!='0'){}
				 			    },
				 			       error: function(e) {
				 			           alert("Please try again later");
				 			       }
				 			   
				 				    });
					    	 	   },
							       error: function(e) {
							           alert("Please try again later");
							       }
							   
								    });
				    	    	 $("#editSubEvent").modal('hide');
				    	    	 //loadAllEvents();
				    	    	 var listId=$('#notebookId').val();
    				        	 loadEventList(userId,listId,listType);
				    	    	 getTodaySchedule();
				    	    	 
				    	    	 // add due to event by date & time order - kishore
				    	    	 loadEventsBooks();
				     			 getListAndNoteNames();
				    	     }
				    	     else
				    	    	 alert("Please try again later");
			       },
			       error: function(e) {
			           alert("Please try again later");
			       }
			   
				    });
				    
				    
				    
				    
		    	   }
		}
		
		
		
	});
	$('#onesubEvent').click(function() {
		var overlapping=true;
		var startTime="";
		var endTime="";
		var noteName=$("#editSubEventName").val();
		var eventDiscription=$("#editSubEventDescription").val();
		var eventLocation=$("#editSubEventLocation").val();
		var allDayEvent=$('input[name=editSubEventAllDay]').is(':checked');
		var stime="";
		var etime="";
		var allDayFlag=true;
		var eventFlag=true;
		var sendEmail=$('input[name=editSubEventMailCheckbox]').is(':checked');
		if(allDayEvent){
			stime=$('#editSubEventStartDate').val();
			if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=stime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					stime = newEventdate;
				}
				startTime=convertStringDateFormatIntoNumberFormat(stime, true);
			}
			etime=$('#editSubEventEndDate').val();
			if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=etime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					etime = newEventdate;
				}
				endTime=convertStringDateFormatIntoNumberFormat(etime, true);
			}
			
		
			
			
		}else{
			stime=$('#editSubEventStartTime').val();
			if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=stime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
				}
				startTime=convertStringDateFormatIntoNumberFormat(stime,false);
			}
			etime=$('#editSubEventEndTime').val();
			if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
				if(is_Ie|| is_safari){
					var dateTime=etime.split(' ');
					var newEventdate=dateTime[0];
					
					var splitedDate =  newEventdate.split('/');
					newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
					
					etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
				}
				endTime=convertStringDateFormatIntoNumberFormat(etime,false);
			}
			
			
			
		}
		
		
		if(noteName != null && noteName != '' && startTime != null
				&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN')
			{
		    var scheduleUrl = urlForServer+"note/updateSubEvents";
		  	var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","alldayevent":"'+allDayEvent+'","description":"'+eventDiscription+'","location":"'+eventLocation+'","listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmail+'","overlappingEvent":"'+overlapping+'"}';
			scheduleParams = encodeURIComponent(scheduleParams);
			
		    $.ajax({
		    	headers: { 
		    	"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn					
		    	},
		    	type: 'POST',
		    	url : scheduleUrl,
		    	cache: false,
		    	contentType: "application/json; charset=utf-8",
		    	data:scheduleParams, 
		    	dataType: "json",
		    	success : function(response){
		    	     if(response!='0'){
		    	      $("#overLappingSubEventModal").modal('hide');
		    	      $("#overLappingAllDaySubEventModal").modal('hide');
		    	    	// loadAllEvents();
		    	         var listId=$('#notebookId').val();
			        	 loadEventList(userId,listId,listType);
		    	    	 getTodaySchedule();
		    	    	 
		    	    	 // add due to event by date & time order - kishore
		    	    	 loadEventsBooks();
		     			 getListAndNoteNames();
		    	     }
		    	     else
		    	    	 alert("Please try again later");
	       },
	       error: function(e) {
	           alert("Please try again later");
	       }
	   
		    });
			}
		
	});
	
$('#wholeSubEvent').click(function() {
	var overlapping=false;
	var startTime="";
	var endTime="";
	var noteName=$("#editSubEventName").val();
	var eventDiscription=$("#editSubEventDescription").val();
	var eventLocation=$("#editSubEventLocation").val();
	var allDayEvent=$('input[name=editSubEventAllDay]').is(':checked');
	var stime="";
	var etime="";
	var allDayFlag=true;
	var eventFlag=true;
	var sendEmail=$('input[name=editSubEventMailCheckbox]').is(':checked');
	if(allDayEvent){
		stime=$('#editSubEventStartDate').val();
		if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=stime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				stime = newEventdate;
			}
			startTime=convertStringDateFormatIntoNumberFormat(stime, true);
		}
		etime=$('#editSubEventEndDate').val();
		if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=etime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				etime = newEventdate;
			}
			endTime=convertStringDateFormatIntoNumberFormat(etime, true);
		}
		
	
		
		
	}else{
		stime=$('#editSubEventStartTime').val();
		if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=stime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			}
			startTime=convertStringDateFormatIntoNumberFormat(stime,false);
		}
		etime=$('#editSubEventEndTime').val();
		if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=etime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			}
			endTime=convertStringDateFormatIntoNumberFormat(etime,false);
		}
		
		
		
	}
	
	
	if(noteName != null && noteName != '' && startTime != null
			&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN')
		{
		
		
	    	
	   
   	 var scheduleUrl = urlForServer+"note/updateSubEvents";
	  	var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","alldayevent":"'+allDayEvent+'","description":"'+eventDiscription+'","location":"'+eventLocation+'","listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmail+'","overlappingEvent":"'+overlapping+'"}';
		scheduleParams = encodeURIComponent(scheduleParams);
		
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : scheduleUrl,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:scheduleParams, 
	    	dataType: "json",
	    	success : function(response){
	    	     if(response!='0'){
	    	    	 var url = urlForServer+"note/fetchEvents";
	    	 		var params = '{"listId":"'+editEventListId+'","eventId":"'+editEventId+'","userId":"'+userId+'"}';
	    	 	    params = encodeURIComponent(params);
	    	 	   
	    	 	    $.ajax({
	    	 	    	headers: { 
	    	 	    	"Mn-Callers" : musicnote,
	    		    	"Mn-time" :musicnoteIn				
	    		    	},
	    	 	    	type: 'POST',
	    	 	    	url : url,
	    	 	    	cache: false,
	    	 	    	contentType: "application/json; charset=utf-8",
	    	 	    	data:params, 
	    	 	    	dataType: "json",
	    	 	    	success : function(response){
	    	    	 
	    	    	 var url = urlForServer+"note/updateNote/"+editEventListId+"/"+userId;
	    	    	 var params = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","eventMembers":"","eventGroups":"","eventSharedAllContact":0,"eventSharedAllContactMembers":"","status":"A","vote":"","links":"","endDate":"'+endTime+'","eventId":"'+editEventId+'","dueDate":"","dueTime":"","access":"private","attachFilePath":"","description":"'+eventDiscription+'","comments":"","location":"'+eventLocation+'","repeatEvent":"'+repeatType+'","eventStartDate":"'+response.startDate+'","eventEndDate":"'+response.endDate+'"}';

	    	 	    params = encodeURIComponent(params);

	    	 	    $.ajax({
	    	 	    	headers: { 
	    	 	    	"Mn-Callers" : musicnote,
	    		    	"Mn-time" :musicnoteIn				
	    		    	},
	    	 	    	type: 'POST',
	    	 	    	url : url,
	    	 	    	cache: false,
	    	 	    	contentType: "application/json; charset=utf-8",
	    	 	    	data:params, 
	    	 	    	dataType: "json",
	    	 	    	success : function(response){
	    	 	    	 if(response!='0'){
	    	 	    		 
	    	 	    	 }
	    	 	    	
	    	 	    },
	    	 	       error: function(e) {
	    	 	           alert("Please try again later");
	    	 	       }
	    	 	   
	    	 		    });
	    	 	   },
	    	       error: function(e) {
	    	           alert("Please try again later");
	    	       }
	    	   
	    		    });
	    	       $("#overLappingSubEventModal").modal('hide');
	    	       $("#overLappingAllDaySubEventModal").modal('hide');
	    	    	 //loadAllEvents();
	    	         var listId=$('#notebookId').val();
		        	 loadEventList(userId,listId,listType);
	    	    	 getTodaySchedule();
	    	    	 
	    	    	 // add due to event by date & time order - kishore
	    	    	 loadEventsBooks();
	     			 getListAndNoteNames();
	    	     }
	    	     else
	    	     {
	    	    	 alert("Please try again later");
	    	     }
	    
       },
       error: function(e) {
           alert("Please try again later");
       }
   
	    });
   	 
	    
	    
	    
		
		
		}
	
});



$('#onesubAllDayEvent').click(function() {
	var overlapping=true;
	var startTime="";
	var endTime="";
	var noteName=$("#editSubEventName").val();
	var eventDiscription=$("#editSubEventDescription").val();
	var eventLocation=$("#editSubEventLocation").val();
	var allDayEvent=$('input[name=editSubEventAllDay]').is(':checked');
	var stime="";
	var etime="";
	var allDayFlag=true;
	var eventFlag=true;
	var sendEmail=$('input[name=editSubEventMailCheckbox]').is(':checked');
	if(allDayEvent){
		stime=$('#editSubEventStartDate').val();
		if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=stime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				stime = newEventdate;
			}
			startTime=convertStringDateFormatIntoNumberFormat(stime, true);
		}
		etime=$('#editSubEventEndDate').val();
		if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=etime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				etime = newEventdate;
			}
			endTime=convertStringDateFormatIntoNumberFormat(etime, true);
		}
		
	
		
		
	}else{
		stime=$('#editSubEventStartTime').val();
		if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=stime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			}
			startTime=convertStringDateFormatIntoNumberFormat(stime,false);
		}
		etime=$('#editSubEventEndTime').val();
		if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
			if(is_Ie|| is_safari){
				var dateTime=etime.split(' ');
				var newEventdate=dateTime[0];
				
				var splitedDate =  newEventdate.split('/');
				newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
				
				etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
			}
			endTime=convertStringDateFormatIntoNumberFormat(etime,false);
		}
		
		
		
	}
	
	
	if(noteName != null && noteName != '' && startTime != null
			&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN')
		{
	  var scheduleUrl = urlForServer+"note/updateSubEvents";
	  	var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","alldayevent":"'+allDayEvent+'","description":"'+eventDiscription+'","location":"'+eventLocation+'","listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmail+'","overlappingEvent":"'+overlapping+'"}';
		scheduleParams = encodeURIComponent(scheduleParams);
		
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
	    	type: 'POST',
	    	url : scheduleUrl,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:scheduleParams, 
	    	dataType: "json",
	    	success : function(response){
	    	     if(response!='0'){
	    	       $("#overLappingSubEventModal").modal('hide');
	    	       $("#overLappingAllDaySubEventModal").modal('hide');
	    	    	 //loadAllEvents();
	    	    	 getTodaySchedule();
	    	    	 var listId=$('#notebookId').val();
		        	 loadEventList(userId,listId,listType);
	    	    	 
	    	    	 // add due to event by date & time order - kishore
	    	    	 loadEventsBooks();
	     			 getListAndNoteNames();
	    	     }
	    	     else
	    	    	 alert("Please try again later");
       },
       error: function(e) {
           alert("Please try again later");
       }
   
	    });
		}
	
});

$('#followingSubEvent').click(function() {
var overlapping=false;
var startTime="";
var endTime="";
var noteName=$("#editSubEventName").val();
var eventDiscription=$("#editSubEventDescription").val();
var eventLocation=$("#editSubEventLocation").val();
var allDayEvent=$('input[name=editSubEventAllDay]').is(':checked');
var stime="";
var etime="";
var allDayFlag=true;
var eventFlag=true;
var sendEmail=$('input[name=editSubEventMailCheckbox]').is(':checked');
if(allDayEvent){
	stime=$('#editSubEventStartDate').val();
	if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
		if(is_Ie|| is_safari){
			var dateTime=stime.split(' ');
			var newEventdate=dateTime[0];
			
			var splitedDate =  newEventdate.split('/');
			newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
			
			stime = newEventdate ;
		}
		startTime=convertStringDateFormatIntoNumberFormat(stime, true);
	}
	etime=$('#editSubEventEndDate').val();
	if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
		if(is_Ie|| is_safari){
			var dateTime=etime.split(' ');
			var newEventdate=dateTime[0];
			
			var splitedDate =  newEventdate.split('/');
			newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
			
			etime = newEventdate;
		}
		endTime=convertStringDateFormatIntoNumberFormat(etime, true);
	}
	

	
	
}else{
	stime=$('#editSubEventStartTime').val();
	if(stime!=null && stime!='' && stime!='NaN/NaN/NaN'){
		if(is_Ie|| is_safari){
			var dateTime=stime.split(' ');
			var newEventdate=dateTime[0];
			
			var splitedDate =  newEventdate.split('/');
			newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
			
			stime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
		}
		startTime=convertStringDateFormatIntoNumberFormat(stime,false);
	}
	etime=$('#editSubEventEndTime').val();
	if(etime!=null && etime!='' && etime!='NaN/NaN/NaN'){
		if(is_Ie|| is_safari){
			var dateTime=etime.split(' ');
			var newEventdate=dateTime[0];
			
			var splitedDate =  newEventdate.split('/');
			newEventdate = monthMap[splitedDate[0].trim()]+"/"+splitedDate[1]+"/"+splitedDate[2];
			
			etime = newEventdate +" "+ dateTime[1]+" "+dateTime[2];
		}
		endTime=convertStringDateFormatIntoNumberFormat(etime,false);
	}
	
	
	
}


if(noteName != null && noteName != '' && startTime != null
		&& startTime != '' && endTime != null && endTime != '' && eventDiscription != null && eventDiscription != ''&& startTime!='NaN/NaN/NaN' && endTime!='NaN/NaN/NaN')
	{
	var url = urlForServer+"note/fetchEvents";
	var params = '{"listId":"'+editEventListId+'","eventId":"'+editEventId+'","userId":"'+userId+'"}';
    params = encodeURIComponent(params);
   
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn					
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
	var url = urlForServer+"note/updateNote/"+editEventListId+"/"+userId;
	var params = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","eventMembers":"","eventGroups":"","eventSharedAllContact":0,"eventSharedAllContactMembers":"","status":"A","vote":"","links":"","endDate":"'+endTime+'","eventId":"'+editEventId+'","dueDate":"","dueTime":"","access":"private","attachFilePath":"","description":"'+eventDiscription+'","comments":"","location":"'+eventLocation+'","repeatEvent":"'+repeatType+'","eventStartDate":"'+response.startDate+'","eventEndDate":"'+response.endDate+'"}';
    params = encodeURIComponent(params);
   
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
    	
    	  if(response!='0'){
    	 var scheduleUrl = urlForServer+"note/updateSubFollowingEvents";
    	  	var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","alldayevent":"'+allDayEvent+'","description":"'+eventDiscription+'","location":"'+eventLocation+'","listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'","sendEmailFlag":"'+sendEmail+'","overlappingEvent":"'+overlapping+'"}';
    		scheduleParams = encodeURIComponent(scheduleParams);
    		
    	    $.ajax({
    	    	headers: { 
    	    	"Mn-Callers" : musicnote,
    	    	"Mn-time" :musicnoteIn					
		    	},
    	    	type: 'POST',
    	    	url : scheduleUrl,
    	    	cache: false,
    	    	contentType: "application/json; charset=utf-8",
    	    	data:scheduleParams, 
    	    	dataType: "json",
    	    	success : function(response){
    	    	     if(response!='0'){
    	    	       $("#overLappingSubEventModal").modal('hide');
    	    	       $("#overLappingAllDaySubEventModal").modal('hide');
    	    	    	 loadAllEvents();
    	    	    	 getTodaySchedule();
    	    	    	 
    	    	    	 // add due to event by date & time order - kishore
    	    	    	 loadEventsBooks();
    	     			 getListAndNoteNames();
    	    	     }
    	    	     else
    	    	    	 alert("Please try again later");
    	   },
    	   error: function(e) {
    	       alert("Please try again later");
    	   }

    	    });
    	  }
    	
    },
       error: function(e) {
           alert("Please try again later");
       }
   
	    });
 
	
    },
    error: function(e) {
        alert("Please try again later");
    }

	    });

	}
});
	
	$('#deleteSubEventClick').click(function() {
		   $('#editSubEventModel').modal('hide');
	       $("#deleteSubEventModal").modal('toggle');

	});
	$('#subEventDelete').click(function() {

	    var scheduleUrl = urlForServer+"note/deleteSubEvents";
	  	var scheduleParams = '{"listId":"'+editEventListId+'","eventId":"'+editEventId+'","subEventId":"'+editSubEventId+'","userId":"'+userId+'"}';
		scheduleParams = encodeURIComponent(scheduleParams);
		
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
	    	},
	    	type: 'POST',
	    	url : scheduleUrl,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:scheduleParams, 
	    	dataType: "json",
	    	success : function(response){
	    	     if(response!='0'){
	    	    	 $("#deleteSubEventModal").modal('hide');   	
	    	    	 loadAllEvents();
	    	    	 getTodaySchedule();
	    	    	 
	    	    	 // add due to event by date & time order - kishore
	    	    	 loadEventsBooks();
	     			 getListAndNoteNames();
	    	     }
	    	     else
	    	    	 alert("Please try again later");
       },
       error: function(e) {
           alert("Please try again later");
       }
   
	    });
	});
	
	/*Handles attaching files to note ends*/
	
	/* Response Based Create Full List and Notes */
	/*function loadList(response){
    	for ( var i = 0; i < response.length; i++) 
		{
    		if(response[i].status=='A'){
    			var listName = response[i].listName;
    			var listId=response[i].listId;
    			var noteData="";
    			
    			for(var j=0;j<response[i].mnNotesDetails.length;j++){
    				var dueDateData="";
    				if(response[i].mnNotesDetails[j].status=='A'){
    					var noteName=response[i].mnNotesDetails[j].noteName;
    					var noteId=response[i].mnNotesDetails[j].noteId;
    					if(response[i].mnNotesDetails[j].dueDate != "" && response[i].mnNotesDetails[j].dueTime != "")
    						dueDateData=loadDueDateContent(response[i].mnNotesDetails[j].dueDate,response[i].mnNotesDetails[j].dueTime);
    					noteData=noteData+'<div class="noteDiv" id="'+noteId+'">'
    						+ '<div class="todo_description">'
    						//This code used for showing menu link now we don't need this option that's why we comment that line.//by Venu
    						//+'<div><a class="close noteMenu dropdown-toggle" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
    						//+'</div>'
    						+'<p>'
    						+ noteName
    						+ '</p></div>'
								+'<div class="badges">'
								+dueDateData
								+ '</div>'
    						+ '</div>';
    					}
    			}
    	
			var data='<div class="listDiv col-md-5" style="float:left;" id="'+listId+'"><div class="modalHeader"><div><a class="close listMenu dropdown-toggle" data-toggle="dropdown" ><b class="glyphicon glyphicon-chevron-down caret-custom"></b></a>'
			+'<ul class="dropdown-menu" id="'+listId+'booklists" style="position: absolute;top: 16%;overflow-y: none;width: auto;left:155px;"></ul>'
			+'</div>'
			+'<div><b class="headerListName">'
			+listName
			+'</b></div>'
			+'</div>'
			+'<div class="listBodyAll">'
			+noteData
			+'</div>'
			+'<div class="js-footer">'
			+'<a href="javascript:void(0);" class="highlight-icon js-list-subscribe list-footer" style="text-decoration: none;">Add New Note</a>'
			+'</div>'
			+'</div>';
			
    			$('#notes').append(data);
    		}
		}
    }*/
	
	function loadCmts(cmtWithParams){
		var cmt= "";
		if(cmtWithParams !=""){
			var cmtArray = new Array();
			if(cmtWithParams.indexOf("[") != -1){
				cmtWithParams = cmtWithParams.substring(cmtWithParams.indexOf("[")+1,cmtWithParams.length);
			}
			if(cmtWithParams.indexOf("]") != -1){
				cmtWithParams = cmtWithParams.substring(0,cmtWithParams.indexOf("]"));
			}
			if(cmtWithParams.indexOf(",") != -1){
					cmtArray = cmtWithParams.split(',');
			}else{
				if(cmtWithParams.trim() != ''){
					cmtArray = cmtWithParams.split(',');
				}
			}
			if(cmtArray!=null && cmtArray!=0){
			cmts ='<div class="badge glyphicon glyphicon-sm cmts " title="This note has '+cmtArray.length+' comment(s).">'
									+'<span class="glyphicon glyphicon-comment"></span><span class="badge-text"  style="word-spacing:-6px;">&nbsp;&nbsp;'+cmtArray.length+'</span></div>';
			}else
				cmts='';
				
		}
		return cmts;
	}
	
	function loadAllEvents()
	{
		alert('Fetching Calendar Events');
		var eve=[];
		$('#calendar').empty();
		
			  var scheduleUrl = urlForServer+"note/fetchScheduleEvents";
			  // alert(eventId +"    URL"+scheduleUrl);
			   
				var scheduleParams = '{"userId":"'+userId+'"}';
				scheduleParams = encodeURIComponent(scheduleParams);

			    $.ajax({
			    	headers: { 
			    	"Mn-Callers" : musicnote,
			    	"Mn-time" :musicnoteIn					
			    	},
			    	type: 'POST',
			    	url : scheduleUrl,
			    	cache: false,
			    	contentType: "application/json; charset=utf-8",
			    	data:scheduleParams, 
			    	dataType: "json",
			    	success : function(data){
						if(data!=null && data!=''){
				    	    for(var i=0;i<data.length;i++){
				    	    	var startDate=new Date(data[i].start);
				    	    	var sdate=startDate.getDate();
				    	    	var smonth=startDate.getMonth();
				    	    	var syear=startDate.getFullYear();
				    	    	var endDate=new Date(data[i].end);
				    	    	var edate=endDate.getDate();
				    	    	var emonth=endDate.getMonth();
				    	    	var eyear=endDate.getFullYear();
				    	    	var curDate=new Date();

				    	    	var allday=false;
				    	    	obj = {};
				    	    	
				    	    		obj["title"]=data[i].title;
					    	    	obj["description"]=data[i].description;
					    	    	obj["start"]=data[i].start;
					    	    	obj["end"]=data[i].end;
				    	    	
				    	    	
				    	    	if(data[i].allDay=='true'){
				    	    		allday=true;
				    	    	}
				    	    	obj["subEventId"]=data[i].subEventId;
				    	    	obj["ownerId"]=data[i].ownerId;
				    	    	obj["listId"]=data[i].listId;
				    	    	obj["eventId"]=data[i].eventId;
				    	    	obj["location"]=data[i].location;
				    	    	obj["repeatEvent"]=data[i].repeatEvent;
				    	    	obj["eventEndDate"]=data[i].eventEndDate;
				    	    	obj["repeatType"]=data[i].repeatType;
				    	    	obj.backgroundColor='#2887BD';
				    	    	obj["allDay"]=allday;
				    	    	eve.push(obj);
				    	    }
						}
						loadCal(eve);
		          },
		          error: function(e) {
		              alert("Please try again later");
		          }
		      
			    });
			    
			    
			

	}
	function loadCalendarComboBoxList()
	{
		$('#calendarlist').empty();
		var calendarUrl=urlForServer+"note/fetchCalendarSharingUsers";
		var param = '{"userId":"'+userId+'","listType":"'+listType+'"}';
		param = encodeURIComponent(param);
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type: 'POST',
	    	url : calendarUrl,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:param, 
	    	dataType: "json",
	    	success : function(sharing){
			var details='';
			var listdetails='';
			if(sharing!=null && sharing!="" && sharing!="0")
    		{
				$('.offset2').remove();
				details=details+'<div class="offset2">'
				+'<div class="btn-group" id="calendareventdetails" role="menu" aria-labelledby="dLabel"style="font-family: Helvetica Neue;font-size:14px;" >'
				+'<a class="btn btn-default" ><i class="glyphicon glyphicon-calendar" ></i> View </a>'
				+'<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" ><span class="caret"></span></a>'
				+'<ul class="dropdown-menu userGroup dropDownStopProgress" id="calendarul"></div>';
				if(details!='')
	    		{
	    			$('#calendarlist').append(details);
	    		}
    			for(var i=0;i<sharing.length;i++)
    			{
    				var color="background:"+sharing[i].color;
    				listdetails=listdetails+'<li  id="'+sharing[i].listId+'" class="userSelectedGroup"><a id="'+sharing[i].listId+'"><canvas width="10" height="10" style="'+color+'"></canvas> '+sharing[i].listName+'</a> </li>';
    			}
    			$('#calendarul').append(listdetails);
    		}	
	    	},
	        error: function(e) {
	            alert("Please try again later");
	        }
		});
	}
	
	function loadAttached(attachWithParams){
	 var attachDiv= '';
		if(attachWithParams!= ""){
			var attachArray = new Array();
						if(attachWithParams.indexOf("[") != -1){
								attachWithParams = attachWithParams.substring(attachWithParams.indexOf("[")+1,attachWithParams.length);
						}
						if(attachWithParams.indexOf("]") != -1){
							attachWithParams = attachWithParams.substring(0,attachWithParams.indexOf("]"));
						}
						if(attachWithParams.indexOf(",") != -1){
								attachArray = attachWithParams.split(',');
							
						}else{
								if(attachWithParams.trim() != ''){
									attachArray = attachWithParams.split(',');
								}
						}
						attachDiv = '<div class="badge attach-badge glyphicon glyphicon-sm"  title="This note has '+attachArray.length+' attachment(s).">'
										+'<span class="glyphicon glyphicon-download-alt"></span>'
										+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+attachArray.length+'</span>'     
									+'</div>';
		}
		return attachDiv;
	}
	
	function loadTag(tagWithParams)
	{
			var tagNames="";
			var tagArray = new Array();
			
			if(tagWithParams!= undefined && tagWithParams.indexOf("[") != -1){
				tagWithParams = tagWithParams.substring(tagWithParams.indexOf("[")+1,tagWithParams.length);
			}
			if(tagWithParams != undefined && tagWithParams.indexOf("]") != -1){
				tagWithParams = tagWithParams.substring(0,tagWithParams.indexOf("]"));
			}
			if(tagWithParams != undefined && tagWithParams.indexOf(",") != -1){
				tagArray = tagWithParams.split(',');
			}else{
				if(tagWithParams != undefined && tagWithParams.trim() != ''){
					tagArray = tagWithParams.split(',');
				}
			}
		//	alert("tagWithParams   "+tagWithParams);
			
			for(var siz=0;siz<tagArray.length;siz++){
				//alert(tagArray[siz]);
				
				var tagId=tagArray[siz].trim();
				if (tagId in notesTagMap)
					tagNames+=" "+notesTagMap[tagId]+" ,";
			}
			
			var lastIndex  = tagNames.lastIndexOf(",");
			tagNames = tagNames.substring(0, lastIndex);
			
				tagss ='<div class="badge glyphicon glyphicon-sm tagss" text="'+tagNames+'" style="white-space: normal;display:inline;background-color:#FFAB60";>'
				+'<span class="glyphicon glyphicon-tags"></span><span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+tagNames+'</span> </div>';
				
			return tagss;
		}
		
	function loadReminder(remainderWithParam)
	{

		var remainder = new Array();
		var reminderDivContent ="";
		if(remainderWithParam.indexOf("[") != -1){
			remainderWithParam = remainderWithParam.substring(remainderWithParam.indexOf("[")+1,remainderWithParam.length);
		}
		if(remainderWithParam.indexOf("]") != -1){
			remainderWithParam = remainderWithParam.substring(0,remainderWithParam.indexOf("]"));
		}
		if(remainderWithParam.indexOf(",") != -1){
			remainder = remainderWithParam.split(',');
		}else{
			remainder = remainderWithParam;
		}
		if(remainder.length > 0 && remainder[0]!=''){ 
			var remData  = remindersMap[parseInt(remainder[0].trim())];
			if(remData != null && remData!= ''){
				var selectedDate=new Date(remData.eventDate);
					
				var condate=new Date(selectedDate);
				var convertedDate="";
				convertedDate=month[condate.getMonth()];
				convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
				var rName = remData.rName;
				if(rName.length > 36){
					rName = rName.substring(0,35)+"...";
				}
				reminderDivContent='<div class="badge reminders glyphicon glyphicon-sm " title="'+rName+' on '+convertedDate+' at '+remData.eventTime+'"'
								+' style="text-decoration: none;background: #DFDD0A;">'
								//background: linear-gradient(to bottom, #DFDD0A 100%, #F3F3F3 0%) repeat scroll 0 0 transparent;">'
								+'<i class="glyphicon glyphicon-time"></i>'
								+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+convertedDate +'</span>'     
							+'</div>';
			}
		}
		return reminderDivContent;
	}
	
	function loadMemoDate(date)
	{	
		var date=new Date(date);
		var day=date.getDate();
		var months=date.getMonth();
		var year=date.getFullYear();
		var hour=date.getHours();
		var minutes=date.getMinutes();
		var seconds=date.getSeconds();
		var dates=month[months]+"/"+day+"/"+year;
		var date1=months+1+"/"+day+"/"+year+" "+hour+":"+minutes;
		var dates1=convertNumberDateFormatIntoStringFormat(date1,false);
		var datedesc='<div class="badge glyphicon glyphicon-sm decs1" title="This note added on '+dates1+'"><span class="glyphicon glyphicon-play-circle"></span><span class="badge-text"  style="word-spacing:-6px;">'+dates+'</span></div>';
		return datedesc;
	}
	
	function loadVote(voteWithParams){
		var votes= "";
		if(voteWithParams !=""){
			var voteArray = new Array();
							
			if(voteWithParams.indexOf("[") != -1){
				voteWithParams = voteWithParams.substring(voteWithParams.indexOf("[")+1,voteWithParams.length);
			}
			if(voteWithParams.indexOf("]") != -1){
				voteWithParams = voteWithParams.substring(0,voteWithParams.indexOf("]"));
			}
			if(voteWithParams.indexOf(",") != -1){
				voteArray = voteWithParams.split(',');
								
			}else{
				if(voteWithParams.trim() != ''){
					voteArray = voteWithParams.split(',');
				}
			}
									
			votes = '<div class="badge vote-badge glyphicon glyphicon-sm"  title="This note has '+voteArray.length+' votes(s).">'
							+'<span class="glyphicon glyphicon-thumbs-up"></span>'
							+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+voteArray.length+'</span>'     
							+'</div>';
		}
		return votes;
	}
	//loadDescloadVoteloadCmts
	function loadDesc(description){
		var desc='';
		if(description != "")
			desc='<div class="badge glyphicon glyphicon-sm decs" title="This note has a description."><span class="glyphicon glyphicon-pencil"></span><span class="badge-text" style="word-spacing:-6px;"></span></div>';
			
		return 	desc;
	}
	function loadDueDateContent(dueDate,dueTime){
    	var selectedDate=new Date(dueDate);//.toLocaleFormat('%m/%d/%Y');
    	dueTime=dueTime.toLowerCase();
    	if(dueTime.indexOf("a") != -1){
    		if(dueTime.indexOf(" ") == -1){
    			var str=new Array();
    			str=dueTime.split('am');
    			dueTime=str[0]+" "+"am";
    		}
    	}else if(dueTime.indexOf("p") != -1){
    		if(dueTime.indexOf(" ") == -1){
    			var str=new Array();
    			str=dueTime.split('pm');
    			dueTime=str[0]+" "+"pm";
    		}
    	}
    	var selectedDateTime=new Date(dueDate+' '+dueTime);
    	var todayDateTime=new Date();
		var dueStatus;
		var title;
		var seconds,minutes=0,hours=0,days;
		if(selectedDate < yesterday)
		{
			dueStatus='due-past';
			title="This note is past due.";
		}
		else if(selectedDate == yesterday)
		{
			dueStatus='due-past';
			title="This note is recently past due";
		}
		else if(selectedDate == tomor)
		{
			seconds = Math.floor((selectedDateTime - (todayDateTime))/1000);
			 minutes = Math.floor(seconds/60);
			 if(minutes > 60)
			 {
				 hours = Math.floor(minutes/60);
				 minutes =Math.floor(minutes%60);
			 }
		}
		else if (selectedDate > tomor)
		{
			dueStatus='due-future';
			title="This note is due later.";
		}
		else if(selectedDateTime < todayDateTime)
		{
				dueStatus='due-now';
				title="This note is recently past due.";
			
		}else if(selectedDateTime > todayDateTime){
			
			seconds = Math.floor((selectedDateTime - (todayDateTime))/1000);
			 minutes = Math.floor(seconds/60);
			 if(minutes > 60)
			 {
				 hours = Math.floor(minutes/60);
				 minutes =Math.floor(minutes%60);
			 }
			 days = Math.floor(hours/24);
			
			dueStatus='due-soon';
			title="This note is due in "+hours+" hours and "+minutes +" minutes";
		}
		
			
			var condate=new Date(dueDate);
			var convertedDate="";
			convertedDate=month[condate.getMonth()];
			convertedDate=convertedDate+"/"+condate.getDate()+"/"+condate.getFullYear();
			
		var dueDateDivContent='<div class="badge '+dueStatus+' glyphicon glyphicon-sm  " title="'+title+'">'
									+'<i class="glyphicon glyphicon-calendar"></i>'
									+'<span class="badge-text" style="word-spacing:-6px;">&nbsp;&nbsp;'+convertedDate +'</span>'     
							  +'</div>';
		
		return dueDateDivContent;
    }
	
	//Tag details for moreActions page design

	function fetchTagInfo(listId,noteId,tag){
		tagButton='';
		
		
	}
	
///////////////////// attach file uploader function
if(listType!='' && listType!='schedule' && listType!='crowd'){
      

}
///////////////////// end attach file uploader function

});
var recordEmptyFlag='';
var OtherEmptyFlag  = '';
/*Functions used to fetch and download files for note*/
function recordedLessons(selectedFiles,listOwnerFlag)
{
	//alert('recordedLessons');
	if(listOwnerFlag){
		var url = urlForServer+"Lesson/getRecordedLessons/"+userId;
		$.support.cors = true;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
			},
			type : 'POST',
			url : url,
			success : function(responseText) {
			
           
			var data=jQuery.parseJSON(responseText);
			$("#viewRecordedLesson").empty();
			if(data!= null && data!= ''){
				recordEmptyFlag = 'true'; 
				for ( var i = 0; i < data.length; i++) {
					var obj = data[i];
					var lessonName = obj['fileName'];
					var uploadDate=obj['uploadDate'];
					var playOption=obj['fullPath'];   
					var fileDummyName=obj['fileDummyName'];
					var span = '' ;
					if(selectedFiles.length > 0 && selectedFiles[0] != null){
						var y=selectedFiles.indexOf(fileName) ;
						if(y != -1){
							span='<span id="'+fileName+'" class="close glyphicon glyphicon-ok" />';
						}else{
							span = '';
						}
					}
					$("#viewRecordedLesson").append('<li><a id="'+playOption+'" href="javascript:void(0);"><i id="'+lessonName+'"  class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileDummyName + '<i  class="col-md-offset-2">'+uploadDate+'</i> '+ span+'</a></li>');
				}
			}else{
				recordEmptyFlag = 'false'; 
			}
           
			},
			error : function(e) {
				console.log("<-------error returned retrieving recorded lessons-------> "+e);
			}
		});
	}else{
		recordEmptyFlag = 'true';	 
		$("#viewRecordedLesson").empty();
		if(selectedFiles.length > 0 && selectedFiles[0] != null){
			for ( var i = 0; i < selectedFiles.length; i++) {
				if(selectedFiles[i].match(".mov") || selectedFiles[i].match(".avi")){ 
					$("#viewRecordedLesson").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);"><i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + selectedFiles[i] + '</a></li>');
				}
			}
		} 
	}
	return recordEmptyFlag;
}

/*function getFileFor(fileName,listOwenrUserId,tempfileName){
		var fileExt=fileName.split('.');
		
		var	ext = fileExt[fileExt.length-1];
	    var href=urlForServer+"Lesson/download/"+listOwenrUserId.trim()+'/'+fileName+'/'+ext+"/"+tempfileName;
	     $.fileDownload(href, {
		   httpMethod: "POST"
	   });  	
}
*/

function getFileFor(fileName,listOwenrUserId,tempfileName){
	var filename;
	filename=fileName.toString().split(".");
	var	ext = filename[1];
	var href=urlForServer+"Lesson/download/"+listOwenrUserId.trim()+'/'+fileName+'/'+ext+'/'+tempfileName+'/'+musicnote;
    $.fileDownload(href, {
	   httpMethod: "POST"
   });  	
}
function getFilesFor(fileNames,listOwenrUserId){
var href=urlForServer+"Lesson/downloadFile/"+listOwenrUserId.trim()+'/'+fileNames+'/'+musicnote;
$.fileDownload(href, {
   httpMethod: "POST"
});  	
}
function otherCheckFiles(selectedFiles,listOwnerFlag,listId,noteId)
{
	

	//alert('otherFiles');
	$("#viewUploadedFiles").empty();
	if(listOwnerFlag){
		
		url= urlForServer+"note/getAttachedFileInNote/"+listId+"/"+noteId+"/"+userId;
		var attObj;
		$.ajax({	
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			url : url,
			success : function(response) {
				var attachData = jQuery.parseJSON(response);
				if(attachData != null && attachData!= ''){
					
					for ( var i = 0; i < attachData.length; i++) {
						attObj = attachData[i];
						var fileUploadName=attObj.fileUploadName;
						var uploadDate=attObj.uploadDate;
						var playOption=attObj.fullPath;   
					
						if(userId == attObj.userId){
							span='<span id="'+attObj.fileName+'" class="close glyphicon glyphicon-ok" />';
							
							if(attObj.fileName.contains(".mov") || attObj.fileName.contains(".mp4") || attObj.fileName.contains(".avi") || attObj.fileName.contains(".wmv")  || attObj.fileName.contains(".wma"))
							   {
								//$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'"  href="javascript:void(0);" style="word-break:break-all;"><i class="glyphicon glyphicon-remove upload" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<input id="'+attObj.userId+'" class="~' +attObj.userId+'" type="checkbox"  name="downloadNoteFile"></input>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');
								$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'"  href="javascript:void(0);" style="word-break:break-all;"><i class="glyphicon glyphicon-remove upload" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +attObj.userId+'" title="Click to download"/>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');

							   }
							
							else if(attObj.fileName.contains(".mp3") || attObj.fileName.contains(".wav") || attObj.fileName.contains(".amr"))
							   {
								//$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'" href="javascript:void(0);" style="position: relative; top: 3px;word-break:break-all;"><i class="glyphicon glyphicon-remove upload" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<input id="'+attObj.userId+'" class="~' +attObj.userId+'" type="checkbox"  name="downloadNoteFile"></input>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');
								$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'" href="javascript:void(0);" style="position: relative; top: 3px;word-break:break-all;"><i class="glyphicon glyphicon-remove upload" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +attObj.userId+'" title="Click to download"></i>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');

							   }
							
							else
							{
								//$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i class="glyphicon glyphicon-remove upload" title="Click to delete"/>&nbsp;<i class="playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<input id="'+attObj.userId+'" class="~' +attObj.userId+'" type="checkbox"  name="downloadNoteFile"></input>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');
								$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i class="glyphicon glyphicon-remove upload" title="Click to delete"/>&nbsp;<i class="playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +attObj.userId+'" title="Click to download"/>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');

							}
						}else{
							span='<span id="'+attObj.fileName+'" style="position: relative; left: -2px; top: 3px;opacity:1;" class="close glyphicon glyphicon-ok"></span>';
							if(attObj.fileName.contains(".mov") || attObj.fileName.contains(".mp4") || attObj.fileName.contains(".avi") || attObj.fileName.contains(".wmv")  || attObj.fileName.contains(".wma"))
							   {
								$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'"  href="javascript:void(0);" style="word-break:break-all;" ><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +attObj.userId+'" title="Click to download"/>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');

							   }
							
							else if(attObj.fileName.contains(".mp3") || attObj.fileName.contains(".wav") || attObj.fileName.contains(".amr"))
							   {
								$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'"  href="javascript:void(0);" style="position: relative; top: 3px;word-break:break-all;"><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +attObj.userId+'" title="Click to download"></i>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');

							   }
							else
							{
								$("#viewUploadedFiles").append('<li><a id="'+attObj.fileName+'" href="javascript:void(0);" style="word-break:break-all;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="playRecorded" title="Click to play" id="'+playOption+'~'+attObj.userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +attObj.userId+'" title="Click to download"></i>' +fileUploadName+'<br>'+'<i  class="">'+uploadDate+'</i>'+ span+'</a></li>');


							}
						}
					}
				}
			},error : function(){
				alert("Please try again later");
				console.log("<-------error returned for new ajax request attach file-------> ");
			}
		});

	}else{
		OtherEmptyFlag = 'true';
		
		if(selectedFiles[0] != null && selectedFiles.length > 0){
			for ( var i = 0; i < selectedFiles.length; i++) {
				
				if(!selectedFiles[i].match(".mov") && !selectedFiles[i].match(".avi")){ 
					$("#viewUploadedFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);" ><i class="playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + selectedFiles[i] +'</a></li>');
				}	
				else if(selectedFiles[i].match(".mov") || selectedFiles[i].match(".mp4") || selectedFiles[i].match(".avi") || selectedFiles[i].match(".wmv")  || selectedFiles[i].match(".wma"))
				{
					$("#viewUploadedFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);" ><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + selectedFiles[i] +'</a></li>');

				}
				else if(selectedFiles[i].match(".mp3") || selectedFiles[i].match(".wav") || selectedFiles[i].match(".amr"))

				{
					$("#viewUploadedFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);" ><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + selectedFiles[i] +'</a></li>');

				}
				
			}
		} 
	}
	return OtherEmptyFlag;	
}

function otherFiles(selectedFiles,listOwnerFlag)
{
	//alert('otherFiles');
	if(listOwnerFlag){
		var url = urlForServer+"Lesson/getOtherFiles/"+userId;
		$.support.cors = true;
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn					
			},
			   type : 'POST',
			   url : url,
			   success : function(responseText) {
			   
			   var data=jQuery.parseJSON(responseText);
			   $("#viewOtherFiles").empty();
			   if(data!= null && data != ''){
				   $("#downloadButton").attr('style','display:block;');
				   OtherEmptyFlag = 'true';
				   for ( var i = 0; i < data.length; i++) {
					   var obj = data[i];
					  // alert("obj===>"+obj);
					   var fileName = obj['fileName'];
					   var fileDummyName=obj['fileDummyName'];
					   var uploadDate=obj['uploadDate'];
					   var playOption=obj['fullPath'];
					  
					   var span = '' ;
					   
					   if(selectedFiles.length > 0 && selectedFiles[0] != null){
							if(selectedFiles.indexOf(fileName) == -1){
								   if(fileName.contains(".mov") ||  fileName.contains(".mp4") || fileName.contains(".avi") || fileName.contains(".wmv") || fileName.contains(".wma") )
								   {
									   $("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top:-3px;"  name="downloadfile"></input> ' + fileDummyName + '<br>'+'<i  class="">'+uploadDate+'</i></a></li>');				
									   
									  // $("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<i id="'+fileName+'" class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');	

								   }
								
								   else if(fileName.contains(".mp3") || fileName.contains(".wav") || fileName.contains(".amr"))
								   {
									   $("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top:-3px;"   name="downloadfile"></input> ' + fileDummyName + '<br>'+'<i  class="">'+uploadDate+'</i></a></li>');
									   //$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<i id="'+fileName+'" class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');	

								   }
								
								else
								{
									$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="playRecorded" id="'+fileName+'~'+userId+'"/>&nbsp;&nbsp;<input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top:-3px;"  name="downloadfile"></input> ' + fileDummyName + '<br>'+'<i  class="">'+uploadDate+'</i></a></li>');
									//$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="playRecorded" id="'+fileName+'~'+userId+'"/>&nbsp;<i id="'+playOption+'" class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');

								}
							}
						}else{
							   if(fileName.contains(".mov") ||  fileName.contains(".mp4") || fileName.contains(".avi") || fileName.contains(".wmv") || fileName.contains(".wma") )
							   {
								   $("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top:-3px;"  name="downloadfile"></input> ' + fileDummyName + '<br>'+'<i  class="">'+uploadDate+'</i></a></li>');
									//$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<i id="'+fileName+'" class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');	

							   }
							
							   else if(fileName.contains(".mp3") || fileName.contains(".wav") || fileName.contains(".amr"))
							   {
								   $("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top:-3px;"   name="downloadfile"></input> ' + fileDummyName + '<br>'+'<i  class="">'+uploadDate+'</i></a></li>');
									//$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<i id="'+fileName+'" class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');	

							   }
							
							else
							{
								$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="playRecorded"  id="'+playOption+'~'+userId+'"/>&nbsp;<input id="'+fileName+'" class="~' +fileName+'" type="checkbox" style="margin-top:-3px;"  name="downloadfile"></input> ' + fileDummyName + '<br>'+'<i  class="">'+uploadDate+'</i></a></li>');
								//$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" style="word-break:break-all;"><i id="'+fileName+'" class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="playRecorded" id="'+fileName+'~'+userId+'"/><i id="'+playOption+'" class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');


							}
						}
					  
					   /*else
					   {
							$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);" ><i class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + fileName +'</a><video  id="client-video"  loop controls src="'+url+userId+"/"+url2+fileName+'"  type="video/mov" width=320 height=240></video></li>');	
   
					   }*/
						//<video  id="client-video"  loop controls src="'+url+userId+"/"+url2+selectedFiles[i]+'"  type="video/mov" width=320 height=240></video>
				   }
			   }else{
				   OtherEmptyFlag = 'false';
			   }
			   
			   },
			   error : function() {
			   console.log("<-------error returned retrieving files-------> ");
			   }
		});
		//$("#attachFileModel").children('.modal-footer').removeClass('hidden');
	}else{
		
		OtherEmptyFlag = 'true';
		$("#viewOtherFiles").empty();
		 
		if(selectedFiles.length > 0 && selectedFiles[0] != null){
			for ( var i = 0; i < selectedFiles.length; i++) {
				if(!selectedFiles[i].match(".mov") && !selectedFiles[i].match(".avi")){ 
					$("#viewOtherFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);"><i class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<input id="'+selectedFiles[i]+'" class="~' +selectedFiles[i]+'" type="checkbox" style="margin-top:-3px;" name="downloadfile"></input> ' + selectedFiles[i] + '</a></li>');
					//$("#viewOtherFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);" ><i class="glyphicon glyphicon-remove other" title="Click to delete"/>&nbsp;<i class="playRecorded" id="'+selectedFiles[i]+'~'+userId+'"/><i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + selectedFiles[i] +'</a></li>');
				}
				else if(selectedFiles[i].match(".mov") || selectedFiles[i].match(".mp4") || selectedFiles[i].match(".avi") || selectedFiles[i].match(".wmv")  || selectedFiles[i].match(".wma"))
				{
					$("#viewOtherFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);"><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<input id="'+selectedFiles[i]+'" class="~' +selectedFiles[i]+'" type="checkbox" style="margin-top:-3px;"  name="downloadfile"></input> ' + selectedFiles[i] + '</a></li>');
					//$("#viewOtherFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);" ><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + selectedFiles[i] +'</a></li>');

				}
				
				else if(selectedFiles[i].match(".mp3") || selectedFiles[i].match(".wav") || selectedFiles[i].match(".amr"))
				{
					$("#viewOtherFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);"><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<input id="'+selectedFiles[i]+'" class="~' +selectedFiles[i]+'" type="checkbox" style="margin-top:-3px;"  name="downloadfile"></input> ' + selectedFiles[i] + '</a></li>');
					//$("#viewOtherFiles").append('<li><a id="'+selectedFiles[i]+'" href="javascript:void(0);" ><i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+selectedFiles[i]+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt" title="Click to download"/>' + selectedFiles[i] +'</a></li>');

				}
				
			}
		} 
	}
	return OtherEmptyFlag;	
}
/*Functions used to fetch and download files for note - ends*/


/* Function for add the events*/
function addEvents(noteName, startTime,endTime,eventDiscription,allDayEvent,eventId){
	  var scheduleUrl = urlForServer+"note/createScheduleNote/"+scheduleListId;
	    if(eventId!="" && eventId!="0")
	    {
		var scheduleParams = '{"eventName":"'+noteName+'","startDate":"'+startTime+'","endDate":"'+endTime+'","userId":"'+userId+'","listType":"schedule","eventId":"'+eventId+'","allDay":"'+allDayEvent+'","description":"'+eventDiscription+'","userFullName":"'+userFullNameMail+'","userMail":"'+userEmail+'"}';
		scheduleParams = encodeURIComponent(scheduleParams);

	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : scheduleUrl,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:scheduleParams, 
	    	dataType: "json",
	    	success : function(response){
	    	     if(response!='0')
	    	    	 alert(response.eventId);
	    	     else
	    	    	 alert("Please try again later");
          },
          error: function(e) {
              alert("Please try again later");
          }
      
	    });
	    }


}

function votedNotes(votedFlag,listId,noteId)
{

	var votedNotes;
	
	   var url = urlForServer+"note/maxNostView";
		
		var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'"}';
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
			type : 'POST',
			data : datastr,
			url : url,
			success : function(responseText) {
			var data = jQuery.parseJSON(responseText);
			if(data!=null && data!='')
			{
						 
			for ( var i = 0; i < data.length; i++) {
				votedNotes=data[i];
						
				if(votedNotes['noteId']!=null && votedNotes['listId']!=null )
				{
					if(votedFlag==true)
					{	
						updateMostVoted(votedNotes['noteId'],votedNotes['listId'],votedNotes['viewed'],votedNotes['countId'],++votedNotes['vote']);
						//voteLists();
					}
					else
					{
						updateMostVoted(votedNotes['noteId'],votedNotes['listId'],votedNotes['viewed'],votedNotes['countId'],--votedNotes['vote']);
						//voteLists();
					}
					
					
				}
				else
				{
					if(votedFlag==true)
					{
					var vote=1;
					var viewed=0;
					insertMostViewed(votedNotes['noteId'],votedNotes['listId'],vote,viewed);
					}
					else
					{
						var vote=0;
						var viewed=0;
						insertMostViewed(votedNotes['noteId'],votedNotes['listId'],vote,viewed);
					}
					//mostPopular();
				}
			}
			
			}
			else
			{
				if(votedFlag==true)
				{
				var vote=1;
				var viewed=0;
				insertMostViewed(noteId,listId,vote,viewed);
				}
				else
				{
					var vote=0;
					var viewed=0;
					insertMostViewed(noteId,listId,vote,viewed);
				}
				
			}
		},
		error : function() {
			console.log("<-------error returned for get most voted -------> ");
			}
		});


}

//var viewed=0;
///var vote=0;
function insertMostViewed(noteId,listId,vote,viewed)
{
   var url = urlForServer+"note/insertMostViewed/"+userId;
	
	var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'","vote":"'+vote+'","viewed":"'+viewed+'"}';
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
	
	},
	error : function() {
		console.log("<-------error returned for get most viewed -------> ");
		}
	});
}
function updateMostVoted(noteId,listId,viewed,countId,vote)
{
   var url = urlForServer+"note/updatemostViewed/"+userId;
   var datastr = '{"listId":"'+listId+'","noteId":"'+noteId+'","viewed":"'+viewed+'","countId":"'+countId+'","vote":"'+vote+'"}';
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
		type : 'POST',
		data : datastr,
		url : url,
		success : function(responseText) {
		var data = jQuery.parseJSON(responseText);
		
	},
	error : function() {
		console.log("<-------error returned for get most viewed -------> ");
		}
	});
}
function loadEventsBooks()
{
	var url = urlForServer+"note/fetchList";
	var params = '{"userId":"'+userId+'","listType":"'+listType+'","fetchEvent":true}';
	//fetchEvent - add due to event by date & time order
    params = encodeURIComponent(params);

    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
    		
    		loadList(response);
//    		getListAndNoteNames();
    		//loadTags();
    	},
        error: function(e) {
            alert("Please try again later");
        }
    
    });
}
var cDate="";
var cDay="";
var cMonth="";
var cYear="";
var cTime="";
function convertStringDateFormatIntoNumberFormat(dates,allday)
{
	if(allday)
	{
		cDate=new Date(dates);
		cDay=cDate.getDate();
		cMonth=cDate.getMonth()+1;
		
		

		cYear=cDate.getFullYear();
		cTime=cMonth+"/"+cDay+"/"+cYear;

		return cTime;
	}
	else
	{
		var start=dates.split(" ");
		cDate=new Date(start[0]);
		cDay=cDate.getDate();
		cMonth=cDate.getMonth()+1;
		
		cYear=cDate.getFullYear();
		var stime=start[1].split(":");
		var shr=stime[0];
		
		if(start[2]=="PM" && shr!=12)
		{
				shr=parseInt(shr)+12;
		} else if(start[2]=="PM" && shr==12){
			shr=parseInt(shr);
		} else if(start[2]=="AM" && shr==12){
			shr=parseInt(shr)-12;
		}
		shr=shr+":"+stime[1];
			
		cTime=cMonth+"/"+cDay+"/"+cYear+" "+shr;
		
		return cTime;
		
	}
	
}
function convertNumberDateFormatIntoStringFormat(dates,allday)
{
	if(allday)
	{
		var mon=dates.split("/");
		cTime=month[parseInt(mon[0])-1]+"/"+mon[1]+"/"+mon[2];
		return cTime;
	}
	else
	{
		var start=dates.split(" ");
		var mon=start[0].split("/");
		if(parseInt(mon[0])==0){
			for(var i=0;i<mon[0].length;i++){
				if(i==1){
					mon[0]=mon[0].charAt(i);
				}
			}
		}
		var timing=convertTwentyFourHourToTwelveHourTime(start[1]);
		var hourEnd = timing.charAt(0);
		if((hourEnd)==0){
		timing=timing.replace(timing.charAt(0),"");
		}
		cTime=month[parseInt(mon[0])-1]+"/"+mon[1]+"/"+mon[2]+" "+timing;
		return cTime;
	}
}

///////time format changes--before time, append "0" karthick
function convertNumberDateFormatIntoStringFormat2(dates,allday)
{
if(allday)
{
	var mon=dates.split("/");
	cTime=month[parseInt(mon[0])-1]+"/"+mon[1]+"/"+mon[2];
	return cTime;
}
else
{
	var start=dates.split(" ");
	var mon=start[0].split("/");
	if(parseInt(mon[0])==0){
		for(var i=0;i<mon[0].length;i++){
			if(i==1){
				mon[0]=mon[0].charAt(i);
			}
		}
	}
	var timing=convertTwentyFourHourToTwelveHourTime(start[1]);
	var hourEnd = timing.charAt(0);
	/*if((hourEnd)==0){
	timing=timing.replace(timing.charAt(0),"");
	}*/
	cTime=month[parseInt(mon[0])-1]+"/"+mon[1]+"/"+mon[2]+" "+timing;
	return cTime;
}
}

function convertNumberDateFormatIntoStringFormat1(dates)
{
	var checkSpace=dates.indexOf(' ') >= 0;
	if(checkSpace==false)
	{
		var mon=dates.split("/");
		cTime=month[parseInt(mon[0])-1]+"/"+mon[1]+"/"+mon[2];
		return cTime;
	}
	else
	{
		var start=dates.split(" ");
		var mon=start[0].split("/");
		if(parseInt(mon[0])==0){
			for(var i=0;i<mon[0].length;i++){
				if(i==1){
					mon[0]=mon[0].charAt(i);
				}
			}
		}
		var timing=convertTwentyFourHourToTwelveHourTime(start[1]);
		var hourEnd = timing.charAt(0);
		if((hourEnd)==0){
		timing=timing.replace(timing.charAt(0),"");
		}
		cTime=month[parseInt(mon[0])-1]+"/"+mon[1]+"/"+mon[2]+" "+timing;
		return cTime;
	}
}

function getattachedFiles(listId,noteId,listOwnerFlag){
	

	var url = urlForServer+"note/getAttachFile/"+listId+"/"+noteId;
	var response="";	
	var selectedFiles;
			$.ajax({	
				headers: { 
				"Mn-Callers" : musicnote,
		    	"Mn-time" :musicnoteIn				
		    	},
				type : 'POST',
				url : url,
				success : function(responseText) 
				{
					response = responseText;
				
					if(response.indexOf("[") != -1){
						response = response.substring(response.indexOf("[")+1,response.length);
					}
					if(response.indexOf("]") != -1){
						response = response.substring(0,response.indexOf("]"));
					}
					
					if(response.trim() !='' ){
						selectedFiles = response.split(',');
					}else{
						selectedFiles = new Array();
					}
					//alert("result-->"+selectedFiles);
					if(!listOwnerFlag && selectedFiles.length == 0 ){
						$("#moveCopyWarning").modal('show');

						$("#move-modal-message1").text("  No Files attached");
						setTimeout(function(){ $('#moveCopyWarning').modal('hide');},4000);
						
					}else{
						recordedLessons(selectedFiles,listOwnerFlag);
						otherCheckFiles(selectedFiles,listOwnerFlag,listId,noteId);
						otherFiles(selectedFiles,listOwnerFlag);
						$('#attachFileModel').modal('show');
					}
					//copySelectedNotes(selectedFiles);
				},
				error : function() 
				{
					alert("Please try again later");
					console.log("<-------error returned for new ajax request attach file-------> ");
				}
			});
			//alert("return-->"+selectedFiles);
			return selectedFiles;
		}

function reLoadList()
{
	if($('#notebookId').val()!=null && $('#notebookId').val()=='note'){
	/*$('#notes').find('.listDiv').each(function( index ){
		$(this).remove();
	});*/
	var url = urlForServer+"note/fetchList";
	var params = '{"userId":"'+userId+'","listType":"'+listType+'","fetchEvent":true}';
	//fetchEvent - add due to event by date & time order
      params = encodeURIComponent(params);
   
    $.ajax({
    	headers: { 
    	"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
    	type: 'POST',
    	url : url,
    	cache: false,
    	contentType: "application/json; charset=utf-8",
    	data:params, 
    	dataType: "json",
    	success : function(response){
    		noteLoadResponse=response;
    		getTagsForNote();
    		//setTimeout(function(){loadList(response);},1000);
    		loadList(response);
    		getListAndNoteNames();
    		//getattachedFilesForNotes(listIdFromList,noteIdFromList,listOwnerFlag,false);
    		
    	},
        error: function(e) {
            alert("Please try again later");
        }
    });
	}else{
		/*$('#notes').find('.listDiv').each(function( index ){
			$(this).remove();
		});*/
		var listId=$('#notebookId').val();
		noteShowingViewLevel='book';
		var url=urlForServer+"note/getListBasedOnListId/"+listId;
		var multiFilterByTagId='';
		var multiFilterByNoteName='';
		
				
		if(listType!=null && listType=="schedule")
			multiFilterByTagId='';
		
		params = '{"userId":"'+userId+'","listType":"'+listType+'","tagId":"'+multiFilterByTagId+'","noteName":"'+multiFilterByNoteName+'"}';
		params = encodeURIComponent(params);
		
		$.ajax({
			headers: { 
			"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : url,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:params, 
	    	dataType: "json",
	    	success : function(response){
	    	bookLoadResponse=response;
	    	getTagsForNoteBookView();
			loadListBasedonList(response,"");
    		//setTimeout(function(){loadListBasedonList(response,"");},1000);
    		getListAndNoteNames();
		}
		});
	}
	
	
}
function addNewEventBook()
{
	
	$("#calenderWarningId").text("");
	$('#addCalenderListModal').modal('toggle');
}
function deleteSubEvents()
{
	
	
	
	    var scheduleUrl = urlForServer+"note/deleteSubEvents";
	  	var scheduleParams = '{"listId":"'+listIdFromList+'","eventId":"'+noteIdFromList+'","subEventId":"'+eventSubId+'","userId":"'+userId+'"}';
		scheduleParams = encodeURIComponent(scheduleParams);
		
	    $.ajax({
	    	headers: { 
	    	"Mn-Callers" : musicnote,
	    	"Mn-time" :musicnoteIn				
	    	},
	    	type: 'POST',
	    	url : scheduleUrl,
	    	cache: false,
	    	contentType: "application/json; charset=utf-8",
	    	data:scheduleParams, 
	    	dataType: "json",
	    	success : function(response){
	    	     if(response!='0'){
	    	        	    	
	    	    	 loadAllEvents();
	    	    	 getTodaySchedule();
	    	    	 
	    	    	 // add due to event by date & time order - kishore
	    	    	 loadEventsBooks();
	     			 getListAndNoteNames();
	    	     }
	    	     else
	    	    	 alert("Please try again later");
       },
       error: function(e) {
           alert("Please try again later");
       }
   
	    });
	    }

/*function hideAudio()
{
	document.getElementById("audio_recording").style.display="none";
}*/


function recordedLessonsForNote(listId,noteId)
{
	var url = urlForServer+"Lesson/getRecordedLessonsForNote/"+userId+"/"+listId+"/"+noteId;
	
	$.support.cors = true;
	$.ajax({
		headers: { 
		"Mn-Callers" : musicnote,
    	"Mn-time" :musicnoteIn				
    	},
           type : 'POST',
           url : url,
           success : function(responseText) {
           
           var data=jQuery.parseJSON(responseText);
           
           $("#viewNoteRecordedLesson").empty();
           if(data!=null && data!='')
           {
        	 
          
           for ( var i = 0; i < data.length; i++) {
			   var obj = data[i];
			   var lessonName = obj['fileName'];
			   var fileDummyName=obj['fileDummyName'];
			   var uploadDate=obj['uploadDate'];
			   var playOption=obj['fullPath'];
			   if(lessonName.contains(".mov") ||  lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv") || lessonName.contains(".wma") )
			   {
				$("#viewNoteRecordedLesson").append('<li><a id="'+lessonName+'" href="javascript:void(0);" style="word-break:break-all;">&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +userId+'" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');	
			   }
			   else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
			   {
				$("#viewNoteRecordedLesson").append('<li><a id="'+lessonName+'" href="javascript:void(0);" style="word-break:break-all;">&nbsp;<i class="glyphicon glyphicon-play playRecorded" title="Click to play" id="'+playOption+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +userId+'" title="Click to download"/>' + fileDummyName +'<br>'+'<i  class="">'+uploadDate+'</i></a></li>');	
			   }
			   else
			   {
			   $("#viewNoteRecordedLesson").append('<li><a id="'+lessonName+'" href="javascript:void(0);" style="word-break:break-all;"><i class="playRecorded" id="'+playOption+'~'+userId+'"/>&nbsp;<i class="glyphicon glyphicon-download-alt ~' +userId+'" title="Click to download"/>' + lessonName + '<br>'+'<i  class="">'+uploadDate+'</i> <span id="'+fileDummyName+'" class="close"  >&times;</span> </a></li>');
			   }
           }
           }
           },
           error : function() {
           console.log("<-------error returned retrieving recorded lessons-------> ");
           }
           });
}
function viewRecord()
{
	 recordedLessons();
}
function wholeList()
{
	
	reLoadList();
	$('#attachFileModel').modal('hide');
	$("#moreActions").modal('hide');	
}

